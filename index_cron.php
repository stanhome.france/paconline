<?php
/**
 * Indexage global journalier
 */
require( dirname(__FILE__) . '/wp-load.php' );

define("IS_CONTEXT", true);

//Log
require_once get_template_directory().'/paconline/logs/log-helper.php';
require_once get_template_directory().'/paconline/include-functions.php';
define("INDEXATION_LOG_PATH", get_template_directory().'/log/indexation.log');

function sh_do_index(){
	
	$index_path = get_template_directory().'/../../plugins/search/lucene/index/stanhome_index';
	$searchObject = new sh_search($index_path);
	clean_all_file_index();
	//Ouvre ou créé l'index
	try {
		$index = $searchObject->open_index();
	} catch (Exception $e) {
		$searchObject->create_index();
		$index = $searchObject->open_index();
	}

	//Réorganise tous les segments de l'index en un seul segment
	$index->optimize();
	
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" a commencée.", INDEXATION_LOG_PATH);
	
	//Indexage des différentes entités
	$cyclesCount = sh_do_index_cycles($searchObject);
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" sur les cycles est terminée.", INDEXATION_LOG_PATH);
	$periodesCount = sh_do_index_periodes($searchObject);
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" sur les périodes est terminée.", INDEXATION_LOG_PATH);
	$eventsCount = sh_do_index_events($searchObject);
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" sur les évènements est terminée.", INDEXATION_LOG_PATH);
	$documentsCount = sh_do_index_documents($searchObject);
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" sur les documents est terminée.", INDEXATION_LOG_PATH);
	$promotionsCount = sh_do_index_promotions($searchObject);
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" sur les promotions est terminée.", INDEXATION_LOG_PATH);
	
	//Réorganise tous les segments de l'index en un seul segment
	$index->optimize();
	
	sh_write_to_log("La tâche planifiée \"Agent d'indexation\" est terminée : ".$cyclesCount." cycle(s), ".$periodesCount." periode(s), ".$eventsCount." événements(s), ".$documentsCount." document(s), ".$promotionsCount." promotion(s) indexé(s).", INDEXATION_LOG_PATH);
}

/**
 * Indexe tous les cycles
 */
function sh_do_index_cycles($searchObject){
	sh_write_to_log("Début de sh_do_index_cycles", INDEXATION_LOG_PATH);	
	
	//Supprime tous les cycles de l'index
	$searchObject->purge_by_type('cycle');
	
	//Indexe tous les cycles
	$cyclesCount = 0;
	$urlBeginning = get_option("siteurl")."/detail-cycle/?id_cycle=";
	$cyclesToIndex = Pods('cycles', array("where" => "permalink like  'cycles%'", "limit" => "-1"));
	
	while ($cyclesToIndex->fetch()){
		$title = $cyclesToIndex->display("identifiant");
		$id = $cyclesToIndex->field("id");
		$url = $urlBeginning.$id;
		
		$cycle_temp = Pods('cycles', $id);
		
		$dateDebutObject = new DateTime($cyclesToIndex->display("date_debut"));
		$dateFinObject = new DateTime($cyclesToIndex->display("date_fin"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = $cycle_temp->row();
		$content = implode(' ', $content);
		
		$content_comment = '';
		$comments = Pods('commentaires', array('where' => "id_pod_cible=$id and pod_cible='Cycles'", 'limit' => "-1"));
		while ($comments->fetch()) {
			$content_comment_row = $comments->row();
			$content_comment .= ' ' . $content_comment_row['content'];
		}
		
		$content .= ' ' . $content_comment;
		$content = strip_tags($content);
		
		sh_write_to_log("title = ".$title, INDEXATION_LOG_PATH);
		sh_write_to_log("url = ".$url, INDEXATION_LOG_PATH);
		sh_write_to_log("id = ".$id, INDEXATION_LOG_PATH);
		sh_write_to_log("date début = ".$dateDebut, INDEXATION_LOG_PATH);
		sh_write_to_log("date fin = ".$dateFin, INDEXATION_LOG_PATH);
		sh_write_to_log("contenu de commentaire = ".$content, INDEXATION_LOG_PATH);
		
		//indexe ce cycle
		$searchObject->add_to_index($title, $url, 'cycle', $id, $dateDebut, $dateFin, $content);

		sh_write_to_log("Après le add_to_index", INDEXATION_LOG_PATH);
		
		$cyclesCount++;
	}

	sh_write_to_log("Fin de sh_do_index_cycles", INDEXATION_LOG_PATH);
	
	return $cyclesCount;
}

/**
 * Indexe toutes les périodes
 */
function sh_do_index_periodes($searchObject){
	//Supprime tous les cycles de l'index
	$searchObject->purge_by_type('periode');
	
	//Indexe tous les cycles
	$periodesCount = 0;
	$urlBeginning = get_option("siteurl")."/detail-cycle/?id_cycle=";
	$periodesToIndex = Pods('cycles', array("where" => "permalink like  'periodes%'", "limit" => "-1"));
	
	while ($periodesToIndex->fetch()){
		$title = $periodesToIndex->display("identifiant");
		$id = $periodesToIndex->field("id");
		$url = $urlBeginning.$id;
		
		$periode_temp = Pods('cycles', $id);
		
		$dateDebutObject = new DateTime($periodesToIndex->display("date_debut"));
		$dateFinObject = new DateTime($periodesToIndex->display("date_fin"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = $periode_temp->row();
		$content = implode(' ', $content);
		
		$content_comment = '';
		$comments = Pods('commentaires', array('where' => "id_pod_cible=$id and pod_cible='Cycles'", 'limit' => "-1"));
		while ($comments->fetch()) {
			$content_comment_row = $comments->row();
			$content_comment .= ' ' . $content_comment_row['content'];
		}
		
		$content .= ' ' . $content_comment;
		$content = strip_tags($content);
		
		//indexe cette période
		$searchObject->add_to_index($title, $url, 'periode', $id, $dateDebut, $dateFin, $content);
		
		$periodesCount++;
	}
	
	return $periodesCount;
}

/**
 * Indexe tous les événements
 */
function sh_do_index_events($searchObject){
	//Supprime tous les événements de l'index
	$searchObject->purge_by_type('event');

	//Indexe tous les écvénements
	$eventsCount = 0;
	$urlBeginning = get_option("siteurl")."/details-evenment-cyclique/?id_event=";
	$eventsToIndex = Pods('events', array("limit" => "-1"));
	
	while ($eventsToIndex->fetch()){
		$title = $eventsToIndex->display("designation");
		$id = $eventsToIndex->field("id");
		$url = $urlBeginning.$id;
		
		$event_temp = Pods('events', $id);
		
		$dateDebutObject = new DateTime($eventsToIndex->display("date_debut"));
		$dateFinObject = new DateTime($eventsToIndex->display("date_fin"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$types_events = get_liste_event();
		$cibles_events = get_liste_cible();
		
		$content = $event_temp->row();
		$content['type_evenement'] = $types_events[$content['type_evenement']];
		$cib_temp = $content['cible_evenement'];
		$cib_temp_2 =  array();
		$cib_temp = explode(',', $cib_temp);
		foreach($cib_temp as $item) {
			$cib_temp_2[] = $cibles_events[$item];
		}
		$cib_temp_2 = implode(',', $cib_temp_2);
		$content['cible_evenement'] = $cib_temp_2;
		$content = implode(' ', $content);
		
		$content_comment = '';
		$comments = Pods('commentaires', array('where' => "id_pod_cible=$id and pod_cible='Events'", 'limit' => "-1"));
		while ($comments->fetch()) {
			$content_comment_row = $comments->row();
			$content_comment .= ' ' . $content_comment_row['content'];
		}
		
		$content .= ' ' . $content_comment;
		$content = strip_tags($content);
		//indexe cet événement
		$searchObject->add_to_index($title, $url, 'event', $id, $dateDebut, $dateFin, $content);

		$eventsCount++;
	}
	
	return $eventsCount;
}

/**
 * Indexe tous les documents
 */
function sh_do_index_documents($searchObject){
	//Supprime tous les documents de l'index
	$searchObject->purge_by_type('document');

	//Indexe tous les documents
	$documentCount = 0;
	$urlBeginning = get_option("siteurl")."/details-document-cyclique/?id_doc=";
	$documentsToIndex = Pods('documents', array("limit" => "-1"));

	while ($documentsToIndex->fetch()){
		$title = $documentsToIndex->display("designation");
		$id = $documentsToIndex->field("id");
		$url = $urlBeginning.$id;
		
		$doc_temp = Pods('documents', $id);
		
		$dateDebutObject = new DateTime($documentsToIndex->display("date_debut_lancement"));
		$dateFinObject = new DateTime($documentsToIndex->display("date_fin_lancement"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$cibles_docs = get_liste_cible();
		
		$content = $doc_temp->row();
		$cib_temp = $content['cible'];
		$cib_temp_2 =  array();
		$cib_temp = explode(',', $cib_temp);
		foreach($cib_temp as $item) {
			$cib_temp_2[] = $cibles_docs[$item];
		}
		$cib_temp_2 = implode(',', $cib_temp_2);
		$content['cible'] = $cib_temp_2;
		$content = implode(' ', $content);
		$content_comment = '';
		$comments = Pods('commentaires', array('where' => "id_pod_cible=$id and pod_cible='Docs'", 'limit' => "-1"));
		while ($comments->fetch()) {
			$content_comment_row = $comments->row();
			$content_comment .= ' ' . $content_comment_row['content'];
		}
		
		$content .= ' ' . $content_comment;
		$content = strip_tags($content);
		//indexe ce document
		$searchObject->add_to_index($title, $url, 'document', $id, $dateDebut, $dateFin, $content);

		$documentCount++;
	}

	return $documentCount;
}

/**
 * Indexe toutes les promotions
 */
function sh_do_index_promotions($searchObject){
	//Supprime tous les documents de l'index
	$searchObject->purge_by_type('promotion');

	//Indexe toutes les promotions
	$promotionCount = 0;
	$urlBeginning = get_option("siteurl")."/details-promotion-cyclique/?id_promo=";
	$promotionsToIndex = Pods('promotions', array("limit" => "-1"));

	while ($promotionsToIndex->fetch()){
		$title = $promotionsToIndex->display("reference_extranet");
		$id = $promotionsToIndex->field("id");
		$url = $urlBeginning.$id;
		
		$promo_temp = Pods('promotions', $id);
		
		$dateDebutObject = new DateTime($promotionsToIndex->display("date_debut_lancement"));
		$dateFinObject = new DateTime($promotionsToIndex->display("date_fin_lancement"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$marques = get_liste_marque();
		$gammes = get_liste_gamme();
		
		$content = $promo_temp->row();
		$content['marque'] = $marques[$content['marque']];
		$content['gamme'] = $marques[$content['gamme']];
		$content = implode(' ', $content);
		
		$content_comment = '';
		$comments = Pods('commentaires', array('where' => "id_pod_cible=$id and pod_cible='Promos'", 'limit' => "-1"));
		while ($comments->fetch()) {
			$content_comment_row = $comments->row();
			$content_comment .= ' ' . $content_comment_row['content'];
		}
		
		$content .= ' ' . $content_comment;
		$content = strip_tags($content);
		
		//indexe ce document
		$searchObject->add_to_index($title, $url, 'promotion', $id, $dateDebut, $dateFin, $content);

		$promotionCount++;
	}

	return $promotionCount;
}

function clean_all_file_index(){
	
	$dossier_traite = get_template_directory().'/../../plugins/search/lucene/index/stanhome_index';
	
	$repertoire = opendir($dossier_traite); // On dénit le rértoire dans lequel on souhaite travailler.
	
	while (false !== ($fichier = readdir($repertoire))) // On lit chaque fichier du rértoire dans la boucle.
	{
		$chemin = $dossier_traite."/".$fichier; // On dénit le chemin du fichier àffacer.
	
		// Si le fichier n'est pas un rértoire.
		if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier))
		{
			@unlink($chemin); // On efface.
		}
	}
	closedir($repertoire); // Ne pas oublier de fermer le dossier ***EN DEHORS de la boucle*	
	sh_write_to_log("Clean all file", INDEXATION_LOG_PATH);
}
sh_do_index();
?>
