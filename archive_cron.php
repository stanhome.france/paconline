<?php
/**
 * Archive les cycles non archivés de plus de 6 mois
 */
require( dirname(__FILE__) . '/wp-load.php' );

require_once get_template_directory().'/paconline/logs/log-helper.php';
define("ARCHIVAGE_LOG_PATH", get_template_directory().'/log/archivage.log');

function sh_do_archive() {
	sh_write_to_log("La tâche planifiée \"Agent d'archivage\" commence.", ARCHIVAGE_LOG_PATH);

	$today = new DateTime();
	
	$delay = get_option('_archivage_delay');

	//Date Interval example : P6M => Period 6 Months
	$interval = new DateInterval("P".$delay."M");

	$beginArchiveDate = $today->sub($interval);
	$dateToCompare = $beginArchiveDate->format("Y-m-d");

	$cyclesToArchive = Pods('cycles', array('where'=>'archive=0 AND date_fin <= "'.$dateToCompare.'"'));
	$archivesCount = 0;
	while ($cyclesToArchive->fetch()){
		$cyclesToArchive->save("archive", true, $cyclesToArchive->display("id"));
		$archivesCount++;
	}
	//Log
	sh_write_to_log("La tâche planifiée \"Agent d'archivage\" s'est bien déroulée. ".$archivesCount." cycles archivé(s).", ARCHIVAGE_LOG_PATH);
}

//Initialisation du délai par défaut si inexistant
if(!get_option('_archivage_delay')){
	add_option('_archivage_delay',6);
}

sh_do_archive();

?>