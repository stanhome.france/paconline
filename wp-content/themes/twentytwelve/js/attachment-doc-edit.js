//Demander confirmation avant upload de fichier s'il y a eu des modifs dans les commentaires
jQuery(document).ready(function($) {
	$("#doc_uploader_form").submit(function() {
		actualDDContent = tinyMCE.get('mceEditor_dd').getContent();
		actualDRContent = tinyMCE.get('mceEditor_dr').getContent();
	
		savedDDContent = $("#dd_comment_saved").html();
		savedDRContent = $("#dr_comment_saved").html();
		
		//console.log(savedDDContent);
		//console.log(actualDDContent);
		
		String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
		
		if (actualDDContent.trim() != savedDDContent.trim() || actualDRContent.trim() != savedDRContent.trim()) {
			result = confirm("Attention : Toutes les modifications non sauvegardées dans les commentaires seront perdues. Voulez-vous tout de même continuer le transfert ?");
	
			if (!result) {
				return false;
			}
		}
	});
});