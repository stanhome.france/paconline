function do_calendar() {
	var year = jQuery('#annee option:selected').val();
	execute_ajax_command('year=a' + year + '&action=gen_cal', do_calendar_success_method);
}

function do_calendar_success_method(msg) {
	alert('La génération de l\'année demandée est effectuée.');
	document.location.href = '../calendrier-par-semaine/';
}
