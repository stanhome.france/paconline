jQuery(document).ready(function($) {
	// boutons supprimer
	$('.delete_doc').unbind('click').click(function() {
		var id = parseInt($(this).attr('id').replace('del_', ''));

		var check = confirm('Voulez-vous vraiment supprimer ce document?');
		if (check) {
			var data = 'id_doc=' + id + '&ajax=delete_doc';
			$.ajax({
				url : '',
				type : 'get',
				data : data,
				success : function(msg) {
					document.location.reload();
				}
			});
		}
	});
});