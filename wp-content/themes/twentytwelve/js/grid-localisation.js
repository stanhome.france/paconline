( function( $ ) {
	var localizationobj = {};
    localizationobj.pagergotopagestring = "Page:";
    localizationobj.pagershowrowsstring = "Nbr d'enregistrements:";
    localizationobj.pagerrangestring = " de ";
    localizationobj.pagernextbuttonstring = "Suivant";
    localizationobj.pagerpreviousbuttonstring = "Précédent";
    localizationobj.sortremovestring = "Entferne Sortierung";
    localizationobj.filterclearstring = "Initialiser";
    localizationobj.filterstring = "Filtrer";
    localizationobj.filtershowrowstring = "Filtrer vos enregistrements:";
    localizationobj.filterorconditionstring = "Ou";
    localizationobj.filterandconditionstring = "Et";
    localizationobj.emptydatastring = "Aucun résultat ne correpond à votre recherche";
    localizationobj.filterstringcomparisonoperators = ["Vide","Pas vide","Contient","Contient(Respecter la casse)","Ne contient pas","Ne contient pas(Respecter la casse)","Commence par","Commence par(Respecter la casse)","Se termine par","Se termine par(Respecter la casse)","égal","égal(Respecter la casse)","nul","not null"]
    $("#jqxgrid").bind("bindingcomplete", function (event) {
    	$("#jqxgrid").jqxGrid('localizestrings', localizationobj);
    });
})( jQuery );

