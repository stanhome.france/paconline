jQuery(document).ready(function($) {
	
	/*
	 * Les champs dates
	 */
	$("#promotion_date_debut_pre_lancement").datepicker();
	$("#promotion_date_fin_pre_lancement").datepicker();
	$("#promotion_date_debut_lancement").datepicker();
	$("#promotion_date_fin_lancement").datepicker();
	$("#promotion_date_lancement_chaud_dd").datepicker();
	$("#promotion_date_lancement_chaud_dr").datepicker();
	
	/*
	 * Calculer les cycles correspondants
	 */
	$("#promotion_semaine_debut").bind('change', function() {
		get_cycles_correspondants('promotion', $("#promotion_semaine_debut").val(), $("#promotion_annee").val(), $("#promotion_semaine_fin").val(), $("#promotion_annee_fin").val(), $("#promotion_attach_list").val());
	});
	$("#promotion_semaine_fin").bind('change', function() {
		get_cycles_correspondants('promotion', $("#promotion_semaine_debut").val(), $("#promotion_annee").val(), $("#promotion_semaine_fin").val(), $("#promotion_annee_fin").val(), $("#promotion_attach_list").val());
	});
	$("#promotion_attach_list").bind('change', function() {
		get_cycles_correspondants('promotion', $("#promotion_semaine_debut").val(), $("#promotion_annee").val(), $("#promotion_semaine_fin").val(), $("#promotion_annee_fin").val(), $("#promotion_attach_list").val());
	});
	
	/*
	 * Afficher champs dates lancment à chaud pour DD et DR quand 
	 * le boutton radio lancement à chaud est coché
	 */
	$('input[type=radio][name=promotion_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});
	
	/*
	 * Calcul Prix HT N
	 */
	$("#calcul_promo").bind('click', function() {
		if($("#tva_promo").val() == '' || $("#prix_p_ttc").val() == '') alert('Saisissez TVA et Prix P. TTC');
		else {
			var data = 'ajax=6&ttc='+$("#prix_p_ttc").val()+'&tva='+$("#tva_promo").val();
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#prix_p_ht").val(msg);
				},
				error: function() {
				}
			});
		}
		return false;
	});
	
	/*
	 * Submit from: add new promotion et redirection vers liste cycles
	 */
	$("#save_new_promotion").submit(function() {
		var empty = false;
		var empty_msg = 'Vous devez aussi saisir les champs suivants: \n';
		if($("#promotion_semaine_debut").val() == '' || $("#promotion_annee").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de début \n";
		}
		if($("#promotion_semaine_fin").val() == '' || $("#promotion_annee_fin").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de fin \n";
		}
		if($("#promotion_reference_extranet").val() == '') {
			empty = true;
			empty_msg += "La référence de la promotion \n";
		}
		if($("#promotion_description").val() == '') {
			empty = true;
			empty_msg += "La description de la promotion \n";
		}
		if($("#promotion_marque_list").val() == '') {
			empty = true;
			empty_msg += "La marque concernée par la promotion \n";
		}
		if($("#promotion_gamme_list").val() == '') {
			empty = true;
			empty_msg += "La gamme concernée par la promotion \n";
		}
		if($("#promotion_date_debut_lancement").val() == '' || $("#promotion_date_fin_lancement").val() == '') {
			empty = true;
			empty_msg += "Les dates de début et de fin de lancement \n";
		}
		if($('input[type=radio][name=promotion_lancement_chaud]:checked').val() == 1 && $("#promotion_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DD \n";
		}
		if($('input[type=radio][name=promotion_lancement_chaud]:checked').val() == 1 && $("#promotion_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DR \n";
		}
		if($("#tva_promo").val() == '') {
			empty = true;
			empty_msg += "La TVA de la promotion \n";
		}
		if($("#prix_p_ttc").val() == '') {
			empty = true;
			empty_msg += "Le prix promo TTC \n";
		}
		if($("#references_en_promo > tbody > tr[id]").length == 0) {
			empty = true;
			empty_msg += "Ajoutez au moins une seule ligne à la promotion \n";
		} else {
			if($("#promotion_is_valid").val() == 'non') {
				empty = true;
				empty_msg += "Validez votre promotion en cliquant sur la calculatrice! \n";
			}
		}
		
		
		if(empty) alert(empty_msg);
		if(empty) return false;
		
		/* Valider les lignes promo avant l'enregistrement de la promo */
	var composition = construct_lignes_promo();
	
//		ajax2valid_lignes_promo(composition);
		/* Fin: Valider les lignes promo avant l'enregistrement de la promo */
		
		var data = '';
		data += 'cycle_ou_periode=' + $("#promotion_attach_list").val() + '&';
		data += 'annee=' + $("#promotion_annee").val() + '&';
		data += 'annee_fin=' + $("#promotion_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#promotion_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#promotion_semaine_fin").val() + '&';
		data += 'reference_extranet=' + encodeURIComponent($("#promotion_reference_extranet").val()) + '&';
		data += 'description=' + encodeURIComponent($("#promotion_description").val()) + '&';
		data += 'marque=' + $("#promotion_marque_list").val() + '&';
		data += 'gamme=' + $("#promotion_gamme_list").val() + '&';
		data += 'reference_facturation=' + encodeURIComponent($("#promotion_reference_facturation").val()) + '&';
		data += 'reference_lot=' + encodeURIComponent($("#promotion_reference_lot").val()) + '&';
		data += 'date_debut_pre_lancement=' + $("#promotion_date_debut_pre_lancement").val() + '&';
		data += 'date_fin_pre_lancement=' + $("#promotion_date_fin_pre_lancement").val() + '&';
		data += 'qte_pre_lancement=' + $("#promotion_qte_pre_lancement").val() + '&';
		data += 'date_debut_lancement=' + $("#promotion_date_debut_lancement").val() + '&';
		data += 'date_fin_lancement=' + $("#promotion_date_fin_lancement").val() + '&';
		data += 'prevision=' + $("#promotion_prevision").val() + '&';
		data += 'qte_limitee=' + $('input[type=radio][name=promotion_qte_limitee]:checked').val() + '&';
		data += 'action_info=' + $('input[type=radio][name=promotion_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=promotion_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=promotion_publie]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#promotion_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#promotion_date_lancement_chaud_dr").val() + '&';
		data += 'prix_normal_ht=' + $("#prix_n_ht").val() + '&';
		data += 'prix_normal_ttc=' + $("#prix_n_ttc").val() + '&';
		data += 'prix_promotion_ht=' + $("#prix_p_ht").val() + '&';
		data += 'prix_promotion_ttc=' + $("#prix_p_ttc").val() + '&';
		data += 'tva=' + $("#tva_promo").val() + '&';
		
		data += 'composition=' + composition + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=P&';
		}
		data += 'ajax=2&scop=new';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'erreur_prix') alert('Les prix doivent etre numeriques!\n Enregistrement non effectué');
				if(msg == 'erreur_prix') return false;
				send_comment('dr', $("#pod_cible").val(), msg);
				send_comment('dd', $("#pod_cible").val(), msg);
				alert('Promotion bien ajoutée');
				window.setTimeout("redirect2DetailPromo("+msg+")", 2000);
			},
			error: function() {
				alert('error ajax!');
			}
		});
		
		
		return false;
	});
	
	/*
	 * Ajout d'une nouvelle promotion et rester sur la meme page
	 */
	$("#save_new_next_promotion").bind('click', function() {
		var empty = false;
		var empty_msg = 'Vous devez aussi saisir les champs suivants: \n';
		if($("#promotion_semaine_debut").val() == '' || $("#promotion_annee").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de début \n";
		}
		if($("#promotion_semaine_fin").val() == '' || $("#promotion_annee_fin").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de fin \n";
		}
		if($("#promotion_reference_extranet").val() == '') {
			empty = true;
			empty_msg += "La référence de la promotion \n";
		}
		if($("#promotion_description").val() == '') {
			empty = true;
			empty_msg += "La description de la promotion \n";
		}
		if($("#promotion_marque_list").val() == '') {
			empty = true;
			empty_msg += "La marque concernée par la promotion \n";
		}
		if($("#promotion_gamme_list").val() == '') {
			empty = true;
			empty_msg += "La gamme concernée par la promotion \n";
		}
		if($("#promotion_date_debut_lancement").val() == '' || $("#promotion_date_fin_lancement").val() == '') {
			empty = true;
			empty_msg += "Les dates de début et de fin de lancement \n";
		}
		if($('input[type=radio][name=promotion_lancement_chaud]:checked').val() == 1 && $("#promotion_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DD \n";
		}
		if($('input[type=radio][name=promotion_lancement_chaud]:checked').val() == 1 && $("#promotion_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DR \n";
		}
		if($("#tva_promo").val() == '') {
			empty = true;
			empty_msg += "La TVA de la promotion \n";
		}
		if($("#prix_p_ttc").val() == '') {
			empty = true;
			empty_msg += "Le prix promo TTC \n";
		}
		if($("#references_en_promo > tbody > tr[id]").length == 0) {
			empty = true;
			empty_msg += "Ajoutez au moins une seule ligne à la promotion \n";
		} else {
			if($("#promotion_is_valid").val() == 'non') {
				empty = true;
				empty_msg += "Validez votre promotion en cliquant sur la calculatrice! \n";
			}
		}
		
		
		if(empty) alert(empty_msg);
		if(empty) return false;
		
		/* Valider les lignes promo avant l'enregistrement de la promo */
		var composition = construct_lignes_promo();
//		ajax2valid_lignes_promo(composition);
		/* Fin: Valider les lignes promo avant l'enregistrement de la promo */

		var data = '';
		data += 'cycle_ou_periode=' + $("#promotion_attach_list").val() + '&';
		data += 'annee=' + $("#promotion_annee").val() + '&';
		data += 'annee_fin=' + $("#promotion_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#promotion_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#promotion_semaine_fin").val() + '&';
		data += 'reference_extranet=' + encodeURIComponent($("#promotion_reference_extranet").val()) + '&';
		data += 'description=' + encodeURIComponent($("#promotion_description").val()) + '&';
		data += 'marque=' + $("#promotion_marque_list").val() + '&';
		data += 'gamme=' + $("#promotion_gamme_list").val() + '&';
		data += 'reference_facturation=' + encodeURIComponent($("#promotion_reference_facturation").val()) + '&';
		data += 'reference_lot=' + encodeURIComponent($("#promotion_reference_lot").val()) + '&';
		data += 'date_debut_pre_lancement=' + $("#promotion_date_debut_pre_lancement").val() + '&';
		data += 'date_fin_pre_lancement=' + $("#promotion_date_fin_pre_lancement").val() + '&';
		data += 'qte_pre_lancement=' + $("#promotion_qte_pre_lancement").val() + '&';
		data += 'date_debut_lancement=' + $("#promotion_date_debut_lancement").val() + '&';
		data += 'date_fin_lancement=' + $("#promotion_date_fin_lancement").val() + '&';
		data += 'prevision=' + $("#promotion_prevision").val() + '&';
		data += 'qte_limitee=' + $('input[type=radio][name=promotion_qte_limitee]:checked').val() + '&';
		data += 'action_info=' + $('input[type=radio][name=promotion_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=promotion_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=promotion_publie]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#promotion_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#promotion_date_lancement_chaud_dr").val() + '&';
		data += 'prix_normal_ht=' + $("#prix_n_ht").val() + '&';
		data += 'prix_normal_ttc=' + $("#prix_n_ttc").val() + '&';
		data += 'prix_promotion_ht=' + $("#prix_p_ht").val() + '&';
		data += 'prix_promotion_ttc=' + $("#prix_p_ttc").val() + '&';
		data += 'tva=' + $("#tva_promo").val() + '&';
		data += 'composition=' + composition + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=P&';
		}
		data += 'ajax=2&scop=new';

		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'erreur_prix') alert('Les prix doivent etre numeriques!\n Enregistrement non effectué');
				if(msg == 'erreur_prix') return false;
				send_comment('dr', $("#pod_cible").val(), msg);
				send_comment('dd', $("#pod_cible").val(), msg);
				alert('Promotion bien ajoutée ');
				$("#promotion_reference_extranet").val('');
				$("#promotion_description").val('');
				$("#promotion_reference_facturation").val('');
				$("#promotion_reference_lot").val('');
				$("#promotion_reference_lot").val('');
				$("#promotion_prevision").val('');
				$("#prix_n_ttc").val('');
				$("#prix_n_ht").val('');
				$("#prix_p_ttc").val('');
				$("#prix_p_ht").val('');
				$("#tva_promo").val('');
				$("#promotion_is_valid").val('non');
				$("#files_to_attach").val('');
				$(".doc_attach_element").remove();
				$("#references_en_promo > tbody > tr[id]").each(function() {
					$(this).remove();
				});
				$("#tr_references_en_promo").hide();
				//Commentaires
				tinyMCE.get('mceEditor_dd').setContent('');
				tinyMCE.get('mceEditor_dr').setContent('');
			},
			error: function() {
				alert('error ajax!');
			}
		});
		
		
		return false;
		
	});
});
/*
 *Ajout une ligne
 */
function add_ligne(btn_delete) {
	var list_refs = new Array;
	var i = 0;
	for(j = 0; j < list.length; j++) {
		if(typeof(list[j]) != 'undefined') {
			list_refs[i] = list[j];
			i++;
		}
	}
	var nbr_ref = list_refs.length;
	if(nbr_ref == 0) alert('Choisissez au moins une références');
	if(nbr_ref == 0) return false;
	var nbr = $("#nombre_lignes_promo").val();
	nbr = parseInt(nbr);
	if(nbr_ref == 1) {
		var ref = list_refs[0];
		var content = '';
		content += '<tr id="'+nbr+'">';
		content += '<td><input type="text" id="qte_'+nbr+'" class="qte_obligatoire" style= "width: 65%;" onchange="changeqtes()"></td>';
		content += '<td><textarea id="ref_lot_'+nbr+'"></textarea></td>';
		//content += '<td><input type="text" id="ref_lot_'+nbr+'"></td>';
		content += '<td><textarea id="design_'+nbr+'"></textarea></td>';
		//content += '<td><input type="text" id="design_'+nbr+'"></td>';
		content += '<td><input type="text" id="refs_'+nbr+'"></td>';
		content += '<td><input type="text" id="prx_n_ht_'+nbr+'" style= "width: 65%;" disabled="disabled"></td>';
		content += '<td><input type="text" id="prx_n_ttc_'+nbr+'" style= "width: 65%;" disabled="disabled"></td>';
		content += '<td class="brd_right"><input type="image" src="'+btn_delete+'" alt="Supprimer" onclick="delete_ligne('+nbr_ref+',\''+nbr+'\');return false;"></td>';
		content += '</tr>';
		$("#last_tr_promo").remove();
		$("#references_en_promo").append(content);
		$("#references_en_promo").append('<tr class="last_tr_promo"><td colspan="7" class="error" align="center"></td></tr>');
		var data = 'reference=' + ref + '&ajax=3';
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			dataType: 'json',
			success: function(msg) {
				if(msg.error == 0) {
					$("#qte_"+nbr).val(msg.quantite);
					$("#design_"+nbr).val(msg.designation);
					$("#refs_"+nbr).val(ref);
					$("#prx_n_ht_"+nbr).val(msg.prix_n_ht);
					$("#prx_n_ttc_"+nbr).val(msg.prix_n_ttc);
					//Aprés l'ajout d'une ligne => validation de toutes les lignes et calcul des prix
					var composition = construct_lignes_promo();
					ajax2valid_lignes_promo(composition);
				}
			},
			error: function() {
				alert('error ajax !!');
			}
		});
		$("#tr_references_en_promo").show();
		$("#jqDialog_box").hide();
	}
	if(nbr_ref > 1) {
		var refs = '';
		for(i=0; i<nbr_ref; i++) {
			if(i != nbr_ref-1) refs += list_refs[i] + '_';
			else refs += list_refs[i];
		}
		var content = '';
		content += '<tr id="'+nbr+'">';
		content += '<td><input type="text" id="qte_'+nbr+'" class="qte_obligatoire" style= "width: 65%;" onchange="changeqtes()"></td>';
		content += '<td><textarea id="ref_lot_'+nbr+'"></textarea></td>';
		//content += '<td><input type="text" id="ref_lot_'+nbr+'"></td>';
		content += '<td><textarea id="design_'+nbr+'"></textarea></td>';
		//content += '<td><input type="text" id="design_'+nbr+'"></td>';
		content += '<td><input type="hidden" id="refs_'+nbr+'"><textarea id="refs_show_'+nbr+'"></textarea></td>';
		//content += '<td><input type="hidden" id="refs_'+nbr+'"><input type="text" id="refs_show_'+nbr+'"></td>';
		content += '<td><input type="text" id="prx_n_ht_'+nbr+'" style= "width: 65%;" disabled="disabled"></td>';
		content += '<td><input type="text" id="prx_n_ttc_'+nbr+'" style= "width: 65%;" disabled="disabled"></td>';
		content += '<td class="brd_right"><input type="image" src="'+btn_delete+'" alt="Supprimer" onclick="delete_ligne('+nbr_ref+',\''+nbr+'\');return false;"></td>';
		content += '</tr>';
		$(".last_tr_promo").remove();
		$("#references_en_promo").append(content);
		$("#references_en_promo").append('<tr class="last_tr_promo"><td colspan="7" class="error" align="center"></td></tr>');
		var data = 'references=' + refs + '&ajax=4';
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			dataType: 'json',
			success: function(msg) {
				if(msg.error == 0) {
					$("#qte_"+nbr).val(msg.quantite);
					$("#design_"+nbr).val(msg.designation);
					$("#refs_"+nbr).val(msg.references);
					$("#refs_show_"+nbr).val(msg.referencesou);
					$("#prx_n_ht_"+nbr).val(msg.prix_n_ht);
					$("#prx_n_ttc_"+nbr).val(msg.prix_n_ttc);
					//Aprés l'ajout d'une ligne => validation de toutes les lignes et calcul des prix
					var composition = construct_lignes_promo();
					ajax2valid_lignes_promo(composition);
				}
			},
			error: function() {
				alert('Erreur AJAX!');
			}
		});
		$("#tr_references_en_promo").show();
		$("#jqDialog_box").hide();
	}
	$("#nombre_lignes_promo").val(nbr+1);
	
	return false;
}

function changeqtes()
{
	var composition = construct_lignes_promo();
	ajax2valid_lignes_promo(composition);
	}


/*
 * Construire la composition des lignes de promo
 */
function construct_lignes_promo() {
	var composition = '';
	var i = 0;
	$("#references_en_promo > tbody > tr[id]").each(function() {
		var id = $(this).attr('id');
		composition += '['+$("#qte_"+id).val()+'|'+$("#ref_lot_"+id).val()+'|';
		composition += encodeURIComponent($("#design_"+id).val())+'|'+$("#refs_"+id).val()+'|';
		composition += $("#prx_n_ht_"+id).val()+'|'+$("#prx_n_ttc_"+id).val();
		composition += ']';
		i++;
	});
	
	return composition;
}

/*
 * Ajax to valid composition des lignes de promo
 */
function ajax2valid_lignes_promo(composition) {
	var data = 'composition='+composition+'&ajax=5&scop=new&tva='+$("#tva_promo").val();
	$.ajax({
		url: '',
		type: 'get',
		data: data,
		dataType: 'json',
		success: function(msg){
			if(msg.error == 0) {
				$("#prix_n_ttc").val(msg.prix_n_ttc);
				$("#prix_n_ht").val(msg.prix_n_ht);
				$("#promotion_is_valid").val('oui');
			}
		},
		error: function() {
			alert('Error Ajax!');
		}
	});
}

/*
 * Supprimer une ligne de la promo one et many
 */
function delete_ligne(type, ref) {
	type = (type>1) ? 'many' : 'one';
	$.ajax({
		url: '',
		type: 'get',
		data: 'ajax=8&scop=new&reference='+ref+'&type='+type,
		success: function(msg) {
			if(msg == '1') {
				$("#"+ref).remove();
				if($("#references_en_promo > tbody > tr[id]").length == 0) {
					$("#tr_references_en_promo").hide();
					$("#promotion_is_valid").val('non');
					// Initialiser le tableau de session contenant les ligne si jamais le bouton
					//calculatrice a été cliqué
					$.ajax({
						url: '',
						type: 'get',
						data: 'ajax=11',
						success: function(msg) {
						},
						error: function() {
							alert('Error Ajax!');
						}
					});
				} else {
					//Aprés l'ajout d'une ligne => validation de toutes les lignes et calcul des prix
					var composition = construct_lignes_promo();
					ajax2valid_lignes_promo(composition);
				}
			}
		},
		error: function() {
			alert('Error Ajax!');
		}
	});
}

/*
 * Annuler l'ajout d'une ligne
 */
function cancel_add_ligne() {
	$("#jqDialog_box").hide();
}

/*
 * Trouver des références par recherche dans popin de promo
 */
function find_references() {
	$("#popup_refs tr").each(function() {
		$(this).remove();
	});
	$("#zone_recherche_ref").append('<tr><td><label>Sasissez votre recherche</label></td><td align="center"><input type="text" id="q_popup"></td><td><input type="button" value="Trouver" onclick="find_article_by_ref()" class="small"></td></tr>');
	$("#btn_find_refs").hide();
	$("#btn_find_all").show();
}
function find_article_by_ref() {
	var ref = $("#q_popup").val();
	if(ref == '') alert('Champ de saisie est vide!');
	if(ref == '') return false;
	var data = 'ajax=12&ref='+ref;
	$.ajax({
		url: '',
		type: 'get',
		dataType: 'json',
		data: data,
		success: function(msg) {
			if(msg.empty == 1) {
				alert('Pas de références correspondant à votre recherche');
				return false;
			} else {
				$.each(msg.result, function() {
					var td_chk_ref = '';
					var td_ref = '';
					var td_design = '';
					$.each(this, function(kle, value) {
						if(kle == 'reference') {
					 		td_chk_ref = '<td align="center"><input type="checkbox" name="ref_'+ value +'" value="'+ value +'"></td>';
					 		td_ref = '<td align="center">'+ value +'</td>';
					 	}
					 	if(kle == 'designation') {
					 		td_design = '<td align="center">'+ value +'</td>';
					 	}
					});
					$("#popup_refs").append('<tr>' + td_chk_ref + td_ref + td_design + '</tr>');
				});
			}
		},
		error: function() {
			alert('Erreur Ajax!');
		}
	});
}
function get_all_refs() {
	$("#btn_find_all").hide();
	$("#btn_find_refs").show();
	$("#zone_recherche_ref tr").each(function() {
		$(this).remove();
	});
	$("#popup_refs tr").each(function() {
		$(this).remove();
	});
	$.ajax({
		url: '',
		type: 'get',
		data: 'ajax=10',
		dataType: 'json',
		success: function(msg) {
			$.each(msg, function() {
				var td_chk_ref = '';
				var td_ref = '';
				var td_design = '';
				$.each(this, function(kle, val) {
				 	if(kle == 'reference') {
				 		td_chk_ref = '<td align="center"><input type="checkbox" name="ref_'+ val +'" value="'+ val +'"></td>';
				 		td_ref = '<td align="center">'+ val +'</td>';
				 	}
				 	if(kle == 'designation') {
				 		td_design = '<td align="center">'+ val +'</td>';
				 	}
				});
				$("#popup_refs").append('<tr>' + td_chk_ref + td_ref + td_design + '</tr>');
			});
			$("#wait_refs").remove();
		},
		error: function() {
			alert('Error Ajax!');
		}
	});
}
function in_array(array, value) {
    for(var i=0;i<array.length;i++) {
        if(array[i] === value) {
            return true;
        }
    }
    return false;
}