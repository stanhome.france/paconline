jQuery(document).ready(function($) {
	/*
	 * Les champs dates
	 */
	$("#cycle_date_debut").datepicker();
	$("#cycle_date_fin").datepicker();
	$("#cycle_diffusion_dr").datepicker();
	$("#cycle_diffusion_dd").datepicker();
	$("#cycle_debut_commerciale").datepicker();
	$("#cycle_fin_commerciale").datepicker();
	$("#cycle_date_lancement_chaud_dd").datepicker();
	$("#cycle_date_lancement_chaud_dr").datepicker();

	/*
	 * Les champs ineditables
	 */
	$(".champ_non_editable").attr('disabled', true);

	/*
	 * Au chargement de cette page calculer la liste des semaines 
	 * puisque les semaines début et fin sont indiquées
	 */
	/*$.ajax({
		url: '',
		type: 'get',
		data: 'semaine_fin=' + $("#cycle_semaine_fin").val() + '&semaine_debut=' + $("#cycle_semaine_debut").val() + '&ajax=6',
		success: function(msg) {
			$("#liste_semaines").val(msg);
		},
		error: function() {
		}
	});*/
	
	
	/*
	 * Changer automatiquement l'identifiant du cycle avec 
	 * la formule Année Cycle N°
	 */
	$("#cycle_numero").bind('change', function() {
		if($(this).val() != '') {
			var annee = $("#cycle_annee_cycle").val();
			$("#cycle_identifiant").val(annee + ' Cycle ' + $("#cycle_numero option:selected").text());
		}
	});
	$("#cycle_annee_cycle").bind('change', function() {
		if($("#cycle_numero").val() != '') {
			var identifiant = $(this).val() + ' Cycle ' + $("#cycle_numero option:selected").text();
			if($("#cycle_identifiant").val() != identifiant) $("#cycle_identifiant").val(identifiant);
		}
	});

	/*
	 * Récupérer la date de début de la semaine de début
	 */
	$("#cycle_semaine_debut").bind('change', function() {
		var annee = $("#cycle_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=1';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#cycle_date_debut").val(msg);
				},
				error: function() {
					alert('error ajax!');
				}
			});
		}
	});

	/*
	 * Récupérer la date de fin de la semaine de fin
	 * et puis calculer la liste des semaines
	 */
	$("#cycle_semaine_fin").bind('change', function() {
		var annee = $("#cycle_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=2';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#cycle_date_fin").val(msg);
				},
				error: function() {
					alert('error ajax!');
				}
			});
		}
		if($(this).val() != '' && $("#cycle_semaine_debut").val() != '') {
			var data = 'semaine_fin=' + $(this).val() + '&semaine_debut=' + $("#cycle_semaine_debut").val() + '&ajax=6';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1') $("#liste_semaines").val(msg);
				},
				error: function() {
					alert('k,kjkj');
				}
			});
		}
	});	

	/*
	 * Afficher champs dates lancment à chaud pour DD et DR quand 
	 * le boutton radio lancement à chaud est coché
	 */
	$('input[type=radio][name=cycle_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});

	/*
	 * Editer le contenu d'un cycle
	 */
	$("#edit_cycle_form").submit(function() {
		var error = false;
		var error_msg = 'Vous devez remplir les champs suivants: \n';
		if($("#cycle_annee").val() == '' || $("#cycle_semaine_debut").val() == '' || $("#cycle_date_debut").val() == '') {
			error_msg += "Année, Semaine et date de début \n";
			error = true;
		}
		if($("#cycle_annee_fin").val() == '' || $("#cycle_semaine_fin").val() == '' || $("#cycle_date_fin").val() == '') {
			error_msg += "Année, Semaine et date de fin \n";
			error = true;
		}
		if($("#cycle_annee_cycle").val() == '') {
			error_msg += "Année du cycle\n";
			error = true;
		}
		if($("#cycle_diffusion_dr").val() == '' || $("#cycle_diffusion_dd").val() == '') {
			error_msg += "Les dates de diffusion aux DD/DR \n";
			error = true;
		}
		if($("#cycle_debut_commerciale").val() == '' || $("#cycle_fin_commerciale").val() == '') {
			error_msg += "Les dates début et fin commerciales \n";
			error = true;
		}

		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dd").val() == ''){
			error_msg += "Date lancement à chaud DD \n";
			error = true;
		}
		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dr").val() == ''){
			error_msg += "Date lancement à chaud DR \n";
			error = true;
		}
		if(error) alert(error_msg);
		if(error) return false;

		var data = '';
		data += 'id_cycle=' + $("#cycle_id").val() + '&';
		data += 'annee=' + $("#cycle_annee").val() + '&';
		data += 'annee_fin=' + $("#cycle_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#cycle_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#cycle_semaine_fin").val() + '&';
		data += 'annee_cycle=' + $("#cycle_annee_cycle").val() + '&';
		data += 'numero=' + $("#cycle_numero").val() + '&';
		data += 'date_debut=' + $("#cycle_date_debut").val() + '&';
		data += 'date_fin=' + $("#cycle_date_fin").val() + '&';
		data += 'identifiant=' + encodeURIComponent($("#cycle_identifiant").val()) + '&';
		data += 'diffusion_dd=' + $("#cycle_diffusion_dd").val() + '&';
		data += 'diffusion_dr=' + $("#cycle_diffusion_dr").val() + '&';
		data += 'debut_commerciale=' + $("#cycle_debut_commerciale").val() + '&';
		data += 'fin_commerciale=' + $("#cycle_fin_commerciale").val() + '&';
		data += 'publie=' + $('input[type=radio][name=cycle_publie]:checked').val() + '&';
		data += 'action_info=' + $('input[type=radio][name=cycle_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=cycle_lancement_chaud]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#cycle_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#cycle_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=C&';
		}
		data += 'ajax=4';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'cycle_existe') alert('Cycle existe déjà');
				if(msg == 'cycle_existe') return false;
				if(msg >= 0) {
					alert('Cycle Bien edité');
					send_comment('dr', $("#pod_cible").val(), $("#id_pod_cible").val());
					send_comment('dd', $("#pod_cible").val(), $("#id_pod_cible").val());
					window.setTimeout("redirect2CycleDetail()", 2000);
				}
			},
			error: function() {
			}
		});
		return false;
		
	});
});

function redirect2CycleDetail(url)
{
	var cycle_detail_page = $("#cycle_detail_page").val();
	window.location = cycle_detail_page;
}