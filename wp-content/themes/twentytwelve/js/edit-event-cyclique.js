jQuery(document).ready(function($) {
	/*
	 * Les champs dates
	 */
	$("#event_date_debut").datepicker();
	$("#event_date_fin").datepicker();
	$("#event_date_debut_pre_lancement").datepicker();
	$("#event_date_fin_pre_lancement").datepicker();
	$("#event_date_debut_extranet").datepicker();
	$("#event_date_fin_extranet").datepicker();
	$("#event_date_lancement_chaud_dd").datepicker();
	$("#event_date_lancement_chaud_dr").datepicker();

	/*
	 * Les champs ineditables
	 */
	$(".champ_non_editable").attr('disabled', true);
	

	/*
	 * Calculer les cycles correspondants
	 */
	$("#event_calcul_cycles_correspondants").bind('click', function() {
		if($("#event_semaine_fin").val() != '' && $("#event_semaine_debut").val() != '') {
			var data = 'debut=' + $("#event_semaine_debut").val() + '&fin=' + $("#event_semaine_fin").val() + '&ajax=13';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#event_cycles_correspondants").val(msg);
				},
				error: function() {
				}
			});
		}
	});
	
	/*
	 * Récupération date début à partir semaine début
	 * et
	 * Récupération date fiin à partir semaine fin
	 */
	$("#event_semaine_debut").bind('change', function() {
		var annee = $("#event_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=11';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#event_date_debut").val(msg);
				},
				error: function() {
				}
			});
		}
		/*
		 * Calculer les cycles correspondants
		 */
		get_cycles_correspondants('event', $("#event_semaine_debut").val(), $("#event_annee").val(), $("#event_semaine_fin").val(), $("#event_annee_fin").val(), $("#event_attach_list").val());		
	});
	
	$("#event_semaine_fin").bind('change', function() {
		var annee = $("#event_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=12';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#event_date_fin").val(msg);
				},
				error: function() {
				}
			});
		}
		/*
		 * Calculer les cycles correspondants
		 */
		get_cycles_correspondants('event', $("#event_semaine_debut").val(), $("#event_annee").val(), $("#event_semaine_fin").val(), $("#event_annee_fin").val(), $("#event_attach_list").val());		
	});
	

	/*
	 * Ajouter multiple cibles et types
	 */
	 $("#edit_type_to_event").bind('click', function() {
		if($("#event_type_list").val() != '') {
			if($("#event_type").val() == '') {
				$("#event_type_label").val(String($("#event_type_list option:selected").text()));
				$("#event_type").val(String($("#event_type_list").val()));
			} else {
				var temp = '';
				temp = $("#event_type").val();
				if(temp.indexOf($("#event_type_list").val()) == -1) {
					$("#event_type").val($("#event_type").val() + ',' + String($("#event_type_list").val()));
					$("#event_type_label").val($("#event_type_label").val() + ' + ' + String($("#event_type_list option:selected").text()));
				}
			}
		}
	});
	 
	$("#event_type_list").change(function() {
		if($("#event_type_list").val() == '') {
			$("#event_type_label").val('');
			$("#event_type").val('');
		}
	});

	$("#edit_cible_to_event").bind('click', function() {
		if($("#event_cible_list").val() != '') {
			if($("#event_cible").val() == '') {
				$("#event_cible_label").val(String($("#event_cible_list option:selected").text()));
				$("#event_cible").val(String($("#event_cible_list").val()));
			} else {
				var temp = '';
				temp = $("#event_cible").val();
				if(temp.indexOf($("#event_cible_list").val()) == -1) {
					$("#event_cible").val($("#event_cible").val() + ',' + String($("#event_cible_list").val()));
					$("#event_cible_label").val($("#event_cible_label").val() + ' + ' + String($("#event_cible_list option:selected").text()));
				}
			}
		}
	});
	
	$("#event_cible_list").change(function() {
		if($("#event_cible_list").val() == '') {
			$("#event_cible_label").val('');
			$("#event_cible").val('');
		}
	});
	

	/*
	 * Afficher champs dates lancment à chaud pour DD et DR quand 
	 * le boutton radio lancement à chaud est coché
	 */
	$('input[type=radio][name=event_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});

	/*
	 * Edition d'un event
	 */

	$("#edit_event_form").submit(function() {
		var empty = false;
		var empty_msg = 'Vous devez aussi saisir les champs suivants : \n';
		if($("#event_designation").val() == '') {
			empty = true;
			empty_msg += 'La désignation de l\'événement \n';
		}
		if($("#event_semaine_debut").val() == '' || $("#event_date_debut").val() == '' || $("#event_annee").val() == '') {
			empty = true;
			empty_msg += 'L\'année,la semaine et la date de début \n';
		}
		if($("#event_semaine_fin").val() == '' || $("#event_date_fin").val() == '' || $("#event_annee_fin").val() == '') {
			empty = true;
			empty_msg += 'L\'année,la semaine et la date de fin \n';
		}
		if($("#event_type_list").val() == '') {
			empty = true;
			empty_msg += 'Le type de l\'événement \n';
		}
		if($("#event_cible").val() == '') {
			empty = true;
			empty_msg += 'Une ou plusieurs cibles \n';
		}
		if($("#event_date_debut_extranet").val() == '' || $("#event_date_fin_extranet").val() == '') {
			empty = true;
			empty_msg += 'Les date de début et fin de lancement \n';
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += 'La date de lancement à chaud DD \n';
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += 'La date de lancement à chaud DR \n';
		}
		if(empty) alert(empty_msg);
		if(empty) return false;

		var data = '';
		data += 'annee=' + $("#event_annee").val() + '&';
		data += 'annee_fin=' + $("#event_annee_fin").val() + '&';
		data += 'designation=' + encodeURIComponent($("#event_designation").val()) + '&';
		data += 'semaine_debut=' + $("#event_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#event_semaine_fin").val() + '&';
		data += 'date_debut=' + $("#event_date_debut").val() + '&';
		data += 'date_fin=' + $("#event_date_fin").val() + '&';
		data += 'type=' + $("#event_type_list").val() + '&';
		data += 'cible=' + $("#event_cible").val() + '&';
		data += 'pre_lancement_debut=' + $("#event_date_debut_pre_lancement").val() + '&';
		data += 'pre_lancement_fin=' + $("#event_date_fin_pre_lancement").val() + '&';
		data += 'qte_pre_lancement=' + $("#event_qte_pre_lancement").val() + '&';
		data += 'extranet_debut=' + $("#event_date_debut_extranet").val() + '&';
		data += 'extranet_fin=' + $("#event_date_fin_extranet").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=event_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=event_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=event_publie]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#event_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#event_date_lancement_chaud_dr").val() + '&';
		data += 'id_event=' + $('#identifiant_event').val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=E&';
		}
		data += 'ajax=3';
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == '-1')
					$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				else {
					alert('Evénement  bien modifié!');
					send_comment('dr', $("#pod_cible").val(), $("#id_pod_cible").val());
					send_comment('dd', $("#pod_cible").val(), $("#id_pod_cible").val());
					window.setTimeout("redirect2EventDetail()", 2000);
				}
			},
			error: function() {
			}
		});
		return false;
		
	});
	
});

function redirect2EventDetail(url)
{
	var event_detail_page = $("#event_detail_page").val();
	window.location = event_detail_page;
}