jQuery(document).ready(function($) {

	var is_red_add = false;
	var is_red_delete = false;
	var is_red_update = false;
	
	$("#accordion_parametres").accordion({ collapsible: true, active:false, heightStyle: 'content' });

	$("#new_tva_value, " +
	  "#liste_multiple_tva, " +
	  "#new_cible_value, " +
	  "#liste_multiple_cible, " +
	  "#new_gamme_value, " +
	  "#liste_multiple_gamme, " +
	  "#new_marque_value, " +
	  "#liste_multiple_marque, " +
	  "#new_evenement_value, " +
	  "#liste_multiple_evenement, " +
	  "#new_categorie_value, " +
	  "#liste_multiple_categorie, " +
	  "#new_cycle_value, " +
	  "#liste_multiple_cycle, " +
	  "#new_periode_value, " +
	  "#texte_archive_delay, " +
	  "#nb_pixels_per_day, " +
	  "#rayon").bind('focus', function() {
		if(is_red_add) {
			is_red_add = false;
			$(this).css({'background' : 'white'});
		}
	});

	$("#liste_multiple_periode").bind('focus', function() {
		if(is_red_delete) {
			is_red_delete = false;
			$(this).css({'background' : 'white'});
		}
	});
	
	// **********************************************************************
	// Handler du bouton ajouter une tva
	// **********************************************************************
	$("#add_new_tva").bind('click', function() {
		if (!is_object_value_ok("#new_tva_value", "add")) return;
		execute_ajax_command('new_tva_value=' + $('#new_tva_value').val() + '&ajax=1', 
							 add_new_tva_success_method);
	});
	
	function add_new_tva_success_method(msg) {
		switch(msg) {
			case '-2' :
				$("#error_add_tva").html('La TVA doit être numérique').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '-1' :
				$("#error_add_tva").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
		}
		if(msg != '-2' && msg != '-1') {
			$("#error_add_tva").html('TVA bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_tva").append('<option value="' + msg + '">TVA ' + $("#new_tva_value").val() +' %</option>');
			$("#new_tva_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une tva
	// **********************************************************************
	$("#delete_tva_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_tva", "delete")) return;
		var liste_tva_delete = $("#liste_multiple_tva").val();
		execute_ajax_command('liste_tva_delete=' + liste_tva_delete + '&ajax=2', 
							 delete_tva_value_success_method);
	});
	
	function delete_tva_value_success_method(msg) {
		var liste_tva_delete = $("#liste_multiple_tva").val();
		switch (msg) {
			case '-1':
				$("#error_delete_tva").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_tva").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_tva").html('TVA bien supprimée(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_tva_delete.length; i++) {
					$("#liste_multiple_tva option[value=" + liste_tva_delete[i] + "]").remove();
				}
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton ajouter une cible
	// **********************************************************************
	$("#add_new_cible").bind('click', function() {
		if (!is_object_value_ok("#new_cible_value", "add")) return;
		execute_ajax_command('new_cible_value=' + encodeURIComponent($("#new_cible_value").val()) + '&ajax=3', 
							 add_new_cible_success_method);
	});
	
	function add_new_cible_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_cible").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_cible").html('Cible bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_cible").append('<option value="' + msg + '">' + $("#new_cible_value").val() +'</option>');
			$("#new_cible_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une cible
	// **********************************************************************
	$("#delete_cible_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_cible", "delete")) return;
		var liste_cible_delete = $("#liste_multiple_cible").val();
		execute_ajax_command('liste_cible_delete=' + liste_cible_delete + '&ajax=4', 
							 delete_cible_value_success_method);
	});
	
	function delete_cible_value_success_method(msg) {
		var liste_cible_delete = $("#liste_multiple_cible").val();
		switch (msg) {
			case '-1':
				$("#error_delete_cible").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_cible").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_cible").html('Cible(s) bien supprimé(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_cible_delete.length; i++) {
					$("#liste_multiple_cible option[value=" + liste_cible_delete[i] + "]").remove();
				}
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton ajouter une gamme
	// **********************************************************************
	$("#add_new_gamme").bind('click', function() {
		if (!is_object_value_ok("#new_gamme_value", "add")) return;
		execute_ajax_command('new_gamme_value=' + encodeURIComponent($("#new_gamme_value").val()) + '&ajax=5', 
							 add_new_gamme_success_method);
	});
	
	function add_new_gamme_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_gamme").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_gamme").html('Gamme bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_gamme").append('<option value="' + msg + '">' + $("#new_gamme_value").val() +'</option>');
			$("#new_gamme_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une gamme
	// **********************************************************************
	$("#delete_gamme_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_gamme", "delete")) return;
		var liste_gamme_delete = $("#liste_multiple_gamme").val();
		execute_ajax_command('liste_gamme_delete=' + liste_gamme_delete + '&ajax=6', 
							 delete_gamme_value_success_method);
	});

	function delete_gamme_value_success_method(msg) {
		var liste_gamme_delete = $("#liste_multiple_gamme").val();
		switch (msg) {
			case '-1':
				$("#error_delete_gamme").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_gamme").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_gamme").html('Gamme(s) bien supprimée(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_gamme_delete.length; i++) {
					$("#liste_multiple_gamme option[value=" + liste_gamme_delete[i] + "]").remove();
				}
				break;
		}
	}
	
	// **********************************************************************
	// Handler du bouton ajouter une marque
	// **********************************************************************
	$("#add_new_marque").bind('click', function() {
		if (!is_object_value_ok("#new_marque_value", "add")) return;
		execute_ajax_command('new_marque_value=' + encodeURIComponent($("#new_marque_value").val()) + '&ajax=7', 
							 add_new_marque_success_method);
	});
	
	function add_new_marque_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_marque").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_marque").html('Marque bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_marque").append('<option value="' + msg + '">' + $("#new_marque_value").val() +'</option>');
			$("#new_marque_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une marque
	// **********************************************************************
	$("#delete_marque_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_marque", "delete")) return;
		var liste_marque_delete = $("#liste_multiple_marque").val();
		execute_ajax_command('liste_marque_delete=' + liste_marque_delete + '&ajax=8', 
							 delete_marque_value_success_method);
	});
	
	function delete_marque_value_success_method(msg) {
		var liste_marque_delete = $("#liste_multiple_marque").val();
		switch (msg) {
			case '-1':
				$("#error_delete_marque").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_marque").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_marque").html('Marque(s) bien supprimée(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_marque_delete.length; i++) {
					$("#liste_multiple_marque option[value=" + liste_marque_delete[i] + "]").remove();
				}
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton ajouter un type d'évènement
	// **********************************************************************
	$("#add_new_evenement").bind('click', function() {
		if (!is_object_value_ok("#new_evenement_value", "add")) return;
		execute_ajax_command('new_evenement_value=' + encodeURIComponent($("#new_evenement_value").val()) + '&ajax=9', 
							 add_new_evenement_success_method);
	});
	
	function add_new_evenement_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_evenement").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_evenement").html('Evènement bien ajouté').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_evenement").append('<option value="' + msg + '">' + $("#new_evenement_value").val() +'</option>');
			$("#new_evenement_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer un type d'évènement
	// **********************************************************************
	$("#delete_evenement_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_evenement", "delete")) return;
		var liste_evenement_delete = $("#liste_multiple_evenement").val();
		execute_ajax_command('liste_evenement_delete=' + liste_evenement_delete + '&ajax=10', 
							 delete_evenement_value_success_method);
	});

	function delete_evenement_value_success_method(msg) {
		var liste_evenement_delete = $("#liste_multiple_evenement").val();
		switch (msg) {
			case '-1':
				$("#error_delete_evenement").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_evenement").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_evenement").html('Evènement(s) bien supprimé(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_evenement_delete.length; i++) {
					$("#liste_multiple_evenement option[value=" + liste_evenement_delete[i] + "]").remove();
				}
				break;
		}
	}
	
	// **********************************************************************
	// Handler du bouton ajouter une catégorie
	// **********************************************************************
	$("#add_new_categorie").bind('click', function() {
		if (!is_object_value_ok("#new_categorie_value", "add")) return;
		execute_ajax_command('new_categorie_value=' + encodeURIComponent($("#new_categorie_value").val()) + '&ajax=11', 
							 add_new_categorie_success_method);
	});
	
	function add_new_categorie_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_categorie").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_categorie").html('Catégorie bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_categorie").append('<option value="' + msg + '">' + $("#new_categorie_value").val() +'</option>');
			$("#new_categorie_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une catégorie
	// **********************************************************************
	$("#delete_categorie_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_categorie", "delete")) return;
		var liste_categorie_delete = $("#liste_multiple_categorie").val();
		execute_ajax_command('liste_categorie_delete=' + liste_categorie_delete + '&ajax=12', 
							 delete_categorie_value_success_method);
	});
	
	function delete_categorie_value_success_method(msg) {
		var liste_categorie_delete = $("#liste_multiple_categorie").val();
		switch (msg) {
			case '-1':
				$("#error_delete_categorie").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_categorie").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_categorie").html('Catégorie(s) bien supprimée(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_categorie_delete.length; i++) {
					$("#liste_multiple_categorie option[value=" + liste_categorie_delete[i] + "]").remove();
				}
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton ajouter un cycle
	// **********************************************************************
	$("#add_new_cycle").bind('click', function() {
		if (!is_object_value_ok("#new_cycle_value", "add")) return;
		execute_ajax_command('new_cycle_value=' + $("#new_cycle_value").val() + '&ajax=13', 
							 add_new_cycle_success_method);
	});

	function add_new_cycle_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_cycle").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_cycle").html('Cycle bien ajouté').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_cycle").append('<option value="' + msg + '">' + $("#new_cycle_value").val() +'</option>');
			$("#new_cycle_value").val('');
		}
	}
	
	// **********************************************************************
	// Handler du bouton supprimer un cycle
	// **********************************************************************
	$("#delete_cycle_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_cycle", "delete")) return;
		var liste_cycle_delete = $("#liste_multiple_cycle").val();
		execute_ajax_command('liste_cycle_delete=' + liste_cycle_delete + '&ajax=14', 
							 delete_cycle_value_success_method);
	});
	
	function delete_cycle_value_success_method(msg) {
		var liste_cycle_delete = $("#liste_multiple_cycle").val();
		switch (msg) {
			case '-1':
				$("#error_delete_cycle").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_cycle").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_cycle").html('Cycle(s) bien supprimé(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_cycle_delete.length; i++) {
					$("#liste_multiple_cycle option[value=" + liste_cycle_delete[i] + "]").remove();
				}
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton ajouter une période
	// **********************************************************************
	$("#add_new_periode").bind('click', function() {
		if (!is_object_value_ok("#new_periode_value", "add")) return;
		execute_ajax_command('new_periode_value=' + $("#new_periode_value").val() + '&ajax=15', 
							 add_new_periode_success_method);
	});
	
	function add_new_periode_success_method(msg) {
		if(msg == '-1') {
			$("#error_add_periode").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		} else {
			$("#error_add_periode").html('Période bien ajoutée').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			$("#liste_multiple_periode").append('<option value="' + msg + '">' + $("#new_periode_value").val() +'</option>');
			$("#new_periode_value").val('');
		}
	}

	// **********************************************************************
	// Handler du bouton supprimer une période
	// **********************************************************************
	$("#delete_periode_value").bind('click', function() {
		if (!is_object_value_ok("#liste_multiple_periode", "delete")) return;
		var liste_periode_delete = $("#liste_multiple_periode").val();
		execute_ajax_command('liste_periode_delete=' + liste_periode_delete + '&ajax=16', 
							 delete_periode_value_success_method);
	});

	function delete_periode_value_success_method(msg) {
		var liste_periode_delete = $("#liste_multiple_periode").val();
		switch (msg) {
			case '-1':
				$("#error_delete_periode").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;						
			case '0':
				$("#error_delete_periode").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_delete_periode").html('Période(s) bien supprimée(s)').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				for(i = 0; i < liste_periode_delete.length; i++) {
					$("#liste_multiple_periode option[value=" + liste_periode_delete[i] + "]").remove();
				}
				break;
		}
	}
	
	// **********************************************************************
	// Handler du bouton submit du formulaire de modification du texte
	// administrable de la page d'accueil
	// **********************************************************************
	$("#texte_header_form").submit(function() {
		execute_ajax_command('texte_header=' + encodeURIComponent($("#texte_header").val()) + '&ajax=17',
							 texte_header_form_success_method);
		return false;
	});
	
	function texte_header_form_success_method(msg) {
		switch(msg){
			case '0':
				$("#error_texte_header").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1':
				$("#error_texte_header").html('Texte bien enregistré').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton enregistrer du délai d'archivage
	// **********************************************************************
	$("#save_archive_delay").bind('click', function() {
		if (!is_object_value_ok("#texte_archive_delay", "update")) return;
		var archivage_delay = $("#texte_archive_delay").val();
		execute_ajax_command('archivage_delay=' + archivage_delay + '&ajax=18',
							 save_archive_delay_success_method);
	});
	
	function save_archive_delay_success_method(msg) {
		switch(msg) {
			case '-3' :
				$("#error_archive_delay").html('Le délai doit être numérique').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '-2' :
				$("#error_archive_delay").html('Le délai doit être supérieur à 0').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '-1' :
				$("#error_archive_delay").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '1' : $("#error_archive_delay").html('Délai bien modifié').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
		}
	}

	// **********************************************************************
	// Handler du bouton enregistrer des paramètres du calendrier
	// **********************************************************************
	$("#save_calender_param").bind('click', function() {
		if (!is_object_value_ok("#nb_pixels_per_day", "update")) return;
		if (!is_object_value_ok("#rayon", "update")) return;
		var nb_pixels_per_day = $("#nb_pixels_per_day").val();
		var rayon = $("#rayon").val();
		execute_ajax_command('nb_pixels_per_day=' + nb_pixels_per_day + '&rayon=' + rayon + '&ajax=calendar', save_calender_param_success_method);
	});
	
	function save_calender_param_success_method(msg) {
			switch(msg) {
				case '-3' :
					$("#error_settings_calendar_view").html('Le nombre de pixels par jour et le rayon doivent être numériques').fadeOut('slow').fadeIn('slow').fadeOut('slow');
					break;
				case '-2' :
					$("#error_settings_calendar_view").html('Le nombre de pixels par jour et le rayon doivent être supérieurs à 0').fadeOut('slow').fadeIn('slow').fadeOut('slow');
					break;
				case '1' : 
					$("#error_settings_calendar_view").html('Paramètres bien pris en compte').fadeOut('slow').fadeIn('slow').fadeOut('slow');
					break;
				default:
					$("#error_settings_calendar_view").html('Paramètres pas pris en compte').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			}
	}
	

	// **********************************************************************
	// Handler du bouton enregistrer des paramètres SSO
	// **********************************************************************
	$("#save_sso_settings").bind('click', function() {
		if (!is_object_value_ok("#secret_key", "update")) return;
		var secret_key = $("#secret_key").val();
		if (!is_object_value_ok("#extranet_url", "update")) return;
		var extranet_url = $("#extranet_url").val();
		//alert('secret_key=' + secret_key + '&extranet_url=' + extranet_url + '&ajax=sso');
		execute_ajax_command('secret_key=' + secret_key + '&extranet_url=' + extranet_url + '&ajax=sso',
							 save_sso_settings_success_method);
	});
	
	function save_sso_settings_success_method(msg) {
		//alert(msg);
		switch(msg) {
			case '-11':
			case '1-1':
			case '-1-1':
				$("#error_sso_settings").html('Un problème d\'accès à la base de données').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
			case '11' : 
				$("#error_sso_settings").html('Paramètres SSO bien modifiés').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				break;
		}
	}
	
	
});
