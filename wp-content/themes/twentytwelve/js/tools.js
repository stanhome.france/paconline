// **********************************************************************
// fonction permettant d'executer une commande ajax
// **********************************************************************
function execute_ajax_command(data, success_method, user_data) {
	$.ajax({
		url: '',
		type: 'GET',
		data: data,
		success: function(msg) { success_method(msg, user_data); }
	});
}

// **********************************************************************
// vérifie si un champ de type textfield est rempli
// **********************************************************************
function is_object_value_ok(obj_name, type_action) {
	if($(obj_name).val() == null || $(obj_name).val() == '') {
		if (type_action == 'update') is_red_update = true;
		if (type_action == 'delete') is_red_delete = true;
		if (type_action == 'add') is_red_add = true;
		$(obj_name).css({'background' : 'red'});
		return false;
	} else {
		$(obj_name).css({'background' : 'white'});
		return true;
	}
}

function replaceAll(txt, replace, with_this) {
	return txt.replace(new RegExp(replace, 'g'),with_this);
}

/**
 * Montre ou cache la télécommande de recherche
 */
function toggleTelecommande(){
	$("#search-telecommande").toggle();
}
//**********************************************************************
//Dynamiser menu sidebar
//**********************************************************************

jQuery(document).ready(function($) {
	//edit page title dynamically
	if($("#title_page").val() != '') $(this).attr("title", $("#title_page").val());
	
	//Dynamisme par défaut à l'affichage de la page
	$('#menu_pac ul li.current').parent().prev().addClass("open");
	$('#menu_pac h5.open').next().show();
	
	//Dynamisme au clic sur un élément
	$('#menu_pac h5.submenu').unbind('click').click(function() {
		if(!$(this).hasClass('open')){
			$('#menu_pac h5.submenu').removeClass('open');
			var class_h5 = $(this).attr('class');
			class_h5 = class_h5.replace(' submenu', '');
			
			$('#menu_pac ul').slideUp(300);
			$('#menu_pac ul.' + class_h5).slideDown(300);
			$(this).addClass('open');
		} else {
			$('#menu_pac ul').slideUp(300);
			$(this).removeClass('open');
		}
	});
	$('table#tree_articles tr:even').css('background','#f3f1f2');
	$('ul.treeview li.lastCollapsable ul.level2 li:even').css('background','#efedee');	
	
	//**********************Action scrollable***********//
	if($(".btn_group").length > 0) actionScrollManaging('.btn_group');
	//************Documents attach�s classement***********//
	$('.doc_attach_element:nth-child(3n)').css('margin-right','0');
	//*********Placeholder input search tested in IE*****************//
	placeHolderIe();
	
	$("#printer").bind('click', function() {
		$('ul.level2').show();
		$('ul.level2').parent('li').removeClass('expandable').addClass('collapsable');
		$('*').css('behavior','none');
		print();
		false;
	});

});

//**********************************************************************
//Envoi Commentaire
//**********************************************************************

function send_comment(visibility, pod_cible, id_pod_cible) {
	var content = '';
	if(visibility == 'dd') content = tinyMCE.get('mceEditor_dd').getContent();
	if(visibility == 'dr') content = tinyMCE.get('mceEditor_dr').getContent();
	var data = 'visibility=' + visibility + '&pod_cible=' + pod_cible + '&id_pod_cible=' + id_pod_cible + '&content=' + encodeURIComponent(content);
	//content = tinyMCE.get('mceEditor_dd').getContent();
	var url_comment = $("#url_to_send_comment").val();
	$.ajax({
		url: url_comment,
		type: 'post',
		data: data,
		success: function(msg) {
			$("#info_comment_" + visibility).html('Commentaire enregistré').fadeOut('slow').fadeIn('slow').fadeOut('slow');
		},
		error: function() {
			alert('error ajax');
		}
	});
}

var actionScrollManaging = (function (obj) {
	var positionBasketInPage = $(obj).offset().top;
	$(window).scroll(
	    function() {
	        if ($(window).scrollTop() >= positionBasketInPage) {
	        	// on dÃ©croche la barre d'action
	        	$('.btn_group').addClass("floatable");
	        	//$(obj).css('top', $(window).scrollTop()-192 + 'px');
	        } else {
	        	// on raccroche la barre d'action
	        	$('.btn_group').removeClass("floatable");
	        	//$(obj).css('top', '-85px');
	        }
	    }
	);
});
//*********Placeholder input search tested in IE*****************//
var placeHolderIe = (function () {
	if ( $.browser.msie ) {
		 $('#searchform input:text,#advancedSearchForm input.search_data').blur(function(){
			 if( $(this).val() ) {
		           $(this).addClass('full');
		     }
			 else{
				 $(this).removeClass('full'); 
			 }
		 });
	}
});

//*************Prévisualisation du contenu d'une entité********************************//
function previsualisation_entite(url) {
	$(".prev_loader").show();
	url += '&is_prev=oui';
	$.ajax({
		url: url,
		type: 'get',
		data: '',
		success: function(content) {
			$("#secondsplit").html(content);
			$("#prev_loader").hide();
			$('#mainSplitter').jqxSplitter({ orientation: 'horizontal', panels: [{ size: 350 }, { size: 150}] });
			$('#mainSplitter').jqxSplitter('expand');
			$(".prev_loader").hide();
		},
		error: function() {
		}
	});
}

function previsualisation_entite2(url) {
	$(".prev_loader").show();
	url += '&is_prev=oui';
	$.ajax({
		url: url,
		type: 'get',
		data: '',
		success: function(content) {
			$("#secondsplit2").html(content);
			$("#prev_loader").hide();
			$('#mainSplitter2').jqxSplitter({ orientation: 'horizontal', panels: [{ size: 350 }, { size: 150}] });
			$('#mainSplitter2').jqxSplitter('expand');
			$(".prev_loader").hide();
		},
		error: function() {
		}
	});
}

//*************Envoyer la page de visualisation vers l'imprimante**************************//
$("#printer").bind('click', function() {
	alert('jfjfj');
	$('li').trigger('click');
	print();
	false;
});

function send_to_imprimante() {
	print();
	false;
}


//**********Sous les entités Documet, Promo, Eveènement,************
//**********afficher un champ calculé en édition dès la ************
//**********saisir de semaine début, fin et rattachment ************
//**********prioritaire pour affichr les cycles ou périodes couverts***.
function get_cycles_correspondants(prefix, semaine_debut, annee, semaine_fin, annee_fin, permalink) {
	if(semaine_debut != '' && annee != '' && semaine_fin != '' && annee_fin != '') {
		var data = 'debut=' + semaine_debut + '&fin=' + semaine_fin + '&ajax=13';
		data += "&annee=" + annee + "&annee_fin=" + annee_fin;
		data += "&permalink=" + permalink;
		$.ajax({
			url: '',
			data: data,
			type: 'get',
			success: function(msg) {
				$("#"+prefix+"_cycles_correspondants").val(msg);
			},
			error: function() {
				alert('Erreur Ajax!');
			}
		});
	}
}

String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};