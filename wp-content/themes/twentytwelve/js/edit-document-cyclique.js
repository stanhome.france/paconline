jQuery(document).ready(function($) {
	/*
	 * Les champs dates
	 */
	$("#document_date_debut_pre_lancement").datepicker();
	$("#document_date_fin_pre_lancement").datepicker();
	$("#document_date_debut_lancement").datepicker();
	$("#document_date_fin_lancement").datepicker();
	$("#document_date_lancement_chaud_dd").datepicker();
	$("#document_date_lancement_chaud_dr").datepicker();

	/*
	 * Les champs ineditables
	 */
	$(".champ_non_editable").attr('disabled', true);

	/*
	 * Calculer les cycles correspondants
	 */
	$("#document_calcul_cycles_correspondants").bind('click', function() {
		if($("#document_semaine_debut").val() != '' && $("#document_semaine_fin").val() != '') {
			var data = 'debut=' + $("#document_semaine_debut").val() + '&fin=' + $("#document_semaine_fin").val() + '&ajax=13';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#document_cycles").val(msg);
				},
				error: function() {
				}
			});
		}
	});
	
	/*
	 * Calculer les cycles correspondants
	 */
	$("#document_semaine_debut").bind('change', function() {
		get_cycles_correspondants('document', $("#document_semaine_debut").val(), $("#document_annee").val(), $("#document_semaine_fin").val(), $("#document_annee_fin").val(), $("#document_attach_list").val());
	});
	$("#document_semaine_fin").bind('change', function() {
		get_cycles_correspondants('document', $("#document_semaine_debut").val(), $("#document_annee").val(), $("#document_semaine_fin").val(), $("#document_annee_fin").val(), $("#document_attach_list").val());
	});

	/*
	 * Ajouter multiple cibles et types
	 */
	 $("#edit_cible_to_document").bind('click', function() {
			if($("#document_cible_list").val() != '') {
				if($("#document_cible").val() == '') {
					$("#document_cible_label").val(String($("#document_cible_list option:selected").text()));
					$("#document_cible").val(String($("#document_cible_list").val()));
				} else {
					var temp = '';
					temp = $("#document_cible").val();
					if(temp.indexOf($("#document_cible_list").val()) == -1) {
						$("#document_cible").val($("#document_cible").val() + ',' + String($("#document_cible_list").val()));
						$("#document_cible_label").val($("#document_cible_label").val() + ' + ' + String($("#document_cible_list option:selected").text()));
					}
				}
			}
		});
		$("#document_cible_list").change(function() {
			if($("#document_cible_list").val() == '') {
				$("#document_cible_label").val('');
				$("#document_cible").val('');
			}
		});

	/*
	 * Afficher champs dates lancment à chaud pour DD et DR quand 
	 * le boutton radio lancement à chaud est coché
	 */
	$('input[type=radio][name=document_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});
	
	/*
	 * Edition d'un doc
	 */
	$("#edit_doc_form").submit(function() {
		var empty = false;
		var empty_msg = "Vous devez aussi saisir les champs suivants : \n";
		if($("#document_designation").val() == '') {
			empty = true;
			empty_msg += "La désignation du document \n";
		}
		if($("#document_semaine_debut").val() == '' || $("#document_annee").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de début \n";
		}
		if($("#document_semaine_fin").val() == '' || $("#document_annee_fin").val() == '') {
			empty = true;
			empty_msg += "L'année et la semaine de fin \n";
		}
		if($("#document_reference").val() == '') {
			empty = true;
			empty_msg += "La référence du document \n";
		}
		if($("#document_cible").val() == '') {
			empty = true;
			empty_msg += "Une ou plusieurs cibles \n";
		}
		if($("#document_date_debut_lancement").val() == '' || $("#document_date_fin_lancement").val() == '') {
			empty = true;
			empty_msg += "Les dates de début et de fin de lancement \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DD \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à chaud DR \n";
		}
		
		if(empty) alert(empty_msg);
		if(empty) return false;

		var data = '';
		data += 'reference=' + encodeURIComponent($("#document_reference").val()) + '&';
		data += 'designation=' + encodeURIComponent($("#document_designation").val()) + '&';
		data += 'annee=' + $("#document_annee").val() + '&';
		data += 'annee_fin=' + $("#document_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#document_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#document_semaine_fin").val() + '&';
		data += 'cible=' + $("#document_cible").val() + '&';
		data += 'critere_distribution=' + encodeURIComponent($("#document_critere_distribution").val()) + '&';
		data += 'reference_lot_appartenance=' + encodeURIComponent($("#document_reference_lot_appartenance").val()) + '&';
		data += 'reference_extranet=' + encodeURIComponent($("#document_reference_extranet").val()) + '&';
		data += 'qte_doc_par_lot=' + $("#document_qte_doc_par_lot").val() + '&';
		data += 'qte_doc_imprimes=' + $("#document_qte_doc_imprimes").val() + '&';
		data += 'date_debut_pre_lancement=' + $("#document_date_debut_pre_lancement").val() + '&';
		data += 'date_fin_pre_lancement=' + $("#document_date_fin_pre_lancement").val() + '&';
		data += 'qt_pre_lancement=' + $("#document_qt_pre_lancement").val() + '&';
		data += 'date_debut_lancement=' + $("#document_date_debut_lancement").val() + '&';
		data += 'date_fin_lancement=' + $("#document_date_fin_lancement").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=document_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=document_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=document_publie]:checked').val() + '&';
		data += 'is_mail=' + $('input[type=radio][name=document_is_mail]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#document_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#document_date_lancement_chaud_dr").val() + '&';
		data += 'id_doc=' + $('#identifiant_doc').val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=D&';
		}
		data += 'ajax=3';

		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == '-1')
					$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				else {
					alert('Document bien modifié!');
					send_comment('dr', $("#pod_cible").val(), $("#id_pod_cible").val());
					send_comment('dd', $("#pod_cible").val(), $("#id_pod_cible").val());
					window.setTimeout("redirect2DocDetail()", 2000);
				}
			},
			error: function() {
			}
		});
		return false;
	});
});


function redirect2DocDetail(url)
{
	var doc_detail_page = $("#doc_detail_page").val();
	window.location = doc_detail_page;
}