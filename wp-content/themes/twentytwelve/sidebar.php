<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<?php 
function currentUrlMatches ($pattern){
	$urlTab = explode("/", $_SERVER['REQUEST_URI']) ;
	$patternToFind = $urlTab[1];
	
	return $pattern == $patternToFind;
}
	
?>

<div id="menu_gauche_pac">
	<div id="menu_pac">
		<h5  <?php if(currentUrlMatches("calendrier")) echo 'class="open"';?>><a href="/calendrier/">CALENDRIER</a></h5>
		
		<h5 class="cycle submenu"><a href="#">CYCLES</a></h5>
			<ul class="cycle"> 
				<li <?php if(currentUrlMatches("cycles") && !isset($_GET['entite'])) echo 'class="current"';?>><a href="/cycles/?filtre=all">Voir liste</a></li>
				<li <?php if(currentUrlMatches("avenants-des-cycles")) echo 'class="current"';?>><a href="/avenants-des-cycles/">Avenants</a></li>
				<?php 
				$menu_admin = '<li ';
				if(currentUrlMatches("remarques-des-cycles")) $menu_admin .= 'class="current"';
				$menu_admin .= '><a href="/remarques-des-cycles/">Remarques</a></li>';
				echo do_shortcode( '[groups_member group="StanHome_CellulePAC,StanHome_ComitePAC"]'.$menu_admin.'[/groups_member]' );
				?>		
			</ul>
			 
			
		<!-- <h5 <?php if(currentUrlMatches("boites-a-outils")) echo 'class="open"';?>><a href="#">BOITE A OUTILS</a></h5> -->

		<?php
		$menu_admin = '<h5 class="admin submenu"><a href="#">ADMINISTRATION</a></h5><ul class="admin"><li ';
		if(currentUrlMatches("listes-parametrables")) $menu_admin .= 'class="current"';
		$menu_admin .= '><a href="/listes-parametrables/">Paramètres</a></li><li ';
		if(currentUrlMatches("articles")) $menu_admin .= 'class="current"';
		$menu_admin .= '><a href="/articles/">Articles</a></li><li ';
		if(currentUrlMatches("calendrier-par-semaine")) $menu_admin .= 'class="current"';
		$menu_admin .= '><a href="/calendrier-par-semaine/">Calendrier</a></li><li ';
// 		if(currentUrlMatches("diffusion")) $menu_admin .= 'class="current"';
// 		$menu_admin .= '><a href="/diffusion/">Diffusions / Impressions</a></li><li ';
		if(currentUrlMatches("logs")) $menu_admin .= 'class="current"';
		$menu_admin .='><a href="/logs/">Logs applicatif</a></li><li ';
		if(currentUrlMatches("diffusion")) $menu_admin .= 'class="current"';
		$menu_admin .='><a href="/diffusion/">Diffusion</a></li><li ';
// 		if(currentUrlMatches("mails")) $menu_admin .= 'class="current"'; //TODO AIMED
// 		$menu_admin .='><a href="/mails/">Liste des mails</a></li><li ';
// 		if(currentUrlMatches("entites-avec-lancement-chaud")) $menu_admin .= 'class="current"';
// 		$menu_admin .='><a href="/entites-avec-lancement-chaud/">Lancements à chaud</a></li><li ';
// 		if(currentUrlMatches("droits-visu-entites")) $menu_admin .= 'class="current"';
// 		$menu_admin .='><a href="/droits-visu-entites/">Droits de visualisation</a></li><li ';
		if(currentUrlMatches("backoffice")) $menu_admin .= 'class="current"';
		$menu_admin .='><a href="/wp-admin/users.php">Utilisateurs</a></li><li ';
		if(currentUrlMatches("backoffice")) $menu_admin .= 'class="current"';
		$menu_admin .='><a href="/wp-admin/admin.php?page=groups-admin">Groupes</a></li></ul>';
		echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$menu_admin.'[/groups_member]' ); ?>
				
		<h5 <?php if(currentUrlMatches("recherche")) echo 'class="open"';?>><a href="/recherche/?noSearchBar=on">RECHERCHE</a></h5>
		
		<h5 <?php ?>><a href="/wp-admin/profile.php">CHANGER DE MOT DE PASSE</a></h5>

		<h5> <a href="<?php echo wp_logout_url().'&redirect_to=/'; ?>">DECONNEXION</a></h5>
	</div>
</div>
