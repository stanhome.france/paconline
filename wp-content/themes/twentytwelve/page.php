<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
if (isset($_COOKIE['wordpress_logged_in_'.COOKIEHASH]) ||  is_page('detail-cycle-print')) {
	$cookie_exp = explode('|', $_COOKIE['wordpress_logged_in_'.COOKIEHASH]);
	$username = $cookie_exp[0];
	$user = get_user_by('login', $username);
	wp_set_current_user( $user->ID, $username );
} else {
	wp_redirect(site_url("/wp-login.php?redirect_to=/"), 302);
}

if (is_page('detail-cycle-print') || is_page('sous-elements') || is_page('sous-avenants')) {
	//ob_start();
?>
<?php if (!is_page('sous-elements')):?><page><?php endif;?>
	<?php
		global $post;
		if ($post->post_type == 'page') {
			require_once get_template_directory() . '/paconline/' . $post->post_name . '.php';
		}
	?>
<?php if (!is_page('sous-elements')):?></page><?php endif;?>
<?php 
	/*$get =  ob_get_contents();
	ob_end_clean();
	require("paconline/html2pdf_v4.03/html2pdf.class.php");
	
	$pdfName = WP_CONTENT_DIR . "/uploads/pdfs/" . 'cycle_' . $_GET['id_cycle'] . ".pdf";
	$html2pdf=new HTML2PDF("P","A4","fr");
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->writeHTML($get);
	$html2pdf->Output($pdfName, "F");*/
} else {


if (!is_page('recuperation-des-articles-pour-le-grid') && !is_page('recuperation-des-articles-pour-le-grid-popin') && !isset($_GET['is_prev'])):
if (!isset($_GET['ajax']) && !isset($_POST['ajax'])) get_header();
?>
<div class="clearfix"></div>
<div id="content" role="main">
	<table cellpadding="0" cellspacing="0" width="100%">
	<tr><td class="sidbar" valign="top">
	<?php if (!isset($_GET['ajax']) && !isset($_POST['ajax']))  get_sidebar();?>
	</td>
	<td valign="top">
		<div id="content_pac">
			<?php 
				global $post;
				if ($post->post_type == 'page') :
			?>
				<?php require_once get_template_directory() . '/paconline/' . $post->post_name . '.php';?>
			<?php endif;?>
		</div>
	</td></tr>
	</table>
	<div class="clearfix"></div>
</div><!-- #content -->

<div style="clear:both;">
<?php //get_footer(); ?>
</div>
</div>
</body>
</html>
<?php elseif (!isset($_GET['is_prev'])):?>
<?php 
				global $post;
				if ($post->post_type == 'page') :
			?>
				<?php require_once get_template_directory() . '/paconline/' . $post->post_name . '.php';?>
			<?php endif;?>
<?php endif;?>

<?php if (isset($_GET['is_prev']) && $_GET['is_prev'] == 'oui'):?>
<?php //get_header();?>
<?php 
	global $post;
	if ($post->post_type == 'page') :
?>
	<?php require_once get_template_directory() . '/paconline/' . $post->post_name . '.php';?>
<?php endif;?>
<?php endif;?>

<?php }?>