<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();
?>

<div class="clearfix"></div>
<div id="content" role="main">
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width=300 valign="top">
			</td>
			<td valign="top">
				<div>
					<div style="font-size:22px; font-weight:bold; color:#999; margin-bottom:30px;">Erreur 404!</div>
					<div style="font-size:14px; color:#444; margin-bottom:5px;">Nous n'avons pas trouvé le contenu que vous recherchez. Veuillez contacter un administrateur de PACOnline.</div>
					<div style="font-size:14px; color:#444;">Ou bien, retournez à la <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">page d'accueil</a></div>
				</div>
			</td>
		</tr>
	</table>
	<div class="clearfix"></div>
</div>
</body>
</html>
