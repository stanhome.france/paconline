<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="ISO-8859-1" />
<!-- <meta charset="<?php bloginfo( 'charset' ); ?>" /> -->
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/template.css">
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/jquery-ui.js"></script>

<!-- 
<link rel="stylesheet" href="<?php echo includes_url('js/bpopup/style.css'); ?>" type="text/css" />
<script type="text/javascript" src="<?php echo includes_url('js/bpopup/jquery.bpopup-0.9.3.min.js'); ?>"></script>
 -->

<!-- Scripts javascript généraux rajoutés par Aliznet -->
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/tools.js"></script>
<!-- Script spécifique au header -->
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/header.js"></script>

<link rel="stylesheet" type="text/css" href="../wp-content/themes/twentytwelve/css/jquery-ui.css">

<!-- JQ WIDGETS -->
<link rel="stylesheet" href="<?php echo includes_url('js/jqwidgets-ver2.7/jqwidgets/styles/jqx.base.css'); ?>" type="text/css" />
<script type="text/javascript" src="<?php echo includes_url('js/jqwidgets-ver2.7/jqwidgets/jqx-all.js'); ?>"></script>

<!-- jQgrid -->
<?php  if (is_page('edit-promotion-cyclique') || is_page('nouvelle-promotion')) { ?>
<link rel="stylesheet" href="<?php echo includes_url('js/jquery.jqGrid-4.4.3/css/ui.jqgrid.css'); ?>" type="text/css" />
<link rel="stylesheet" href="<?php echo includes_url('js/jquery.jqGrid-4.4.3/plugins/ui.multiselect.css'); ?>" type="text/css" />
<script type="text/javascript" src="<?php echo includes_url('js/jquery.jqGrid-4.4.3/js/i18n/grid.locale-fr.js'); ?>"></script>
<script type="text/javascript" src="<?php echo includes_url('js/jquery.jqGrid-4.4.3/plugins/ui.multiselect.js'); ?>"></script>
<script type="text/javascript" src="<?php echo includes_url('js/jquery.jqGrid-4.4.3/js/jquery.jqGrid.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo includes_url('js/jquery.jqGrid-4.4.3/plugins/jquery.tablednd.js'); ?>"></script>
<script type="text/javascript" src="<?php echo includes_url('js/jquery.jqGrid-4.4.3/plugins/jquery.contextmenu.js'); ?>"></script>
<?php } ?>
<?php get_template_part('paconline/push-to-head');?>

<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
<style type="text/css">
div.btn_group1,.frame,.frame form td.grey_area,.frame form table.grey_area,#footer_pac,a.lien,input[type="button"],input[type="submit"],#searchform,
table#tree_articles td a,.jqx-radiobutton-default,.jqx-radiobutton-check-checked{behavior:url('<?php echo get_template_directory_uri(); ?>/css/PIE.htc');}
</style>
<![endif]-->
<?php if(PHP_DEBUG_DISPLAY):?>
	<?php 
		#DEBUG
		// Options array for Debug object
		$options = array(
		'HTML_DIV_images_path' => get_template_directory_uri()."/images/debug",
		'HTML_DIV_css_path' => get_template_directory_uri()."/css/debug",
		'HTML_DIV_js_path' => get_template_directory_uri()."/js/debug"
				);
		
		/**
		 * Include Debug Class
		*/
		require_once(get_template_directory().'/paconline/debug/Debug.php');
		
		// Debug object
		$Dbg = new PHP_Debug($options);
		#DEBUG
	?>
	<script type="text/javascript" src="<?php echo $options['HTML_DIV_js_path']; ?>/html_div.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $options['HTML_DIV_css_path']; ?>/html_div.css" />
<?php endif;?>
</head>

<body>
<?php if (!isset($_GET['is_prev'])):?>
<div id="page">
<?php 
	if(PHP_DEBUG_DISPLAY){
		#DEBUG
		$Dbg->display();
		#DEBUG
	}
?>
<?php if (!isset($_GET['print'])):?>
<div id="header_pac">
	<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
	<?php endif; ?>
	
	<?php if (!isset($_GET['noSearchBar'])) { 
			if (!is_404()) { ?>
	<form id="searchform" action="recherche" method="get" role="search">
		<input type="hidden" name="noSearchBar" value="on"/>
		<input type="text" name="searchTerms" id="searchTerms" value="<?php if(isset($_GET['searchTerms'])) echo $_GET['searchTerms']; ?>" class="search_data" placeholder="Recherche..."/>
		<div class="btn-right-fleche" onclick="toggleTelecommande();" title="Filtres"></div>
		<input type="submit" id="searchsubmit" value="" class="btn-left-loupe" title="Rechercher"/>
		
		<div id="search-telecommande-container">
			<div id="search-telecommande" <?php if(!isset($_GET['searchTerms'])) echo 'style="display:none;"';?>>
				<div id="cycle" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['cycle']) && $_GET['cycle']=="true")) echo 'class="jqChecked"';?>>
					cycles
				</div>
				<div id="periode" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['periode']) && $_GET['periode']=="true")) echo 'class="jqChecked"';?>>
					périodes
				</div>
				<div id="event" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['event']) && $_GET['event']=="true")) echo 'class="jqChecked"';?>>
					événements
				</div>
				<div id="document" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['document']) && $_GET['document']=="true")) echo 'class="jqChecked"';?>>
					documents
				</div>
				<div id="promotion" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['promotion']) && $_GET['promotion']=="true")) echo 'class="jqChecked"';?>>
					promotions
				</div>
				<div id="archive" <?php if(isset($_GET['archive']) && $_GET['archive']=="true") echo 'class="jqChecked"';?>>
					archivés
				</div>
			</div>
		</div>
	</form>
	<?php	} 
		} ?>
</div>
<?php endif;?>
<?php endif;?>	
	