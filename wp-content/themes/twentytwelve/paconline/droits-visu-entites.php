<script type="text/javascript" src="<?php echo includes_url('js/jqdialog/jqdialog.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo includes_url('js/jqdialog/jqdialog.css') ?>">

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        
        $('#tree_cycles').jqxTree({ height: '500px' });


        $("#close_all").jqxButton({ width: '120', height: '25'});
        $("#open_all").jqxButton({ width: '120', height: '25'});
                
        $('.open_all').unbind('click').click(function() {
    		$('#tree_cycles').jqxTree('expandAll');
    	});

    	$('.close_all').unbind('click').click(function() {
    		$('#tree_cycles').jqxTree('collapseAll');
    	});

	});

    function edit_droits_visu(type_entite, id_entite, actual_level) {
        is_selected_0 = '';
        is_selected_10 = '';
        is_selected_20 = '';
        is_selected_30 = '';
        is_selected_40 = '';
        switch(actual_level) {
	        case 0:
	        	is_selected_0 = "selected";
	            break;
	        case 10:
	        	is_selected_10 = "selected";
	            break;
	        case 20:
	        	is_selected_20 = "selected";
	            break;
	        case 30:
	        	is_selected_30 = "selected";
	            break;
	        case 40:
	        	is_selected_40 = "selected";
	            break;
        }
		var content = '';
		content += "<h3>Modifier ls droits</h3><br/><br/>";
		content += "<table>";
		content += "<tr>";
		content += "<td>Choisir:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>";
		content += "<td>";
		content += "<select id='new_level'>";
		content += "<option "+is_selected_0+" value='0'>cellPAC</option>";
		content += "<option "+is_selected_10+" value='10'>cellPAC,comPAC</option>";
		content += "<option "+is_selected_20+" value='20'>cellPAC,comPAC,Presta</option>";
		content += "<option "+is_selected_30+" value='30'>cellPAC,comPAC,Presta,DR</option>";
		content += "<option "+is_selected_40+" value='40'>cellPAC,comPAC,Presta,DR,DD</option>";
		content += "</select>";
		content += "</td>";
		content += "<td><input style='width:72px !important' type='button' value='Enregistrer' onclick='save_new_level()'></td>";
		content += "<td><input style='width:65px !important' type='button' value='Annuler' onclick='cancel_level()'></td>";
		content += "</tr>";
		content += "</table>";
		content += "<input type='hidden' id='type_entie' name='type_entie' value='"+type_entite+"'>";
		content += "<input type='hidden' id='id_entite' name='id_entite' value='"+id_entite+"'>";
		content += "<br/><br/>";
		$.jqDialog.content(content);
    }

    function save_new_level() {
    	var data = 'type_entie='+$("#type_entie").val();
		data += '&id_entite='+$("#id_entite").val();
		data += '&diffusion_level='+$("#new_level").val();
		data += "&ajax=1";
		$.ajax({
			url: '',
			type: 'get',
			dataType: 'json',
			data: data,
			success: function(msg){
				if(msg.success == '1') {
					$("#"+$("#type_entie").val()+"_"+$("#id_entite").val()).html(msg.new_title);
					$("#jqDialog_box").hide();
				}
			},
			error: function() {
				alert('Error Ajax!');
			}
		});
    }

    function cancel_level() {
    	$("#jqDialog_box").hide();
    }
</script>

<?php 
require_once 'include-functions.php';

if (isset($_GET['ajax']) && $_GET['ajax'] != '-1') {
	ob_end_clean();
	require_once 'droits-visu-entites/ajax.php';
	exit();
}

$numeros_cycles = get_liste_numeros_cycles();
$cibles = get_liste_cible();
$types = get_liste_event();
$marques = get_liste_marque();
$gammes = get_liste_gamme();

// recherche du groupe le plus fort d'un user dans la liste des groups auquels il appartient
// le groupe le plus fort se trouve dans $high_level_group
$high_level_group = get_high_level_group_from_current_user();


// N'afficher que les cycles non archivés et visibles par le groupe dont le user courant fait partie
$where_extension = "";
if ($high_level_group == "StanHome_CellulePAC" or $high_level_group == "StanHome_ComitePAC_Work") {
	$where_extension = "";
}
if ($high_level_group == "StanHome_Presta") {
	$where_extension = " and diffusion_level = 20";	
}
if ($high_level_group == "StanHome_DD") {
	$where_extension = " and (diffusion_level = 30 or diffusion_level = 40)";
}
if ($high_level_group == "StanHome_DR") {
	$where_extension = " and diffusion_level = 40";
}
$cycles = Pods("cycles", array("where"=>"archive = 0".$where_extension, "orderby" => "t.date_debut", "limit" => "-1"));
$total = $cycles->total();

?>


<div class="frame btn_grp">
<div class="btn_group">
	<div class="btn_group1">
		<input type="hidden" id="title_page" value="<?php if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Périodes'?>">
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<input type="button" value="Ouvrir tout" class="open_all" id="open_all">
		<div class="clearfix"></div>
	</div>
</div>
</div>
<div class="frame" >
<?php if ($total > 0):?>
	<h3 class="ttl">Les droits de visualisation sur les entités</h3>
	<div id="tree_cycles">
	<ul style="width:100%;">
		<?php
			$intercale_cycle_color = 0; 
			while ($cycles->fetch()): 
				if ($intercale_cycle_color % 2 == 0) {
					$li_cycle_bg = "#e8e8e8";
				} else {
					$li_cycle_bg = "transparent";
				}
				$intercale_cycle_color++; ?>
		<li>
			<div style="background-color:<?php echo $li_cycle_bg; ?>; padding:5px; width:100%;">
			<?php 
				$cycle_periode_url = get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$cycles->field('id');
			?>
				<input type="image" id="c_edit_<?php echo $cycles->field('id')?>" src="<?php bloginfo('template_directory'); echo '/images/users-visu-edit.png' ?>" title="Modifier les droits de visualisation de cette entité" alt="Modifier les droits de visualisation de cette entité" onclick="edit_droits_visu('c', <?php echo $cycles->field('id')?>, <?php echo (!is_null($cycles->field('diffusion_level'))) ? $cycles->field('diffusion_level') : 0?>)">
				<a id="c_<?php echo $cycles->field('id')?>" href="<?php echo $cycle_periode_url?>"><?php echo $cycles->display('identifiant')?> visible par <?php echo droitVisuToString($cycles)?></a>
			</div>
			<ul>
				<?php $cycle_id = $cycles->field('id')?>
				<?php $cycle_permalink = $cycles->field('permalink')?>
				<!-- Promotions -->
				<?php 
					$cycles_promos = Pods('cycles_promos', array('where' => 'id_cycle='.$cycle_id, 'limit' => '-1'));
					$array_promos = array();
					if ($cycles_promos->total() > 0):
						while ($cycles_promos->fetch()):
							$promo = Pods('promotions', array('where' => 'id='.$cycles_promos->field('id_promo').' AND permalink=\''.$cycle_permalink.'\''));
							if ($promo->total() > 0):
								while ($promo->fetch()):
									$array_promos[] = $promo->field('id');
				?>
							<?php endwhile;?>
						<?php endif;?>
					<?php endwhile;?>
				<?php endif;?>
				<?php if (count($array_promos) > 0):?>
				<li>Promotions
					<ul>
						<?php foreach ($array_promos as $item):?>
						<?php 
							$promotion = Pods('promotions', array('where' => 'id='.$item));
							$promotion->fetch();
							$promo_url = get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $promotion->field('id');
						?>
						<li>
							<?php 
								$level = '';
								/*if(isset($promotion->display('diffusion_level'))) $level = $promotion->display('diffusion_level');
								else $level = '';*/
							?>
							<input type="image" id="p_edit_<?php echo $promotion->field('id')?>" src="<?php bloginfo('template_directory'); echo '/images/users-visu-edit.png' ?>" title="Modifier les droits de visualisation de cette entité" alt="Modifier les droits de visualisation de cette entité" onclick="edit_droits_visu('p', <?php echo $promotion->field('id')?>, <?php echo (!is_null($promotion->field('diffusion_level'))) ? $promotion->field('diffusion_level') : 0?>)">
							<a id="p_<?php echo $promotion->field('id')?>" href="<?php echo $promo_url?>"><?php echo stripslashes($promotion->display('reference_extranet').' : '.$promotion->display('description'))?> visible par <?php echo droitVisuToString($promotion)?></a>
						</li>
						<?php endforeach;?>
					</ul>
				</li>
				<?php endif;?>
				
				<!-- Events -->
				<?php 
					$cycles_events = Pods('cycles_events', array('where' => 'id_cycle='.$cycle_id, 'limit' => '-1'));
					$array_events = array();
					if ($cycles_events->total() > 0):
						while ($cycles_events->fetch()):
							$event = Pods('events', array('where' => 'id='.$cycles_events->field('id_event').' AND permalink=\''.$cycle_permalink.'\''));
							if ($event->total() > 0):
								while ($event->fetch()):
									$array_events[] = $event->field('id');
				?>
							<?php endwhile;?>
						<?php endif;?>
					<?php endwhile;?>
				<?php endif;?>
				<?php if (count($array_events) > 0):?>
				<li>Evènements
					<ul>
						<?php foreach ($array_events as $item):?>
						<?php 
							$event = Pods('events', array('where' => 'id='.$item));
							$event->fetch();
							$event_url = get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event->field('id');
						?>
						<li>
							<?php //$level = isset($event->field('diffusion_level')) ? $event->field('diffusion_level') : '' ?>
							<input type="image" id="e_edit_<?php echo $event->field('id')?>" src="<?php bloginfo('template_directory'); echo '/images/users-visu-edit.png' ?>" title="Modifier les droits de visualisation de cette entité" alt="Modifier les droits de visualisation de cette entité" onclick="edit_droits_visu('e', <?php echo $event->field('id')?>, <?php echo (!is_null($event->field('diffusion_level'))) ? $event->field('diffusion_level') : 0?>)">
							<a id="e_<?php echo $event->field('id')?>" href="<?php echo $event_url?>"><?php echo stripslashes($event->display('designation'))?> visible par <?php echo droitVisuToString($event)?></a>
						</li>
						<?php endforeach;?>
					</ul>
				</li>
				<?php endif;?>
				
				<!-- Events -->
				<?php 
					$cycles_docs = Pods('cycles_docs', array('where' => 'id_cycle='.$cycle_id, 'limit' => '-1'));
					$array_docs = array();
					if ($cycles_docs->total() > 0):
						while ($cycles_docs->fetch()):
							$doc = Pods('documents', array('where' => 'id='.$cycles_docs->field('id_doc').' AND permalink=\''.$cycle_permalink.'\''));
							if ($doc->total() > 0):
								while ($doc->fetch()):
									$array_docs[] = $doc->field('id');
				?>
							<?php endwhile;?>
						<?php endif;?>
					<?php endwhile;?>
				<?php endif;?>
				<?php if (count($array_docs) > 0):?>
				<li>Documents
					<ul>
						<?php foreach ($array_docs as $item):?>
						<?php 
							$doc = Pods('documents', array('where' => 'id='.$item));
							$doc->fetch();
							$doc_url = get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc->field('id');
						?>
						<li>
							<?php //$level = isset($doc->field('diffusion_level')) ? $doc->field('diffusion_level') : ''?>
							<input type="image" id="d_edit_<?php echo $doc->field('id')?>" src="<?php bloginfo('template_directory'); echo '/images/users-visu-edit.png' ?>" title="Modifier les droits de visualisation de cette entité" alt="Modifier les droits de visualisation de cette entité" onclick="edit_droits_visu('d', <?php echo $doc->field('id')?>, <?php echo (!is_null($doc->field('diffusion_level'))) ? $doc->field('diffusion_level') : 0?>)">
							<a id="d_<?php echo $doc->field('id')?>" href="<?php echo $doc_url?>"><?php echo stripslashes($doc->display('designation'))?> visible par <?php echo droitVisuToString($doc)?></a>
						</li>
						<?php endforeach;?>
					</ul>
				</li>
				<?php endif;?>
				
			</ul>
		</li>
		<?php endwhile;?>
	</ul>
	</div>
<?php else:?>
	<h4>
		<?php 
			if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Pas de périodes enregistrées';
			else echo 'Pas de cycles enregistrés';	
		?>
	</h4>
<?php endif;?>
</div>
