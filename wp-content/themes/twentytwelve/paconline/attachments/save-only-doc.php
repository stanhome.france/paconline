<?php

$filenames = explode(' ', $_GET['files_to_attach']);

foreach($filenames as $filename) {

	$whereQuery = 'id_entity='.$entity_id.' and ';
	$whereQuery .= 'type="'.$_GET["type_entite"].'" and complete_path="'.get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/".$filename.'"';
		
	// mémoriser l'attachement dans l'entité
	$pod = Pods('attachments', array('where' => $whereQuery));
	if (!$pod->fetch()) {
		$pod = Pods('attachments');
		switch ($_GET["type_entite"]) {
			case 'C': // traitement pour les cycles
			case 'R': // traitement pour les périodes
			case 'P': // traitement pour les promotions
			case 'E': // traitement pour les évènements
			case 'D': // traitement pour les documents
				$data = array('id_entity' => $entity_id,
							  'type' => $_GET["type_entite"],
							  'complete_path' => get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/".$filename);
				$id = $pod->add($data);
					
				// Indexage du document
				//require_once get_template_directory().'/paconline/recherche/index_helper.php';
				//indexAttachment($id, $data['complete_path']);
	
				break;
			case '*': // traitement pour les documents permanents
				$data = array('type' => $_GET["type_entite"],
							  'complete_path' => get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/".$filename);
				$pod->add($data);
				break;
			default:
		}
	}
}
?>