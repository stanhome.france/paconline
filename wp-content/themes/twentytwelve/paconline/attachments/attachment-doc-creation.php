<p class="ttl">Documents attachés</p>
<div class="frame doc_attach">
	<!-- Bloc ajout d'un document -->
	<div class="doc_uploader_form">
		<form action="" id="doc_uploader_form" method="post" target="postiframe">
			<input type="file" id="doc_to_upload" name="doc_to_upload"/>
			<input type="hidden" id="files_to_attach" name="files_to_attach" value=""/>
			<input type="button" id="formsubmit" value="Transférer"/>
		</form>
	</div>
	
	<div id="all_docs"></div>
</div>

<script>
jQuery(document).ready(function($) {
	$("#formsubmit").unbind('click').click(function () {
		if ($("#doc_to_upload").val() != '') {
			var iframe = $('<iframe name="postiframe" id="postiframe" style="display:none;"/>');
	        $("body").append(iframe);
	        var form = $('#doc_uploader_form');
	        form.attr("action", "<?php echo get_permalink(get_page_by_path('upload-only-doc')); ?>");
	        form.attr("enctype", "multipart/form-data");
	        form.attr("encoding", "multipart/form-data");
	        form.attr("file", $('#doc_to_upload').val());
	        form.submit();
	
	        $("#postiframe").load(function () { 
	            $("#files_to_attach").val($(this).contents().find('#file_uploaded').val());
	
	            // on vide all_docs
	            $('.doc_attach_element').remove();
	            $('#clearboth').remove();
	
	            // ajouter les items de files_to_attach sous la forme d'une liste d'objets dans la div doc_attach_element
	        	var items = $('#files_to_attach').val().split(' ');
	        	
	        	$.each(items, function(index, value) {
	        		
	        		if (value != '') {
	        			$('#all_docs').append('<div class="doc_attach_element"><img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle" style="margin-right:10px;"/><img src="../wp-content/themes/twentytwelve/images/remove.png" width=20 align="absmiddle" style="margin-right:10px; cursor:pointer;" class="delete_doc" id="del*' + value + '"/><a href="<?php echo get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/"; ?>' + value + '" target="_blank">' + value + '</a><div class="clearfix"></div></div>');
	        		}
	        	});
	        	$('#all_docs').append('<div style="clear:both;" id="clearboth"></div>');
	
	        	$('#doc_to_upload').val('');
			});
		}
        return false;
    });
});
</script>
