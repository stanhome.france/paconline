<style type="text/css">
.jqx-widget-content {
   border-color: #FFFFFF ;
}
</style>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        
    	 var ids_json = $("#ids_json").val();
         ids_json=ids_json.split(",");
         $.each(ids_json, function(cle, val) {
         	$('#tree_avenants_'+val).jqxTree({ width:"98%" });
         });

        $("#close_all").jqxButton({ width: '120', height: '35'});
        /*$("#open_all").jqxButton({ width: '120', height: '35'});
                
        $('.open_all').unbind('click').click(function() {
    		$('#tree_avenants').jqxTree('expandAll');
    	});*/

	});
</script>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$("span[id]").bind('click', function() {
			var id = $(this).attr('id').substring(14);
			if(!$("#li_cycle_"+id).hasClass('done')) {
				$("#dv_cycle_"+id).show();
				$(".prev_loader").show();
				$.ajax({
					url: '/sous-avenants/',
					type: 'get',
					data: 'cycle_id='+id+'&permalink= periodes',
					success: function(html) {
						$("#ul_cycle_"+id).append(html);
						$("#li_cycle_"+id).addClass('done');
						$(".prev_loader").hide();
						$("#dv_cycle_"+id).jqxTree({ width: "95%" });
					}
				});
			} else {
				if($("#dv_cycle_"+id).css('display')=='none') $("#dv_cycle_"+id).show();
				else $("#dv_cycle_"+id).hide();
			}
		});

		$('.close_all').unbind('click').click(function() {
			var ids_json = $("#ids_json").val();
			ids_json=ids_json.split(",");
    		$.each(ids_json, function(cle, val) {
    			$("#dv_cycle_"+val).jqxTree('collapseAll');
            	$('#tree_avenants_'+val).jqxTree('collapseAll');
            	$("#dv_cycle_"+val).hide();
            });
    	});
	});
</script>

<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<!--  <input type="button" value="Ouvrir tout" class="open_all" id="open_all">-->
		<div class="clearfix"></div>
	</div>
</div>
<div class="frame">
	<h3 class="ttl">Les avenants</h3>
	<?php
		$ids_json = array(); 
		$cycles = Pods('cycles', array('where' => "permalink='periodes' and archive=0", 'limit' => "-1"));
		if ($cycles->total() > 0) {
			while($cycles->fetch()) {
				$cycle_id = $cycles->display('id');
				$ids_json[] = $cycle_id;
	?>
			<div id="tree_avenants_<?php echo $cycle_id?>">
				<ul style="width: 100%;">
					<li id="li_cycle_<?php echo $cycle_id?>">
						<div style="background-color: #e8e8e8; padding:5px; width:100%;"><?php echo $cycles->display('identifiant')?></div>
						<ul><li class="startTree" style="display: none">xdx</li></ul>
					</li>					
				</ul>
			</div>
			<div id="dv_cycle_<?php echo $cycle_id?>" style="padding-left: 25px;">
				<ul id="ul_cycle_<?php echo $cycle_id?>"><li class="startTree" style="display: none">xdx</li></ul>
			</div>
	<?php
			} 
		}
	?>
</div>
<input type="hidden" id="ids_json" value='<?php echo implode(',', $ids_json)?>'/>
<div class="prev_loader">
	<img src="<?php bloginfo('template_directory'); echo '/images/ajax-loader.gif' ?>">
</div>