<?php
/**
 * Ajoute à l'index du moteur de recherche un document attaché
 * @param int $id ID en base du document attaché
 * @param string $completePath chemin complet vers le fichier
 */
function indexAttachment($id, $completePath){
	$index_path = get_template_directory().'/../../plugins/search/lucene/index/stanhome_index';
	$searchObject = new sh_search($index_path);
	
	//Ouvre ou créé l'index
	try {
		$index = $searchObject->open_index();
	} catch (Exception $e) {
		$searchObject->create_index();
		$index = $searchObject->open_index();
	}
	
	$fileName = basename($completePath);
	
	$file_path = explode("uploads", $completePath);
	$uploads = wp_upload_dir();
	$file_path = $uploads["basedir"].$file_path[1];
	
	$searchObject->add_to_index($fileName, $file_path, "attachment", $id);
	$index->optimize();
}

/**
 * Supprime un document attaché de l'index
 * @param int $id ID du document en base de donnée
 */
function deleteAttachmentFromIndex($id){
	$index_path = get_template_directory().'/../../plugins/search/lucene/index/stanhome_index';
	$searchObject = new sh_search($index_path);
	
	//Ouvre ou créé l'index
	try {
		$index = $searchObject->open_index();
	} catch (Exception $e) {
		$searchObject->create_index();
		$index = $searchObject->open_index();
	}
	
	$searchObject->delete_from_index($id, "attachment");
}