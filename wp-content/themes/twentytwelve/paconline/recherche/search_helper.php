<?php
/**
 * Récupère les cycles correspondants à un terme de recherche
 * @param sh_search $s Objet Search Tools
 * @param string $query Terme de recherche
 * @param bool $archive Rechercher les cycles archivés ou non
 */
function searchCycles( $query, $archive, $dateDebut, $dateFin){
    $cycleHits =  search_by_type($query, 'cycle', $dateDebut, $dateFin);
	$totalCycleHits = count($cycleHits);
	if($totalCycleHits > 0)
	{
		$whereQuery= "archive=".$archive;

		$whereQuery.= " AND (";
		for ($i=0; $i<$totalCycleHits; $i++){
		    $dataId = $cycleHits[$i]->id != null ? $cycleHits[$i]->id : $cycleHits[$i][id];
			if($dataId != null)
			{
				$whereQuery.=" id=".$dataId;
				//pas de 'OR' pour le dernier élément
				if($i < $totalCycleHits-1){
					$whereQuery.=" OR";
				}
			}
		}
		$whereQuery.= ")";

		$cycles = Pods('cycles', array("where"=>$whereQuery, "orderby"=> "date_debut DESC", "limit"=>"-1"));
		if($cycles->total_found() > 0)
			return $cycles;
	}

	return false;
}


/**
 * Récupère les événements correspondants à un terme de recherche
 * @param sh_search $s Objet Search Tools
 * @param string $query Terme de recherche
 * @param bool $archive Rechercher dans les cycles archivés ou non
 */
function searchEvents(   $query, $archive, $dateDebut, $dateFin){
    $eventHits =   search_by_type($query, 'event', $dateDebut, $dateFin);
	$totalEventHits = count($eventHits);
	if($totalEventHits > 0)
	{
		//JOIN CYCLES
		$joinQuery = "LEFT JOIN wp_pods_cycles_events ON t.id=wp_pods_cycles_events.id_event LEFT JOIN wp_pods_cycles ON wp_pods_cycles_events.id_cycle=wp_pods_cycles.id";

		//WHERE
		$whereQuery= "wp_pods_cycles.archive=".$archive;

		$whereQuery.= " AND (";
		for ($i=0; $i<$totalEventHits; $i++){
		    $dataId = $eventHits[$i]->id != null ? $eventHits[$i]->id : $eventHits[$i][id];
			if($dataId != null)
			{
				$whereQuery.=" t.id=".$dataId;
				//pas de 'OR' pour le dernier élément
				if($i < $totalEventHits-1){
					$whereQuery.=" OR";
				}
			}
		}
		$whereQuery.= ")";

		$events = Pods('events', array("join"=>$joinQuery, "where"=>$whereQuery, "orderby"=> "t.date_debut DESC", "limit"=>"-1"));
		if($events->total_found() > 0)
			return $events;
	}

	return false;
}

/**
 * Récupère les cycles associés à un événement
 * @param int $eventId Id de l'événement en base de données
 * @param bool $archive Inclure les cycles archivés ou non
 */
function getEventCycles($eventId, $archive){
	$joinQuery = "LEFT JOIN wp_pods_cycles_events ON t.id=wp_pods_cycles_events.id_cycle";

	$whereQuery= "archive=".$archive;
	$whereQuery.= " AND wp_pods_cycles_events.id_event=".$eventId;

	$cyclesForThisEvent = Pods('cycles', array("join"=>$joinQuery,  "where"=>$whereQuery, "orderby"=>"t.date_debut DESC", "limit"=>"-1"));

	if($cyclesForThisEvent->total_found() > 0){
		return $cyclesForThisEvent;
	}
	else {
		return false;
	}
}

/**
 * Récupère les documents correspondants à un terme de recherche
 * @param sh_search $s Objet Search Tools
 * @param string $query Terme de recherche
 * @param bool $archive Rechercher dans les cycles archivés ou non
 */
function searchDocuments(   $query, $archive, $dateDebut, $dateFin){
    $documentHits =   search_by_type($query, 'document', $dateDebut, $dateFin);
	$totalDocumentHits = count($documentHits);
	if($totalDocumentHits > 0)
	{
		//JOIN CYCLES
		$joinQuery = "LEFT JOIN wp_pods_cycles_docs ON t.id=wp_pods_cycles_docs.id_doc LEFT JOIN wp_pods_cycles ON wp_pods_cycles_docs.id_cycle=wp_pods_cycles.id";

		//WHERE
		$whereQuery= "wp_pods_cycles.archive=".$archive;

		$whereQuery.= " AND (";
		for ($i=0; $i<$totalDocumentHits; $i++){
		    $dataId = $documentHits[$i]->id != null ? $documentHits[$i]->id : $documentHits[$i][id];
			if($dataId != null)
			{
				$whereQuery.=" t.id=".$dataId;
				//pas de 'OR' pour le dernier élément
				if($i < $totalDocumentHits-1){
					$whereQuery.=" OR";
				}
			}
		}
		$whereQuery.= ")";

		$documents = Pods('documents', array("join"=>$joinQuery, "where"=>$whereQuery, "orderby"=> "t.date_debut_lancement DESC", "limit"=>"-1"));

		if($documents->total_found() > 0)
			return $documents;
	}

	return false;
}

/**
 * Récupère les cycles associés à un document
 * @param int $docId Id du document en base de données
 * @param bool $archive Inclure les cycles archivés ou non
 */
function getDocumentCycles($docId, $archive){
	$joinQuery = "LEFT JOIN wp_pods_cycles_docs ON t.id=wp_pods_cycles_docs.id_cycle";

	$whereQuery= "archive=".$archive;
	$whereQuery.= " AND wp_pods_cycles_docs.id_doc=".$docId;

	$cyclesForThisDocument = Pods('cycles', array("join"=>$joinQuery,  "where"=>$whereQuery, "orderby"=>"t.date_debut DESC", "limit"=>"-1"));

	if($cyclesForThisDocument->total_found() > 0){
		return $cyclesForThisDocument;
	}
	else {
		return false;
	}
}

/**
 * Récupère les promotions correspondants à un terme de recherche
 * @param sh_search $s Objet Search Tools
 * @param string $query Terme de recherche
 * @param bool $archive Rechercher dans les cycles archivés ou non
 */
function searchPromotions(   $query, $archive, $dateDebut, $dateFin){
    $promotionHits =   search_by_type($query, 'promotion', $dateDebut, $dateFin);
	$totalPromotionsHits = count($promotionHits);
	if($totalPromotionsHits > 0)
	{
		//JOIN CYCLES
		$joinQuery = "LEFT JOIN wp_pods_cycles_promos ON t.id=wp_pods_cycles_promos.id_promo LEFT JOIN wp_pods_cycles ON wp_pods_cycles_promos.id_cycle=wp_pods_cycles.id";

		//WHERE
		$whereQuery= "wp_pods_cycles.archive=".$archive;

		$whereQuery.= " AND (";
		for ($i=0; $i<$totalPromotionsHits; $i++){
		    $dataId = $promotionHits[$i]->id != null ? $promotionHits[$i]->id : $promotionHits[$i][id];
			if($dataId != null)
			{
				$whereQuery.=" t.id=".$dataId;
				//pas de 'OR' pour le dernier élément
				if($i < $totalPromotionsHits-1){
					$whereQuery.=" OR";
				}
			}
		}
		$whereQuery.= ")";

		$promotions = Pods('promotions', array("join"=>$joinQuery, "where"=>$whereQuery, "orderby"=> "t.date_debut_lancement DESC", "limit"=>"-1"));

		if($promotions->total_found() > 0)
			return $promotions;
	}

	return false;
}

/**
 * Récupère les cycles associés à une promotions
 * @param int $docId Id de la promotion en base de données
 * @param bool $archive Inclure les cycles archivés ou non
 */
function getPromotionCycles($promoId, $archive){
	$joinQuery = "LEFT JOIN wp_pods_cycles_promos ON t.id=wp_pods_cycles_promos.id_cycle";

	$whereQuery= "archive=".$archive;
	$whereQuery.= " AND wp_pods_cycles_promos.id_promo".$promoId;

	$cyclesForThisPromotion = Pods('cycles', array("join"=>$joinQuery,  "where"=>$whereQuery, "orderby"=>"t.date_debut DESC", "limit"=>"-1"));

	if($cyclesForThisPromotion->total_found() > 0){
		return $cyclesForThisPromotion;
	}
	else {
		return false;
	}
}

/**
 * Récupère les documents attachés correspondants à un terme de recherche
 * @param sh_search $s Objet Search Tools
 * @param string $query Terme de recherche
 */
function searchAttachments(   $query){
    $attachmentsHits =   search_by_type($query, 'attachment');
	$totalAttachmentsHits = count($attachmentsHits);
	if($totalAttachmentsHits > 0)
	{
		//WHERE
		$whereQuery = "";
		for ($i=0; $i<$totalAttachmentsHits; $i++){
		    $dataId = $attachmentsHits[$i]->id != null ? $attachmentsHits[$i]->id : $attachmentsHits[$i][id];
			if($dataId != null)
			{
				$whereQuery.=" t.id=".$dataId;
				//pas de 'OR' pour le dernier élément
				if($i < $totalAttachmentsHits-1){
					$whereQuery.=" OR";
				}
			}
		}

		$attachments = Pods('attachments', array("where"=>$whereQuery, "orderby"=> "t.id DESC", "limit"=>"-1"));
		
		if($attachments->total_found() > 0)
			return $attachments;
	}

	return false;
}
/**
 * Effectue une recherche selon un type spécifique
 * @param string $searchTerm Terme de la recherche
 * @param string $type Type d'entité (ex : cycle)
 * @param bool $archive Critère "archivé ou non"
 * @param string $beginDate Date de début au format yyymmdd (optionnel)
 * @param string $endDate Date de fin au format yyymmdd (optionnel)
 */
function search_by_type ($searchTerm, $type, $beginDate="", $endDate=""){
    
    //Ne pas oublier les dates de debut et fin TODO AIMED
    $result = array();
    
    if ($type == 'attachment')
    {
        $SearchAttachement = Pods('attachments', array('where' => 'complete_path like \'%'.$searchTerm.'%\'',"limit" => "-1"))->data();
        if ($SearchAttachement != null)
        {
            $result = array_merge($result, $SearchAttachement);
        }
    }
    if ($type == 'cycle')
    {
        $SearchCycles = Pods('cycles', array('where' => 'identifiant like \'%'.$searchTerm.'%\'',"limit" => "-1"))->data();
        if ($SearchCycles != null)
        {
            $result = array_merge($result, $SearchCycles);
        }
        $SearchAvenants = Pods('avenants', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'C\'',"limit" => "-1"))->data();
        
        if ($SearchAvenants != null)
        {
            for ($i=0; $i<count($SearchAvenants); $i++){
                array_push($result, array('id' => $SearchAvenants[$i]->id_cycle));
            }
        }
        
        $SearchCommentaires = Pods('$commentaires', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND pod_cible = \'Cycles\'',"limit" => "-1"))->data();
        
        if ($SearchCommentaires != null)
        {
            for ($i=0; $i<count($SearchCommentaires); $i++){
                array_push($result, array('id' => $SearchCommentaires[$i]->id_pod_cible));
            }
        }
        
        $SearchRemarques = Pods('remarques', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'C\'',"limit" => "-1"))->data();
        
        if ($SearchRemarques != null)
        {
            for ($i=0; $i<count($SearchRemarques); $i++){
                array_push($result, array('id' => $SearchRemarques[$i]->id_entite));
            }
        }
        
        
    }
    if ($type == 'event')
    {
        $SearchEvent = Pods('events', array('where' => 'designation like \'%'.$searchTerm.'%\'',"limit" => "-1"))->data();
        if ($SearchEvent != null)
        {
            $result = array_merge($result, $SearchEvent);
        }
        $SearchAvenants = Pods('avenants', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'E\'',"limit" => "-1"))->data();
        
        if ($SearchAvenants != null)
        {
            for ($i=0; $i<count($SearchAvenants); $i++){
                array_push($result, array('id' => $SearchAvenants[$i]->id_cycle));
            }
        }
        $SearchCommentaires = Pods('$commentaires', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND pod_cible = \'Events\'',"limit" => "-1"))->data();
        
        if ($SearchCommentaires != null)
        {
            for ($i=0; $i<count($SearchCommentaires); $i++){
                array_push($result, array('id' => $SearchCommentaires[$i]->id_pod_cible));
            }
        }
        $SearchRemarques = Pods('remarques', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'E\'',"limit" => "-1"))->data();
        
        if ($SearchRemarques != null)
        {
            for ($i=0; $i<count($SearchRemarques); $i++){
                array_push($result, array('id' => $SearchRemarques[$i]->id_entite));
            }
        }
        
    }
    if ($type == 'document')
    {
        $SearchDoc = Pods('documents', array('where' => 'reference_document like \'%'.$searchTerm.'%\' OR ' . 'designation like \'%'.$searchTerm.'%\' OR ' . 'reference_lot_appartenance like \'%'.$searchTerm.'%\' OR ' . 'reference_extranet like \'%'.$searchTerm.'%\' ' ,"limit" => "-1"))->data();
        if ($SearchDoc != null)
        {
            $result = array_merge($result, $SearchDoc);
        }
        $SearchAvenants = Pods('avenants', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'D\'',"limit" => "-1"))->data();
        
        if ($SearchAvenants != null)
        {
            for ($i=0; $i<count($SearchAvenants); $i++){
                array_push($result, array('id' => $SearchAvenants[$i]->id_cycle));
            }
        }
        $SearchCommentaires = Pods('$commentaires', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND pod_cible = \'Docs\'',"limit" => "-1"))->data();
        
        if ($SearchCommentaires != null)
        {
            for ($i=0; $i<count($SearchCommentaires); $i++){
                array_push($result, array('id' => $SearchCommentaires[$i]->id_pod_cible));
            }
        }
        $SearchRemarques = Pods('remarques', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'D\'',"limit" => "-1"))->data();
        
        if ($SearchRemarques != null)
        {
            for ($i=0; $i<count($SearchRemarques); $i++){
                array_push($result, array('id' => $SearchRemarques[$i]->id_entite));
            }
        }
        
    }
    if ($type == 'promotion')
    {
        $SearchPromo = Pods('promotions', array('where' => 'detail_promotion like \'%'.$searchTerm.'%\' OR ' . 'reference_facturation like \'%'.$searchTerm.'%\' OR ' . 'description like \'%'.$searchTerm.'%\' OR ' . 'reference_extranet like \'%'.$searchTerm.'%\' ' ,"limit" => "-1"))->data();
        if ($SearchPromo != null)
        {
            $result = array_merge($result, $SearchPromo);
        }
        $SearchAvenants = Pods('avenants', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'P\'',"limit" => "-1"))->data();
        
        if ($SearchAvenants != null)
        {
            for ($i=0; $i<count($SearchAvenants); $i++){
                array_push($result, array('id' => $SearchAvenants[$i]->id_cycle));
            }
        }
        $SearchCommentaires = Pods('$commentaires', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND pod_cible = \'Promos\'',"limit" => "-1"))->data();
        
        if ($SearchCommentaires != null)
        {
            for ($i=0; $i<count($SearchCommentaires); $i++){
                array_push($result, array('id' => $SearchCommentaires[$i]->id_pod_cible));
            }
        }
        $SearchRemarques = Pods('remarques', array('where' => 'content like \'%'.$searchTerm.'%\'' . 'AND type_entite = \'P\'',"limit" => "-1"))->data();
        
        if ($SearchRemarques != null)
        {
            for ($i=0; $i<count($SearchRemarques); $i++){
                array_push($result, array('id' => $SearchRemarques[$i]->id_entite));
            }
        }
        
    }
    return $result;
}
/**
 * Récupère l'entité à laquelle ce document est attaché
 * @param string $entityType Type de l'entité (C = Cycles, R = Période, P = Promotion, E = Evènement, D = Document)
 * @param int $entityId ID en base de l'entité
 */
function getAttachmentEntity($entityType, $entityId){
	//Nom du pod à requeter
	$podType = '';
	//Libellé du type d'entité utilisé pour l'affichage
	$displayType = '';
	//Champ du pod à utiliser pour l'affichage du titre de l'entité (designation pour la plupart)
	$podTitleField = '';
	//Url vers la page détail de l'entité
	$urlBeginning = get_option("siteurl")."/";
	
	switch ($entityType) {
		case 'C':
			//Cycle
			$podType = 'cycles';
			$podTitleField = 'identifiant';
			$displayType = 'au cycle';
			$urlBeginning .= "detail-cycle/?id_cycle=";
			
		break;
		
		case 'R':
			//Période
			$podType = 'cycles';
			$podTitleField = 'identifiant';
			$displayType = 'à la période';
			$urlBeginning .= "detail-cycle/?id_cycle=";

		break;
			
		case 'P':
			//Promotion
			$podType = 'promotions';
			$displayType = 'à la promotion';
			$podTitleField = 'reference_extranet'; //TODO AIMED
			$urlBeginning .= "details-promotion-cyclique/?id_promo=";
		break;
			
		case 'E':
			//Evènement
			$podType = 'events';
			$podTitleField = 'designation';
			$displayType = "à l'évènement";
			$urlBeginning .= "details-evenment-cyclique/?id_event=";
		break;
			
		case 'D':
			//Evènement
			$podType = 'documents';
			$podTitleField = 'designation';
			$displayType = 'au document';
			$urlBeginning .= "details-document-cyclique/?id_doc=";
		break;
		
		default:
		break;
	}
	
	$pod = Pods($podType, array());
	$pod->fetch($entityId);
	
	$displayTitle = $pod->display($podTitleField);
	
	$dataArray = array("title"=>$displayTitle, "type"=>$displayType, "url"=>$urlBeginning);
	return $dataArray;
}