<?php 
	$where_to_back = '';
	if (isset($_GET['entite']) && $_GET['entite'] == 'periode') $where_to_back .= '?entite=periode'
?>
<style type="text/css">
.jqx-widget-content {
   border-color: #FFFFFF ;
}
</style>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var ids_json = $("#ids_json").val();
        ids_json=ids_json.split(",");
        $.each(ids_json, function(cle, val) {
        	$('#tree_cycles_'+val).jqxTree({ width:"98%" });
        });

        var source = ['Choisir', 'Cycle',   'Evènement', 'Promotion', 'Document'];
        $('#new_item').jqxComboBox({ dropDownHeight: 150, selectedIndex: 0, source: source, width:'100px', height:'22px' });

    	$("#new_item").on('change', function(event) {
    		var args = event.args;
    		if (args) {
    			var index = args.index;
    			switch(index) {
    				case 0:
    					break;
    				case 1:
    					window.location = '<?php echo get_permalink(get_page_by_path('nouveau-cycle')) . '?entite=cycle' ?>';
    					break;
    				case 2:
    					window.location = '<?php echo get_permalink(get_page_by_path('nouvel-evenement')) . $where_to_back?>';
    					break;
    				case 3:
    					window.location = '<?php echo get_permalink(get_page_by_path('nouvelle-promotion')) . $where_to_back?>';
    					break;
    				case 4:
    					window.location = '<?php echo get_permalink(get_page_by_path('nouveau-document')) . $where_to_back?>';
    					break;
    			}
    		}
    	});
            
        $("#filtre_radio_all").jqxRadioButton({ width: 58, height: 25 });
        $("#filtre_radio_events").jqxRadioButton({ width: 100, height: 25 });
        $("#filtre_radio_docs").jqxRadioButton({ width: 100, height: 25 });
        $("#filtre_radio_promos").jqxRadioButton({ width: 100, height: 25 });

    	var filtre = '<?php echo isset($_GET['filtre']) ? $_GET['filtre'] : 'all' ?>';
    	switch(filtre) {
    		case 'all':
    			$("#filtre_radio_all").jqxRadioButton('check');
    			break;
    		case 'events':
    			$("#filtre_radio_events").jqxRadioButton('check');
    			break;
    		case 'docs':
    			$("#filtre_radio_docs").jqxRadioButton('check');
    			break;
    		case 'promos':
    			$("#filtre_radio_promos").jqxRadioButton('check');
    			break;
    	}

        
        
        $('#filtre_radio_all').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('cycles')).'/?filtre=all'?>';
        }); 
        $('#filtre_radio_events').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('cycles')) . '/?filtre=events'?>';  
        }); 
        $('#filtre_radio_docs').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('cycles')) . '/?filtre=docs'?>';
        }); 
        $('#filtre_radio_promos').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('cycles')) . '/?filtre=promos'?>';
        }); 


        $("#close_all").jqxButton({ width: '120', height: '25'});
        /*$("#open_all").jqxButton({ width: '120', height: '25'});
                
        $('.open_all').unbind('click').click(function() {
    		$('.tree_cycles').jqxTree('expandAll');
    	});*/

    	$('.close_all').unbind('click').click(function() {
    		$.each(ids_json, function(cle, val) {
    			$("#dv_cycle_"+val).jqxTree('collapseAll');
            	$('#tree_cycles_'+val).jqxTree('collapseAll');
            	$("#dv_cycle_"+val).hide();
            });
    	});


    	$('#mainSplitter').jqxSplitter({ orientation: 'horizontal', panels: [{ size: 500 }, { size: 0}] });

	});
</script>

<?php 
require_once 'include-functions.php';

// recherche du groupe le plus fort d'un user dans la liste des groups auquels il appartient
// le groupe le plus fort se trouve dans $high_level_group
$high_level_group = get_high_level_group_from_current_user();

$where_temp = '';
if (isset($_GET['entite']) && $_GET['entite'] == 'periode') $where_temp .= ' AND permalink like "periodes%"';
else $where_temp .= ' AND t.permalink like "cycles%"';

// N'afficher que les cycles non archivés et visibles par le groupe dont le user courant fait partie
$where_extension = "";
if ($high_level_group == "StanHome_CellulePAC" or $high_level_group == "StanHome_ComitePAC_Work") {
	$where_extension = "";
}
if ($high_level_group == "StanHome_Presta") {
	$where_extension = " and diffusion_level >= 10 and publie = 1";	
}
if ($high_level_group == "StanHome_DR") {
	$where_extension = " and (diffusion_level = 30 or diffusion_level = 40) and publie = 1";
}
if ($high_level_group == "StanHome_DD") {
	$where_extension = " and diffusion_level = 40 and publie = 1";
}
$cycles = Pods("cycles", array("where"=>"archive = 0".$where_extension . $where_temp, "orderby" => "t.date_debut", "limit" => "-1"));
$total = $cycles->total();

?>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$("span[id]").bind('click', function() {
			var id = $(this).attr('id').substring(14);
			if(!$("#li_cycle_"+id).hasClass('done')) {
				$("#dv_cycle_"+id).show();
				$(".prev_loader").show();
				$.ajax({
					cache: true,
					url: '/sous-elements/',
					type: 'get',
					data: 'cycle_id='+id+'&where_extension=<?php echo $where_extension?>&filtre='+$("#filtre_load").val(),
					success: function(html) {
						$("#ul_cycle_"+id).append(html);
						$("#li_cycle_"+id).addClass('done');
						$(".prev_loader").hide();
						$("#dv_cycle_"+id).jqxTree({ width: "95%" });
					}
				});
			} else {
				if($("#dv_cycle_"+id).css('display')=='none') $("#dv_cycle_"+id).show();
				else $("#dv_cycle_"+id).hide();
			}
		});
	});
</script>

<div class="frame btn_grp">
<div class="btn_group">
	<div class="btn_group1">
		<input type="hidden" id="title_page" value="<?php if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Périodes'?>">
		
		<?php echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]<p>Ajouter</p><p id="new_item"></p>[/groups_member]' ); ?>
		
		<p id="filtre_radio_all">Tous</p>
		<p id="filtre_radio_events">Evènements</p>
		<p id="filtre_radio_docs">Documents</p>
		<p id="filtre_radio_promos">Promotions</p>
	
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<!-- <input type="button" value="Ouvrir tout" class="open_all" id="open_all">-->
		<div class="clearfix"></div>
	</div>
</div>
</div>
<div id="mainSplitter" style="border-radius: 10px; border-color: #AAAAAA;" >	
<div class="frame" style="border:none;">

<div class="splitter-panel" style="overflow: auto;">
<?php if ($total > 0):?>
	<h3 class="ttl">Voir le détail</h3>
		<?php
			$ids_json = array();
			$intercale_cycle_color = 0; 
			while ($cycles->fetch()) { 
				$cycle_id = $cycles->field('id');
				if ($intercale_cycle_color % 2 == 0) {
					$li_cycle_bg = "#e8e8e8";
				} else {
					$li_cycle_bg = "transparent";
				}
				$intercale_cycle_color++;
				$ids_json[] = $cycle_id;
		?>
		<div id="tree_cycles_<?php echo $cycle_id?>">
		<ul style="width:100%;">
		<li id="li_cycle_<?php echo $cycle_id?>">
			<div style="background-color:<?php echo $li_cycle_bg; ?>; padding:5px; width:100%;">
			<?php display_binary_icons($cycles);
				  display_diffusion_icons($cycles);
				  $cycle_periode_url = get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$cycles->field('id');
			?>
				<input type="image" src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>"onclick="previsualisation_entite('<?php echo $cycle_periode_url?>')" title="Prévisualisation" alt="Prévisualisation"/>
				
				<a <?php if ($cycles->display('lancement_chaud') == 'Yes'):?> style="color: red;" <?php endif;?>  href="<?php echo $cycle_periode_url?>"><?php echo $cycles->display('identifiant')?> : 
				se déroule du <?php echo php2html_date_dd_mm_yyyy($cycles->display('date_debut')) ?> au  <?php  echo php2html_date_dd_mm_yyyy($cycles->display('date_fin')) ?> : diffusion aux DR le <?php  echo php2html_date_dd_mm_yyyy($cycles->display('diffusion_dr')) ?>
				et aux DD le <?php  echo php2html_date_dd_mm_yyyy($cycles->display('diffusion_dd')) ?></a>
			</div>
			<ul><li class="startTree" style="display: none">xdx</li></ul>
		</li>
		</ul>
		</div>
		<div id="dv_cycle_<?php echo $cycle_id?>" style="padding-left: 25px;">
		<ul id="ul_cycle_<?php echo $cycle_id?>"><li class="startTree" style="display: none">xdx</li></ul>
		</div>
			<?php } ?>
<?php else:?>
	<h4>
		<?php 
			if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Pas de périodes enregistrées';
			else echo 'Pas de cycles enregistrés';	
		?>
	</h4>
<?php endif;?>
</div>
</div>
<div class="splitter-panel" id="secondsplit" style="overflow: auto;position: relative;z-index:200;"></div>
</div>

<input type="hidden" id="filtre_load" value="<?php echo $_GET['filtre']?>"/>
<?php if ($total > 0):?>
<input type="hidden" id="ids_json" value='<?php echo implode(',', $ids_json)?>'/>
<?php endif;?>
<div class="prev_loader">
	<img src="<?php bloginfo('template_directory'); echo '/images/ajax-loader.gif' ?>">
</div>