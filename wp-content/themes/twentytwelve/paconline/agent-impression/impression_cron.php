<?php
ini_set('allow_url_include', 'on');
ini_set("max_execution_time", 600); 
ini_set("memory_limit","256M");
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/

require_once get_template_directory().'/paconline/logs/log-helper.php';
require_once dirname(dirname(__FILE__)) . '/include-functions.php';

/**
 * Fonction "Cron" qui génére les cycles/périodes 
 * et leurs entités attachées 'documents, évents et promotions)
 * en PDF
 */
function sh_do_impression($manuel=false, $items=array()) {
	$log_path_file = get_template_directory().'/log/impression/system.log';
	$pdfs_to_join_to_mail = array();
	if ($manuel) {
		$user = wp_get_current_user();
		$user = $user->user_login;
		$content = PHP_EOL . PHP_EOL."###########################################################################" . PHP_EOL . PHP_EOL;
		sh_write_to_log($content, $log_path_file);
		$content = "Impression manuelle commence lancée par: $user";
		sh_write_to_log($content, $log_path_file);
		if (count($items) == 0) {
			$content = "Aucun(e) cycle/période selectionnée en mode manuel! Impression quitée.";
			sh_write_to_log($content, $log_path_file);
			return false;
		} else {
			$items_ids = implode(', ', $items);
			$content = "Les cycles/périodes à imprimer sont: $items_ids";
			sh_write_to_log($content, $log_path_file);
			foreach ($items as $item) {
				$content = "Impression du cycle (de la période): $item";
				sh_write_to_log($content, $log_path_file);
				$print_url = get_option("siteurl")."/detail-cycle-print/?id_cycle=".$item;
				$content_pdf = file_get_contents($print_url);
				echo "$content_pdf<br/>";
				$content_pdf = str_replace('€', '&euro;', $content_pdf);
				$doc = new DOMDocument();
				$doc->loadHTML(utf8_decode($content_pdf));
				$content_pdf = $doc->saveHTML();
				require_once get_template_directory() . '/paconline/html2pdf_v4.03/html2pdf.class.php';
				$cycle = Pods('cycles', array('where' => "id=$item"));
				$cycle->fetch();
				$identifiant = $cycle->field('identifiant');
				$pdfName = WP_CONTENT_DIR . "/uploads/pdfs/" . $identifiant.'.pdf';
				$pdfs_to_join_to_mail[] = $pdfName;
				$html2pdf=new HTML2PDF("P","A4","fr");
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content_pdf);
				$html2pdf->Output($pdfName, "F");
				reset_flag_impression($item);
				$content = "Flag d'impression est remis à 0";
				sh_write_to_log($content, $log_path_file);
			}
			$content = "Fin de l'impression manuelle";
			sh_write_to_log($content, $log_path_file);
		}
	} else {
		$content = "Impression automatique (par cron) commence";
		sh_write_to_log($content, $log_path_file);
		###recuperation de tous les cycles et périodes à imprimer en pdf###
		$pod_cycles = Pods('cycles', array('where' => 'a_imprimer=1'));
		if ($pod_cycles->total() > 0) {
			while ($pod_cycles->fetch()) {
				$content = "Impression du cycle (de la période): " . $pod_cycles->field('id');
				sh_write_to_log($content, $log_path_file);
				$content = "Url associée: " . get_permalink(get_page_by_path('detail-cycle-print')) . "?id_cycle=".$pod_cycles->field('id');
				sh_write_to_log($content, $log_path_file);
				include get_permalink(get_page_by_path('detail-cycle-print')) . "?id_cycle=".$pod_cycles->field('id');
				$pdfs_to_join_to_mail[] = WP_CONTENT_DIR . "/uploads/pdfs/" . 'cycle_'.$item.'.pdf';
				reset_flag_impression($pod_cycles->field('id'));
				$content = "Flag d'impression est remis à 0";
				sh_write_to_log($content, $log_path_file);
			}
		} else {
			$content = 'Pas de cycles/périodes à imprimer en mode automatique (via cron)';
			sh_write_to_log($content, $log_path_file);
		}
		$content = "Fin de l'impression automatique";
		sh_write_to_log($content, $log_path_file);
	}
	if (count($pdfs_to_join_to_mail) > 0) {
		$content = "Création du mail à envoyer à la cellule PAC, contenant les fichiers joints " . implode(', ', $pdfs_to_join_to_mail);
		sh_write_to_log($content, $log_path_file);
		$mail_model = Pods('mails', array('where' => 'agents=\'impression\''));
		if ($mail_model->total() > 0) {
			while ($mail_model->fetch()) {
				$mail_pac = new mailPac($mail_model->field('objet'), $mail_model->field('contenu'));
				$group = new Groups_Group(1);
				$group = $group->read_by_name('StanHome_CellulePAC');
				$group_id = $group->group_id;
				$mail_pac->addGroupToMailList($group_id);
				$mail_pac->sendMailToAddedGroups($log_path_file, $pdfs_to_join_to_mail);
				$content = "Envoi d'email à la cellule PAC";
				sh_write_to_log($content, $log_path_file);
			}
		}
		
	}
	$content = PHP_EOL . PHP_EOL."###########################################################################" . PHP_EOL . PHP_EOL;
	sh_write_to_log($content, $log_path_file);
}

/**
 * Fonction pour changer le flag a_imprimer à 0 quand le(a) cycle(période) est bien imprimé
 */
function reset_flag_impression($id_cycle) {
	$pod_cycles = Pods('cycles', array('where' => "id=$id_cycle"));
	$data = array('a_imprimer' => 0);
	$edited_cycle_id = $pod_cycles->save($data);
	//return $edited_cycle_id;
}

add_action('sh_impression_agent', 'sh_do_impression');