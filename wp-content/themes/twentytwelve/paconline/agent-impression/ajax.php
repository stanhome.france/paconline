<?php
require_once get_template_directory().'/paconline/agent-impression/impression_cron.php';

/*
 * Lancer l'agent d'impression des cycles/périodes manuellement
 */
if ($_GET['ajax'] == 1) {
	$ids_cycles_periodes = explode(',', $_GET['items']);
	array_pop($ids_cycles_periodes);
	sh_do_impression(true, $ids_cycles_periodes);
}