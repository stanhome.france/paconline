<?php
require_once 'include-functions.php';

if (!isset($_GET['pagenum']) || !isset($_GET['pagesize'])) {
	echo '<h3>Ressource non disponible</h3>';
	exit();
}

$pagenum = (int)$_GET['pagenum'] + 1;
$pagesize = $_GET['pagesize'];

$articles_temp = Pods('articles', array('limit' => "-1"));
$totalRows = $articles_temp->total();

$where = "";

if (isset($_GET['filterscount'])) {
	$filterscount = $_GET['filterscount'];
	if ($filterscount > 0) {
		$where .= " (";
		$tmpdatafield = "";
		$tmpfilteroperator = "";
		for ($i=0; $i < $filterscount; $i++) {
			$filtervalue = $_GET["filtervalue" . $i];
			$filtercondition = $_GET["filtercondition" . $i];
			$filterdatafield = $_GET["filterdatafield" . $i];
			$filteroperator = $_GET["filteroperator" . $i];
			
			if ($tmpdatafield == "") {
				$tmpdatafield = $filterdatafield;
			} else if ($tmpdatafield <> $filterdatafield) {
				$where .= ")AND(";
			} else if ($tmpdatafield == $filterdatafield) {
				if ($tmpfilteroperator == 0) {
					$where .= " AND ";
				} else {
					$where .= " OR ";	
				}
			}
			switch($filtercondition) {
				case "CONTAINS":
					$where .= " " . $filterdatafield . " LIKE '%" . $filtervalue ."%'";
					break;
				case "DOES_NOT_CONTAIN":
					$where .= " " . $filterdatafield . " NOT LIKE '%" . $filtervalue ."%'";
					break;
				case "EQUAL":
					$where .= " " . $filterdatafield . " = '" . $filtervalue ."'";
					break;
				case "NOT_EQUAL":
					$where .= " " . $filterdatafield . " <> '" . $filtervalue ."'";
					break;
				case "GREATER_THAN":
					$where .= " " . $filterdatafield . " > '" . $filtervalue ."'";
					break;
				case "LESS_THAN":
					$where .= " " . $filterdatafield . " < '" . $filtervalue ."'";
					break;
				case "GREATER_THAN_OR_EQUAL":
					$where .= " " . $filterdatafield . " >= '" . $filtervalue ."'";
					break;
				case "LESS_THAN_OR_EQUAL":
					$where .= " " . $filterdatafield . " <= '" . $filtervalue ."'";
					break;
				case "STARTS_WITH":
					$where .= " " . $filterdatafield . " LIKE '" . $filtervalue ."%'";
					break;
				case "ENDS_WITH":
					$where .= " " . $filterdatafield . " LIKE '%" . $filtervalue ."'";
					break;
			}
			if ($i == $filterscount - 1) {
				$where .= ")";
			}
			$tmpfilteroperator = $filteroperator;
			$tmpdatafield = $filterdatafield;	
		}
	}
}

$articles_aff = Pods('articles', array('orderby' => "t.reference ASC", 'where' => $where, 'limit' => "-1"));
$total_aff = $articles_aff->total();


$articles = Pods('articles', array('orderby' => "t.reference ASC", 'limit' => $pagesize, 'page' => $pagenum, 'where' => $where));
$total = $articles->total();

if ($total > 0) {
	while ($articles->fetch()) {
		$data[] = array(
			'reference' => $articles->display('reference'),
			'designation' => $articles->display('designation'),
			'prix_ht' => pod2html_prix($articles->display('prix_ht')) . '€',
			'prix_ttc' => pod2html_prix($articles->display('prix_ttc')) . '€',
			'action_edit' => '<a href="'. get_permalink(get_page_by_path('edit-article')) . '?id_article=' . $articles->field('id') . '">Editer</a>',
			'action_delete' => '<a href="" id="delete_article_'. $articles->field('id') .'" onclick="delete_article('. $articles->field('id') .');return false;">Supprimer</a>'
		);
	}

	$result[] = array(
		'TotalRows' => $total_aff,
		'Rows' => $data
	);
	
	echo  json_encode($result);
}