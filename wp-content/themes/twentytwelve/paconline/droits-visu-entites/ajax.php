<?php

if ($_GET['ajax'] == 1) {
	$type_entie = $_GET['type_entie'];
	$id_entite = $_GET['id_entite'];
	$diffusion_level = $_GET['diffusion_level'];
	
	$result = array();
	$result['success'] = 0;
	$result['new_title'] = '';
	switch ($type_entie) {
		case 'c':
			$pod = Pods('cycles', $id_entite);
			$data = array('diffusion_level' => $diffusion_level);
			if ($pod->save($data)) {
				$result['success'] = 1;
				$pod = Pods('cycles', $id_entite);
				$result['new_title'] = $pod->display('identifiant') . ' visible par ' . droitVisuToString($pod);
			}
			break;
		case 'p':
			$pod = Pods('promotions', $id_entite);
			$data = array('diffusion_level' => $diffusion_level);
			if ($pod->save($data)) {
				$result['success'] = 1;
				$pod = Pods('promotions', $id_entite);
				$result['new_title'] = stripslashes($pod->display('reference_extranet')).' '.stripslashes($pod->display('description')) . ' visible par ' . droitVisuToString($pod);
			}
			break;
		case 'e':
			$pod = Pods('events', $id_entite);
			$data = array('diffusion_level' => $diffusion_level);
			if ($pod->save($data)) {
				$result['success'] = 1;
				$pod = Pods('events', $id_entite);
				$result['new_title'] = stripslashes($pod->display('designation')) . ' visible par ' . droitVisuToString($pod);
			}
			break;
		case 'd':
			$pod = Pods('documents', $id_entite);
			$data = array('diffusion_level' => $diffusion_level);
			if ($pod->save($data)) {
				$result['success'] = 1;
				$pod = Pods('documents', $id_entite);
				$result['new_title'] = stripslashes($pod->display('designation')) . ' visible par ' . droitVisuToString($pod);
			}
			break;
	}
	
	echo json_encode($result);
	
	exit();
}