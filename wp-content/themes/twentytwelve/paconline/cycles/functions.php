<?php
/**
 * Récupérer la liste des numéros de cycles
 */
function get_liste_numeros_cycles() {
	$liste = array();
	if (get_option('_liste_cycles')) {
		$liste = unserialize(get_option('_liste_cycles'));
	}
	return $liste;
}

/**
 * Récupérer la liste des numéros de periodes
 */
function get_liste_numeros_periodes() {
	$liste = array();
	if (get_option('_liste_periodes')) {
		$liste = unserialize(get_option('_liste_periodes'));
	}
	return $liste;
}

/**
 * Récupérer la date de début à partir du numéro de la semaine de début et l'année
 */
function get_date_debut_par_semaine($semaine, $annee) {
	$debut_semaine = new DateTime();
	$debut_semaine->setISODate($annee, $semaine);
	$debut_semaine = $debut_semaine->format('d/m/Y');
	return $debut_semaine;
}

/**
 * Récupérer la date de fin à partir du numéro de la semaine de fin et l'année
 */
function get_date_fin_par_semaine($semaine, $annee) {
	$debut_semaine_debut = new DateTime();
	$debut_semaine_debut->setISODate($annee, $semaine);
	$debut_semaine_debut = $debut_semaine_debut->format('d/m/Y');
	$temp = explode('/', $debut_semaine_debut);
	$fin_semaine = new DateTime($temp[2] . '-' . $temp[1] . '-' .$temp[0]);
	$fin_semaine->modify('+6 day');
	$fin_semaine = $fin_semaine->format('d/m/Y');
	echo $fin_semaine;
}

/**
 *  Convertir la date du format JS à celui de MySQL
 */
function js2mysql_date($js_date) {
	if ($js_date == '') return null;
	$js_array = explode('/', $js_date);
	$mysql_date = new DateTime($js_array[2] . '-' . $js_array[1] . '-' .$js_array[0]);
	$mysql_date = $mysql_date->format('Y-m-d');
	return $mysql_date;
}

/**
 * Formatter les dates provenant du POD CMS pour les afficher
 */
function pod2html_date($pod_date) {
	if ($pod_date == '0000-00-00') return null;
	$pod_array = explode('-', $pod_date);
	$html_date = $pod_array[2] . '/' . $pod_array[1] . '/' . $pod_array[0];
	return $html_date;
}

/**
 * Formatter les dates provenant du PHP pour les afficher
 */
function php2html_date($php_date) {
	if ($php_date == '0000-00-00') return null;
	$php_array = explode('-', $php_date);
	$html_date = $php_array[2] . '/' . $php_array[1] . '/' . $php_array[0];
	return $html_date;
}

/**
 * Formatter les dates provenant du PHP pour les afficher
 */
function php2html_date_dd_mm_yyyy($php_date) {
    if ($php_date == '0000-00-00') return null;
    $php_array = explode('/', $php_date);
    $html_date = $php_array[1] . '/' . $php_array[0] . '/' . $php_array[2];
    return $html_date;
}

/**
 * Formatter les dates provenant du POD CMS pour les afficher
 */
function pod2js_date($pod_date) {
	if ($pod_date == '0000-00-00') return null;
	$pod_array = explode('-', $pod_date);
	$html_date = $pod_array[2] . '/' . $pod_array[1] . '/' . $pod_array[0];
	return $html_date;
}

/**
 * Récupérer liste de semaines entre semaine début et semaine fin
 */
function get_liste_semaines($semaine_debut, $annee, $semaine_fin, $annee_fin) {
	$result= "S$semaine_debut $annee,";
	if ($semaine_debut > $semaine_fin) {
		for ($i = $semaine_debut+1; $i <= 52; $i++) {
			$result .= "S$i $annee,";
		}
		for ($i = 1; $i <= $semaine_fin; $i++) {
			$result .= ($i == $semaine_fin) ? "S$i $annee_fin" : "S$i $annee_fin,";
		}
	} else {
		for ($i = $semaine_debut+1; $i <= $semaine_fin; $i++) {
			$result .= ($i == $semaine_fin) ? "S$i $annee" : "S$i $annee,";
		}
	}
	return $result;
}


