<?php
require_once dirname(dirname(__FILE__)) . '/include-functions.php';

define("DIFFUSION_LOG_PATH", get_template_directory().'/log/diffusion.log');

/*
 * Récupération date debut pour cycle
 */
if ($_GET['ajax'] == 1) {
	if (isset($_GET['semaine']) && $_GET['semaine'] != '' && isset($_GET['annee']) && $_GET['annee'] != '') {
		$semaine = (int)$_GET['semaine'];
		$annee = (int)$_GET['annee'];
		if (!is_numeric($semaine) || !is_numeric($annee))
			$debut_semaine = -1;
		else 
			$debut_semaine = getDateDebut($annee, $semaine);
	}
	echo $debut_semaine;
}


/*
 * Récupération date fin pour cycle
 */
if ($_GET['ajax'] == 2) {
	if (isset($_GET['semaine']) && $_GET['semaine'] != '' && isset($_GET['annee']) && $_GET['annee'] != '') {
		$semaine = (int)$_GET['semaine'];
		$annee = (int)$_GET['annee'];
		if (!is_numeric($semaine) || !is_numeric($annee))
			$fin_semaine = -1;
		else 
			$fin_semaine = getDateFin($annee, $semaine);
	}
	echo $fin_semaine;
}

/*
 *  Ajout d'un nouveau cycle
 */
if ($_GET['ajax'] == 3) {
	##eviter les doublons par identifiant##
	if (entiteExiste('cycles', $_GET['identifiant'])) {
		echo 'cycle_existe';
		exit();
	}
	#######################################
	
	$cycles = Pods('cycles');
	$data = array(
		'cycle_numero' => $_GET['numero'],
		'permalink' => $_GET['cycle_ou_periode'],
		'semaine_debut' => $_GET['semaine_debut'],
		'date_debut' => js2mysql_date($_GET['date_debut']),
		'semaine_fin' => $_GET['semaine_fin'],
		'date_fin' => js2mysql_date($_GET['date_fin']),
		'identifiant' => $_GET['identifiant'],
		'diffusion_dr' => js2mysql_date($_GET['diffusion_dr']),
		'diffusion_dd' => js2mysql_date($_GET['diffusion_dd']),
		'date_debut_commerciale' => js2mysql_date($_GET['debut_commerciale']),
		'date_fin_commerciale' => js2mysql_date($_GET['fin_commerciale']),
		'annee' => $_GET['annee'],
		'annee_fin' => $_GET['annee_fin'],
		'annee_du_cycle' => $_GET['annee_du_cycle'],
		'action_info' => $_GET['action_info'],
		//'lancement_chaud' => $_GET['lancement_chaud'],
		'publie' => $_GET['publie'],
		//'date_lancement_chaud_dd' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
		//'date_lancement_chaud_dr' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : ''
	);
	
	$new_cycle_id = $cycles->add($data);
	
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $new_cycle_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	
	echo $new_cycle_id;
}

/*
 *  Edition d'un cycle
 */
if ($_GET['ajax'] == 4) {
	if (isset($_GET['id_cycle']) && $_GET['id_cycle'] > 0) {
		##eviter les doublons par identifiant##
		if (entiteExiste('cycles', $_GET['identifiant'], true, $_GET['id_cycle'])) {
			echo 'cycle_existe';
			exit();
		}
		#######################################
		$cycles = Pods('cycles'); 
		
		$data = array(
			'annee' => $_GET['annee'],
			'annee_fin' => $_GET['annee_fin'],
			'semaine_debut' => $_GET['semaine_debut'],
			'semaine_fin' => $_GET['semaine_fin'],
			'annee_du_cycle' => $_GET['annee_cycle'],
			'cycle_numero' => $_GET['numero'],
			'date_debut' => js2mysql_date($_GET['date_debut']),
			'date_fin' => js2mysql_date($_GET['date_fin']),
			'identifiant' => $_GET['identifiant'],
			'diffusion_dr' => js2mysql_date($_GET['diffusion_dr']),
			'diffusion_dd' => js2mysql_date($_GET['diffusion_dd']),
			'date_debut_commerciale' => js2mysql_date($_GET['debut_commerciale']),
			'date_fin_commerciale' => js2mysql_date($_GET['fin_commerciale']),
			'publie' => $_GET['publie'],
			'action_info' => $_GET['action_info'],
			//'lancement_chaud' => $_GET['lancement_chaud'],
			//'date_lancement_chaud_dd' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
			//'date_lancement_chaud_dr' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : ''
			
		);
		$edited_cycle_id = $cycles->save($data, null, $_GET['id_cycle']);
		
		if (isset($_GET['files_to_attach'])) {
			$entity_id = $edited_cycle_id;
			require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
		}
	
		echo $edited_cycle_id;
	}
}

/*
 * Suppression d'un cycle
 */
if ($_GET['ajax'] == 5) {
	if (!isset($_GET['id_cycle'])) {
		echo -1;
		exit();
	}
	$id_cycle = $_GET['id_cycle'];
	$cycle_pod = Pods('cycles', $id_cycle);
	$remarques_pod = Pods('remarques', array('where' => "id_entite=$id_cycle AND type_entite='C'"));
	if ($cycle_pod->delete()) {
		while ($remarques_pod->fetch()) {
			$pod_temp = Pods('remarques', $remarques_pod->field('id'));
			$pod_temp->delete();
		}
		echo 1;
	}
	else 
		echo 0;
}

/*
 * Calcul liste des semaines entre semaine début et semaine fin
 * cette information n'est pas enregistrée dans la base de données (pod cycles)
 * car on peut la déduire à partir de la semaine de début et la semaine de fin
 */
if ($_GET['ajax'] == 6) {
	$semaine_debut = (int)$_GET['semaine_debut'];
	$semaine_fin = (int)$_GET['semaine_fin'];
	$annee = $_GET['annee'];
	$annee_fin = $_GET['annee_fin'];
	echo get_liste_semaines($semaine_debut, $annee, $semaine_fin, $annee_fin);
}


/*
 * ACtivation ou désactivation de l'agent d'impresion pour un(e) cycle/période
 */
if ($_GET['ajax'] == 8) {
	if (entiteExiste('cycles', $_GET['id_cycle_periode'], true, $_GET['id_cycle_periode'])) {
	    echo 'cycles';
	    exit();
	}
	$id_cycle_periode = $_GET['id_cycle_periode'];
	$pod_cycles = Pods('cycles', $id_cycle_periode);
	$is_on = $_GET['is_on'];
	$is_on = $is_on * (-1) + 1;
	$data = array('a_imprimer' => $is_on); 
	$edited_cycle_id = $pod_cycles->save($data,null,$id_cycle_periode,null);
	echo $is_on;
}
/*
 * Traitement sur l'action de diffusion
 */
if ($_GET['ajax'] == 7) {
	if (!isset($_GET['id_cycle'])) {
		echo -1;
		exit();
	}
	$id_cycle = $_GET['id_cycle'];
	$cycle_pod = Pods('cycles');
	$cycle_pod->save("diffusion", "1", $id_cycle);
	$cycle_pod->save("diffusion_level", 10, $id_cycle);

	$cycle_pod = Pods('cycles', $id_cycle);
	if ($cycle_pod->fetch()) {
		$type_entity = $cycle_pod->display('permalink');
		if ($type_entity == "cycles") $type_entity = "C";
	}
	
	$entite_designation = appendToSubject($type_entity, $id_cycle);
	$mail_model = Pods('mails', array('where' => 'agents=\'diffusion\''));
	if ($mail_model->fetch()) {
		$subject = $mail_model->field('objet') .': ' . $entite_designation;
		$contenu = $mail_model->field('contenu');
		$subject = prependToSubjectDiffusion($id_cycle). " : " . stripslashes($mail_model->field('objet'));
		$contenu = prependToSubjectDiffusion($id_cycle) . '<br/>' . stripslashes($mail_model->field('contenu'));

		$mail_pac = new mailPac($subject, $contenu);
		$group = new Groups_Group(1);
		$group1 = $group->read_by_name('StanHome_CellulePAC');
		$group_id = $group1->group_id;
		$mail_pac->addGroupToMailList($group_id);
		$group1 = $group->read_by_name('StanHome_ComitePAC');
		$group_id = $group1->group_id;
		$mail_pac->addGroupToMailList($group_id);
		$group1 = $group->read_by_name('StanHome_Presta');
		$group_id = $group1->group_id;
		$mail_pac->addGroupToMailList($group_id);
		$mail_pac->sendMailToAddedGroups(DIFFUSION_LOG_PATH, array());
	}
	
	$asso_cycle_docs = Pods('cycles_docs', array("where" => "id_cycle = ".$id_cycle));
	while ($asso_cycle_docs->fetch()) {
		$id_doc =  $asso_cycle_docs->display("id_doc");
		$check_doc = Pods("documents", $id_doc);
		if ($check_doc->fetch()) {
			if ($check_doc->display("diffusion_level") == 0) {
				$docs = Pods("documents");
				$docs->save("diffusion", "1", $id_doc);
				$docs->save("diffusion_level", 10, $id_doc);
			}
		}
	}

	$asso_cycle_events = Pods('cycles_events', array("where" => "id_cycle = ".$id_cycle));
	while ($asso_cycle_events->fetch()) {
		$id_event =  $asso_cycle_events->display("id_event");
		$check_event = Pods("events", $id_event);
		if ($check_event->fetch()) {
			if ($check_event->display("diffusion_level") == 0) {
				$events = Pods("events");
				$events->save("diffusion", "1", $id_event);
				$events->save("diffusion_level", 10, $id_event);
			}
		}
	}

	$asso_cycle_promos = Pods('cycles_promos', array("where" => "id_cycle = ".$id_cycle));
	while ($asso_cycle_promos->fetch()) {
		$id_promo =  $asso_cycle_promos->display("id_promo");
		$check_promo = Pods("promotions", $id_promo);
		if ($check_promo->fetch()) {
			if ($check_promo->display("diffusion_level") == 0) {
				$promos = Pods("promotions");
				$promos->save("diffusion", "1", $id_promo);
				$promos->save("diffusion_level", 10, $id_promo);
			}
		}
	}
	echo "1";
}

