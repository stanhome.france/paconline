<?php
/**
 * Indexage global journalier
 */
//Log
require_once get_template_directory().'/paconline/logs/log-helper.php';
define("INDEXATION_LOG_PATH", get_template_directory().'/log/indexation.log');

function sh_do_index(){
	
	$index_path = get_template_directory().'/../../plugins/search/lucene/index/stanhome_index';
	$searchObject = new sh_search($index_path);
	
	//Ouvre ou créé l'index
	try {
		$index = $searchObject->open_index();
	} catch (Exception $e) {
		$searchObject->create_index();
		$index = $searchObject->open_index();
	}
	
	sh_write_to_log("La tâche planifiée \"Agent d'indexage\" a commencée.", INDEXATION_LOG_PATH);
	
	//Indexage des différentes entités
	$cyclesCount = sh_do_index_cycles($searchObject);
	$eventsCount = sh_do_index_events($searchObject);
	$documentsCount = sh_do_index_documents($searchObject);
	$promotionsCount = sh_do_index_promotions($searchObject);
	
	//Réorganise tous les segments de l'index en un seul segment
	$index->optimize();
	
	sh_write_to_log("La tâche planifiée \"Agent d'indexage\" est terminée : ".$cyclesCount." cycle(s), ".$eventsCount." événements(s), ".$documentsCount." document(s), ".$promotionsCount." promotion(s) indexé(es).", INDEXATION_LOG_PATH);
}

/**
 * Indexe tous les cycles
 */
function sh_do_index_cycles($searchObject){
	//Supprime tous les cycles de l'index
	$searchObject->purge_by_type('cycle');
	
	//Indexe tous les cycles
	$cyclesCount = 0;
	$urlBeginning = get_option("siteurl")."/detail-cycle/?id_cycle=";
	$cyclesToIndex = Pods('cycles', array());
	
	while ($cyclesToIndex->fetch()){
		$title = $cyclesToIndex->display("identifiant");
		$id = $cyclesToIndex->display("id");
		$url = $urlBeginning.$id;
		
		$dateDebutObject = new DateTime($cyclesToIndex->display("date_debut"));
		$dateFinObject = new DateTime($cyclesToIndex->display("date_fin"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = get_content_from_url($url, "content_pac");
		
		//indexe ce cycle
		$searchObject->add_to_index($title, $url, 'cycle', $id, $dateDebut, $dateFin, $content);
		
		$cyclesCount++;
	}
	
	return $cyclesCount;
}

/**
 * Indexe tous les événements
 */
function sh_do_index_events($searchObject){
	//Supprime tous les événements de l'index
	$searchObject->purge_by_type('event');

	//Indexe tous les écvénements
	$eventsCount = 0;
	$urlBeginning = get_option("siteurl")."/details-evenment-cyclique/?id_event=";
	$eventsToIndex = Pods('events', array());

	while ($eventsToIndex->fetch()){
		$title = $eventsToIndex->display("designation");
		$id = $eventsToIndex->display("id");
		$url = $urlBeginning.$id;
		
		$dateDebutObject = new DateTime($eventsToIndex->display("date_debut"));
		$dateFinObject = new DateTime($eventsToIndex->display("date_fin"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = get_content_from_url($url, "content_pac");

		//indexe cet événement
		$searchObject->add_to_index($title, $url, 'event', $id, $dateDebut, $dateFin, $content);

		$eventsCount++;
	}
	
	return $eventsCount;
}

/**
 * Indexe tous les documents
 */
function sh_do_index_documents($searchObject){
	//Supprime tous les documents de l'index
	$searchObject->purge_by_type('document');

	//Indexe tous les documents
	$documentCount = 0;
	$urlBeginning = get_option("siteurl")."/details-document-cyclique/?id_doc=";
	$documentsToIndex = Pods('documents', array());

	while ($documentsToIndex->fetch()){
		$title = $documentsToIndex->display("designation");
		$id = $documentsToIndex->display("id");
		$url = $urlBeginning.$id;
		
		$dateDebutObject = new DateTime($documentsToIndex->display("date_debut_lancement"));
		$dateFinObject = new DateTime($documentsToIndex->display("date_fin_lancement"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = get_content_from_url($url, "content_pac");

		//indexe ce document
		$searchObject->add_to_index($title, $url, 'document', $id, $dateDebut, $dateFin, $content);

		$documentCount++;
	}

	return $documentCount;
}

/**
 * Indexe toutes les promotions
 */
function sh_do_index_promotions($searchObject){
	//Supprime tous les documents de l'index
	$searchObject->purge_by_type('promotion');

	//Indexe toutes les promotions
	$promotionCount = 0;
	$urlBeginning = get_option("siteurl")."/details-promotion-cyclique/?id_promo=";
	$promotionsToIndex = Pods('promotions', array());

	while ($promotionsToIndex->fetch()){
		$title = $promotionsToIndex->display("reference_extranet");
		$id = $promotionsToIndex->display("id");
		$url = $urlBeginning.$id;
		
		$dateDebutObject = new DateTime($promotionsToIndex->display("date_debut_lancement"));
		$dateFinObject = new DateTime($promotionsToIndex->display("date_fin_lancement"));
		$dateDebut = $dateDebutObject->format("Ymd");
		$dateFin = $dateFinObject->format("Ymd");
		
		$content = get_content_from_url($url, "content_pac");
		error_log($content);

		//indexe ce document
		$searchObject->add_to_index($title, $url, 'promotion', $id, $dateDebut, $dateFin, $content);

		$promotionCount++;
	}

	return $promotionCount;
}

//Rend la méthode sh_do_index "appelable" par un CRON
add_action('sh_index_agent', 'sh_do_index');