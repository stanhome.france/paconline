<?php 
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'evenements-des-cycles/ajax.php';
	exit();
}

$annee_en_cours = date('Y');

$options_cible = get_liste_cible();
$options_event = get_liste_event();

?>


<form id="save_new_event" action="" method="get">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Nouvel Evénement</h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Désignation</label></td>
				<td><input type="text" id="event_designation" class="long"/> </td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<?php require_once 'cycle-ou-periode.php';?>
			<tr>
				<td align="right"><label>Type</label></td>
				<td>
					<select id="event_type_list">
						<option value="">Vide...</option>
						<?php if (count($options_event) > 0):?>
							<?php foreach ($options_event as $kle_event => $val_event):?>
						<option value="<?php echo $kle_event?>"><?php echo stripslashes($val_event)?></option>	
							<?php endforeach;?>
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Cycle(s)</label></td>
				<td colspan="2">
					<input type="text" id="event_cycles_correspondants" disabled="disabled"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<div class="espc_drt">
					<label>Cible(s)</label>
						<select id="event_cible_list">
							<option value="">Vide...</option>
							<?php if (count($options_cible) > 0):?>
								<?php foreach ($options_cible as $kle_cible => $val_cible):?>
							<option value="<?php echo $kle_cible?>"><?php echo stripslashes($val_cible)?></option>	
								<?php endforeach;?>
							<?php endif;?>
						</select>
						<div class="clearfix"></div>
						<input type="button" id="add_new_cible_to_event" value="Ajouter >>" class="small">
					</div>
				</td>
				<td>
					<input type="hidden" width="" id="event_cible">
					<input type="text" id="event_cible_label" class="champ_non_editable vlong" />
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><input type="text" id="event_date_debut_pre_lancement"></td>
							<td align="center"><input type="text" id="event_date_fin_pre_lancement"></td>
							<td align="center" class="brd_right"><input type="text" id="event_qte_pre_lancement"></td>
						</tr>
						<tr>
							<th>Lancement</th>
							<td align="center"><input type="text" id="event_date_debut_extranet"> </td>
							<td align="center"><input type="text" id="event_date_fin_extranet"> </td>
							<td  align="center" class="brd_right" style="border-right: 0px;"></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr><td></td><td><label id="info"></label></td></tr>
		</table>
	</div>
</form>


<?php 
$pod_cible = 'Events';

include_once 'comment-bloc.php';

$type_entity = 'E';
include_once('attachments/attachment-doc-creation.php');
?>

<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript">
( function( $ ) {
	
	/*
	 * Les champs dates
	 */
	$("#event_date_debut").datepicker();
	$("#event_date_fin").datepicker();
	$("#event_date_debut_pre_lancement").datepicker();
	$("#event_date_fin_pre_lancement").datepicker();
	$("#event_date_debut_extranet").datepicker();
	$("#event_date_fin_extranet").datepicker();
	$("#event_date_lancement_chaud_dd").datepicker();
	$("#event_date_lancement_chaud_dr").datepicker();

	/*
	 * Les champs ineditables
	 */
	$(".champ_non_editable").attr('disabled', true);

	/*
	 * Récupération date début à partir semaine début
	 * et
	 * Récupération date fiin à partir semaine fin
	 */
	$("#event_semaine_debut").bind('change', function() {
		var annee = $("#event_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=11';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#event_date_debut").val(msg);
				},
				error: function() {
				}
			});
		}
		/*
		 * Calculer les cycles correspondants
		 */
		if($("#event_semaine_fin").val() != '' && $("#event_semaine_debut").val() != '' && $("#event_attach_list").val() != '') {
			var data = 'debut=' + $("#event_semaine_debut").val() + '&fin=' + $("#event_semaine_fin").val() + '&ajax=13';
			data += "&annee=" + $("#event_annee").val() + "&annee_fin=" +$("#event_annee_fin").val();
			data += "&permalink=" + $("#event_attach_list").val();
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#event_cycles_correspondants").val(msg);
				},
				error: function() {
					alert('Erreur');
				}
			});
		}
	});
	
	$("#event_semaine_fin").bind('change', function() {
		var annee = $("#event_annee_fin").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=12';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#event_date_fin").val(msg);
				},
				error: function() {
				}
			});
		}
		/*
		 * Calculer les cycles correspondants
		 */
		if($("#event_semaine_fin").val() != '' && $("#event_semaine_debut").val() != '' && $("#event_attach_list").val() != '') {
			var data = 'debut=' + $("#event_semaine_debut").val() + '&fin=' + $("#event_semaine_fin").val() + '&ajax=13';
			data += "&annee=" + $("#event_annee").val() + "&annee_fin=" +$("#event_annee_fin").val();
			data += "&permalink=" + $("#event_attach_list").val();
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#event_cycles_correspondants").val(msg);
				},
				error: function() {
					alert('Erreur');
				}
			});
		}
	});

	$("#event_attach_list").bind('change', function() {
		if($("#event_semaine_fin").val() != '' && $("#event_semaine_debut").val() != '' && $("#event_attach_list").val() != '') {
			var data = 'debut=' + $("#event_semaine_debut").val() + '&fin=' + $("#event_semaine_fin").val() + '&ajax=13';
			data += "&annee=" + $("#event_annee").val() + "&annee_fin=" +$("#event_annee_fin").val();
			data += "&permalink=" + $("#event_attach_list").val();
			data += "&permalink=" + $("#event_attach_list").val();
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					$("#event_cycles_correspondants").val(msg);
				},
				error: function() {
					alert('Erreur');
				}
			});
		}
	});

	/*
	 * Ajouter multiple cibles 
	 */
	$("#add_new_cible_to_event").bind('click', function() {
		if($("#event_cible_list").val() != '') {
			if($("#event_cible").val() == '') {
				$("#event_cible_label").val(String($("#event_cible_list option:selected").text()));
				$("#event_cible").val(String($("#event_cible_list").val()));
			} else {
				var temp = '';
				temp = $("#event_cible").val();
				if(temp.indexOf($("#event_cible_list").val()) == -1) {
					$("#event_cible").val($("#event_cible").val() + ',' + String($("#event_cible_list").val()));
					$("#event_cible_label").val($("#event_cible_label").val() + ' + ' + String($("#event_cible_list option:selected").text()));
				}
			}
		}
	});
	$("#event_cible_list").change(function() {
		if($("#event_cible_list").val() == '') {
			$("#event_cible_label").val('');
			$("#event_cible").val('');
		}
	});

	/*
	 * Afficher champs dates lancment à chaud pour DD et DR quand 
	 * le boutton radio lancement à chaud est coché
	 */
	$('input[type=radio][name=event_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});

	/*
	 * Submit du formulaire d'ajout d'un nouvel event 
	 * Puis redirection vers page liste cycles
	 */
	$("#save_new_event").submit(function(){
		/*
		 * Les champs obligatoires
		 */
		var empty = false;
		var error_msg = 'Vous devez saisir aussi : \n';
		if($("#event_designation").val() == '') {
			empty = true;
			error_msg += 'La désignation de l\'événement \n';
		}
		if($("#event_semaine_debut").val() == '' || $("#event_date_debut").val() == '' || $("#event_annee").val() == '') {
			empty = true;
			error_msg += 'L\'année,la semaine et la date de début \n';
		}
		if($("#event_semaine_fin").val() == '' || $("#event_date_fin").val() == '' || $("#event_annee_fin").val() == '') {
			empty = true;
			error_msg += 'L\'année,la semaine et la date de fin \n';
		} 
		if($("#event_type_list").val() == '') {
			empty = true;
			error_msg += 'Le type de l\'événement \n';
		}
		if($("#event_cible").val() == '') {
			empty = true;
			error_msg += 'Une ou plusieurs cibles \n';
		}
		if($("#event_date_debut_extranet").val() == '' || $("#event_date_fin_extranet").val() == '') {
			empty = true;
			error_msg += 'Les dates début et fin de lancement \n';
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dd").val() == ''){
			error_msg += 'La date de lancement à chaud DD \n';
			empty = true;
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dr").val() == ''){
			error_msg += 'La date de lancement à chaud DR \n';
			empty = true;
		}
		if(empty) alert(error_msg);
		if(empty) return false;

		var data = '';
		data += 'cycle_ou_periode=' + $("#event_attach_list").val() + '&';
		data += 'annee=' + $("#event_annee").val() + '&';
		data += 'annee_fin=' + $("#event_annee_fin").val() + '&';
		data += 'designation=' + encodeURIComponent($("#event_designation").val()) + '&';
		data += 'semaine_debut=' + $("#event_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#event_semaine_fin").val() + '&';
		data += 'date_debut=' + $("#event_date_debut").val() + '&';
		data += 'date_fin=' + $("#event_date_fin").val() + '&';
		data += 'type=' + $("#event_type_list").val() + '&';
		data += 'cible=' + $("#event_cible").val() + '&';
		data += 'pre_lancement_debut=' + $("#event_date_debut_pre_lancement").val() + '&';
		data += 'pre_lancement_fin=' + $("#event_date_fin_pre_lancement").val() + '&';
		data += 'qte_pre_lancement=' + $("#event_qte_pre_lancement").val() + '&';
		data += 'extranet_debut=' + $("#event_date_debut_extranet").val() + '&';
		data += 'extranet_fin=' + $("#event_date_fin_extranet").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=event_action_info]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=event_publie]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=event_lancement_chaud]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#event_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#event_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=E&';
		}
		data += 'ajax=1';

		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'Référence du document needs to be unique')
					$("#info").html('Référence du document doit etre unique!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				else {
					alert('Evénement  bien ajouté!');
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					window.setTimeout("redirect2DetailEvent("+msg+")", 2000);
				}
			},
			error: function() {
			}
		});
		
		return false;
	});

	/*
	 * Ajout d'un event et rester sur la meme page
	 */
	$("#save_new_next_event").bind('click', function() {
		/*
		 * Les champs obligatoires
		 */
		var empty = false;
		var error_msg = 'Vous devez saisir aussi : \n';
		if($("#event_designation").val() == '') {
			empty = true;
			error_msg += 'La désignation de l\'événement \n';
		}
		if($("#event_semaine_debut").val() == '' || $("#event_date_debut").val() == '' || $("#event_annee").val() == '') {
			empty = true;
			error_msg += 'L\'année,la semaine et la date de début \n';
		}
		if($("#event_semaine_fin").val() == '' || $("#event_date_fin").val() == '' || $("#event_annee_fin").val() == '') {
			empty = true;
			error_msg += 'L\'année,la semaine et la date de fin \n';
		} 
		if($("#event_type_list").val() == '') {
			empty = true;
			error_msg += 'Le type de l\'événement \n';
		}
		if($("#event_cible").val() == '') {
			empty = true;
			error_msg += 'Une ou plusieurs cibles \n';
		}
		if($("#event_date_debut_extranet").val() == '' || $("#event_date_fin_extranet").val() == '') {
			empty = true;
			error_msg += 'Les dates début et fin de lancement \n';
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dd").val() == ''){
			error_msg += 'La date de lancement à chaud DD \n';
			empty = true;
		}
		if($('input[type=radio][name=event_lancement_chaud]:checked').val() == 1 && $("#event_date_lancement_chaud_dr").val() == ''){
			error_msg += 'La date de lancement à chaud DR \n';
			empty = true;
		}
		if(empty) alert(error_msg);
		if(empty) return false;

		var data = '';
		data += 'cycle_ou_periode=' + $("#event_attach_list").val() + '&';
		data += 'annee=' + $("#event_annee").val() + '&';
		data += 'annee_fin=' + $("#event_annee_fin").val() + '&';
		data += 'designation=' + encodeURIComponent($("#event_designation").val()) + '&';
		data += 'semaine_debut=' + $("#event_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#event_semaine_fin").val() + '&';
		data += 'date_debut=' + $("#event_date_debut").val() + '&';
		data += 'date_fin=' + $("#event_date_fin").val() + '&';
		data += 'cycles=' + $("#event_cycles_correspondants").val() + '&';
		data += 'type=' + $("#event_type_list").val() + '&';
		data += 'cible=' + $("#event_cible").val() + '&';
		data += 'pre_lancement_debut=' + $("#event_date_debut_pre_lancement").val() + '&';
		data += 'pre_lancement_fin=' + $("#event_date_fin_pre_lancement").val() + '&';
		data += 'qte_pre_lancement=' + $("#event_qte_pre_lancement").val() + '&';
		data += 'extranet_debut=' + $("#event_date_debut_extranet").val() + '&';
		data += 'extranet_fin=' + $("#event_date_fin_extranet").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=event_action_info]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=event_publie]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=event_lancement_chaud]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#event_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#event_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=E&';
		}
		data += 'ajax=1';

		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'Référence du document needs to be unique')
					$("#info").html('Référence du document needs to be unique!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				else {
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					$("#event_designation").val('');
					$("#files_to_attach").val('');
					$(".doc_attach_element").remove();
					//Commentaires
					tinyMCE.get('mceEditor_dd').setContent('');
					tinyMCE.get('mceEditor_dr').setContent('');
					alert('Evénement  bien ajouté!');
				}
			},
			error: function() {
			}
		});
		
		return false;
	});
})( jQuery );

function redirect2DetailEvent(id_new_event)
{
	window.location = '<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event='?>'+id_new_event;
}
</script>