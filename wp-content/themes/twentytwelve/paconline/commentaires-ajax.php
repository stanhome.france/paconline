<?php
ob_end_clean();

$commentaires = Pods('commentaires', array('where' => "pod_cible='" . $_POST['pod_cible'] . "' AND id_pod_cible=" . $_POST['id_pod_cible'] . " AND visibility='" . $_POST['visibility'] . "'"));

$auteur = wp_get_current_user();
$auteur = $auteur->user_login;
	
if ($commentaires->total() > 0) {
	$existing_comment = $commentaires->field('id');
	$temp_commentaires = Pods('commentaires');
	$data = array(
		'content'      => $_POST['content'],
		'auteur'       => $auteur
	);
	$edited_existing_comment = $temp_commentaires->save($data, null, $existing_comment);
	echo $edited_existing_comment;
} else {
	$data = array(
		'visibility'   => $_POST['visibility'],
		'pod_cible'    => $_POST['pod_cible'],
		'id_pod_cible' => $_POST['id_pod_cible'],
		'content'      => $_POST['content'],
		'auteur'       => $auteur
	);
	$temp_commentaires = Pods('commentaires');
	$new_item_comment = $temp_commentaires->add($data);
	echo $new_item_comment;
}

exit();