

<?php
require_once 'include-functions.php';

$mails = Pods('mails', array());

?>
<?php require_once 'barre-actions.php';?>
<div class="frame">	
	<h3 class="ttl">Les mails modèles</h3>
<?php if ($mails->total() > 0):?>
	<div id="tree_mails">
		<ul style="width:100%;">
			<?php $intercale_mails_color = 0; ?>
			<?php while ($mails->fetch()):?>
				<?php
					if ($intercale_mails_color % 2 == 0) {
						$li_mail_bg = "#e8e8e8";
					} else {
						$li_mail_bg = "tranparent";
					}
					$intercale_mails_color++;
				?>
				<li>
					<div style="background-color:<?php echo $li_mail_bg; ?>; padding:5px; width:100%;">
						<a href="<?php echo get_permalink(get_page_by_path('visu-mail')) . '?id_mail=' . $mails->display('id')?>">Objet: <?php echo stripslashes($mails->display('objet'))?>, envoyé aprés <?php echo str_replace(',', ' et ', $mails->display('agents'))?></a>
					</div>
				</li>
			<?php endwhile;?>
		</ul>
	</div>
<?php else:?>
	<h4 class="ttl">Pas de mails enregistrés</h4>
<?php endif;?>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#tree_mails').jqxTree({ height: '500px', width:'100%' });
});
</script>