<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] == 1) {
	ob_end_clean();
	require_once 'articles/suppression-article-ajax.php';
	exit();
}

if (isset($_GET['ajax']) && $_GET['ajax'] == 4) {
	ob_end_clean();
	require_once 'find-articles.php';
	exit();
}

$options_tva = get_liste_tva();
$options_marque = get_liste_marque();
$options_gamme = get_liste_gamme();

$articles = Pods('articles', array('orderby' => "t.reference ASC", 'limit' => 15));
$total = $articles->total()

?>

<?php if ($total > 0):?>
<form>
	<?php require_once 'barre-actions.php';?>
	<div class="frame">		
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<h2 class="ttl">Liste des articles</h2>
				</td>
			</tr>
			<tr>
			<td colspan="2">
				<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
					<div id="jqxgrid"></div>
				</div>
			</td>
			</tr>
		</table>
	</div>
</form>
<?php else:?>
<form>
	<?php require_once 'barre-actions.php';?>
	<div class="frame">		
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<h2 class="ttl">Pas d'articles enregistrés</h2>
				</td>
			</tr>
		</table>
	</div>
</form>
<?php endif;?>
<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/grid-localisation.js"; ?>"></script>
<script type="text/javascript">
( function( $ ) {
	/*$("#btn_valid_search").bind('click', function() {
		if($("#q_reference").val() == '') alert('Champ obligatoire');
		if($("#q_reference").val() == '') return false;
		var data = 'ajax=4&reference='+$("#q_reference").val();
		$.ajax({
			url: '',
			type: 'get',
			dataType: 'json',
			data: data,
			success: function(msg) {
				if(msg.empty == 1) alert("Aucune référence ne correspond à votre recherche");
				else {
					$("#pagination_articles").hide();
				}
			},
			error: function() {
				alert('Erreur Ajax!');
			}
		});
	});*/

	var url = '<?php echo get_permalink(get_page_by_path('recuperation-des-articles-pour-le-grid'))?>';
	var source =
    {
        datatype: "json",
        datafields: [
            { name: 'reference', type: 'string' },
            { name: 'designation' },
            { name: 'prix_ht' },
            { name: 'prix_ttc' },
            { name: 'action_edit' },
            { name: 'action_delete' }
        ],
        url: url,
        filter: function () {
            $("#jqxgrid").jqxGrid('updatebounddata');
        },
        root: 'Rows',
        beforeprocessing: function (data) {
            source.totalrecords = data[0].TotalRows;
        }
    };
	var dataAdapter = new $.jqx.dataAdapter(source);
	$("#jqxgrid").jqxGrid({
			width: 760,
			source: dataAdapter,
			filterable: true,
			autoheight: true,
            pageable: true,
            virtualmode: true,
            selectionmode: 'multiplerowsextended',
            pagesizeoptions: ['10', '20', '50'],
            autoshowfiltericon: false,
            altrows: true,
            rendergridrows: function () {
                return dataAdapter.records;
            },
			columns: [
				{ text: 'Référence', dataField: 'reference', width: 100 },
				{ text: 'Désignation', dataField: 'designation', width: 300 },
				{ text: 'Prix HT', dataField: 'prix_ht', width: 90 },
				{ text: 'prix TTC', dataField: 'prix_ttc', width: 90, cellsalign: 'right' },
				{ text: '', dataField: 'action_edit', width: 90, cellsalign: 'right', cellsformat: 'c2' },
				{ text: '', dataField: 'action_delete', cellsalign: 'right', width: 90, cellsformat: 'c2' }
			]
	});
})( jQuery );

/*
 * Fonction pour supprmer un article
 */
function delete_article(id_article) {
	if(confirm("Etes vous certain de vouloir supprimer cet article?")) {
		$.ajax({
			url: '',
			type: 'get',
			data: 'id_article= ' + id_article + '&ajax=1',
			success: function(msg) {
				switch(msg) {
					case '-1':
						$("#info_delete_" + id_article).html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						break;
					case '0':
						$("#info_delete_" + id_article).html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						break;
					case '1':
						$("#li_article_" + id_article).remove();
						break;
				}
			},
			error: function() {
			}
		});
	}
}

function show_zone_recherche() {
	$("#btn_show_zone_recherche").hide();
	$("#tbl_area_search").show();
}
</script>