<?php 
require_once 'articles/ajout-article.php'; 

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'articles/ajout-article-ajax.php';
	exit();
}

$options_tva = get_liste_tva();
$options_marque = get_liste_marque();
$options_gamme = get_liste_gamme();

?>
<div id="save_new_article" class="form_new_pod">	
	<form id="save_new_article_form" action="" method="get">
		<?php require_once 'barre-actions.php';?>
		<div class="frame">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2" valign="top">
						<h2 class="ttl">Nouvel Article</h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td align="right">
						<label>Référence</label>
						<input type="text" id="article_reference"><div class="clearfix"></div>
						<span id="error_reference"></span>
					</td>
					<td align="right" width="50%">
						<label>Désignation</label>
						<input type="text" id="article_designation"><div class="clearfix"></div>
						<span id="error_designation" class="red"></span>
					</td>
				</tr>
				<tr>
					<td align="right"><label>Prix TTC</label>
						<input type="text" id="article_ttc">
						<span id="error_ttc"></span></td>
					<td align="right" ><label>TVA</label>
						<select id="article_tva">
							<option value="">Choisir...</option>
							<?php if (count($options_tva) > 0):?>
								<?php foreach ($options_tva as $kle => $value):?>
									<option value="<?php echo $kle?>"><?php echo "TVA $value %" ?></option>
								<?php endforeach;?>						
							<?php endif;?>
						</select><br class="clearfix"/>
						<span id="error_tva"></span></td>						

				</tr>
				<tr>
					<td align="right">
						<input type="button" value="Calculer Prix HT" id="calcult_ht" class="small">
					</td>
					<td align="right"><label>Prix HT</label>
						<input type="text" id="article_ht" disabled="disabled"><br class="clearfix"/>
						<span id="error_ht"></span></td>
				</tr>				
				<tr><td><span id="info"></span></td></tr>
			</table>
		</div>
	</form>	
</div>

<script type="text/javascript">
( function( $ ) {

	/*
	 *Ajout Nouvel Article
	 */
	$("#save_new_article_form").submit(function() {
		var errors_empty = false;
		if($("#article_designation").val() == '') {
			$("#error_designation").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_reference").val() == '') {
			$("#error_reference").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_tva").val() == '') {
			$("#error_tva").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_ht").val() == '') {
			$("#error_ht").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_ttc").val() == '') {
			$("#error_ttc").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if(errors_empty) return false;
		
		var data = '';
		data += 'designation=' + encodeURIComponent($("#article_designation").val()) + '&';
		data += 'reference=' + $("#article_reference").val() + '&';
		data += 'tva=' + $("#article_tva").val() + '&';
		data += 'ht=' + $("#article_ht").val() + '&';
		data += 'ttc=' + $("#article_ttc").val() + '&';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data + 'ajax=1',
			success: function(msg) {
				switch(msg) {
					case '-3':
						alert('Erreur');
						break;
					case 'ht':
						alert('Prix HT doit etre numérique');
						break;
					case 'ttc':
						alert('Prix TTC doit etre numérique');
						break;
					case 'article_existe':
						alert('Article avec la meme référence existe dèjà');
						break;
				}
				if(msg >= 0) {
					
					$("#info").html('Article Bien Ajouté').fadeOut('slow').fadeIn('slow').fadeOut('slow');
					window.location = '<?php echo get_permalink(get_page_by_path('articles'))?>';
				}
			},
			error: function() {
			}
		});
		return false;
	});
	/*
	 * Calcul automatique du prixx ttc à partir du prix ht et tva
	 */
	$("#calcult_ht").bind('click', function() {
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') alert('Prix TTC et TVA doivent etre saisis');
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') return false;
		var data = 'ttc='+$("#article_ttc").val()+'&tva='+$("#article_tva").val();
		$.ajax({
			url: '',
			type: 'get',
			data: data + '&ajax=3',
			success: function(msg) {
				switch(msg) {
					case 'ttc':
						alert('Le prix TTC doit etre numerique');
						break;
					case '-3':
						alert('Saisissez TVA');
						break;
				}
				if(msg != '-3' && msg != 'ttc') $("#article_ht").val(msg);
			},
			error: function() {
			}
		});
	});
	$("#article_ttc").bind('blur', function() {
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') alert('Prix TTC et TVA doivent etre saisis');
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') return false;
		var data = 'ttc='+$("#article_ttc").val()+'&tva='+$("#article_tva").val();
		$.ajax({
			url: '',
			type: 'get',
			data: data + '&ajax=3',
			success: function(msg) {
				switch(msg) {
					case 'ttc':
						alert('Le prix TTC doit etre numerique');
						break;
					case '-3':
						alert('Saisissez TVA');
						break;
				}
				if(msg != '-3' && msg != 'ttc') $("#article_ht").val(msg);
			},
			error: function() {
			}
		});
	});
	
	/*
	 * AJouter un nouvel article et rester dans la page
	 */
	 $("#save_new_next_article").bind('click', function(){
		 var errors_empty = false;
			if($("#article_designation").val() == '') {
				$("#error_designation").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				errors_empty = true;
			}
			if($("#article_reference").val() == '') {
				$("#error_reference").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				errors_empty = true;
			}
			if($("#article_tva").val() == '') {
				$("#error_tva").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				errors_empty = true;
			}
			if($("#article_ht").val() == '') {
				$("#error_ht").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				errors_empty = true;
			}
			if($("#article_ttc").val() == '') {
				$("#error_ttc").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				errors_empty = true;
			}
			if(errors_empty) return false;
			
			var data = '';
			data += 'designation=' + encodeURIComponent($("#article_designation").val()) + '&';
			data += 'reference=' + $("#article_reference").val() + '&';
			data += 'tva=' + $("#article_tva").val() + '&';
			data += 'ht=' + $("#article_ht").val() + '&';
			data += 'ttc=' + $("#article_ttc").val() + '&';
			$.ajax({
				url: '',
				type: 'get',
				data: data + 'ajax=1',
				success: function(msg) {
					switch(msg) {
						case '-3':
							$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case 'ht':
							$("#error_ht").html('Ce champ doit etre numérique').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case 'ttc':
							$("#error_ttc").html('Ce champ doit etre numérique').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case 'article_existe':
							alert('Article avec la meme référence existe dèjà');
							break;
					}
					if(msg >= 0) {
						$("#info").html('Article Bien Ajouté').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						$("#article_designation").val('');
						$("#article_reference").val('');
						$("#article_tva").val('');
						$("#article_ht").val('');
						$("#article_ttc").val('');
					}
				},
				error: function() {
				}
			});
			return false;
	 });
	
})( jQuery );
</script>
