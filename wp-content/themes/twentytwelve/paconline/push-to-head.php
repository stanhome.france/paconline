<?php
if(is_page()) {
	global $post;
	$page_name = $post->post_name;
	
	####Pages sur les quelles on va appliquer le css forms.css####
	$css_forms = array(
		"cycles",
		"edit-cycle", "edit-event-cyclique", "edit-document-cyclique", "edit-promotion-cyclique",
		"detail-cycle", "details-evenment-cyclique", "details-document-cyclique", "details-promotion-cyclique",
		"nouveau-cycle", "nouvel-evenement", "nouveau-document", "nouvelle-promotion",
		"remarques"
	);
	
	if (in_array($page_name, $css_forms)) {
		echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/forms.css" />';
	}
	
	####Pages sur les quelles on va appliquer le css article.css####
	$css_article = array(
		"articles", "edit-article", "nouvel-article"
	);
	
	if (in_array($page_name, $css_article)) {
		echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/article.css" />';
	}
	####Pages sur les quelles on va appliquer le css calendar.css####
	$css_calendar = array(
		"calendrier"
	);
	
	if (in_array($page_name, $css_calendar)) {
		echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/calendar.css" />';
	}	
	####Mode ReadOnly Pour TinyMce dans les pages de visualisation####
	$readOnly_TinyMce = array(
		"detail-cycle", "details-evenment-cyclique", "details-document-cyclique", "details-promotion-cyclique"
	);
	if(isset($_SESSION['readonly_tinymce'])) unset($_SESSION['readonly_tinymce']);
	if (in_array($page_name, $readOnly_TinyMce)) {
		$_SESSION['readonly_tinymce'] = true;
		echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/print.css" media="print" />';
	}
}
?>    