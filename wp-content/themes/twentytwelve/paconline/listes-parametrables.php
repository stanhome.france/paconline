<?php 
require_once 'listes-parametrables/tva.php';
require_once 'listes-parametrables/cible.php';
require_once 'listes-parametrables/gamme.php';
require_once 'listes-parametrables/marque.php';
require_once 'listes-parametrables/evenement.php';
require_once 'listes-parametrables/categorie.php';
require_once 'listes-parametrables/cycle.php';
require_once 'listes-parametrables/periode.php';
require_once 'listes-parametrables/archivage.php';
require_once 'listes-parametrables/sso.php';
/*
 * Ce code est résérvé seulement pour les requetes AJAX
* dans ce cas seulement ce code PHP est éxécuté
*/

if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'listes-parametrables/tva-ajax.php';
	require_once 'listes-parametrables/cible-ajax.php';
	require_once 'listes-parametrables/gamme-ajax.php';
	require_once 'listes-parametrables/marque-ajax.php';
	require_once 'listes-parametrables/evenement-ajax.php';
	require_once 'listes-parametrables/categorie-ajax.php';
	require_once 'listes-parametrables/cycle-ajax.php';
	require_once 'listes-parametrables/periode-ajax.php';
	require_once 'listes-parametrables/header-ajax.php';
	require_once 'listes-parametrables/archivage-ajax.php';
	require_once 'listes-parametrables/calendar-ajax.php';
	require_once 'listes-parametrables/sso-ajax.php';
	exit();
}

/*
 * Code pour uploader image header
*/
if (isset($_POST['ajax']) && $_POST['ajax'] != -1) {
	require_once 'listes-parametrables/header-ajax.php';
}

$options_tva = (get_options_tva()) ? unserialize(get_options_tva()) : array();
$options_cible = (get_options_cible()) ? unserialize(get_options_cible()) : array();
$options_gamme = (get_options_gamme()) ? unserialize(get_options_gamme()) : array();
$options_marque = (get_options_marque()) ? unserialize(get_options_marque()) : array();
$options_evenement = (get_options_evenement()) ? unserialize(get_options_evenement()) : array();
$options_categorie = (get_options_categorie()) ? unserialize(get_options_categorie()) : array();
$options_cycle = (get_options_cycle()) ? unserialize(get_options_cycle()) : array();
$options_periode = (get_options_periode()) ? unserialize(get_options_periode()) : array();

?>

<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/listes-parametrables.js"></script>

<div id="accordion_parametres">
	<h3 class="onglet_title_parametres">TVA</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_tva" id="liste_multiple_tva" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php if ($options_tva) : ?>
				<?php foreach ($options_tva as $k => $v) : ?>
				<option value="<?php echo $k ?>">
					<?php echo "TVA $v %"; ?>
				</option>
				<?php endforeach;?>
				<?php endif;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_tva_value" name="delete_tva_value" value="Supprimer"> 
		</div>
		<div class="left left-spaced">
			<legend>Nouvelle TVA</legend>
			<label>Saisir la nouvelle TVA : </label> 
			<br/>
			<input type="text" id="new_tva_value" name="new_tva_value" class="mediumInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_tva" name="add_new_tva" value="Ajouter"> 
			
			<div class="top-spaced">
			<label id="error_add_tva" style="color: red;"></label>
			<label id="error_delete_tva" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Numéros de cycles</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_cycle" id="liste_multiple_cycle" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_cycle as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_cycle_value" name="delete_cycle_value" value="Supprimer"> 
		</div>
		<div class="left left-spaced">
			<legend>Nouveau cycle</legend>
			<label>Saisir le nouveau cycle : </label> 
			<br/>
			<input type="text" id="new_cycle_value" name="new_cycle_value" class="mediumInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_cycle" name="add_new_cycle" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_cycle" style="color: red;"></label>
				<label id="error_delete_cycle" style="color: red;"></label>
			</div>
		</div>
	</div>
 

	<h3 class="onglet_title_parametres">Cibles</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_cible" id="liste_multiple_cible" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_cible as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_cible_value" name="delete_cible_value" value="Supprimer"> 
			<br/>
		</div>
		<div class="left left-spaced">
			<legend>Nouvelle cible</legend>
			<label>Saisir la nouvelle cible : </label> 
			<br/>
			<input type="text" id="new_cible_value" name="new_cible_value" class="normalInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_cible" name="add_new_cible" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_cible" style="color: red;"></label>
				<label id="error_delete_cible" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Catégories des articles</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_categorie" id="liste_multiple_categorie" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_categorie as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_categorie_value" name="delete_categorie_value" value="Supprimer"> 
		</div>
		<div class="left left-spaced">
			<legend>Nouvelle categorie</legend>
			<label>Saisir la nouvelle categorie : </label> 
			<br/>
			<input type="text" id="new_categorie_value" name="new_categorie_value" class="normalInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_categorie" name="add_new_categorie" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_categorie" style="color: red;"></label>
				<label id="error_delete_categorie" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Types d’évènements</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_evenement" id="liste_multiple_evenement" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_evenement as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_evenement_value" name="delete_evenement_value" value="Supprimer"> 
		</div>
		<div class="left left-spaced">
			<legend>Nouvel évènement</legend>
			<label>Saisir le nouvel évènement : </label> 
			<br/>
			<input type="text" id="new_evenement_value" name="new_evenement_value" class="normalInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_evenement" name="add_new_evenement" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_evenement" style="color: red;"></label>
				<label id="error_delete_evenement" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Marques</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_marque" id="liste_multiple_marque" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_marque as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_marque_value" name="delete_marque_value" value="Supprimer">
		</div>
		<div class="left left-spaced">
			<legend>Nouvelle marque</legend>
			<label>Saisir la nouvelle marque : </label> 
			<br/>
			<input type="text" id="new_marque_value" name="new_marque_value" class="normalInput">
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_marque" name="add_new_marque" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_marque" style="color: red;"></label>
				<label id="error_delete_marque" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Gamme</h3>
	<div class="onglet_content_parametres">
		<div class="left">
			<legend>Suppression</legend>
			<select name="liste_multiple_gamme" id="liste_multiple_gamme" multiple="multiple">
				<option value="">Sélectionner...</option>
				<?php foreach ($options_gamme as $k => $v): ?>
				<option value="<?php echo $k; ?>">
					<?php echo stripslashes($v); ?>
				</option>
				<?php endforeach;?>
			</select> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="delete_gamme_value" name="delete_gamme_value" value="Supprimer"> 
		</div>
		<div class="left left-spaced">
			<legend>Nouvelle gamme</legend>
			<label>Saisir la nouvelle gamme : </label> 
			<br/>
			<input type="text" id="new_gamme_value" name="new_gamme_value" class="normalInput"> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="add_new_gamme" name="add_new_gamme" value="Ajouter"> 
			<div class="top-spaced">
				<label id="error_add_gamme" style="color: red;"></label>
				<label id="error_delete_gamme" style="color: red;"></label>
			</div>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Paramètres généraux</h3>
	<div class="onglet_content_parametres">
		<div>
			<legend>Image de la page d'accueil</legend>
			<div class="left" style="margin: 10px;">
				<img alt="" src="<?php echo get_option('_image_page_accueil')?>" width="200" />
			</div>
			<div class="left left-spaced">
				<div style="margin-bottom: 10px;">Choisissez une image de grande taille de préférence.</div>
				<form action="" id="image_header_uploader" method="post" enctype="multipart/form-data">
					<input type="hidden" id="ajax" name="ajax" value="17">
					<input type="file" id="image_header" name="image_header"> 
					<br/>
					<input style="margin-left: 0px; margin-top : 10px;" type="submit" value="Uploader">
				</form>
				<label id="error_upload_header"> <?php if (isset($error_upload_header_image) && $error_upload_header_image):?>
					Erreur Pendant l'upload de l'image <?php unset($error_upload_header_image)?>
					<?php endif;?>
				</label>
			</div>
			<div class="clearfix"></div>
		</div>
		<br/>
		<div>
			<legend>Texte affiché sur la page d'accueil</legend>
			<form action="" id="texte_header_form" method="get">
			<textarea id="texte_header" rows="10" cols="70"><?php echo get_option('blogdescription')?></textarea>
			<br/>
			<div class="left">
				<input style="margin-left: 0px;" type="submit" value="Enregistrer">
			</div>
			<div class="left left-spaced" style="margin-top : 10px;">
				<label id="error_texte_header"></label>
			</div>
			</form>
		</div>
		<div class="clearfix"></div>
		<br/>
		<div>
			<legend>Archivage</legend>
			<label>Archiver les cycles âgés de plus de </label>
			<input type="text" name="texte_archive_delay" id="texte_archive_delay" value="<?php echo get_archivage_delay();?>" class="smallInput"/>
			<label>mois</label> 
			<br/>
			<input style="margin-left: 0px;" type="button" id="save_archive_delay" name="save_archive_delay" value="Enregistrer">
			<label id="error_archive_delay" style="color: red;"></label>
		</div>
		<br/>
		<div>
			<legend>Paramétrage de la vue calendaire</legend>
			<label>Nombre de pixels prévu par jour </label> 
			<input class="smallInput" type="text" name="nb_pixels_per_day" id="nb_pixels_per_day"	value="<?php if (get_option("nb_pixels_per_day")) { echo get_option("nb_pixels_per_day"); } else { echo "8"; } ?>" /> pixels
			</br> 
			<label>Rayon de visibilité de la vue calendaire </label> 
			<input class="mediumInput" type="text" name="rayon" id="rayon"	value="<?php if (get_option("rayon")) { echo get_option("rayon"); } else { echo "8"; } ?>" /> semaines
			</br>
			<input style="margin-left: 0px;" type="button" id="save_calender_param" name="save_calender_param" value="Enregistrer"> 
			<label id="error_settings_calendar_view" style="color: red;"></label>
		</div>
	</div>

	<h3 class="onglet_title_parametres">Authentification SSO</h3>
	<div class="onglet_content_parametres">
		<div>
			<label>Clé secrète </label>
			<input type="text" name="secret_key" id="secret_key" value="<?php echo get_secret_key(); ?>" class="normalInput"/>
			</br>
			<label>Url vers l'extranet </label> 
			<input type="text" name="extranet_url" id="extranet_url" value="<?php echo get_extranet_url(); ?>" class="normalInput"/>
			</br>
			<input style="margin-left: 0px;" type="button" id="save_sso_settings" name="save_sso_settings" value="Enregistrer">
			<label id="error_sso_settings" style="color: red;"></label>
		</div>
		<br/>
	</div>
</div>