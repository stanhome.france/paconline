<?php

if (isset($_GET["annee"])) {
	$list_year_to_open = $_GET["annee"];
} else {
	$list_year_to_open = 0;
}

if (isset($_GET["list"])) {
	$list_to_open = $_GET["list"];
} else {
	$list_to_open = "";
}


if (isset($_GET['action']) && $_GET['action'] == 'gen_cal') {
	require('calendrier-par-semaine/generate-calendar.php');
} else {
	if (isset($_GET['action']) && $_GET['action'] == 'add_vac') {
		require('calendrier-par-semaine/add-school-date.php');
	} else {
		if (isset($_GET['action']) && $_GET['action'] == 'del_vac') {
			require('calendrier-par-semaine/del-school-date.php');
		} else {
			if (isset($_GET['action']) && $_GET['action'] == 'mod_sem') {
				require('calendrier-par-semaine/mod-week-date.php');
			} else {
				include_once('cycles/functions.php');
				 
				$jour_semaine = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
				$mois_annee = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	
?>
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="'../wp-content/themes/twentytwelve/js/tools.js') ?>"></script>

<script>
jQuery(document).ready(function ($) {
	var source_annee = [
<?php for($i = Date('Y') - 1; $i < Date('Y') + 4; $i++) { ?>
			"<?php echo $i; ?>"<?php if ($i < Date('Y') + 3) echo ','; ?>
<?php } ?>
	];

	$('#annee').jqxComboBox({ dropDownHeight:123, selectedIndex:0, source:source_annee, width:'60px', height:'22px' });

	$(".school_delete_btn").jqxButton({ width: '100', height: '25'});
	$(".add_school_ok_btn").jqxButton({ width: '70', height: '30'});
	$(".add_school_cancel_btn").jqxButton({ width: '100', height: '30'});

	//var zonage = ["Zone A - Noël", "Zone A - Février", "Zone A - Pâques", "Zone A - Toussaint", "Zone A - Eté"];
	//$(".designation").jqxInput({ placeHolder:"Entrez le libellé d'une zone", height:25, width:150, minLength:1, source:zonage });
		
	$('#calendar').jqxTree({ height: '500px',width: '100%',  keyboardNavigation: false });
	
	$('#calendar').on('select', function (event) {
		var args = event.args;
	    var item = $('#calendar').jqxTree('getItem', args.element);
	    var elt = $('#' + item.element.id);

		// associer les date picker
		elt.find("input.date_debut").datepicker();
		elt.find("input.date_fin").datepicker();
	    
	    if (elt.hasClass('semaines')) {
			elt.find('div.modified').hide();
			elt.find('div.modifier').show();

			// binder l'actin sur le bouton ok du formulaire
			elt.find('input.mod_week_ok_btn').unbind('click').click(function() {
				var p = $(this).parent();
				var text_modified = p.parent().children('.modified');
				var id = p.children('.id').val();
				var designation = p.children('.designation').val();
				var date_debut = p.children('.date_debut').val();
				var date_fin = p.children('.date_fin').val();
				var annee = p.children('.annee').val();
				p.hide();
				text_modified.show();
				text_modified.html(designation + ' : Du ' + date_debut + ' au ' + date_fin);
				execute_ajax_command('id=' + id + '&date_debut=' + date_debut + '&date_fin=' + date_fin + '&action=mod_sem', 
									 mod_week_ok_btn_success_method, 
									 $(this));
			});

			function mod_week_ok_btn_success_method(msg, user_data) {
				var p = user_data.parent();
				var designation = p.children('.designation').val();
				var annee = p.children('.annee').val();
				alert('Les dates de la semaine ' + designation + ' ont été mises à jour.');
				document.location.href = '../calendrier-par-semaine/?annee=' + annee + '&list=semaines';
			}

			elt.find('input.mod_week_cancel_btn').unbind('click').click(function() {
				$("#calendar").jqxTree('selectItem', null);
				var p = $(this).parent();
				var text_modified = p.parent().children('.modified');
				p.hide();
				text_modified.show();
				return false;
			});
	    }

	    if (elt.hasClass('school_holidays')) {
	    	elt.find('div.add_school_bloc').show();

	    	elt.find('input.designation').unbind('click').click(function() {
		    	return false;
	    	});
	    	
	    	elt.find('input.add_school_ok_btn').unbind('click').click(function() {
	    		var parent = $(this).parent();
	    		var annee = parent.children('.annee');
	    		var designation = parent.children('.designation');
	    		var date_debut = parent.children('.date_debut');
	    		var date_fin = parent.children('.date_fin');
	    		if (designation.val() == '' || date_debut.val() == '' || date_fin.val() == '') {
	    			alert('Tous les champs de ce formulaire sont obligatoires');
	    		} else {
	    			execute_ajax_command('y=' + annee.val() + '&designation=' + designation.val() + '&date_deb=' + date_debut.val() + '&date_fin=' + date_fin.val() + '&action=add_vac', 
	    								 add_school_ok_btn_success_method, 
	    								 $(this));
	    		}
	    	});
	    	
	    	function add_school_ok_btn_success_method(msg, user_data) {
	    		var parent = user_data.parent();
	    		var annee = parent.children('.annee');
	    		alert('Les dates de vacances scolaires ont bien été enregistrées.');
	    		document.location.href = '../calendrier-par-semaine/?annee=' + annee.val() + '&list=vacances';
	    	}

	    	elt.find('input.add_school_cancel_btn').unbind('click').click(function() {
	    		$("#calendar").jqxTree('selectItem', null);
	    		var p = $(this).parent();
				p.hide();
				return false;
			});
	    }

	    if (elt.hasClass('school_date_item')) {
	    	elt.find('input.school_delete_btn').unbind('click').click(function() {
	    		var parent = $(this).parent();
	    		var id = parent.children('.item_to_del');
	    		var annee = parent.children('.annee').val();
	    		
	    		var check = confirm('Voulez-vous vraiment supprimer cette entrée du calendrier des vacances scolaires?');
	    		if (check) {
	    			execute_ajax_command('item_to_del=' + id.val() + '&action=del_vac', school_delete_btn_success_method, $(this));
	    		}
	    	});

	    	function school_delete_btn_success_method(msg, user_data) {
	    		var parent = user_data.parent();
	    		var annee = parent.children('.annee').val();
	    		alert('Les dates de vacances scolaires ont bien été supprimées.');
	    		document.location.href = '../calendrier-par-semaine/?annee=' + annee + '&list=vacances';
	    	}
	    }
	});

    $('.open_all').unbind('click').click(function() {
		$('#calendar').jqxTree('expandAll');
	});

	$('.close_all').unbind('click').click(function() {
		$('#calendar').jqxTree('collapseAll');
	});

<?php 
	if ($list_year_to_open != 0) { ?>
	$('#calendar').jqxTree('expandItem', $("#annee_" + <?php echo $list_year_to_open; ?>)[0]);
<?php 	if ($list_to_open == "semaines") { ?>
	$('#calendar').jqxTree('expandItem', $("#semaine_" + <?php echo $list_year_to_open; ?>)[0]);
<?php 	}
	 	if ($list_to_open == "vacances") { ?>
	$('#calendar').jqxTree('expandItem', $("#school_" + <?php echo $list_year_to_open; ?>)[0]);
<?php 	}
	} ?>

    $('.generate_calendar').unbind('click').click(function() {
    	var item = $("#annee").jqxComboBox('getSelectedItem');
    	var year = item.value;
    	execute_ajax_command('year=a' + year + '&action=gen_cal', do_calendar_success_method);
    });

    function do_calendar_success_method(msg) {
    	alert('La génération de l\'année demandée est effectuée.');
    	document.location.href = '../calendrier-par-semaine/';
    }
});
</script>

<div class="btn_group">
	<div class="btn_group1">
		<div style="float:left; margin-top:10px; margin-left:10px; margin-right:10px;">Créer un calendrier pour l'année : </div>
		<div style="float:left; margin-top:6px;" id="annee"></div>
		<input type="button" value="Générer le calendrier" class="long generate_calendar" style="margin-top:6px;"/>
		<input type="button" value="Tout ouvrir" class="open_all" style="margin-top:6px;"/>
		<input type="button" value="Tout fermer" class="close_all" style="margin-top:6px;"/>
		<div class="clearfix"></div>
	</div>
</div>
<div class="frame">
	<h3 class="ttl">Calendrier</h3>
	<div id="calendar">
	<ul>
<?php
	$years = Pods('annees', array('orderby' => 'annee asc'));
	while($years->fetch()) { 
		$id_year = $years->field('id'); ?>
		<li id="annee_<?php echo $id_year; ?>">
			<div class='title' style='font-weight:bold;'>Année <?php echo $years->display('annee'); ?></div>
			<ul class='list'>
				<li>
					<div class='title close'>Jours fériés</div>
					<ul class='list' style='display:none;'>
<?php	$feries = Pods('feries', array('where' => 'id_annee ='.$id_year));
		while($feries->fetch()) { 
			$date_pod = $feries->display('date_jour_ferie'); 
			$date_pod_exploded = explode('/', $date_pod);
			$date_ts = mktime(0, 0, 0, $date_pod_exploded[0], $date_pod_exploded[1], $date_pod_exploded[2]);
			$j_sem = $jour_semaine[Date('w', $date_ts)];
			$m_ann = $mois_annee[Date('n', $date_ts) - 1];
			$num_jour = Date('j', $date_ts);
			$year = Date('Y', $date_ts);
?>
						<li>
							<div><?php echo $feries->display('designation'); ?> : <?php echo $j_sem.' '.$num_jour.' '.$m_ann.' '.$year; ?></div>
						</li>
<?php 	} ?>
					</ul>
				</li>
				<li id="semaine_<?php echo $id_year; ?>">
					<div class='title'>Semaines commerciales</div>
					<ul class='list'>
<?php	$semaines = Pods('calendriers', array('where' => 'id_annee ='.$id_year.' and semaine=1 and vacances=0',
											  'limit' => 53));
		while($semaines->fetch()) { ?>
						<li class="semaines">
							<div>
								<div class='modified'><?php echo $semaines->display('designation'); ?> : Du <?php echo pod2html_date($semaines->display('date_debut'))?> au <?php echo pod2html_date($semaines->display('date_fin')); ?></div>
								<div class='modifier' style="display:none;">
									<input type='hidden' value='<?php echo $id_year; ?>' class='annee'/>
									<input type='hidden' value='<?php echo $semaines->field('id'); ?>' class='id'/>
									<input type='hidden' value='<?php echo $semaines->display('designation'); ?>' class='designation'/>
									<?php echo $semaines->display('designation'); ?> : Du
									<input type='text' value='<?php echo pod2html_date($semaines->display('date_debut'))?>' class='date_debut'/>
									au
									<input type='text' value='<?php echo pod2html_date($semaines->display('date_fin')); ?>' class='date_fin'/>
									<input class='mod_week_ok_btn' type='button' value='Valider'/>
									<input class='mod_week_cancel_btn' type='button' value='Annuler'/>
								</div>
							</div>
						</li>
<?php 	} ?>
					</ul>
				</li>
				<li class="school_holidays" id="school_<?php echo $id_year; ?>">
					<div class='title'>Vacances scolaires</div>
					<div class='add_school_bloc close' style='display:none;'>
						<input type='hidden' value='<?php echo $id_year; ?>' class='annee'/>
						Libéllé
						<input type='text' value='' class='designation'/>
						Du
						<input type='text' value='' class='date_debut'/>
						au
						<input type='text' value='' class='date_fin'/>
						<input class='add_school_ok_btn' type='button' value='OK'/>
						<input class='add_school_cancel_btn' type='button' value='Annuler'/>
					</div>
					<ul>
<?php 	$school = Pods('calendriers', array('where' => 'id_annee ='.$id_year.' and vacances=1 and semaine=0',
											'limit' => 100,
											'orderby' => 'designation ASC'));
		while($school->fetch()) { ?>
						<li class="school_date_item">
							<div>
								<?php echo $school->display('designation'); ?> : Du <?php echo pod2html_date($school->display('date_debut'))?> au <?php echo pod2html_date($school->display('date_fin')); ?>
								<input type='hidden' value='<?php echo $id_year; ?>' class='annee'/>
								<input type='button' value='Supprimer' class='school_delete_btn'/>
								<input type='hidden' value='<?php echo $school->field('id'); ?>' class='item_to_del'/>
							</div>
						</li>
<?php 	} ?>
					</ul>
				</li>
			</ul>
		</li>	
<?php 
	} ?>
	</ul>

<?php 		}
		}
	}
} ?>
	</div>
</div>
