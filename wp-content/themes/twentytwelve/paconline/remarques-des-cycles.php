<script type="text/javascript">
    jQuery(document).ready(function ($) {
        
        $('#tree_remarques').jqxTree({ height: '500px', width:'100%' });

        $("#close_all").jqxButton({ width: '120', height: '35'});
        $("#open_all").jqxButton({ width: '120', height: '35'});
                
        $('.open_all').unbind('click').click(function() {
    		$('#tree_remarques').jqxTree('expandAll');
    	});

    	$('.close_all').unbind('click').click(function() {
    		$('#tree_remarques').jqxTree('collapseAll');
    	});

	});
</script>

<?php
include_once('cycles/functions.php');
$where_temp = ' AND permalink like  \'cycles%\'';
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<input type="button" value="Ouvrir tout" class="open_all" id="open_all">
		<div class="clearfix"></div>
	</div>
</div>
<div class="frame">
	<h3 class="ttl">Les remarques</h3>
	<div id="tree_remarques">
		<ul style="width: 100%;">
			<?php
				$remarques = array(); 
				$pod_cycles = Pods('cycles', array('where' => 'permalink like \'cycles%\' and archive=0', 'limit' => '-1'));
				if ($pod_cycles->total() > 0) {
					while ($pod_cycles->fetch()) { ?>
					<!-- Remarques du cycle -->
					<?php
						$cid = $pod_cycles->field('id');
						$remarques[$cid] = array(); 
						$pod_remarques_cycle = Pods('remarques', array('where' => 'type_entite=\'C\' AND id_entite='.$cid, 'limit' => '-1'));
						if ($pod_remarques_cycle->total() > 0) {
							$remarques[$cid]['cycle'] = $cid;
						}
					?>
					
					<!-- Remarques de promo -->
					<?php 
						$pod_cycles_promos = Pods('promotions', array('join' => 'LEFT JOIN wp_pods_cycles_promos ON wp_pods_cycles_promos.id_promo = t.id',
																	  'where' => 'wp_pods_cycles_promos.id_cycle='.$cid, 'limit' => '-1'));
						while ($pod_cycles_promos->fetch()) {
							$pod_remarques_promo = Pods('remarques', array('where' => 'type_entite=\'P\' AND id_entite='.$pod_cycles_promos->field('id'), 'limit' => '-1'));
							if ($pod_remarques_promo->total() > 0) {
								$remarques[$cid]['promo'][] = $pod_cycles_promos->field('id');
							}
						}
					?>
					
					<!-- Remarques de events -->
					<?php 
						$pod_cycles_events = Pods('events', array('join' => 'LEFT JOIN wp_pods_cycles_events ON wp_pods_cycles_events.id_event = t.id',
																  'where' => 'wp_pods_cycles_events.id_cycle='.$cid, 'limit' => '-1'));
						while ($pod_cycles_events->fetch()) {
							$pod_remarques_event = Pods('remarques', array('where' => 'type_entite=\'E\' AND id_entite='.$pod_cycles_events->field('id'), 'limit' => '-1'));
							if ($pod_remarques_event->total() > 0) {
								$remarques[$cid]['event'][] = $pod_cycles_events->field('id');
							}
						}
					?>
					
					<!-- Remarques de docs -->
					<?php 
						$pod_cycles_docs = Pods('documents', array('join' => 'LEFT JOIN wp_pods_cycles_docs ON wp_pods_cycles_docs.id_doc = t.id',
																   'where' => 'wp_pods_cycles_docs.id_cycle='.$cid, 'limit' => '-1'));
						while ($pod_cycles_docs->fetch()) {
							$pod_remarques_doc = Pods('remarques', array('where' => 'type_entite=\'D\' AND id_entite='.$pod_cycles_docs->field('id'), 'limit' => '-1'));
							if ($pod_remarques_doc->total() > 0) {
								$remarques[$cid]['doc'][] = $pod_cycles_docs->field('id');
							}
						}
					}
				} ?>
			
			<!--  affichage des remarques par cycle -->
			
			<?php
				foreach ($remarques as $id_cycle => $remarque) {
					$cycle = Pods('cycles', $id_cycle);
					$identifiant = $cycle->display('identifiant');
					if (count($remarques[$id_cycle])) { ?>
				<li>
					<?php echo $identifiant; ?>
					<ul>
						<?php if (isset($remarque['cycle'])) {
								$remarques_cycle = Pods('remarques', array('where' => 'type_entite=\'C\' AND id_entite='.$remarque['cycle'], 'limit' => '-1'));
								if ($remarques_cycle->total() > 0) {
									while ($remarques_cycle->fetch()) {
						?>
									<li><a href="<?php echo get_permalink(get_page_by_path('detail-cycle')) . '?id_cycle=' . $remarque['cycle']?>"><?php echo $identifiant . ' - ' . pod2html_date($remarques_cycle->display('creation_date')) . ' - ' . $remarques_cycle->display('auteur') . ' - ' . stripslashes($remarques_cycle->display('content'))?></a></li>
						<?php 		}
								}
							}
							if (isset($remarque['promo'])) { ?>
							<li> Promotions
								<ul>
								<?php foreach ($remarque['promo'] as $promo) {
										$promo_pod = Pods('promotions', $promo);
										$remarques_promo = Pods('remarques', array('where' => 'type_entite=\'P\' AND id_entite='.$promo, 'limit' => '-1'));
										if ($remarques_promo->total() > 0) {
											while ($remarques_promo->fetch()) {
								?>
											<li><a href="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $promo?>"><?php echo $identifiant . ' - ' . stripslashes($promo_pod->display('reference_extranet').' : '.$promo_pod->display('description')) . ' - ' . pod2html_date($remarques_promo->display('creation_date')) . ' - ' . $remarques_promo->display('auteur') . ' - ' . stripslashes($remarques_promo->display('content'))?></a></li>
								<?php 		}
										}
									} ?>
								</ul>
							</li>
						<?php }
							if (isset($remarque['event'])) { ?>
							<li> Events
								<ul>
							<?php foreach ($remarque['event'] as $event) {
										$event_pod = Pods('events', $event);
										$remarques_event = Pods('remarques', array('where' => 'type_entite=\'E\' AND id_entite='.$event, 'limit' => '-1'));
										if ($remarques_event->total() > 0) {
											while ($remarques_event->fetch()) {
										?>
											<li><a href="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event?>"><?php echo $identifiant . ' - ' . stripslashes($event_pod->display('designation')) . ' - ' . pod2html_date($remarques_event->display('creation_date')) . ' - ' . $remarques_event->display('auteur') . ' - ' . stripslashes($remarques_event->display('content'))?></a></li>
							<?php 			}
										}
									} ?>
								</ul>
							</li>
						<?php }
							if (isset($remarque['doc'])) { ?>
							<li> Documents
								<ul>
							<?php foreach ($remarque['doc'] as $doc) {
										$doc_pod = Pods('documents', $doc);
										$remarques_doc = Pods('remarques', array('where' => 'type_entite=\'D\' AND id_entite='.$doc, 'limit' => '-1'));
										if ($remarques_doc->total() > 0) {
											while ($remarques_doc->fetch()) {
							?>
											<li><a href="<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc?>"><?php echo $identifiant . ' - ' . stripslashes($doc_pod->display('designation')) . ' - ' . pod2html_date($remarques_doc->display('creation_date')) . ' - ' . $remarques_doc->display('auteur') . ' - ' . stripslashes($remarques_doc->display('content'))?></a></li>
							<?php 			}
										}
									} ?>
								</ul>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php }
				}
				print_r('<pre>');
				//var_dump($remarques);
				print_r('</pre>');
			?>
		</ul>
	</div>
</div>
