<?php 
	require_once 'logs/log-helper.php';
	
	if(isset($_POST["purgeActionIndexation"])){
		$log_path_file = get_template_directory().'/log/indexation.log';
		sh_purge_log($log_path_file);
	}
	if(isset($_POST["purgeActionImpression"])){
		$log_path_file = get_template_directory().'/log/impression/system.log';
		sh_purge_log($log_path_file);
	}
	if(isset($_POST["purgeActionDiffusion"])){
		$log_path_file = get_template_directory().'/log/diffusion.log';
		sh_purge_log($log_path_file);
	}
	if(isset($_POST["purgeActionArchivage"])){
		$log_path_file = get_template_directory().'/log/archivage.log';
		sh_purge_log($log_path_file);
	}
?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	 $("#log_container").jqxPanel({width: '100%'});

	 $("#purgeActionArchivage").jqxButton({ width: 170, height: 35});
	 $("#purgeActionIndexation").jqxButton({ width: 170, height: 35});
	 $("#purgeActionImpression").jqxButton({ width: 175, height: 35});  
	 $("#purgeActionDiffusion").jqxButton({ width: 175, height: 35});  
	 
	 $('#tree_logs').jqxTree({ height: '500px', width:'100%' });
});
</script>
<div class="btn_group">
	<div class="btn_group1">
		<table>
			<tr>
				<td>
					<form method="post">
						<button type="submit" id="purgeActionArchivage" name="purgeActionArchivage" style="margin-left:10px;">Vider le log d'archivage</button>
					</form>
				</td>
				<td>
					<form method="post">
						<button type="submit" id="purgeActionDiffusion" name="purgeActionDiffusion" style="margin-left:10px;">Vider le log de diffusion</button>
					</form>
				</td>
				<td>
					<form method="post">
						<button type="submit" id="purgeActionImpression" name="purgeActionImpression" style="margin-left:10px;">Vider le log d'impression</button>
					</form>
				</td>
				<td>
					<form method="post">
						<button type="submit" id="purgeActionIndexation" name="purgeActionIndexation" style="margin-left:10px;">Vider le log d'indexation</button>
					</form>
				</td>
			</tr>
		</table>
		<div class="clearfix"></div>
	</div>
</div>
<div class="frame">
	<h3 class="ttl">Logs applicatif</h3>
	<div id="tree_logs">
		<ul style="width:100%;">
			<li>
				<div>Agent d'archivage</div>
				<ul>
					<li>
						<div id="log_container">
							<?php echo sh_read_from_log(get_template_directory().'/log/archivage.log');?>
						</div>
					</li>
				</ul>
			</li>
			<li>
				<div>Agent de diffusion</div>
				<ul>
					<li>
						<div id="log_container">
							<?php echo sh_read_from_log(get_template_directory().'/log/diffusion.log');?>
						</div>
					</li>
				</ul>
			</li>
			<li>
				<div>Agent d'impression</div>
				<ul>
					<li>
						<div id="log_container">
							<?php echo sh_read_from_log(get_template_directory().'/log/impression/system.log');?>
						</div>
					</li>
				</ul>
			</li>
			<li>
				<div>Agent d'indexation</div>
				<ul>
					<li>
						<div id="log_container">
							<?php echo sh_read_from_log(get_template_directory().'/log/indexation.log'); ?>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>