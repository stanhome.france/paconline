<?php 
$currentId = $current_cycle->display('id');
$is_periode = false;
$where = ' AND permalink like "cycles%"';

// recherche du groupe le plus fort d'un user dans la liste des groups auquels il appartient
// le groupe le plus fort se trouve dans $high_level_group
$high_level_group = get_high_level_group_from_current_user();
$current_date = date('Y/m/d');
$where_extension = "";
?>

<p class="ttl">Eléments attachés au cycle</p>
<div id="mainSplitter" style="border-radius: 10px;">	
<div class="frame" style="border:none;">
<?php ############################Promos#################### 
	$cycles_promos = Pods('cycles_promos', array('where' => 'id_cycle='.$currentId, 'limit' => "-1", 'orderby' => "t.id_promo ASC"));
	$total_cycles_promos = $cycles_promos->total();
	if ($total_cycles_promos > 0) {
?>
<ul id="tree_sous_promos" class="treeview_red">
	<li class="open"><span>Promotions</span>
		<ul>
		<?php 
			$cycles_promos_ids = array();
			while ($cycles_promos->fetch()) $cycles_promos_ids[] = $cycles_promos->field('id_promo');
			$cycles_promos_ids = '(' . implode(',', $cycles_promos_ids) . ')';
			foreach ($marques as $cle_marque => $val_marque) {
				$pod_promos_marques = Pods('promotions',
					array('where'
						=>
						"id IN $cycles_promos_ids AND marque=$cle_marque" . $where.$where_extension,
						'limit' => "-1"
					)	
				);
				if ($pod_promos_marques->total() > 0) {
		?>
		<li>
			<span><?php echo stripslashes($val_marque); ?></span>
			<ul class="level2">
				<?php while ($pod_promos_marques->fetch()) { ?>
					<li>
						<?php display_binary_icons($pod_promos_marques, 'P'); 
							  //display_diffusion_icons($pod_promos_marques);
							  $composition = ' : ';
							  $cycles_periodes = cycles_periodes_suffixe($pod_promos_marques->field('semaine_debut'), $pod_promos_marques->field('annee'), $pod_promos_marques->field('semaine_fin'), $pod_promos_marques->field('annee_fin'), $pod_promos_marques->field('permalink'));
							  $promo_url = get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $pod_promos_marques->field('id');
						?>
						<input type="image" src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>" onclick="previsualisation_entite('<?php echo $promo_url?>')">
						<a <?php if ($pod_promos_marques->display('lancement_chaud') == 'Yes') { ?> style="color: red;" <?php } ?> href="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $pod_promos_marques->field('id')?>"><?php echo $pod_promos_marques->display('reference_extranet').' : '.$pod_promos_marques->display('description'); ?>
							<?php echo $composition . '    ' . $cycles_periodes;?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</li>
		<?php 
				}
			} ?>
		</ul>
	</li>
</ul>
<?php
	}

	######################Events#######################
	$cycles_events = Pods('cycles_events', array('where' => "id_cycle=".$currentId, 'limit' => "-1", 'orderby' => "t.id_event ASC"));
	$total_cycles_events = $cycles_events->total();
	if ($total_cycles_events > 0) {
?>
<ul id="tree_sous_events" class="treeview-red">
	<li class="open"><span>Evènements</span>
		<ul class="level2">
		<?php 
			while ($cycles_events->fetch()) {
				$event = Pods('events', array('where' => "id=" . $cycles_events->field('id_event') . $where.$where_extension));
				$total_event = $event->total();
				if ($total_event > 0) {
					while ($event->fetch()) {
		?>
		<?php if (($event->display('lancement_chaud') == 'Yes' ) &&
				    ('StanHome_DD' == $high_level_group &&  date_conversion($event->display('date_lancement_chaud_dd')) > $current_date )
		    ||('StanHome_DR' == $high_level_group &&  date_conversion($event->display('date_lancement_chaud_dr')) > $current_date )
		    ||('StanHome_Presta' == $high_level_group &&  date_conversion($event->display('date_lancement_chaud_dd')) > $current_date )
		    ){  ?>
					<?php } else { ?>
			<li>
				<?php display_binary_icons($event, 'E');
					  //display_diffusion_icons($event);
				?>
				<?php 
					$cycles_periodes = cycles_periodes_suffixe($event->field('semaine_debut'), $event->field('annee'), $event->field('semaine_fin'), $event->field('annee_fin'), $event->field('permalink'));
					$event_url = get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event->field('id')
				?>
				<input type="image" src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>" onclick="previsualisation_entite('<?php echo $event_url?>')">
				<a 
				<?php if ($event->display('lancement_chaud') == 'Yes' ) {  ?>
				 style="color: red;" <?php 
					} ?> href="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event->field('id')?>"><?php echo $event->display('designation'); ?>
					<?php echo ' : ' . $cycles_periodes;?>
				</a>
			</li>
			<?php }   ?>
		<?php 
					}
				}
			} ?>
		</ul>
	</li>

</ul>
<?php 
	}

	######################Docs#######################
	$cycles_docs = Pods('cycles_docs', array('where' => 'id_cycle='.$currentId, 'limit' => "-1", 'orderby' => "t.id_doc ASC"));
	$total_cycles_docs = $cycles_docs->total();
	if ($total_cycles_docs > 0) {
?>
<ul id="tree_sous_docs" class="treeview_red">
	<li class="open"><span>Documents</span>
		<ul class="level2">
		<?php 
			while ($cycles_docs->fetch()) {
				$doc = Pods('documents', array('where' => "id=" . $cycles_docs->field('id_doc') . $where.$where_extension));
				$total_doc = $doc->total();
				if ($total_doc > 0) {
					while ($doc->fetch()) {
		?>
			<li>
				<?php display_binary_icons($doc, 'D');
					  if ($doc->display('is_mail') == 'Yes') { ?>
					<img src="<?php bloginfo('template_directory'); echo '/images/etat/mail.png' ?>">
				<?php }
					  $doc_url = get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc->field('id')
				?>
				<input type="image" src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>" onclick="previsualisation_entite('<?php echo $doc_url?>')">
				<a <?php if ($doc->display('lancement_chaud') == 'Yes') { ?> style="color: red;" <?php } ?> href="<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc->field('id')?>"><?php echo $doc->display('reference_document') . ' : ' . $doc->display('designation'); ?>
				<?php 
					$cycles_periodes = cycles_periodes_suffixe($doc->field('semaine_debut'), $doc->field('annee'), $doc->field('semaine_fin'), $doc->field('annee_fin'), $doc->field('permalink'));
					echo ' : ' . $cycles_periodes;
				?>
				</a>
			</li>
		<?php 
					}
				}
			} ?>
		</ul>
	</li>
</ul>
<?php } ?>



</div>
<div class="splitter-panel" id="secondsplit2" style="overflow: auto;position: relative;z-index:200;"></div>
</div>
<div class="prev_loader">
	<img src="<?php bloginfo('template_directory'); echo '/images/ajax-loader.gif' ?>">
</div>

<script type="text/javascript">
( function( $ ) {
	
	$("#tree_sous_events").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	$("#tree_sous_docs").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	$("#tree_sous_promos").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	$("#tree_sous_events_parallel").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	$("#tree_sous_docs_parallel").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	$("#tree_sous_promos_parallel").treeview({
		animated: "fast",
		collapsed: true,
		control: "#treecontrol",
		toggle: function() {
			window.console && console.log("%o was toggled", this);
		}
	});

	//$('#mainSplitter').jqxSplitter({ orientation: 'horizontal', panels: [{ size: 500 }, { size: 0}] });
	//$('#mainSplitter2').jqxSplitter({ orientation: 'horizontal', panels: [{ size: 500 }, { size: 0}] });
	
})( jQuery );
</script>