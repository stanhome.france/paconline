<?php
require_once 'include-functions.php';

global $post;
$page_name = $post->post_name;
$page_name = explode('-', $page_name);
$page_type = $page_name[0];

###########################Pages création d'une nouvelle entité##############################
if (in_array($page_type, array('nouveau', 'nouvel', 'nouvelle'))) {
	$prefix_id = get_prefix_id($page_name[1]);
?>
	<tr>
		<td align="right"><label>Attacher à</label></td>
		<td>
			<select id="<?php echo $prefix_id?>_attach_list">
				<option value="cycles">Cycles</option>
			</select>
		</td>
	</tr>
<?php
}

###########################Pages d'édition d'une entité##############################
if ($page_type == 'edit') {
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
	$prefix_id = get_prefix_id($page_name[1])
?>
	<tr>
		<td align="right"><label>Attacher à</label></td>
		<td>
			<select id="<?php echo $prefix_id?>_attach_list" disabled="disabled">
				<option value="cycles" <?php if (preg_match("#^cycles-{0,1}[0-9]*$#", $current_obj->permalink)) echo 'selected'?> >Cycles</option>
			</select>
		</td>
	</tr>
<?php	
}
