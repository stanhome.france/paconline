<?php 
require_once 'cycles/functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'cycles/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 9) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}

$annee_en_cours = date('Y');

$options_numeros_cycles = get_liste_numeros_cycles();
$options_numeros_periodes = get_liste_numeros_periodes();

?>

<form id="save_new_cycle_form" action="" method="get">
	<input type="hidden" id="is_entite" value="<?php echo isset($_GET['entite']) ? $_GET['entite'] : ''?>">
	<input type="hidden" id="title_page" value="<?php if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Nouvelle période'?>">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">
					<?php if (isset($_GET['entite']) && $_GET['entite'] == 'cycle'):?>
						Nouveau Cycle
					<?php endif;?>
					<?php if (isset($_GET['entite']) && $_GET['entite'] == 'periode'):?>
						Nouvelle période
					<?php endif;?>
					</h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="2"></td></tr>
			<tr>
				<td align="right">
					<?php if (isset($_GET['entite']) && $_GET['entite'] == 'cycle'):?>
						<label>Année du cycle</label>
					<?php endif;?>
					<?php if (isset($_GET['entite']) && $_GET['entite'] == 'periode'):?>
						<label>Année de période</label>
					<?php endif;?>
				</td>
				<td>
					<select id="cycle_annee_cycle" >
						<option value="">Choisir...</option>
						<?php for ($i = -1; $i < 2; $i++):?>
						<?php $annee = (int)$annee_en_cours + $i?>
						<option value="<?php echo $annee?>" ><?php echo $annee?></option>
						<?php endfor;?>
					</select>
					<div class="clearfix"></div>
					<span id="error_annee_cycle" class="red"></span>
				</td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr id="tr_num_cycle">
				<td align="right"><label>Numéro de cycle</label></td>
				<td>
					<select id="cycle_numero">
						<option value="">Choisir...</option>
						<?php if (count($options_numeros_cycles) > 0):?>
							<?php foreach ($options_numeros_cycles as $kle_cycle => $val_cycle):?>
						<option value="<?php echo $kle_cycle?>"><?php echo $val_cycle?></option>
							<?php endforeach;?>
						<?php endif;?>
					</select>
				</td>
				<td><label id="error_numero"></label></td>
			</tr>
			<tr>
				<td align="right"><label>Identifiant</label></td>
				<td><input type="text" id="cycle_identifiant" class="long"/></td>				
			</tr>
			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center" class="brd_right">Date fin</th>
						</tr>
						<tr>
							<th><label>Commerciale</label></th>
							<td align="center"><input type="text" id="cycle_debut_commerciale"></td>
							<td align="center" class="brd_right"><input type="text" id="cycle_fin_commerciale"></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_commerciale" class="red"></span></td></tr>
					</table>
				</td>
				<td valign="top">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="100%">
						<tr>
							<th align="center" colspan="2" class="brd_right">Diffusion aux DR/DD</th>
						</tr>
						<tr>
							<td>DR : <input type="text" id="cycle_diffusion_dr" /></td>
							<td class="brd_right">DD : <input type="text" id="cycle_diffusion_dd" /></td>
						</tr>
						<tr><td class="error" colspan="2"><span id="eroor_diffusion" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr><td></td><td><label id="info"></label></td></tr>
		</table>
	</div>
</form>
<?php
$pod_cible = 'Cycles';
$id_pod_cible = '';
$content_comment_dd = '';
$content_comment_dr = '';

include_once 'comment-bloc.php';

$type_entity = 'C';
include_once('attachments/attachment-doc-creation.php');
?>

<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript">
( function( $ ) {

	/*
	 * Les champs dates
	 */
	$("#cycle_date_debut").datepicker();
	$("#cycle_date_fin").datepicker();
	$("#cycle_diffusion_dr").datepicker();
	$("#cycle_diffusion_dd").datepicker();
	$("#cycle_debut_commerciale").datepicker();
	$("#cycle_fin_commerciale").datepicker();
	$("#cycle_date_lancement_chaud_dd").datepicker();
	$("#cycle_date_lancement_chaud_dr").datepicker();

	/*
	 * Changer automatiquement l'identifiant du cycle avec 
	 * la formule Année Cycle NÂ°
	 */
	$("#cycle_numero").bind('change', function() {
		if($(this).val() != '') {
			var annee = $("#cycle_annee_cycle").val();
			$("#cycle_identifiant").val(annee + ' Cycle ' + $("#cycle_numero option:selected").text());
		}
	});
	$("#cycle_annee_cycle").bind('change', function() {
		if($("#is_entite").val() == 'cycle' && $("#cycle_numero").val() != '') {
			var identifiant = $(this).val() + ' Cycle ' + $("#cycle_numero option:selected").text();
			if($("#cycle_identifiant").val() != identifiant) $("#cycle_identifiant").val(identifiant);
		}
		if($("#is_entite").val() == 'periode' && $("#periode_numero").val() != '') {
			var identifiant = $(this).val() + ' Période ' + $("#periode_numero option:selected").text();
			if($("#cycle_identifiant").val() != identifiant) $("#cycle_identifiant").val(identifiant);
		}
	});

	/*
	 * Récupérer la date de début de la semaine de début
	 */
	$("#cycle_semaine_debut").bind('change', function() {
		var annee = $("#cycle_annee").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=1';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#cycle_date_debut").val(msg );
						$("#cycle_debut_commerciale").val(msg);
				},
				error: function() {
					alert('error ajax!');
				}
			});
		}
	});

	/*
	 * Récupérer la date de fin de la semaine de fin
	 * et puis calculer la liste des semaines
	 */
	$("#cycle_semaine_fin").bind('change', function() {
		var annee = $("#cycle_annee_fin").val();
		if($(this).val() != '') {
			var data = 'semaine=' + $(this).val() + '&annee=' + annee + '&ajax=2';
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1')
						$("#cycle_date_fin").val(msg);
						$("#cycle_fin_commerciale").val(msg);
				},
				error: function() {
					alert('error ajax!');
				}
			});
		}
		if($(this).val() != '' && $("#cycle_semaine_debut").val() != '') {
			var data = 'semaine_fin=' + $(this).val() + '&semaine_debut=' + $("#cycle_semaine_debut").val() + '&ajax=6&';
			data += 'annee_fin=' + $("#cycle_annee_fin").val() + '&annee=' + $("#cycle_annee").val();
			$.ajax({
				url: '',
				type: 'get',
				data: data,
				success: function(msg) {
					if(msg != '-1') $("#liste_semaines").val(msg);
				},
				error: function() {
					alert('Error Ajax!');
				}
			});
		}
	});

	/*
	 * Afficher champs dates lancment à  chaud pour DD et DR quand 
	 * le boutton radio lancement à  chaud est coché
	 */
	$('input[type=radio][name=cycle_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});

	/*
	 * Selon le choix cycles ou periodes on affiche la liste numéro de cycle ou numéro de période
	 */
	$("#cycle_attach_list").change(function() {
		if($("#cycle_attach_list").val() == 'cycles') {
			$("#tr_num_cycle").show();
			$("#tr_num_periode").hide();
		} else {
			$("#tr_num_cycle").hide();
			$("#tr_num_periode").show();
		}
	});
	
	/*
	 * Submit du formulaire : Enregistrer
	 */
	String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
	
	$("#save_new_cycle_form").submit(function() {
		var error = false;
		var error_msg = 'Vous devez aussi saisir les champs suivants: \n';
		if($("#is_entite").val() == 'cycle') {
    		if($("#cycle_numero").val() == '' && $("#cycle_attach_list").val() == 'cycles') {
    			error_msg += "Le numéro du cycle \n";
    			error = true;
    		}
		}
		if($("#is_entite").val() == 'periode') {
			if($("#periode_numero").val() == '' && $("#cycle_attach_list").val() == 'periodes') {
			error_msg += "Le numéro de période \n";
			error = true;
			}
		}	
		if($("#cycle_semaine_debut").val() == '' || $("#cycle_date_debut").val() == '' || $("#cycle_annee").val() == '') {
			error_msg += "Année, Semaine et date de début \n";
			error = true;
		}
		if($("#cycle_semaine_fin").val() == '' || $("#cycle_date_fin").val() == ''  || $("#cycle_annee_fin").val() == '') {
			error_msg += "Année, Semaine et date de fin \n";
			error = true;
		}
		if($("#cycle_identifiant").val() == '') {
			error_msg += "L'identifiant du cycle \n";
			error = true;
		}
		if($("#cycle_diffusion_dr").val() == '' || $("#cycle_diffusion_dd").val() == '') {
			error_msg += "Les dates de diffusions DD/DR \n";
			error = true;
		}
		if($("#cycle_debut_commerciale").val() == '' || $("#cycle_fin_commerciale").val() == '') {
			error_msg += "Les dates commerciales de début et fin \n";
			error = true;
		}
		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dd").val() == ''){
			error_msg += "La date de lancement à  chaud DD \n";
			error = true;
		}
		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dr").val() == ''){
			error_msg += "La date de lancement à  chaud DR \n";
			error = true;
		}
		if(error) alert(error_msg);
		if(error) return false;

		var numero = '';
		if($("#is_entite").val() == 'cycle') {
			numero = $("#cycle_numero").val();
		}
		if($("#is_entite").val() == 'periode') {
			numero = $("#periode_numero").val();
		}
		var data = '';
		data += 'annee=' + $("#cycle_annee").val() + '&';
		data += 'annee_fin=' + $("#cycle_annee_fin").val() + '&';
		data += 'annee_du_cycle=' + $("#cycle_annee_cycle").val() + '&';
		data += 'cycle_ou_periode=' + $("#is_entite").val() + 's' + '&';
		data += 'numero=' + numero + '&';
		data += 'semaine_debut=' + $("#cycle_semaine_debut").val() + '&';
		data += 'date_debut=' + $("#cycle_date_debut").val() + '&';
		data += 'semaine_fin=' + $("#cycle_semaine_fin").val() + '&';
		data += 'date_fin=' + $("#cycle_date_fin").val() + '&';
		data += 'identifiant=' + encodeURIComponent($("#cycle_identifiant").val()) + '&';
		data += 'diffusion_dr=' + $("#cycle_diffusion_dr").val() + '&';
		data += 'diffusion_dd=' + $("#cycle_diffusion_dd").val() + '&';
		data += 'debut_commerciale=' + $("#cycle_debut_commerciale").val() + '&';
		data += 'fin_commerciale=' + $("#cycle_fin_commerciale").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=cycle_action_info]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=cycle_publie]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=cycle_lancement_chaud]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#cycle_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#cycle_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=C&';
		}
		data += 'ajax=3';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
			if(msg == 'cycle_existe'&& $("#is_entite").val() == 'cycle') alert('Le cycle existe déjà ');
			if(msg == 'cycle_existe'&& $("#is_entite").val() == 'periode') alert('La période existe déjà ');
			if(msg == 'cycle_existe') return false;
				if(msg >= 0) {
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					if($("#is_entite").val() == 'cycle') alert('Cycle Bien Ajouté');
					else alert('Période Bien Ajoutée');
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					window.setTimeout("redirect2DetailCycle("+msg+")", 2000);
				}
			},
			error: function() {
			}
		});
		return false;
	});

	/*
	 * Enregistrer un nouveau cycle et rester dans la meme page
	 */
	$("#save_new_next_cycle").bind('click', function() {

		var error = false;
		var error_msg = 'Vous devez aussi saisir les champs suivants: \n';
		if($("#cycle_numero").val() == '' && $("#cycle_attach_list").val() == 'cycles') {
			error_msg += "Le numéro du cycle \n";
			error = true;
		}
		if($("#periode_numero").val() == '' && $("#cycle_attach_list").val() == 'periodes') {
			error_msg += "Le numéro de période \n";
			error = true;
		}
		if($("#cycle_semaine_debut").val() == '' || $("#cycle_date_debut").val() == '' || $("#cycle_annee").val() == '') {
			error_msg += "Année, Semaine et date de début \n";
			error = true;
		}
		if($("#cycle_semaine_fin").val() == '' || $("#cycle_date_fin").val() == ''  || $("#cycle_annee_fin").val() == '') {
			error_msg += "Année, Semaine et date de fin \n";
			error = true;
		}
		if($("#cycle_identifiant").val() == '') {
			error_msg += "L'identifiant du cycle \n";
			error = true;
		}
		if($("#cycle_diffusion_dr").val() == '' || $("#cycle_diffusion_dd").val() == '') {
			error_msg += "Les dates de diffusions DD/DR \n";
			error = true;
		}
		if($("#cycle_debut_commerciale").val() == '' || $("#cycle_fin_commerciale").val() == '') {
			error_msg += "Les dates commerciales de début et fin \n";
			error = true;
		}
		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dd").val() == ''){
			error_msg += "La date de lancement à  chaud DD \n";
			error = true;
		}
		if($('input[type=radio][name=cycle_lancement_chaud]:checked').val() == 1 && $("#cycle_date_lancement_chaud_dr").val() == ''){
			error_msg += "La date de lancement à  chaud DR \n";
			error = true;
		}
		if(error) alert(error_msg);
		if(error) return false;

		var numero = '';
		numero = $("#cycle_numero").val();
		var data = '';
		data += 'annee=' + $("#cycle_annee").val() + '&';
		data += 'annee_fin=' + $("#cycle_annee_fin").val() + '&';
		data += 'annee_du_cycle=' + $("#cycle_annee_cycle").val() + '&';
		data += 'numero=' + numero + '&';
		data += 'cycle_ou_periode=' + $("#is_entite").val() + 's' + '&';
		data += 'semaine_debut=' + $("#cycle_semaine_debut").val() + '&';
		data += 'date_debut=' + $("#cycle_date_debut").val() + '&';
		data += 'semaine_fin=' + $("#cycle_semaine_fin").val() + '&';
		data += 'date_fin=' + $("#cycle_date_fin").val() + '&';
		data += 'identifiant=' + encodeURIComponent($("#cycle_identifiant").val()) + '&';
		data += 'diffusion_dr=' + $("#cycle_diffusion_dr").val() + '&';
		data += 'diffusion_dd=' + $("#cycle_diffusion_dd").val() + '&';
		data += 'debut_commerciale=' + $("#cycle_debut_commerciale").val() + '&';
		data += 'fin_commerciale=' + $("#cycle_fin_commerciale").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=cycle_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=cycle_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=cycle_publie]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#cycle_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#cycle_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=C&';
		}
		data += 'ajax=3';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg == 'cycle_existe'&& $("#is_entite").val() == 'cycle') alert('Le cycle existe déjà ');
				if(msg == 'cycle_existe'&& $("#is_entite").val() == 'periode') alert('La période existe déjà ');
				if(msg == 'cycle_existe') return false;
				if(msg >= 0) {
					if($("#is_entite").val() == 'cycle') alert('Cycle Bien Ajouté');
					else alert('Période Bien Ajoutée');
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					$("#cycle_identifiant").val('');
					$("#liste_semaines").val('');
					$("#files_to_attach").val('');
					$(".doc_attach_element").remove();
					content = tinyMCE.get('mceEditor_dd').setContent('');
					content = tinyMCE.get('mceEditor_dr').setContent('');
				}
			},
			error: function() {
			}
		});
		return false;
	});

	//AIMED1504
		$('.delete_doc').unbind('click').live('click', function() {
			var id_doc = $(this).attr('id').replace('del*', '');
			var $complete_path = $(this).parent().find("a").attr('href');
			$filename = $(this).parent().find("a").html();
			var check = confirm('Voulez-vous vraiment supprimer ce document ?');
			if (check) {
				$(this).parent().remove();
					$.ajax({
							url: '',
							type: 'get',
							data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=9',
							success: function(msg) {
							},
							error: function() {
							}
						});
				}
		});
	
})( jQuery );

function redirect2DetailCycle(id_new_cycle)
{
	window.location = '<?php echo get_permalink(get_page_by_path('detail-cycle')) . '?id_cycle='?>'+id_new_cycle;
}
</script>