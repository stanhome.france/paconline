<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/resultat-recherche.js"></script>

<?php 
require_once 'include-functions.php';
require_once 'recherche/search_helper.php';
?>

<?php if(isset($_GET['searchTerms']) && $_GET['searchTerms']!=""):?>
	<?php 
		$query = $_GET['searchTerms'];
		//Débuts d'url d'accès aux entités
		$CycleUrlBeginning = get_option("siteurl")."/detail-cycle/?id_cycle=";
		$EventUrlBeginning = get_option("siteurl")."/details-evenment-cyclique/?id_event=";
		$DocumentUrlBeginning = get_option("siteurl")."/details-document-cyclique/?id_doc=";
		$PromotionUrlBeginning = get_option("siteurl")."/details-promotion-cyclique/?id_promo=";
	
		$displayCycles = isset($_GET['cycle']) && $_GET['cycle']=="true";
		$displayEvents = isset($_GET['event']) && $_GET['event']=="true";
		$displayDocuments = isset($_GET['document']) && $_GET['document']=="true";
		$displayPromotions = isset($_GET['promotion']) && $_GET['promotion']=="true";
	
		if(isset($_GET['archive']) && $_GET['archive']=="true"){
			$archive = 1;
		}
		else {
			$archive = 0;
		}
		
		//Dates
		$dateDebut = "";
		$dateFin ="";
			
		if(isset($_GET['dateDebut']) && $_GET['dateDebut'] != ""){
			$dateDebutObject = DateTime::createFromFormat("d/m/Y", $_GET['dateDebut']);
			$dateDebut = $dateDebutObject->format('Ymd');
		}
		if(isset($_GET['dateFin']) && $_GET['dateFin'] != ""){
			$dateFinObject = DateTime::createFromFormat("d/m/Y", $_GET['dateFin']);
			$dateFin = $dateFinObject->format('Ymd');
		}
		
		
		//Récupère les différentes entités correspondant au terme de la recherche
		if($displayCycles){
		    $cycles = searchCycles($query, $archive, $dateDebut, $dateFin);
		}
		if($displayEvents){
		    $events = searchEvents($query, $archive, $dateDebut, $dateFin);
		}
		if($displayDocuments){
		    $documents = searchDocuments($query, $archive, $dateDebut, $dateFin);
		}
		if($displayPromotions){
		    $promotions = searchPromotions($query, $archive, $dateDebut, $dateFin);
		}
		
		$attachments = searchAttachments($query);
?>

	<h3 class="ttl">Résultats de recherche pour "<?php echo $_GET['searchTerms'];?>"</h3>

	<div id="search_results">
		<ul>
			<!-- CYCLES -->
			<?php if($displayCycles):?>
				<li>Cycles<ul>
					<?php if($cycles):?>
						<?php 
						$intercale_cycle_color = 0; 
						while ($cycles->fetch()):
							//Alterner couleurs
							if ($intercale_cycle_color % 2 == 0) {
								$li_bg = "#e8e8e8";
							} else {
								$li_bg = "tranparent";
							}
							$intercale_cycle_color++;
						?>
							<?php 
								$cycleUrl = $CycleUrlBeginning.$cycles->field("id");
								$cycleTitle = $cycles->display("identifiant");
								$cycleDateObject = new DateTime($cycles->display("date_debut"));
								$cycleDateDisplay = $cycleDateObject->format('d/m/Y');
							?>
							<li style="background-color: <?php echo $li_bg;?>"><a href="<?php echo $cycleUrl;?>" title="Voir les détails de ce cycle"><?php echo $cycleDateDisplay;?> - <?php echo $cycleTitle;?> </a></li>
						<?php endwhile;?>
					<?php else:?>
					<li>Aucun résultat trouvé.</li>
				<?php endif;?>
				</ul>
				</li>
			<?php endif;?>
			<!-- CYCLES -->
			
					<!-- EVENEMENTS -->
			<?php if($displayEvents):?>
				<li>Evènements<ul>
				<?php if($events):?>
						<?php 
						$intercale_cycle_color = 0;
						while ($events->fetch()):
							//Alterner couleurs
							if ($intercale_cycle_color % 2 == 0) {
								$li_bg = "#e8e8e8";
							} else {
								$li_bg = "tranparent";
							}
							$intercale_cycle_color++;
						?>
							<?php 
								//Infos de cet événement
								$eventUrl = $EventUrlBeginning.$events->display("id");
								$eventTitle = $events->display("designation");
								$eventDateObject = new DateTime($events->display("date_debut"));
								$eventDateDisplay = $eventDateObject->format('d/m/Y');
					
								//Récupère les cycles associés à cet événement
								//$cyclesForThisEvent = getEventCycles($events->display("id"), $archive);
							?>
							<li style="background-color: <?php echo $li_bg;?>"><a href="<?php echo $eventUrl;?>" title="voir les détails de cet évènement"><?php echo $eventDateDisplay;?> - <?php echo $eventTitle;?> </a></li>
						<?php endwhile;?>
					<?php else:?>
					<li>Aucun résultat trouvé.</li>
				<?php endif;?>
				</ul>
				</li>
			<?php endif;?>
			<!-- EVENEMENTS -->
			
			<!-- DOCUMENTS -->
			<?php if($displayDocuments):?>
				<li>Documents<ul>
				<?php if($documents):?>
						<?php 
						$intercale_cycle_color = 0;
						while ($documents->fetch()):
							//Alterner couleurs
							if ($intercale_cycle_color % 2 == 0) {
								$li_bg = "#e8e8e8";
							} else {
								$li_bg = "tranparent";
							}
							$intercale_cycle_color++;
						?>
							<?php 
								//Infos de ce document
								$documentUrl = $DocumentUrlBeginning.$documents->display("id");
								$documentTitle = $documents->display("designation");
								$documentDateObject = new DateTime($documents->display("date_debut_lancement"));
								$documentDateDisplay = $documentDateObject->format('d/m/Y');
					
								//Récupère les cycles associés à ce document
								//$cyclesForThisDocument = getDocumentCycles($documents->display("id"), $archive);
							?>
							<li style="background-color: <?php echo $li_bg;?>"><a href="<?php echo $documentUrl;?>" title="Voir les détails de ce document"><?php echo $documentDateDisplay;?> - <?php echo $documentTitle;?> </a></li>
						<?php endwhile;?>
					<?php else:?>
					<li>Aucun résultat trouvé.</li>
				<?php endif;?>
				</ul>
				</li>
			<?php endif;?>
			<!-- DOCUMENTS -->
			
			<!-- PROMOTIONS -->
			<?php if($displayPromotions):?>
				<li>Promotions<ul>
				<?php if($promotions):?>
						<?php 
						$intercale_cycle_color = 0;
						while ($promotions->fetch()):
							//Alterner couleurs
							if ($intercale_cycle_color % 2 == 0) {
								$li_bg = "#e8e8e8";
							} else {
								$li_bg = "tranparent";
							}
							$intercale_cycle_color++;
						?>
							<?php 
								//Infos de cette promotion
								$promotionUrl = $PromotionUrlBeginning.$promotions->display("id");
								$promotionTitle = $promotions->display("reference_extranet").' : '.$promotions->display("description");
								$promotionDateObject = new DateTime($promotions->display("date_debut_lancement"));
								$promotionDateDisplay = $promotionDateObject->format('d/m/Y');
					
								//Récupère les cycles associés à cette promotion
								//$cyclesForThisPromotion = getPromotionCycles($promotions->display("id"), $archive);
							?>
							<li style="background-color: <?php echo $li_bg;?>"><a href="<?php echo $promotionUrl;?>" title="Voir les détails de cette promotion"><?php echo $promotionDateDisplay;?> - <?php echo $promotionTitle;?> </a></li>
						<?php endwhile;?>
					<?php else:?>
					<li>Aucun résultat trouvé.</li>
				<?php endif;?>
				</ul>
				</li>
			<?php endif;?>
			<!-- PROMOTIONS -->
			
			<!-- DOCUMENTS ATTACHES -->
			<!-- 
			<li>Documents attachés<ul>-->
			<?php if($attachments):?>
					<?php 
					$intercale_cycle_color = 0; 
					while ($attachments->fetch()):
						//Alterner couleurs
						if ($intercale_cycle_color % 2 == 0) {
							$li_bg = "#e8e8e8";
						} else {
							$li_bg = "tranparent";
						}
						$intercale_cycle_color++;
					?>
						<?php 
							//Infos de ce document attaché
							$attachmentUrl = $attachments->display("complete_path");
							$attachmentTitle = basename($attachmentUrl);
							$entityId = $attachments->display("id_entity");
							$entityType = $attachments->display("type");
				
							//Récupère l'entité à laquelle ce doc est attaché
							$entityAttachedTo = getAttachmentEntity($entityType, $entityId);
						?>
						<li  style="background-color: <?php echo $li_bg;?>">
							<a href="<?php echo $attachmentUrl; ?>" target="_blank" title="Voir le document">
								<img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle"/>
								<?php echo $attachmentTitle; ?>
							</a>
							<span class="grayLabel">- Attaché <?php echo $entityAttachedTo['type'];?></span> <a href="<?php echo $entityAttachedTo['url'].$entityId;?>" title="Voir les détails de l'entité"><?php echo $entityAttachedTo['title'];?></a>
						</li>
					<?php endwhile;?>
				<?php else:?>
				<li>Aucun résultat trouvé.</li>
			<?php endif;?>
			 
			</ul>
			</li>
			<!-- DOCUMENTS ATTACHES -->
		</ul>
	</div>
<?php else:?>
	<h3 class="ttl">Recherche impossible</h3>
	Veuillez entrer un terme de recherche.
<?php endif;?>
