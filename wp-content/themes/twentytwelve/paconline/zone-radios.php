<?php
require_once 'include-functions.php';

global $post;
$page_name = $post->post_name;
$page_name = explode('-', $page_name);
$page_type = $page_name[0];
###########################Pages création d'une nouvelle entité##############################
if (in_array($page_type, array('nouveau', 'nouvel', 'nouvelle'))) {
	$prefix_id = get_prefix_id($page_name[1]);
	?>
	<td align="right" class="grey_area">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td><label>Action Info :</label></td>
				<td class="radio" align="left" >
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_action_info" value="1" >
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_action_info" value="0" checked="checked">
				</td>
				<td><label id="eroor_action_info"></label></td>
			</tr>
			<tr>
				<td><label>Publié :</label></td>
				<td class="radio" align="left" >
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_publie" value="1" checked="checked">
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_publie" value="0">
				</td>
				<td><label id="eroor_action_info"></label></td>
			</tr>
			<?php if ($prefix_id != 'cycle'):?>
			<tr>
				<td><label>Lancement à chaud :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_lancement_chaud" value="1" >
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_lancement_chaud" value="0" checked="checked">
				</td>
				<td align="left"><label id="eroor_lancement_chaud"></label></td>
			</tr>
			<tr style="display: none;" id="lancment_chaud_dd">
				<td><label>Date Lancement à chaud DD</label></td>
				<td align="left"><input type="text" id="<?php echo $prefix_id?>_date_lancement_chaud_dd"></td>
				<td align="left"><label id="eroor_lancement_chaud_dd"></label></td>
			</tr>
			<tr style="display: none;" id="lancment_chaud_dr">
				<td><label>Date Lancement à chaud DR</label></td>
				<td align="left"><input type="text" id="<?php echo $prefix_id?>_date_lancement_chaud_dr"></td>
				<td align="left"><label id="eroor_lancement_chaud_dr"></label></td>
			</tr>
			<?php endif;?>
			<?php 
				if ($prefix_id == 'document'):
			?>
			<tr>
				<td><label>Mailing :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_is_mail" value="1" >
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_is_mail" value="0" checked="checked">
				</td>
				<td align="left"><label id="eroor_is_mail"></label></td>
			</tr>
			<?php endif;?>
		</table>
	</td>
	<?php 
}

###########################Pages d'édition d'une entité##############################
if ($page_type == 'edit') {
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
	$prefix_id = get_prefix_id($page_name[1]);
	?>
	<td align="right" class="grey_area">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td align="right" width="56%"><label>Publié :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_publie" value="1" <?php if ($current_obj->publie == 1) echo 'checked'?>>
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_publie" value="0" <?php if ($current_obj->publie == 0) echo 'checked'?>>
					<span id="eroor_action_info" class="red"></span>
				</td>
			</tr>				
			<tr>
				<td align="right"><label>Action Info :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_action_info" value="1" <?php if ($current_obj->action_info == 1) echo 'checked'?>>
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_action_info" value="0" <?php if ($current_obj->action_info == 0) echo 'checked'?>>
					<div class="clearfix"></div>
					<span id="eroor_action_info" class="red"></span>
				</td>
			</tr>
			<?php if ($prefix_id != 'cycle'):?>
			<tr>
				<td align="right" ><label class="red">Lancement à chaud :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_lancement_chaud" value="1" <?php if ($current_obj->lancement_chaud == 1) echo 'checked'?>>
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_lancement_chaud" value="0" <?php if ($current_obj->lancement_chaud == 0) echo 'checked'?>>
					<div class="clearfix"></div>
					<span id="eroor_lancement_chaud" class="red"></span>
				</td>
			</tr>
			<tr <?php if ($current_obj->lancement_chaud == 0): ?>style="display: none;"<?php endif;?> id="lancment_chaud_dd">
				<td align="right"><label>Date Lancement à chaud DD :</label></td>
				<td align="left">
					<input type="text" id="<?php echo $prefix_id?>_date_lancement_chaud_dd" <?php if ($current_obj->lancement_chaud == 1):?> value="<?php echo pod2js_date($current_obj->date_lancement_chaud_dd) ?>" <?php endif;?>>
					<div class="clearfix"></div>
					<span id="eroor_lancement_chaud_dd" class="red"></span>
				</td>
			</tr>
			<tr <?php if ($current_obj->lancement_chaud == 0):?>style="display: none;" <?php endif;?>id="lancment_chaud_dr">
				<td align="right"><label>Date Lancement à chaud DR :</label></td>
				<td align="left">
					<input type="text" id="<?php echo $prefix_id?>_date_lancement_chaud_dr" <?php if ($current_obj->lancement_chaud == 1):?> value="<?php echo pod2js_date($current_obj->date_lancement_chaud_dr) ?>" <?php endif;?>>
					<div class="clearfix"></div>
					<span id="eroor_lancement_chaud_dr" class="red"></span>
				</td>
			</tr>
			<?php endif;?>
			<?php 
				if ($prefix_id == 'document'):
			?>
			<tr>
				<td align="right"><label>Mailing :</label></td>
				<td class="radio" align="left">
					<label>Oui</label><input type="radio" name="<?php echo $prefix_id?>_is_mail" value="1" <?php if ($current_obj->is_mail == 1) echo 'checked'?>>
					<label>Non</label><input type="radio" name="<?php echo $prefix_id?>_is_mail" value="0" <?php if ($current_obj->is_mail == 0) echo 'checked'?>>
				</td>
				<td><label id="eroor_is_mail"></label></td>
			</tr>
			<?php endif;?>
		</table>
	</td>
	<?php
}

###########################Pages de visualisation d'une entité#############################
if ($page_type == 'detail' || $page_type == 'details') { 
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
    $prefix_id = get_prefix_id($page_name[1]);
?>
	<td align="right" <?php echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]class="grey_area"[/groups_member]' ); ?>>
	<?php
	$to_display_part = '<table cellpadding="0" cellspacing="0" width="100%">'; 
	$to_display_part .= '<tr>';
	$to_display_part .= '<td align="right" width="56%"><label>Publié :</label></td>';
	$to_display_part .= '<td align="left">'.($current_obj->publie == 1 ? 'Oui' : 'Non').'</td>';
	$to_display_part .= '</tr>';				
	$to_display_part .= '<tr>';
	$to_display_part .= '<td  align="right"><label>Action Info :</label></td>';
	$to_display_part .= '<td align="left">'.($current_obj->action_info == 1 ? 'Oui' : 'Non').'</td>';
	$to_display_part .= '</tr>';
	$to_display_part .= '<tr>';
	if ($prefix_id != 'cycle') {
	$to_display_part .= '<td align="right"><label class="red">Lancement à chaud :</label></td>';
	$to_display_part .= '<td align="left">'.($current_obj->lancement_chaud == 1 ? 'Oui' : 'Non').'</td>';
	$to_display_part .= '</tr>';
	if ($current_obj->lancement_chaud == 1) {
		$to_display_part .= '<tr>';
		$to_display_part .= '<td align="right" ><label class="red">Date Lancement à chaud DD:</label></td>';
		$to_display_part .= '<td align="left">'.pod2js_date($current_obj->date_lancement_chaud_dd).'</td>';
		$to_display_part .= '</tr>';
		$to_display_part .= '<tr>';
		$to_display_part .= '<td align="right" ><label class="red">Date Lancement à chaud DR:</label></td>';
		$to_display_part .= '<td align="left">'.pod2js_date($current_obj->date_lancement_chaud_dr).'</td>';
		$to_display_part .= '</tr>';
	}
	}
	$to_display_part .= '<tr>';
	$to_display_part .= '</tr>';
	if ($prefix_id == 'cycle') {
		$to_display_part .= '<tr>';
		$to_display_part .= '<td align="right"><label>Archivé :</label></td>';
		$to_display_part .= '<td align="left">'.($current_obj->archive == 1 ? 'Oui' : 'Non').'</td>';
		$to_display_part .= '</tr>';
	}
	if ($prefix_id == 'document') {
		$to_display_part .= '<tr>';
		$to_display_part .= '<td  align="right"><label>Mailing :</label></td>';
		$to_display_part .= '<td align="left">'.($current_obj->is_mail == 1 ? 'Oui' : 'Non').'</td>';
		$to_display_part .= '</tr>';
	}
	$to_display_part .= '</table>';
	$to_display_part .= '</td>';
	echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
} ?>