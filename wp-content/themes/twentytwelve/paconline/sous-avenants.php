<?php
include_once('cycles/functions.php');
$cycle_id = $_GET['cycle_id'];
$permalink = $_GET['permalink'];
?>

<?php
$avenants = array();
$pod_cycles = Pods('cycles', array('where' => "id=$cycle_id and archive=0", 'limit' => '-1'));
if ($pod_cycles->total() > 0) {
	while ($pod_cycles->fetch()) {
		$cid = $pod_cycles->field('id'); ?>
<!-- avenants du cycle -->
		<?php
		$avenants[$cid] = array();
		$pod_avenants_cycle = Pods('avenants', array('where' => 'type_entite=\'C\' AND id_entite='.$cid, 'limit' => '-1'));
		if ($pod_avenants_cycle->total() > 0) {
			$avenants[$cid]['cycle'] = $cid;
		}
		?>

<!-- avenants de promo -->
		<?php
		$pod_cycles_promos = Pods('promotions', array('join' => 'LEFT JOIN wp_pods_cycles_promos ON wp_pods_cycles_promos.id_promo = t.id',
																	  'where' => 'wp_pods_cycles_promos.id_cycle='.$cid.' and t.permalink=\''.$permalink.'\'', 'limit' => '-1'));
		while ($pod_cycles_promos->fetch()) {
			$pod_avenants_promo = Pods('avenants', array('where' => 'type_entite=\'P\' AND id_entite='.$pod_cycles_promos->field('id'), 'limit' => '-1'));
			if ($pod_avenants_promo->total() > 0) {
				$avenants[$cid]['promo'][] = $pod_cycles_promos->field('id');
			}
		}
		?>

<!-- avenants de events -->
		<?php
		$pod_cycles_events = Pods('events', array('join' => 'LEFT JOIN wp_pods_cycles_events ON wp_pods_cycles_events.id_event = t.id',
																  'where' => 'wp_pods_cycles_events.id_cycle='.$cid.' and t.permalink=\''.$permalink.'\'', 'limit' => '-1'));
		while ($pod_cycles_events->fetch()) {
			$pod_avenants_event = Pods('avenants', array('where' => 'type_entite=\'E\' AND id_entite='.$pod_cycles_events->field('id'), 'limit' => '-1'));
			if ($pod_avenants_event->total() > 0) {
				$avenants[$cid]['event'][] = $pod_cycles_events->field('id');
			}
		}
		?>

<!-- avenants de docs -->
		<?php
		$pod_cycles_docs = Pods('documents', array('join' => 'LEFT JOIN wp_pods_cycles_docs ON wp_pods_cycles_docs.id_doc = t.id',
																   'where' => 'wp_pods_cycles_docs.id_cycle='.$cid.' and t.permalink=\''.$permalink.'\'', 'limit' => '-1'));
		while ($pod_cycles_docs->fetch()) {
			$pod_avenants_doc = Pods('avenants', array('where' => 'type_entite=\'D\' AND id_entite='.$pod_cycles_docs->field('id'), 'limit' => '-1'));
			if ($pod_avenants_doc->total() > 0) {
				$avenants[$cid]['doc'][] = $pod_cycles_docs->field('id');
			}
		}
	}
}
?>

<!--  affichage des avenants par cycle -->

<?php
foreach ($avenants as $id_cycle => $avenant) {
	$cycle = Pods('cycles', $id_cycle);
	$identifiant = $cycle->display('identifiant');
	if (count($avenants[$id_cycle]) > 0) {
		?>
<?php if (isset($avenant['cycle'])) {
	$avenants_cycle = Pods('avenants', array('where' => 'type_entite=\'C\' AND id_entite='.$avenant['cycle'], 'limit' => '-1'));
	if ($avenants_cycle->total() > 0) {
		while ($avenants_cycle->fetch()) {
			?>
	<li><a
		href="<?php echo get_permalink(get_page_by_path('detail-cycle')) . '?id_cycle=' . $avenant['cycle']?>"><?php echo $identifiant . ' - ' . $avenants_cycle->display('creation_date') . ' - ' . $avenants_cycle->display('auteur') . ' - ' . stripslashes($avenants_cycle->display('content'))?></a></li>
		<?php 		}
	}
}
if (isset($avenant['promo'])) { ?>
	<li>Promotions
	<ul>
	<?php foreach ($avenant['promo'] as $promo) {
		$promo_pod = Pods('promotions', $promo);
		$avenants_promo = Pods('avenants', array('where' => 'type_entite=\'P\' AND id_entite='.$promo, 'limit' => '-1'));
		if ($avenants_promo->total() > 0) {
			while ($avenants_promo->fetch()) {
				?>
		<li><a
			href="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $promo?>"><?php echo $identifiant . ' - ' . stripslashes($promo_pod->display('reference_extranet').' : '.$promo_pod->display('description')) . ' - ' . $avenants_promo->display('creation_date') . ' - ' . $avenants_promo->display('auteur') . ' - ' . stripslashes($avenants_promo->display('content'))?></a></li>
			<?php 	}
		}
	}
	?>
	</ul>
	</li>
	<?php }
	if (isset($avenant['event'])) { ?>
	<li>Events
	<ul>
	<?php foreach ($avenant['event'] as $event) {
		$event_pod = Pods('events', $event);
		$avenants_event = Pods('avenants', array('where' => 'type_entite=\'E\' AND id_entite='.$event, 'limit' => '-1'));
		if ($avenants_event->total() > 0) {
			while ($avenants_event->fetch()) {
				?>
		<li><a
			href="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event?>"><?php echo $identifiant . ' - ' . stripslashes($event_pod->display('designation')) . ' - ' . $avenants_event->display('creation_date') . ' - ' . $avenants_event->display('auteur') . ' - ' . stripslashes($avenants_event->display('content'))?></a></li>
			<?php 	}
		}
	} ?>
	</ul>
	</li>
	<?php }
	if (isset($avenant['doc'])) { ?>
	<li>Documents
	<ul>
	<?php foreach ($avenant['doc'] as $doc) {
		$doc_pod = Pods('documents', $doc);
		$avenants_doc = Pods('avenants', array('where' => 'type_entite=\'D\' AND id_entite='.$doc, 'limit' => '-1'));
		if ($avenants_doc->total() > 0) {
			while ($avenants_doc->fetch()) {
				?>
		<li><a
			href="<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc?>"><?php echo $identifiant . ' - ' . stripslashes($doc_pod->display('designation')) . ' - ' . $avenants_doc->display('creation_date') . ' - ' . $avenants_doc->display('auteur') . ' - ' . stripslashes($avenants_doc->display('content'))?></a></li>
			<?php 	}
		}
	}
	?>
	</ul>
	</li>
	<?php }?>
	<?php 	} else {
		?>
		<li style="color: red;">Pas d'avenant pour ce cycle</li>
		<?php }
}
?>