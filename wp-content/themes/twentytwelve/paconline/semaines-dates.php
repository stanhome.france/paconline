<?php
require_once 'include-functions.php';

global $post;
$page_name = $post->post_name;
$page_name = explode('-', $page_name);
$page_type = $page_name[0];

###########################Pages création d'une nouvelle entité##############################
if (in_array($page_type, array('nouveau', 'nouvel', 'nouvelle'))) {
	$prefix_id = get_prefix_id($page_name[1]);
?>
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><label>Semaine de début</label></td>
				<td  valign="top">
					<select id="<?php echo $prefix_id?>_annee">
							<?php for ($i = -1; $i < 2; $i++):?>
						<option value="<?php echo (int)$annee_en_cours + $i?>" <?php if ($annee_en_cours == (int)$annee_en_cours + $i) echo "selected";?>><?php echo (int)$annee_en_cours + $i?></option>
							<?php endfor;?>
					</select>
				</td>
				<td valign="top">
					<select id="<?php echo $prefix_id?>_semaine_debut">
						<option value="">Choisir...</option>
						<?php for ($i = 1; $i <= 53; $i++):?>
						<option value="<?php echo $i?>">S<?php echo $i?></option>
						<?php endfor;?>
					</select>
				</td>
				<?php if (in_array($prefix_id, array('cycle', 'event'))):?><td><input type="text" id="<?php echo $prefix_id?>_date_debut"></td><?php endif;?>
				<td><label id="error_semaine_debut"></label></td>
			</tr>
			<tr>
				<td><label>Semaine de fin</label></td>
				<td  valign="top">
					<select id="<?php echo $prefix_id?>_annee_fin">
							<?php for ($i = -1; $i < 2; $i++):?>
						<option value="<?php echo (int)$annee_en_cours + $i?>" <?php if ($annee_en_cours == (int)$annee_en_cours + $i) echo "selected";?>><?php echo (int)$annee_en_cours + $i?></option>
							<?php endfor;?>
					</select>
				</td>
				<td  valign="top">
					<select id="<?php echo $prefix_id?>_semaine_fin">
						<option value="">Choisir...</option>
						<?php for ($i = 1; $i <= 53; $i++):?>
						<option value="<?php echo $i?>">S<?php echo $i?></option>
						<?php endfor;?>
					</select>
				</td>
				<?php if (in_array($prefix_id, array('cycle', 'event'))):?><td><input type="text" id="<?php echo $prefix_id?>_date_fin"></td><?php endif;?>
				<td><label id="error_semaine_fin"></label></td>
			</tr>
		</table>
<?php
}

###########################Pages d'édition d'une entité##############################
if ($page_type == 'edit') {
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
     	
?>
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><label>Semaine de début</label></td>
				<td>
					<select id="<?php echo $prefix_id?>_annee">
							<?php for ($i = -1; $i < 2; $i++):?>
								<?php $annee = (int)$annee_en_cours + $i?>
						<option value="<?php echo $annee?>" <?php if ($current_obj->annee  == $annee) echo 'selected'?>><?php echo (int)$annee_en_cours + $i?></option>
							<?php endfor;?>
					</select>
					<div class="clearfix"></div>
					<span id="error_annee" class="red"></span>
				</td>
				<td>
					<select id="<?php echo $prefix_id?>_semaine_debut">
						<option value="">Choisir...</option>
						<?php for ($i = 1; $i <= 53; $i++):?>
						<option value="<?php echo $i?>" <?php if ($current_obj->semaine_debut  == $i) echo 'selected'?>>S<?php echo $i?></option>
						<?php endfor;?>
					</select>
				</td>
				<?php if (in_array($prefix_id, array('cycle', 'event'))):?>
				<td>
					<input type="text" id="<?php echo $prefix_id?>_date_debut"  value="<?php echo pod2js_date($current_obj->date_debut ) ?>">
					<div class="clearfix"></div>
					<span id="error_semaine_debut" class="red"></span>
				</td>
				<?php endif;?>
			</tr>
			<tr>
				<td><label>Semaine de fin</label></td>
				<td>
					<select id="<?php echo $prefix_id?>_annee_fin">
							<?php for ($i = -1; $i < 2; $i++):?>
								<?php $annee = (int)$annee_en_cours + $i?>
						<option value="<?php echo $annee?>" <?php if ($current_obj->annee_fin  == $annee) echo 'selected'?>><?php echo (int)$annee_en_cours + $i?></option>
							<?php endfor;?>
					</select>
				</td>
				<td>
					<select id="<?php echo $prefix_id?>_semaine_fin">
						<option value="">Choisir...</option>
						<?php for ($i = 1; $i <= 53; $i++):?>
						<option value="<?php echo $i?>" <?php if ($current_obj->semaine_fin  == $i) echo 'selected'?>>S<?php echo $i?></option>
						<?php endfor;?>
					</select>
				</td>
				<?php if (in_array($prefix_id, array('cycle', 'event'))):?>
				<td>
					<input type="text" id="<?php echo $prefix_id?>_date_fin"  value="<?php echo pod2js_date($current_obj->date_fin ) ?>">
					<div class="clearfix"></div>
					<span id="error_semaine_debut" class="red"></span>
				</td>
				<?php endif;?>
			</tr>
		</table>
<?php	
}

###########################Pages de visualisation d'une entité##############################
if ($page_type == 'detail' || $page_type == 'details') {
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
    ?>
	<table cellpadding="0" cellspacing="0">	
		<tr>
			<td><label>Semaine de début:</label></td>
			<td>
				<select disabled="disabled">
					<option><?php echo $current_obj->annee ?></option>
				</select>
			</td>
			<td>
				<select disabled="disabled">
					<option value="">Choisir...</option>
					<?php for ($i = 1; $i <= 53; $i++):?>
					<option value="<?php echo $i?>" <?php if ($current_obj->semaine_debut  == $i) echo 'selected'?>>S<?php echo $i?></option>
					<?php endfor;?>
				</select>
			</td>
			<?php if (in_array($prefix_id, array('cycle', 'event'))):?>
				<td>
					<input type="text" value="<?php echo pod2js_date($current_obj->date_debut ) ?>" disabled="disabled">
					<div class="clearfix"></div>
				</td>
			<?php endif;?>
		</tr>
		<tr>
			<td><label>Semaine de fin:</label></td>
			<td>
				<select disabled="disabled">
					<option><?php echo $current_obj->annee_fin ?></option>
				</select>
			</td>
			<td>
				<select id="cycle_semaine_fin" disabled="disabled">
					<option value="">Choisir...</option>
					<?php for ($i = 1; $i <= 53; $i++):?>
					<option value="<?php echo $i?>"  <?php if ($current_obj->semaine_fin  == $i) echo 'selected'?>>S<?php echo $i?></option>
					<?php endfor;?>
				</select>
			</td>
			<?php if (in_array($prefix_id, array('cycle', 'event'))):?>
				<td>
					<input type="text" value="<?php echo pod2js_date($current_obj->date_fin ) ?>" disabled="disabled">
					<div class="clearfix"></div>
				</td>
			<?php endif;?>
		</tr>
	</table>
<?php
}
