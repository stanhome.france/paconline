<?php
require_once 'include-functions.php';

global $post;
$page_name = $post->post_name;
$page_name = explode('-', $page_name);
$page_type = $page_name[0];

###########################Pages création d'une nouvelle entité##############################
if (in_array($page_type, array('nouveau', 'nouvel', 'nouvelle')) && $page_name[1] != 'article') {
	$prefix_id = get_prefix_id($page_name[1]);
	$where_to_back = 'cycles';
	if ($prefix_id == 'mail') $where_to_back = 'mails';
	$where_to_back2 = '';
	if (isset($_GET['entite']) && $_GET['entite'] == 'periode') $where_to_back2 = '?entite=periode';
	if ($where_to_back == 'cycles') {
		if ($where_to_back2 != '') {
			$where_to_back2 .= '&filtre=all';
		} else {
			$where_to_back2 .= '?filtre=all';
		}
	}
	
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Revenir" onclick="confirm_before_leave('<?php echo get_permalink(get_page_by_path($where_to_back)) . $where_to_back2?>')">
		<?php 
		$to_display_part = '<input type="submit" value="Enregistrer">';
		if ($prefix_id != 'mail') {
			$to_display_part .= '<input type="button" id="save_new_next_'.$prefix_id.'" value="Enregistrer et passer au suivant" class="long">';
		}
		echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
		 ?>
		<div class="clearfix"></div>
	</div>
</div>
<?php 
}

###########################Pages d'édition d'une entité##############################
if ($page_type == 'edit' && $page_name[1] != 'article') {
    $current_obj = $data_current_cycle;
	$page_detail = get_prev_page_link($page_name[1]);
	$get_id = get_id_parameter($page_name[1])
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Revenir" onclick="confirm_before_leave('<?php echo get_permalink(get_page_by_path($page_detail)) . '?'.$get_id.'=' . $current_obj->id?>')">
		<?php echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]<input type="submit" value="Enregistrer">[/groups_member]' ); ?>
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

###########################Pages de visualisation d'une entité#############################
if (preg_match("#^detail[s]{0,1}$#", $page_type)) {
    
    $current_obj = null;
    switch ($page_name[1]) {
        case 'cycle':
            $current_obj = $data_current_cycle;
            break;
        case 'evenement':
            $current_obj = $data_current_event;
            break;
        case 'evenment':
            $current_obj = $data_current_event;
            break;
        case 'event':
            $current_obj = $data_current_event;
            break;
        case 'document':
            $current_obj = $data_current_doc;
            break;
        case 'promotion':
            $current_obj = $data_current_promo;
            break;
    }
	$prefix_id = get_prefix_id($page_name[1]);
	$table_name = get_table_name($prefix_id);
	$get_id = get_id_parameter($page_name[1]);
	$page_edit = get_edit_page_link($page_name[1]);
	$target = get_target_remarque_avenant($page_name[1]);
	$where_to_back = '?filtre=all';
?>
<div class="btn_group">
	<div class="btn_group1">
	
		<a href="<?php echo get_permalink(get_page_by_path('cycles')) . $where_to_back; ?>" class="lien">Revenir</a>
		<?php 
		$to_display_part = '<a href="'.get_permalink(get_page_by_path($page_edit)).'?'.$get_id.'='.$current_obj->id.'" class="lien">Modifier</a>';
		$to_display_part .= '<a href="#" id="delete_'.$prefix_id.'" class="lien">Supprimer</a>';
		$to_display_part .= '<a href="'.get_permalink(get_page_by_path('remarques')).'?id_entite='.$current_obj->id.'&type_entite='.$target.'" class="lien">Remarque</a>';
		
		
		if (isset($_GET[$get_id])) {
		    $pod = Pods($table_name, array('where' => "id=" .$_GET[$get_id]));     
			if ($pod->fetch()) {
				$to_display_part .= '<a href="'.get_permalink(get_page_by_path('avenants')).'?id_entite='.$current_obj->id.'&type_entite='.$target.'" class="lien">Avenant</a>';
			}
		}
		
		if (isset($_GET["id_cycle"])) {
		    $pod = Pods("cycles", array('where' => "id=" . $_GET['id_cycle'])); 
			if ($pod->fetch()) {
				if ($pod->display("diffusion") == "Non") {
					$to_display_part .= '<a href="#" id="diffusion_'.$_GET["id_cycle"].'" class="diffusion_btn lien">Diffusion</a>';
				}
			}
		}
		
		$to_display_part .= '<a href="javascript:void(0);" id="printer"  class="lien">Imprimer</a>';
		echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
		
		$to_display_part = '<a href="javascript:void(0);" id="printer"  class="lien">Imprimer</a>';
		echo do_shortcode( '[groups_member group="StanHome_ComitePAC"]'.$to_display_part.'[/groups_member]' );
		echo do_shortcode( '[groups_member group="StanHome_Presta"]'.$to_display_part.'[/groups_member]' );
		echo do_shortcode( '[groups_member group="StanHome_DR"]'.$to_display_part.'[/groups_member]' );
		echo do_shortcode( '[groups_member group="StanHome_DD"]'.$to_display_part.'[/groups_member]' );
		
		$to_display_part = '<a href="'.get_permalink(get_page_by_path('remarques')).'?id_entite='.$current_obj->id.'&type_entite='.$target.'" class="lien">Remarque</a>';
		echo do_shortcode( '[groups_member group="StanHome_ComitePAC"]'.$to_display_part.'[/groups_member]' );
		
// 		if ($page_name[1] == 'cycle') {
// 			$is_on = $current_obj->a_imprimer;
// 			if ($is_on == '0') $is_on = 'Off';
// 			else $is_on = 'On';
// 			$to_display_part = '<a id="toggle_agent_impression" href="javascript:void(0);" class="lien" style="font-size: 11px !important;width: 121px;">Agent d\'impression : <span id="print_on_off">' . $is_on . '</span></a>';
// 			echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
// 		}
		?>
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page des remarques####################################################
if ($page_type == 'remarques' && !isset($_POST['action'])) {
?>
<div class="btn_group">
	<div class="btn_group1">
		<a href="<?php echo $return_url; ?>" class="lien">Revenir</a>
		<input type="submit" value="Envoyer">
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page des remarques####################################################
if ($page_type == 'avenants' && !isset($_POST['action'])) {
?>
<div class="btn_group">
	<div class="btn_group1">
		<a href="<?php echo $return_url; ?>" class="lien">Revenir</a> 
		<input type="submit" value="Envoyer">
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page des articles####################################################
if ($page_type == 'articles') {
?>
<div class="btn_group">
	<div class="btn_group1">
		<a class="lien" href="<?php echo get_permalink(get_page_by_path('nouvel-article'))?>">Nouvel article</a>
	<!-- 	<a class="lien" href="#" id="btn_show_zone_recherche" onclick="show_zone_recherche();return false;">Recherche</a>
		<table id="tbl_area_search" style="display: none;"  cellpadding="0" cellspacing="0" >
		<tr>
			<td><input style="margin-left: 20px;" type="text" id="q_reference"></td>
			<td><input type="button" value="Valider" id="btn_valid_search"></td>
			<td><input type="button" value=Annuler id="btn_cancel_search"></td>
		</tr>
	</table>-->
	<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page edit article####################################################
if ($page_type == 'edit' && $page_name[1] == 'article') {
	$liste_articles = 'articles';
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Revenir" onclick="window.location='<?php echo get_permalink(get_page_by_path($liste_articles))?>'">
		<input type="submit" value="Enregistrer">		
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page nouvel article####################################################
if ($page_type == 'nouvel' && $page_name[1] == 'article') {
	$liste_articles = 'articles';
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Revenir" onclick="window.location='<?php echo get_permalink(get_page_by_path($liste_articles))?>'">
		<input type="submit" value="Enregistrer">
		<input type="button" id="save_new_next_article" value="Enregistrer et passer au suivant">
		<div class="clearfix"></div>
	</div>
</div>
<?php
}

#######################Page liste des mails####################################################
if ($page_type == 'mails') {
	$models_alloed_to_be_added = array();
	$models_pod = Pods('mails', array('limit' => '-1', 'where' => 'agents IN (\'impression\', \'diffusion\', \'remarques\', \'avenants\')'));
	if ($models_pod->total()) {
		while ($models_pod->fetch()) {
			$models_alloed_to_be_added[] = $models_pod->field('agents');
		}
	}
	if (count($models_alloed_to_be_added) < 4):	
?>
	<div class="btn_group">
		<div class="btn_group1">
			<input type="button" value="Nouveau" id="add_new_mail" onclick="window.location='<?php echo get_permalink(get_page_by_path('nouveau-mail'))?>'">
			<div class="clearfix"></div>
		</div>
	</div>
<?php
	endif;
}

#######################Page view/edit mail####################################################
if (implode('-', $page_name) == 'visu-mail') {
?>
	<div class="btn_group">
		<div class="btn_group1">
			<input type="button" value="Revenir" onclick="confirm_before_leave('<?php echo get_permalink(get_page_by_path('mails'))?>')">
			<input type="submit" value="Enregistrer" id="save_existed_mail">
			<input type="button" value="Supprimer" onclick="delete_mail_model(<?php echo $current_mail->display('id')?>)">
			<div class="clearfix"></div>
		</div>
	</div>
<?php 
}

?>
<script type="text/javascript">
function confirm_before_leave(where_to_back) {
	if(confirm('Si vous avez modifié vos données, enregistrez les avant de quiiter sinon elle seront perdues')) {
		window.location = where_to_back;
	}
}
</script>
