<script type="text/javascript">
    jQuery(document).ready(function ($) {
        
        $('#tree_diffusion').jqxTree({ height: '500px', width:'100%' });

    	$(".entite_diffusion_suppress_btn").jqxButton({ width: '100', height: '25'});
        $("#close_all").jqxButton({ width: '120', height: '35'});
        $("#open_all").jqxButton({ width: '120', height: '35'});
                
        $('.open_all').unbind('click').click(function() {
    		$('#tree_diffusion').jqxTree('expandAll');
    	});

    	$('.close_all').unbind('click').click(function() {
    		$('#tree_diffusion').jqxTree('collapseAll');
    	});

    	//Lancer l'impression des cycles en pdf manuellement
        $("#start_agent_impression").bind('click', function() {
    		var items = $("input[type=checkbox]:checked");
    		var items_to_print_now = '';
    		$.each(items, function() {
    			items_to_print_now += $(this).attr("id") + ',';
    		});
    		if(items_to_print_now == '') alert('Choisissez au moins un(e) cycles/période');
    		if(items_to_print_now == '') return false;
    		$(".prev_loader").show();
            $.ajax({
    			url: '',
    			type: 'get',
    			data: 'ajax=1&items='+items_to_print_now,
    			success: function(msg) {
    				alert('L\'impression est terminée');
    				$(".prev_loader").hide();
    			},
    			error: function() {
    			}
    		});
        });

        $('#tree_diffusion').on('select', function (event) {
    		var args = event.args;
    	    var item = $('#tree_diffusion').jqxTree('getItem', args.element);
    	    var elt = $('#' + item.element.id);
    	    if (elt.hasClass('diffusion')) {
    	    	elt.find('input.entite_diffusion_suppress_btn').unbind('click').click(function() {
    				var p = $(this).parent();
    				var id = p.children('.entiteId').val();
    				execute_ajax_command('id=' + id + '&action=supp_diff', 
    									 entite_diffusion_suppress_btn_success_method, 
										 $(this));
    	    	});
    	    }
		});

		function entite_diffusion_suppress_btn_success_method(msg, user_data) {
			alert('L\'entité a été retirée de la diffusion.');
			document.location.href = '../diffusion/';
		}
		
	});
</script>

<?php
require_once ('cycles/functions.php');

if (isset($_GET['action']) && $_GET['action'] == 'supp_diff') {
		require('diffusion/suppress_diffusion.php');
}
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'agent-impression/ajax.php';
	exit();
}

?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<input type="button" value="Ouvrir tout" class="open_all" id="open_all">
		<input type="button" value="Impression manuelle" style="height: 35px; width: 134px;" id="start_agent_impression">
		<div class="clearfix"></div>
	</div>
</div>	
<div class="frame">
	<h3 class="ttl">Les diffusions et impressions programmées</h3>
	<div id="tree_diffusion">
		<ul style="width:100%;">
		<?php
			$diffusions = Pods('cycles', array('where'=>'permalink like "cycles%" and archive=0 and diffusion=1 and diffusion_level < 40 and diffusion_level > 0'));
			$total = $diffusions->total();
			if ($total > 0) {
		?>
			<li>
				<div>Diffusions sur les cycles</div>
				<ul>
				<?php
					$intercale_diffusion_color = 0; 
					while ($diffusions->fetch()) { 
						if ($intercale_diffusion_color % 2 == 0) {
							$li_diffusion_bg = "#e8e8e8";
						} else {
							$li_diffusion_bg = "tranparent";
						}
						$intercale_diffusion_color++;
				?>
					<li class="diffusion">
						<div style="background-color:<?php echo $li_diffusion_bg; ?>; padding:5px; width:100%;">
						<?php if ($diffusions->display('action_info') == 'Yes') { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/computer-icon.png' ?>" title="Action informatique" alt="Action informatique">
						<?php }
							  if ($diffusions->display('diffusion') == 'Yes' and $diffusions->display('diffusion_level') >= 10 and $diffusions->display('diffusion_level') <= 30) { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon.png' ?>" title="En cours de diffusion" alt="En cours de diffusion">
						<?php }
							  if ($diffusions->display('diffusion') == 'Yes' and $diffusions->display('diffusion_level') == 40) { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon_on.png' ?>" title="Diffusé" alt="Diffusé">
						<?php }
							  if ($diffusions->display('diffusion') == 'No') { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon_off.png' ?>" title="Pas diffusé" alt="Pas diffusé">
						<?php } ?>
							<a href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$diffusions->field('id')?>"><?php echo stripSlashes($diffusions->display('identifiant')); ?></a>
							<input type='hidden' value='<?php echo $diffusions->field('id'); ?>' class='entiteId'/>
							<input type='button' value='Enlever' class='entite_diffusion_suppress_btn'/>
						</div>
					</li>
				<?php
					} 
				?>
				</ul>
			</li>
		<?php
			} 
			$diffusions = Pods('cycles', array('where'=>'permalink="periodes" and archive=0 and diffusion=1 and diffusion_level < 40 and diffusion_level > 0'));
			$total = $diffusions->total();
			if ($total > 0) {
		?>
			<li>
				<div>Diffusions sur les périodes</div>
				<ul>
				<?php
					$intercale_diffusion_color = 0; 
					while ($diffusions->fetch()) { 
						if ($intercale_diffusion_color % 2 == 0) {
							$li_diffusion_bg = "#e8e8e8";
						} else {
							$li_diffusion_bg = "tranparent";
						}
						$intercale_diffusion_color++;
				?>
					<li class="diffusion">
						<div style="background-color:<?php echo $li_diffusion_bg; ?>; padding:5px; width:100%;">
						<?php if ($diffusions->display('action_info') == 'Yes') { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/computer-icon.png' ?>" title="Action informatique" alt="Action informatique">
						<?php }
							  if ($diffusions->display('diffusion') == 'Yes' and $diffusions->display('diffusion_level') == 10) { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon.png' ?>" title="En cours de diffusion" alt="En cours de diffusion">
						<?php }
							  if ($diffusions->display('diffusion') == 'Yes' and $diffusions->display('diffusion_level') == 40) { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon_on.png' ?>" title="Diffusé" alt="Diffusé">
						<?php }
							  if ($diffusions->display('diffusion') == 'No') { ?>
							<img src="<?php bloginfo('template_directory'); echo '/images/etat/diffusion-icon_off.png' ?>" title="Pas diffusé" alt="Pas diffusé">
						<?php } ?>
							<a href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$diffusions->field('id')?>"><?php echo stripSlashes($diffusions->display('identifiant')); ?></a>
							<input type='hidden' value='<?php echo $diffusions->field('id'); ?>' class='entiteId'/>
							<input type='button' value='Enlever' class='entite_diffusion_suppress_btn'/>
						</div>
					</li>
				<?php
					} 
				?>
				</ul>
			</li>
		<?php
			} 
		?>
		<?php 
			$cycles_a_imprimer = Pods('cycles', array('where'=>'permalink=\'cycles\' AND a_imprimer=1 and archive = 0', 'orderby' => 't.identifiant', 'limit' => "-1"));
			if ($cycles_a_imprimer->total() > 0):
		?>
			<li>
				<div>Cycles en attente d'impression</div>
				<ul>
				<?php 
					$intercale_impression_color = 0; 
					while ($cycles_a_imprimer->fetch()): 
						if ($intercale_impression_color % 2 == 0) {
							$li_impression_bg = "#e8e8e8";
						} else {
							$li_impression_bg = "tranparent";
						}
						$intercale_impression_color++;
				?>
					<li>
						<div style="background-color:<?php echo $li_impression_bg; ?>; padding:5px; width:100%;">
						<input type="checkbox" id="<?php echo $cycles_a_imprimer->field('id')?>"/>
						<a href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$cycles_a_imprimer->field('id')?>"><?php echo stripSlashes($cycles_a_imprimer->display('identifiant')); ?></a>
						</div>
					</li>
				<?php 
					endwhile;
				?>	
				</ul>
			</li>
		<?php endif;?>
		<?php 
			$periodes_a_imprimer = Pods('cycles', array('where'=>'permalink=\'periodes\' AND a_imprimer=1 and archive = 0', 'orderby' => 't.identifiant', 'limit' => "-1"));
			if ($periodes_a_imprimer->total() > 0):
		?>
			<li>
				<div>Périodes en attente d'impression</div>
				<ul>
				<?php 
					$intercale_impression_color = 0; 
					while ($periodes_a_imprimer->fetch()): 
						if ($intercale_impression_color % 2 == 0) {
							$li_impression_bg = "#e8e8e8";
						} else {
							$li_impression_bg = "tranparent";
						}
						$intercale_impression_color++;
				?>
					<li>
						<div style="background-color:<?php echo $li_impression_bg; ?>; padding:5px; width:100%;">
						<input type="checkbox" id="<?php echo $periodes_a_imprimer->field('id')?>"/>
						<a href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$periodes_a_imprimer->field('id')?>"><?php echo stripSlashes($periodes_a_imprimer->display('identifiant')); ?></a>
						</div>
					</li>
				<?php 
					endwhile;
				?>	
				</ul>
			</li>
		<?php endif;?>
		</ul>
	</div>
</div>
<div class="prev_loader">
	<img src="<?php bloginfo('template_directory'); echo '/images/ajax-loader.gif' ?>">
</div>