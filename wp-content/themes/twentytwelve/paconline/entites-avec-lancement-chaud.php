<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$('#tree_lancements_chauds').jqxTree({ height: '500px', width:'100%' });

		$("#close_all").jqxButton({ width: '120', height: '35'});
        $("#open_all").jqxButton({ width: '120', height: '35'});
                
        $('.open_all').unbind('click').click(function() {
    		$('#tree_lancements_chauds').jqxTree('expandAll');
    	});

    	$('.close_all').unbind('click').click(function() {
    		$('#tree_lancements_chauds').jqxTree('collapseAll');
    	});
	});
</script>

<?php require_once 'include-functions.php';?>
<?php 
	$where_lancement_chaud = ' AND lancement_chaud=1';
?>
<div class="btn_group">
	<div class="btn_group1">
		<input type="button" value="Fermer tout" class="close_all" id="close_all">
		<input type="button" value="Ouvrir tout" class="open_all" id="open_all">
		<div class="clearfix"></div>
	</div>
</div>
<div class="frame">
	<h3 class="ttl">Les entités avec lancement à chaud</h3>
	<?php 
		$lancements_chauds = array();
		$pod_cycles = Pods('cycles', array('where' => 'archive=0', 'limit' => '-1'));
		if ($pod_cycles->total() > 0):
	?>
		<?php while ($pod_cycles->fetch()):?>
			<?php 
				$lancements_chauds[$pod_cycles->field('id')] = array();
			?>
			
			<!-- Lancements à chaud des promotions -->
			<?php 
				$pod_cycles_promos = Pods('cycles_promos', array('where' => 'id_cycle='.$pod_cycles->field('id'), 'limit' => '-1'));
				if ($pod_cycles_promos->total() > 0):
			?>
				<?php while ($pod_cycles_promos->fetch()):?>
					<?php 
						$pod_promos = Pods('promotions', array('where' => 'id='.$pod_cycles_promos->field('id_promo').' AND permalink=\''.$pod_cycles->field('permalink').'\''.$where_lancement_chaud, 'limit' => '-1'));
						if ($pod_promos->total() > 0):
					?>
						<?php while ($pod_promos->fetch()):?>
							<?php $lancements_chauds[$pod_cycles->field('id')]['promo'][] = $pod_promos->field('id')?>
						<?php endwhile;?>
					<?php endif;?>
				<?php endwhile;?>
			<?php endif;?>
			
			<!-- Lancements à chaud des events -->
			<?php 
				$pod_cycles_events = Pods('cycles_events', array('where' => 'id_cycle='.$pod_cycles->field('id'), 'limit' => '-1'));
				if ($pod_cycles_events->total() > 0):
			?>
				<?php while ($pod_cycles_events->fetch()):?>
					<?php 
						$pod_events = Pods('events', array('where' => 'id='.$pod_cycles_events->field('id_event').' AND permalink=\''.$pod_cycles->field('permalink').'\''.$where_lancement_chaud, 'limit' => '-1'));
						if ($pod_events->total() > 0):
					?>
						<?php while ($pod_events->fetch()):?>
							<?php $lancements_chauds[$pod_cycles->field('id')]['event'][] = $pod_events->field('id')?>
						<?php endwhile;?>
					<?php endif;?>
				<?php endwhile;?>
			<?php endif;?>
			
			<!-- Lancements à chaud des docs -->
			<?php 
				$pod_cycles_docs = Pods('cycles_docs', array('where' => 'id_cycle='.$pod_cycles->field('id'), 'limit' => '-1'));
				if ($pod_cycles_docs->total() > 0):
			?>
				<?php while ($pod_cycles_docs->fetch()):?>
					<?php 
						$pod_docs = Pods('documents', array('where' => 'id='.$pod_cycles_docs->field('id_doc').' AND permalink=\''.$pod_cycles->field('permalink').'\''.$where_lancement_chaud, 'limit' => '-1'));
						if ($pod_docs->total() > 0):
					?>
						<?php while ($pod_docs->fetch()):?>
							<?php $lancements_chauds[$pod_cycles->field('id')]['doc'][] = $pod_docs->field('id')?>
						<?php endwhile;?>
					<?php endif;?>
				<?php endwhile;?>
			<?php endif;?>
			
		<?php endwhile;?>
	<?php endif;?>
	<?php
		print_r('<pre>');
		//var_dump($lancements_chauds);
		print_r('</pre>');
	?>
	<div id="tree_lancements_chauds">
		<ul style="width: 100%;">
		<?php foreach ($lancements_chauds as $cycle_cle => $items):?>
			<?php if (count($items) > 0):?>
			<li>
				<?php 
					$pod_cycle = Pods('cycles', $cycle_cle);
					$identifiant = $pod_cycle->display('identifiant');
					echo $identifiant;
				?>
				<ul>
					<?php if (isset($items['promo']) && count($items['promo'])):?>
						<li>
							Promotions
							<ul>
								<?php 
									foreach ($items['promo'] as $promo):
										$pod_promo = Pods('promotions', $promo);
										$pod_promo->fetch();
										$reference_extranet = $pod_promo->display('reference_extranet').' : '.$pod_promo->display('description');
										$dd = formatDate2print($pod_promo->field('date_lancement_chaud_dd'));
										$dr = formatDate2print($pod_promo->field('date_lancement_chaud_dr'));
								?>
								<li>
									<a href="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $promo?>" style="color: red;">
										<?php echo stripslashes($reference_extranet) . ' - DD: ' . $dd . ' | DR: ' . $dr?>
										<?php diffuse_dd_dr($pod_promo->field('diffusion_level'))?>
									</a>
								</li>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endif;?>
					<?php if (isset($items['event']) && count($items['event'])):?>
						<li>
							Evénements
							<ul>
								<?php 
									foreach ($items['event'] as $event):
										$pod_event = Pods('events', $event);
										$pod_event->fetch();
										$reference_extranet = $pod_event->display('designation');
										$dd = formatDate2print($pod_event->field('date_lancement_chaud_dd'));
										$dr = formatDate2print($pod_event->field('date_lancement_chaud_dr'));
								?>
								<li>
									<a href="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $event?>" style="color: red;">
										<?php echo stripslashes($reference_extranet) . ' - DD: ' . $dd . ' | DR: ' . $dr?>
										<?php diffuse_dd_dr($pod_event->field('diffusion_level'))?>
									</a>
								</li>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endif;?>
					<?php if (isset($items['doc']) && count($items['doc'])):?>
						<li>
							Documents
							<ul>
								<?php 
									foreach ($items['doc'] as $doc):
										$pod_doc = Pods('documents', $doc);
										$pod_doc->fetch();
										$reference_extranet = $pod_doc->display('designation');
										$dd = formatDate2print($pod_doc->field('date_lancement_chaud_dd'));
										$dr = formatDate2print($pod_doc->field('date_lancement_chaud_dr'));
								?>
								<li>
									<a href="<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $doc?>" style="color: red;">
										<?php echo stripslashes($reference_extranet) . ' - DD: ' . $dd . ' | DR: ' . $dr?>
										<?php diffuse_dd_dr($pod_doc->field('diffusion_level'))?>
									</a>
								</li>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endif;?>
				</ul>
			</li>
			<?php endif;?>
		<?php endforeach;;?>
		</ul>
	</div>
</div>