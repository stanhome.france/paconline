<link rel="stylesheet" type="text/css" href="<?php echo includes_url('js/jquery/jquery.treeview/jquery.treeview.css') ?>" />
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.treeview/jquery.treeview.js') ?>"></script>
<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'cycles/ajax.php';
	exit();
}

$annee_en_cours = date('Y');
$options_numeros_cycles = get_liste_numeros_cycles();

$marques = get_liste_marque();

if (!isset($_GET['id_cycle']) || $_GET['id_cycle'] == '') { ?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();
} else {
     
    $current_cycle = Pods('cycles', array('where' => "id=" . $_GET['id_cycle']));
    $data_current_cycle = $current_cycle->data();
    $data_current_cycle = $data_current_cycle[0]; 

?>
<?php 
	if (is_null($data_current_cycle)) {
?>
<h3 class="ttl">Cycle Introuvable</h3>
<?php 	exit();
	} else {
		$pod_cible = 'Cycles';
		$id_pod_cible = $_GET['id_cycle'];
		
		############################récupération commentaires##################### 
		$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
		$content_comment_dd = '';
		$content_comment_dr = '';
		if ($commentaires_dd->total() > 0)
			$content_comment_dd = $commentaires_dd->display('content');
			
		$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
		$content_comment_dr = '';
		if ($commentaires_dr->total() > 0)
			$content_comment_dr = $commentaires_dr->display('content');
		##########################################################################//FIN TODO
?>


<form>
	<?php if (!isset($_GET['is_prev']) && !isset($_GET['print'])):?><?php require_once 'barre-actions.php';?><?php endif;?>
	<div class="frame">
	<input type="hidden" id="is_on" value="<?php echo $data_current_cycle->a_imprimer?>" />
	<input type="hidden" id="title_page" value="<?php if ($data_current_cycle->permalink == 'periodes') echo 'Détail Période'?>">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td valign="top" colspan="2">
					<?php 
						$titre = '';
						
						if ( $data_current_cycle->permalink == 'cycles') $titre = 'Cycle';
						elseif ($data_current_cycle->permalink == 'periodes') $titre = 'Période';
					?>
					<h2 class="ttl">Détail <?php echo $titre?> : <span class="red"><?php echo $data_current_cycle->identifiant?></span></h2>
				</td>
				<?php require('zone-radios.php'); ?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Identifiant :</label></td>
				<td><?php echo $data_current_cycle->identifiant?></td>
				<td rowspan="2" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr><td colspan="2"></td></tr>
			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center" class="brd_right">Date fin</th>
						</tr>
						<tr>
							<th>Commerciale</th>
							<td align="center"><?php echo pod2js_date($data_current_cycle->date_debut_commerciale)?></td>
							<td align="center" class="brd_right"><?php echo pod2js_date($data_current_cycle->date_fin_commerciale)?></td>								
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>							
					</table>
				</td>
				<td>
					<?php 
					$to_display_part = '<table cellpadding="0" cellspacing="0" class="tab_commerc" width="100%">';
					$to_display_part .= '<tr>';
					$to_display_part .= '<th align="center" colspan="2" class="brd_right">Diffusion aux DR/DD</th>';
					$to_display_part .= '</tr>';
					$to_display_part .= '<tr>';
					$to_display_part .= '<td>DR : '.pod2js_date($data_current_cycle->diffusion_dr).'</td>';
					$to_display_part .= '<td class="brd_right">DD : '.pod2js_date($data_current_cycle->diffusion_dd).'</td>';
					$to_display_part .= '</tr>';
					$to_display_part .= '<tr><td class="error" colspan="3"></td></tr>';
					$to_display_part .= '</table>';
					echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
					?>
				</td>
			</tr>
			<tr><td colspan="3"><span id="info"></span></td></tr>
		</table>
	</div>
</form>
	
<?php 	require_once 'entites-attachees.php' ;
	}
}
?>

<?php include_once 'comment-bloc.php';?>

<!-- Ici, la visualisation des documents attachés et les remarques -->
<?php
	$my_entity_id = $data_current_cycle->id;
	$type_entity = 'C';
	include_once("attachments/attachment-doc-visu.php"); 
	include_once("remarques/remarques-visu.php"); 
	include_once("avenants/avenants-visu.php"); 
	?>
<script type="text/javascript">
( function( $ ) {
	/*
	 * Suppression d'un cycle
	 */
	$("#delete_cycle").bind('click', function() {
		if(confirm("Etes vous certain de vouloir supprimer ce cycle?")) {
			$.ajax({
				url: '',
				data: 'id_cycle= ' + <?php echo $_GET['id_cycle']?> + '&ajax=5',
				type: 'get',
				success: function(msg) {
					switch(msg) {
						case '-1':
							$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '0':
							$("#info").html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '1':
							$("#info").html('Cycle supprimé!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							window.location = '<?php echo get_permalink(get_page_by_path('cycles')) ?>';
							break;
					}
				},
				error: function() {
				}
			});
		}
		return false;
	});

	/*
	 * Activer ou désactiver l'agent d'impression pour le cycle ou la période courante
	 */
	$("#toggle_agent_impression").bind('click', function() {
		var is_on = $("#is_on").val();
		$.ajax({
			url: '',
			data: 'ajax=8&id_cycle_periode=<?php echo $data_current_cycle->id?>&is_on='+is_on,
			type: 'get',
			success: function(msg) {
				if(msg == '1') {
					$("#is_on").val(msg);
					$("#print_on_off").html('On');
				}
				else if (msg == '0') {
					$("#is_on").val(msg);
					$("#print_on_off").html('Off');
				}
				else alert('Un problème est survenu!!');
			},
			error: function() {
				
			}
		});
	});

	// handler du bouton diffusion
	$('.diffusion_btn').unbind('click').click(function() {
		var id = $(this).attr('id').replace('diffusion_', '');
		$.ajax({
			url: '',
			data: 'id_cycle=' + id + '&ajax=7',
			type: 'get',
			success: function(msg) {
				switch(msg) {
					case '-1':
						$("#info").html('Erreur. L\'identifiant est inexistant.').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						break;
					case '1':
						$("#info").html('Cycle en cours de diffusion').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						$('.diffusion_btn').hide();
						break;
					default:
						$("#info").html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				}
			}
		});
});

})( jQuery );
</script>