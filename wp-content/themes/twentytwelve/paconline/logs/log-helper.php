<?php
//Retourne le chemin du fichier log stanhome
function getLogFilePath($log_path_file=false){
	if (!$log_path_file) $log_path_file = get_template_directory().'/log/stanhome-log.log';
	if(!file_exists($log_path_file)){
		$handle = fopen($log_path_file, "w");
		fclose($handle);
	}
	return $log_path_file;
}

//Ecrit à la suite du fichier log stanhome
function sh_write_to_log($content, $log_path_file=false){
	$now = new DateTime();
	$nowToString = $now->format("d/m/Y H:i:s");
	$content = $nowToString." ".$content.PHP_EOL;
	file_put_contents(getLogFilePath($log_path_file), $content, FILE_APPEND);
}

//Vide le contenu du fichier log
function sh_purge_log($log_path_file=false){
	file_put_contents(getLogFilePath($log_path_file), "");
}

//Lit le contenu du fichier log stanhome
function sh_read_from_log($log_path_file=false){
	$content = file_get_contents(getLogFilePath($log_path_file));
	if($content == ""){
		return "Aucun log.";
	}
	else{
		$htmlFormatedContent = "";
		$contentArray = explode(PHP_EOL, $content);
		foreach ($contentArray as $contentElement) {
			$htmlFormatedContent .= $contentElement."<br/>";
		}
		return $htmlFormatedContent;
	}
}