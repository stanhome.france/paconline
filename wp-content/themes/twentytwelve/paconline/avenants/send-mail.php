<?php 
	$current_mail = Pods('mails', array('where' => 'agents=\'avenants\''));
	$current_mail->fetch();
	
	global $wpdb;
	$requete = "SELECT name FROM wp_groups_group WHERE name <> 'Registered'";
	$names_groups = $wpdb->get_results($requete);
?>
<form id="mail_to_cellule_pac" action="" method="post">
	<?php require_once get_template_directory().'/paconline/barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Envoyer un mail</h2>
				</td>
			</tr>
			<tr>
				<td width="100px;"><label>Objet du Mail</label></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><input id="mail_objet" name="mail_objet" style="width:81%;" type="text" value="<?php echo stripslashes($current_mail->display('objet'))?>"></td>
			</tr>
			<tr>
				<td width="100px;"><label>Destinataires</label></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<select name="mail_destination[]" multiple="multiple">
						<?php foreach ($names_groups as $groupe):?>
							<option value="<?php echo $groupe->name;?>"><?php echo $groupe->name;?></option>
						<?php endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td><label>Contenu du Mail</label></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><textarea id="mail_contenu" name="mail_contenu" cols="100"><?php echo stripslashes($current_mail->display('contenu'))?></textarea></td>
			</tr>
		</table>
	</div>
	<input type="hidden" id="return_url" name="return_url" value="<?php echo $_POST['return_url']?>">
	<input type="hidden" id="confirm_return_url" name="confirm_return_url" value="yes">
</form>

<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>
<script type="text/javascript">
	( function( $ ) {
		tinyMCE.init({
			height: "500",
	        mode : "exact",
	        elements : "mail_contenu",
	        language : "fr",
	        entity_encoding : "raw",
	        plugins : "paste,table",
	        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
	        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
	        theme_advanced_buttons3 : "tablecontrols"
		});
	})( jQuery );
</script>