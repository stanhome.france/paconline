<?php 
	require_once dirname(dirname(__FILE__)) . '/include-functions.php';
	$remarques = Pods('avenants', array('where' => 'id_entite = '.$my_entity_id.' and type_entite="'.$type_entity.'"'));
	if ($remarques->fetch()) {
?>
<p class="ttl">Les avenants</p>
<?php $i = 1; ?>
<div class="frame">
	<table cellpadding="0" cellspacing="0" class="tab_commerc" width="100%">
		<tr>
			<th align="center">Numéro</th>
			<th align="center">Date</th>
			<th align="center">Auteur</th>
			<th align="center" class="brd_right">Avenant</th>
		</tr>
		<?php 
			do {
				$creation_date= $remarques->display('creation_date');
				
		?>
		<tr>
			<td align="center"><?php echo $i?></td>
			<td align="center"><?php echo $creation_date?></td>
			<td align="center"><?php echo $remarques->display('auteur')?></td>
			<td align="left" class="brd_right"><?php echo stripSlashes($remarques->display('content'))?></td>
		</tr>
		<?php
				$i++;
			} while ($remarques->fetch())
		?>
		<tr>
			<td colspan="4" class="error"></td>
		</tr>
	</table>
</div>
<?php } ?>