<?php
/*
 * Envoyer un avenant
*/


if(isset($_POST['action']) && $_POST['action'] == 'send_comment') {
	// ajouter l'avenent dans la base
	$pod = Pods('avenants');
	
	$auteur = wp_get_current_user();
	$auteur = $auteur->user_login;
	$data = array('id_entite' => $_POST["id_entite"],
				  'type_entite' => $_POST["type_entite"],
				  'creation_date' => Date("Y-m-d"),
				  'content' => $_POST["comment"],
				  'auteur' => $auteur);
	$new_item_id = $pod->add($data);
	
	// Envoyer un mail à la cellule PAC
	$destinations = array();
	foreach ($_POST['mail_destination'] as $destination) {
		$destinations[] = $destination;
	}

	$subject = $_POST['mail_objet'] . ': ' . appendToSubject($_POST["type_entite"], $_POST["id_entite"]);
	$subject = 'AVENANT ' . $subject;
	$content = '"AVENANT"<br/>' . stripslashes($_POST['mail_contenu']);
	$destinations_dd = array('StanHome_DD');
	$destinations_dr = array('StanHome_DR');
	$destinations_finales = array();
	$remove_dd_dr = removeMailAvenantDdDr($_POST["type_entite"], $_POST["id_entite"]);
	if($remove_dd_dr[0]) {
		foreach($destinations as $item) {
			if($item != 'StanHome_DR') $destinations_finales[] = $item;
		}
	}
	if($remove_dd_dr[1]) {
		foreach($destinations_finales as $item) {
			if($item != 'StanHome_DD' && !in_array($item, $destinations_finales)) $destinations_finales[] = $item;
		}
	}
	$from_infos = get_email_and_name_by_login($auteur);
	send_mail_avenant(stripslashes($subject), $content, $destinations_finales, $from_infos);
}
?>