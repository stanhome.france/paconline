<?php
require_once get_template_directory().'/paconline/include-functions.php';

$current_mail = Pods('mails', array('where' => 'agents=\'avenants\''));
$current_mail->fetch();

global $wpdb;
$requete = "SELECT name FROM wp_groups_group WHERE name <> 'Registered'";
$names_groups = $wpdb->get_results($requete);

/*
 * Ce code est résérvé seulement pour les requetes AJAX
* dans ce cas seulement ce code PHP est éxécuté
*/
if (isset($_POST['action']) && $_POST['action'] == "send_comment") {
	require_once 'avenants/send_comment.php';
?>
	<script type="text/javascript">
		window.location.href = "<?php echo $_POST['return_url'];?>";
	</script>
<?php
} else {
	// ************************************************************************
	// Préparation de la page qui suit
	// ************************************************************************
	switch($_GET['type_entite']) {
		case 'C':
			$cycle = Pods("cycles", array('where' => 'id = '.$_GET['id_entite']));
			$cycle->fetch();
			$designation = $cycle->display('identifiant');
			$txt = "le cycle ".$designation;
			$return_url = get_permalink(get_page_by_path('detail-cycle/')).'?id_cycle='.$_GET['id_entite'];
			break;
		case 'P':
			$promotion = Pods('promotions', array('where' => 'id = '.$_GET['id_entite']));
			$designation = "";
			$txt = "la promotion ".$designation;
			$return_url = get_permalink(get_page_by_path('details-promotion-cyclique/')).'?id_promo='.$_GET['id_entite'];
			break;
		case 'E':
			$event = Pods("events", array('where' => 'id = '.$_GET['id_entite']));
			$event->fetch();
			$designation = $event->display('designation');
			$txt = "l'évènement ".$designation;
			$return_url = get_permalink(get_page_by_path('details-evenment-cyclique/')).'?id_event='.$_GET['id_entite'];
			break;
		case 'D':
			$document = Pods("documents", array('where' => 'id = '.$_GET['id_entite']));
			$document->fetch();
			$designation = $document->display('designation');
			$txt = "le document ".$designation;
			$return_url = get_permalink(get_page_by_path('details-document-cyclique/')).'?id_doc='.$_GET['id_entite'];
			break;
		case 'R':
			$designation = "";
			$txt = "la période ".$designation;
			break;
		default:
			$designation = "";
			$txt = "une entité inconnue ".$designation;
	}
?>

<form action="" method="post">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<input type="hidden" name="id_entite" id="id_entite" value="<?php echo $_GET['id_entite']; ?>"/>
					<input type="hidden" name="type_entite" id="type_entite" value="<?php echo $_GET['type_entite']; ?>"/>
					<input type="hidden" name="return_url" id="return_url" value="<?php echo $return_url; ?>"/>
					<input type="hidden" name="action" id="action" value="send_comment"/>
					<h2 class="ttl">Avenant sur <?php  echo $txt; ?> </span></h2>
				</td>
			</tr>
			<tr>
				<td>Avenant</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<textarea cols=80 rows=5 class="comment" name="comment" id="comment"></textarea>
				</td>
			</tr>
			<tr>
				<td>Objet du Mail</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input id="mail_objet" name="mail_objet" style="width:81%;" type="text" value="<?php echo stripslashes($current_mail->display('objet'))?>">
				</td>
			</tr>
			<tr>
				<td>Destinataires</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<?php foreach ($names_groups as $groupe):?>
						<input type="checkbox" name="mail_destination[]" value="<?php echo $groupe->name;?>"> <?php echo $groupe->name;?> <br/>
					<?php endforeach;?>
				</td>
			</tr>
			<tr>
				<td>Contenu du Mail</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<textarea id="mail_contenu" name="mail_contenu" cols="100" ><?php echo stripslashes($current_mail->display('contenu'))?></textarea>
				</td>
			</tr>
		</table>
	</div>
</form>

<?php 
}
?>

<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>
<script type="text/javascript">
	( function( $ ) {
		tinyMCE.init({
			height: "500",
	        mode : "exact",
	        elements : "mail_contenu",
	        language : "fr",
	        entity_encoding : "raw",
	        plugins : "paste,table",
	        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
	        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
	        theme_advanced_buttons3 : "tablecontrols"
		});
	})( jQuery );
</script>