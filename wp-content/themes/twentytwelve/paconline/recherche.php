<?php
require_once 'include-functions.php';
require_once 'recherche/search_helper.php';
?>

<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/recherche.js"></script>

<h3 class="ttl">Recherche avancée</h3>

<form id="advancedSearchForm" action="" method="get" role="search">
	<!-- Ne pas afficher la search bar persistante -->
	<input type="hidden" name="noSearchBar" value="on"/>

	<input type="text" name="searchTerms" id="searchTerms" value="<?php if(isset($_GET['searchTerms'])) echo $_GET['searchTerms']; ?>" class="search_data" placeholder="Recherche...">
	<div class="filterContainer">
		<input type="hidden" id="is_search_terms" value="<?php echo isset($_GET['searchTerms']) ? 'oui' : 'non'?>">
		<div id="cycle" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['cycle']) && $_GET['cycle']=="true")) echo 'class="jqChecked"';?>">
			cycles
		</div>
		<div id="event" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['event']) && $_GET['event']=="true")) echo 'class="jqChecked"';?>">
			événements
		</div>
		<div id="document" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['document']) && $_GET['document']=="true")) echo 'class="jqChecked"';?>">
			documents
		</div>
		<div id="promotion" <?php if(!isset($_GET['searchTerms']) || (isset($_GET['promotion']) && $_GET['promotion']=="true")) echo 'class="jqChecked"';?>">
			promotions
		</div>
		<div id="archive" <?php if(isset($_GET['archive']) && $_GET['archive']=="true") echo 'class="jqChecked"';?>">
			archivés
		</div>
	</div>
	
	<div class="clearfix"></div>

	<div class="dateContainer">
		<label for="dateDebut">Date de début</label>
		<input type="text" name="dateDebut" id="dateDebut" <?php if(isset($_GET['dateDebut'])) echo 'value="'.$_GET['dateDebut'].'"'?>/>
		<label for="dateFin">Date de fin</label>
		<input type="text" name="dateFin" id="dateFin" <?php if(isset($_GET['dateFin'])) echo 'value="'.$_GET['dateFin'].'"'?>/>
	</div>
	
	<div>
		<input type="submit" id="searchsubmit" value="Lancer la recherche" title="Rechercher">
	</div>
</form>

<?php 
	if(isset($_GET['searchTerms'])) { 
		if (strlen($_GET['searchTerms']) > 2) { ?>
	<div id="search-result-container">
		<?php require_once 'resultat-recherche.php';?>
	</div>
<?php	} else { ?>
	<div id="search-result-container">
		Le terme à rechercher doit contenir plus de 3 caractères. La recherche ne peut pas être effectuée.
	</div>
<?php 	}
	} ?>

<script type="text/javascript">
( function( $ ) {
	if($("#is_search_terms").val() == 'oui') document.getElementById("searchTerms").focus();
})( jQuery );
</script>