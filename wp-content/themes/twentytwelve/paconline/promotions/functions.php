<?php
/**
 * Récupérer la liste des références
 * pour la séléction dans le formulaire de promotion
 */
function get_selection_reference() {
	$result = array();
	$pod_articles = Pods('articles', array('select' => '`t`.reference, `t`.designation', 'limit' => '-1'));
	if ($pod_articles->total() > 0) {
		while ($pod_articles->fetch()) {
			$result[] = array(
				'reference' => $pod_articles->display('reference'),
				'designation' => $pod_articles->display('designation')
			);
		}
	}
	return $result;
}

/**
 * Valider les lignes de la promo avant de les sauvegarder
 */
function valid_lignes_promo($composition) {
	$lignes = array();
	$promo = array();
	$result = array();
	$tab_prix_ttc = array();
	$qte = array();
	$scop = $_GET['scop'];
	$user = wp_get_current_user();
	$user = $user->user_login;
	$temp_composition = explode('][', $composition);
	unset($_SESSION[$user]);
	foreach ($temp_composition as $item) {
		$l = str_replace('[', '', $item);
		$l = str_replace(']', '', $l);
		$lignes[] = $l;
	}
	
	foreach ($lignes as $ligne) {
		$ligne = explode('|', $ligne);
		//$refs = explode(',', $ligne[3]);
		$data = array(
			'quantite' => $ligne[0],
			'ref_lot' => $ligne[1],
			'designation' => $ligne[2],
			'reference' => $ligne[3],
			'prix_n_ht' => $ligne[4],
			'prix_n_ttc' => $ligne[5]
			
		);
		$_SESSION[$user][$scop][] = $data;
		
		####calcul prix N. HT et TTC##
		$tab_prix_ttc[] = $ligne[5];
		$qte[] = $ligne[0];
		$references[] = explode(',', $ligne[3]);
		#############################
	}
	$is_there_multiple = false;
	foreach ($references as $reference) {
		if (count($reference) > 1) $is_there_multiple = true;
	}
	if ($is_there_multiple) {
		$result['prix_n_ttc'] = 0;
		$result['prix_n_ht'] = 0;
	}
	else {
		$p_ttc = 0;
		for ($i = 0; $i < count($tab_prix_ttc); $i++) {
			$p_ttc += $tab_prix_ttc[$i] * $qte[$i];
		}
		$result['prix_n_ttc'] = $p_ttc;
		$liste_tva = get_liste_tva();
		$tva_temp = $liste_tva[$_GET['tva']];
		$result['prix_n_ht'] = round($result['prix_n_ttc'] / (1 + $tva_temp/100), 2);
	}
	
	if (count($_SESSION[$user][$scop]) > 0) {
		$result["error"] = 0;
	} else {
		$result["error"] = 1;
	}
	return $result;
}

/**
 * Calcul du Prix HT à partir du Prix TTC et TVA
 */
function getPrixHt($ttc, $tva) {
	$liste_tva = get_liste_tva();
	$tva = $liste_tva[$tva];
	$ht = $ttc / (1 + $tva / 100);
	return round($ht, 2);
}