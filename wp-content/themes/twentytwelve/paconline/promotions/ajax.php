<?php
require_once dirname(dirname(__FILE__)) . '/include-functions.php';

/*
 * Calcul des cycles correspondants aux semaines debut et fin
 */
if ($_GET['ajax'] == 13) {
	if (isset($_GET['debut']) && $_GET['debut'] != '' && isset($_GET['fin']) && $_GET['fin'] != '') {
		echo get_cycles_between_two_weeks($_GET['debut'], $_GET['annee'], $_GET['fin'], $_GET['annee_fin'], $_GET['permalink']);
	}
}

/*
 * Ajout de la promo dans la bases
 */
if ($_GET['ajax'] == 2) {
    
	$promos = Pods('promotions');
	$cycles_new = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	//$cycles_periodes = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin'], $_GET['cycle_ou_periode']);
	//$higher_level = getHigherLevelDiffusion($cycles_periodes);
	$user = wp_get_current_user();
	$user = $user->user_login;
	if (!is_numeric($_GET['prix_normal_ht'])
		|| !is_numeric($_GET['prix_normal_ttc'])
		|| !is_numeric($_GET['prix_promotion_ht'])
		|| !is_numeric($_GET['prix_promotion_ttc'])) {
			echo 'erreur_prix';
			exit();
		}
	###Faire à nouveau les calculs pendant l'enregistrement pour éviter les surprise###
	$save_prix_ht_promo = getPrixHt($_GET['prix_promotion_ttc'], $_GET['tva']);
	$composition =  $_GET['composition'];
	###################################################################################
	$data = array(
		'permalink' => $_GET['cycle_ou_periode'],
	    'cycles' => $cycles_new,
	    'reference_extranet' => $_GET['reference_extranet'],
	    'description' => $_GET['description'],
	    'marque' => $_GET['marque'],//ERROR si marque = 0
		'reference_facturation' => $_GET['reference_facturation'],//ERROR
	    'reference_lot' => $_GET['reference_lot'],
	    'date_debut_pre_lancement' => js2mysql_date($_GET['date_debut_pre_lancement']),
	    'date_fin_pre_lancement' => js2mysql_date($_GET['date_fin_pre_lancement']),
	    'qte_pre_lancement' => $_GET['qte_pre_lancement'],
	    'date_debut_lancement' => js2mysql_date($_GET['date_debut_lancement']),
	    'date_fin_lancement' => js2mysql_date($_GET['date_fin_lancement']),
// // 	    'qte_lancement' //supprimer de la BDD 
	    'prevision' => $_GET['prevision'],
	    'qte_limitee' => $_GET['qte_limitee'],
	    'action_info' => $_GET['action_info'],
	    'lancement_chaud' => $_GET['lancement_chaud'],
	    'detail_promotion' => $composition, 
	    'date_lancement_chaud_dd' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
	    'date_lancement_chaud_dr' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : '',
	    'gamme' => $_GET['gamme'],//ERROR si gamme = 0
	    'semaine_debut' => $_GET['semaine_debut'],
	    'semaine_fin' => $_GET['semaine_fin'],
		'annee' => $_GET['annee'],
	    'tva' => $_GET['tva'],
	    'prix_normal_ht' => $_GET['prix_normal_ht'],//ERROR
	    'prix_normal_ttc' => $_GET['prix_normal_ttc'],//ERROR
	    'prix_promo_ht' => $save_prix_ht_promo,
	    'prix_promo_ttc' => $_GET['prix_promotion_ttc'],
		'annee_fin' => $_GET['annee_fin'],
		'publie' => $_GET['publie'],
		'diffusion_level' => 10  
	);
	
	$new_promo_id = $promos->add($data);
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $new_promo_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	if ($new_promo_id > 0) {
		$cycles_promos = Pods('cycles_promos');
		$cycles_correspondants = explode(',', $cycles_new);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_promo' => $new_promo_id);
			$cycles_promos->add($data);
		}
		echo $new_promo_id;
	}
}

/*
 * récupérer les infos d'une référence en promo
 */
if ($_GET['ajax'] == 3) {
	$result = array();
	$reference = $_GET['reference'];
	$articles = Pods('articles', array('where' => "reference='$reference'", 'select' => "`t`.designation, `t`.prix_ht, `t`.prix_ttc"));
	if ($articles->total() > 0) {
		$result = array('error' => 0,'quantite' => 1, 'designation' => $articles->display('designation'), 'reference' => $reference, 'prix_n_ht' => pod2html_prix($articles->display('prix_ht')), 'prix_n_ttc' => pod2html_prix($articles->display('prix_ttc')));
	}
	else $result['error'] = 1;
	$result = json_encode($result);
	echo $result;
}

/*
 * Récupération des infos de plusieurs références de meme ligne en promo
 */
if ($_GET['ajax'] == 4) {
	$result = array();
	$prix_ht = array();
	$prix_ttc = array();
	$designations = '';
	$references = str_replace('_', ',', $_GET['references']);
	$refs = '(\'' . str_replace('_', '\',\'', $_GET['references']) .'\')';
	$articles = Pods('articles', array('where' => "reference IN $refs", 'select' => "`t`.designation, `t`.prix_ht, `t`.prix_ttc"));
	if ($articles->total() > 0) {
		$result['quantite'] = 1;
		$result['error'] = 0;
		while ($articles->fetch()) {
			$designations .= $articles->display('designation') . ',';
			$prix_ht[] = $articles->display('prix_ht');
			$prix_ttc[] = $articles->display('prix_ttc');
		}
		$temp_design = str_replace(',', ' ou ' , $designations);
		$result['designation'] = substr($temp_design, 0, strlen($temp_design)-4);
		$temp_refs = str_replace(',', ' ou ' , $references);
		$result['references'] = $references;
		$result['referencesou'] =  substr($temp_refs, 0, strlen($temp_design)-1);
		$prix_ht = array_unique($prix_ht);
		$prix_ttc = array_unique($prix_ttc);
		if (count($prix_ht) == 1 && count($prix_ttc) == 1) {
			$result['prix_n_ht'] = pod2html_prix($prix_ht[0]);
			$result['prix_n_ttc'] = pod2html_prix($prix_ttc[0]);
		} else {
			$result['prix_n_ht'] = 0.00;
			$result['prix_n_ttc'] = 0.00;
		}
	}
	else $result['error'] = 1;
	$result = json_encode($result);
	echo $result;
}


/*
 * Calcul Prixx TTC  
 */
if ($_GET['ajax'] == 6) {
	$ttc = $_GET['ttc'];
	$tva = $_GET['tva'];
	$liste_tva = get_liste_tva();
	$tva = $liste_tva[$tva];
	$ht = $ttc / (1 + $tva / 100);
	echo round($ht, 2);
	exit();
}

/*
 * Suppression d'une promo
 */
if ($_GET['ajax'] == 7) {
	$id_promo = $_GET['id_promo'];
	$promo_pod = Pods('promotions', $id_promo);
	$remarques_pod = Pods('remarques', array('where' => "id_entite=$id_promo AND type_entite='P'"));
	if ($promo_pod->delete()) {
		$promo_cycle_pod = Pods('cycles_promos', array('where' => "id_promo=$id_promo"));
		$total_cycles_promos = $promo_cycle_pod->total();
		if ($total_cycles_promos > 0) {
			while ($promo_cycle_pod->fetch()) {
				$temp_pod = pods('cycles_promos', $promo_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		while ($remarques_pod->fetch()) {
			$pod_temp = Pods('remarques', $remarques_pod->field('id'));
			$pod_temp->delete();
		}
		echo 1;
	}
	else
		echo 0;
	exit();
}


/*
 * Supprimer une ligne de reference promo pendant la modification
 */
if ($_GET['ajax'] == 8) {
	$reference = $_GET['reference'];
	$type = $_GET['type'];
	$scop = $_GET['scop'];
	unset($_SESSION[$scop][$type][$reference]);
	unset($_SESSION[$scop][$type][str_replace('_', ',', $reference)]);
	echo 1;
	exit();
}

/*
 * Supprimer le tableau session de reference si le tableau est vidé pendant la création
 */
if ($_GET['ajax'] == 11) {
	$user = wp_get_current_user();
	$user = $user->user_login;
	unset($_SESSION[$user]);
}

/*
 * edition d'une promo
 */
if ($_GET['ajax'] == 9) {
	$user = wp_get_current_user();
	$user = $user->user_login;
	if (!is_numeric($_GET['prix_normal_ht'])
		|| !is_numeric($_GET['prix_normal_ttc'])
		|| !is_numeric($_GET['prix_promotion_ht'])
		|| !is_numeric($_GET['prix_promotion_ttc'])) {
			echo 'erreur_prix';
			exit();
		}
	$cycles_edi = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	###Faire à nouveau les calculs pendant l'enregistrement pour éviter les surprise###
	$save_prix_ht_promo = getPrixHt($_GET['prix_promotion_ttc'], $_GET['tva']);
	$composition =  $_GET['composition'];
	$data = array( 
		'cycles' => $cycles_edi, 
	    'reference_extranet' => $_GET['reference_extranet'],
	    'description' => $_GET['description'],
	    'marque' => $_GET['marque'],//ERROR si marque = 0
	    'reference_facturation' => $_GET['reference_facturation'],//ERROR
	    'reference_lot' => $_GET['reference_lot'],
	    'date_debut_pre_lancement' => js2mysql_date($_GET['date_debut_pre_lancement']),
	    'date_fin_pre_lancement' => js2mysql_date($_GET['date_fin_pre_lancement']),
	    'qte_pre_lancement' => $_GET['qte_pre_lancement'],
	    'date_debut_lancement' => js2mysql_date($_GET['date_debut_lancement']),
	    'date_fin_lancement' => js2mysql_date($_GET['date_fin_lancement']),
	    // // 	    'qte_lancement' //supprimer de la BDD
	    'prevision' => $_GET['prevision'],
	    'qte_limitee' => $_GET['qte_limitee'],
	    'action_info' => $_GET['action_info'],
	    'lancement_chaud' => $_GET['lancement_chaud'],
	    'detail_promotion' => $composition,
	    'date_lancement_chaud_dd' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
	    'date_lancement_chaud_dr' => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : '',
	    'gamme' => $_GET['gamme'],//ERROR si gamme = 0
	    'semaine_debut' => $_GET['semaine_debut'],
	    'semaine_fin' => $_GET['semaine_fin'],
	    'annee' => $_GET['annee'],
	    'tva' => $_GET['tva'],
	    'prix_normal_ht' => $_GET['prix_normal_ht'],//ERROR
	    'prix_normal_ttc' => $_GET['prix_normal_ttc'],//ERROR
	    'prix_promo_ht' => $save_prix_ht_promo,
	    'prix_promo_ttc' => $_GET['prix_promotion_ttc'],
	    'annee_fin' => $_GET['annee_fin'],
	    'publie' => $_GET['publie'],
	    'diffusion_level' => 10  
	);
	$pods = Pods('promotions'); 
	$edited_promo_id = $pods->save($data, null, $_GET['id_promo']);
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $edited_promo_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	if ($edited_promo_id > 0) {
		####suppression des relations avec les cycles correspondants####
		$promo_cycle_pod = Pods('cycles_promos', array('where' => "id_promo=$edited_promo_id"));
		$total_cycles_promos = $promo_cycle_pod->total();
		if ($total_cycles_promos > 0) {
			while ($promo_cycle_pod->fetch()) {
				$temp_pod = pods('cycles_promos', $promo_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		####etablissement des nouvelles relations avec de nouveux cycles####
		$cycles_promos = Pods('cycles_promos');
		$cycles_correspondants = explode(',', $cycles_edi);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_promo' => $edited_promo_id);
			$cycles_promos->add($data);
		}
		echo $edited_promo_id;
	}
	
}

/*
 *  Valider la composition de la promotion
 */
if ($_GET['ajax'] == 5) {
	$composition = $_GET['composition'];
	$result = valid_lignes_promo($composition);
	$user = wp_get_current_user();
	$user = $user->user_login;
	$_SESSION[$user]['prix_n_ttc'] = $result['prix_n_ttc'];
	if ($result['prix_n_ttc'] == 0) $_SESSION[$user]['prix_n_ttc'] = 0.001;
	$_SESSION[$user]['prix_n_ht'] = $result['prix_n_ht'];
	if ($result['prix_n_ht'] == 0) $_SESSION[$user]['prix_n_ht'] = 0.001;
	echo json_encode($result);
	exit();
}

/*
 * Récupération de la liste des références pour la popup
 * d'ajout d'une ligne de promotion
 */
if ($_GET['ajax'] == 10) {
	$result = get_selection_reference();
	echo json_encode($result);
	exit();
}

/*
 * Récupération une ref aprés recherche dans popup
 */
if ($_GET['ajax'] == 12) {
	$result = array();
	$result['empty'] = 0;
	$result['result'] = array();
	$reference = $_GET['ref'];
	$pod_articles = Pods('articles', array('where' => "reference LIKE '%$reference%'"));
	if ($pod_articles->total() > 0) {
		while ($pod_articles->fetch()) {
			$result['result'][] = array('reference' => $pod_articles->display('reference'), 'designation' => $pod_articles->display('designation'));
		}
	} else {
		$result['empty'] = 1;
	}
	echo json_encode($result);
	exit();
}