<?php
require_once 'cycles/functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'cycles/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 9) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}
$annee_en_cours = date('Y');

$options_numeros_cycles = get_liste_numeros_cycles();
$options_numeros_periodes = get_liste_numeros_periodes();


if (!isset($_GET['id_cycle']) || $_GET['id_cycle'] == '') { ?>
<h3 class="ttl">Page Introuvable</h3>
<?php 
	exit();
} else { 
    $current_cycle = Pods('cycles', array('where' => "id=" . $_GET['id_cycle']));
	$data_current_cycle = $current_cycle->data();
	$data_current_cycle = $data_current_cycle[0];
	if (is_null($data_current_cycle)) {
?>
<h3 class="ttl">Cycle Introuvable</h3>
<?php 
		exit();
	} else {
		$pod_cible = 'Cycles';
		$id_pod_cible = $_GET['id_cycle'];
		//############################récupération commentaires#####################  
		$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
		$content_comment_dd = '';
		$content_comment_dr = '';
		if ($commentaires_dd->total() > 0)
			$content_comment_dd = $commentaires_dd->display('content');
			
		$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
		$content_comment_dr = '';
		if ($commentaires_dr->total() > 0)
			$content_comment_dr = $commentaires_dr->display('content');
?>
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/edit-cycle.js"; ?>"></script>
<form id="edit_cycle_form" action="" method="get">
	<input type="hidden" id="cycle_identifiant" value="<?php echo $data_current_cycle->identifiant; ?>"/>
	<input type="hidden" id="cycle_id" value="<?php echo $_GET['id_cycle']; ?>">
	<input type="hidden" id="cycle_detail_page" value="<?php echo get_permalink(get_page_by_path('detail-cycle')) . '?id_cycle=' .$_GET['id_cycle'] ?>">
	<input type="hidden" id="title_page" value="<?php if ($data_current_cycle->permalink == 'periodes') echo 'Edit Période'?>">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Modification Cycle : <span class="red"><?php echo $data_current_cycle->identifiant?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="2"></td></tr>
			<tr>
				<td align="right"><label>Année du cycle</label></td>
				<td>
					<select id="cycle_annee_cycle" >
						<option value="">Choisir...</option>
						<?php for ($i = -1; $i < 2; $i++):?>
						<?php $annee = (int)$annee_en_cours + $i?>
						<option value="<?php echo $annee?>" <?php if ($data_current_cycle->annee == $annee) echo "selected"?>><?php echo $annee?></option>
						<?php endfor;?>
					</select>
					<div class="clearfix"></div>
					<span id="error_annee_cycle" class="red"></span>
				</td>
				<td rowspan="2" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<?php if ( strpos($data_current_cycle->permalink, 'cycles')=== 0) :?>			
			<tr>
				<td align="right"><label>Numéro de cycle</label></td>
				<td>
					<select id="cycle_numero">
						<option value="">Choisir...</option>
						<?php if (count($options_numeros_cycles) > 0):?>
							<?php foreach ($options_numeros_cycles as $kle_cycle => $val_cycle):?>
						<option value="<?php echo $kle_cycle?>" <?php if ($data_current_cycle->cycle_numero == $kle_cycle) echo 'selected'?>><?php echo $val_cycle?></option>
							<?php endforeach;?>
						<?php endif;?>
					</select>
					<div class="clearfix"></div>
					<span id="error_numero" class="red"></span>
				</td>
			</tr> 
			<?php endif;?>
			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center" class="brd_right">Date fin</th>
						</tr>
						<tr>
							<th><label>Commerciale</label></th>
							<td align="center"><input type="text" id="cycle_debut_commerciale" value="<?php echo pod2js_date($data_current_cycle->date_debut_commerciale)?>"></td>
							<td align="center" class="brd_right"><input type="text" id="cycle_fin_commerciale" value="<?php echo pod2js_date($data_current_cycle->date_fin_commerciale)?>"></td>								
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_commerciale" class="red"></span></td></tr>
					</table>
				</td>
				<td>
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="100%">
						<tr>
							<th align="center" colspan="2" class="brd_right">Diffusion aux DR/DD</th>
						</tr>
						<tr>
							<td>DR : <input type="text" id="cycle_diffusion_dr" value="<?php echo pod2js_date($data_current_cycle->diffusion_dr)?>"/></td>
							<td class="brd_right">DD : <input type="text" id="cycle_diffusion_dd" value="<?php echo pod2js_date($data_current_cycle->diffusion_dd)?>"></td>
						</tr>
						<tr><td class="error" colspan="3"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="3"><span id="info"></span></td></tr>
		</table>
	</div>
</form>
	
<?php 
include_once('comment-bloc.php');

$my_entity_id = $data_current_cycle->id;
$type_entity = 'C';
include_once("remarques/remarques-visu.php");
include_once('attachments/attachment-doc-creation.php');
?>

<script>
jQuery(document).ready(function($) {
<?php
	$my_entity_id = $_GET['id_cycle'];
	//AIMED1504
	$pod = Pods("attachments", array("where" => "id_entity=".$my_entity_id." and type='".$type_entity."'"));
	if ($pod->fetch()) {
	    do {
	        ?>
	            var value =" <?php echo basename($pod->display('complete_path')); ?>" ;
				var id = <?php echo basename($pod->display('id')); ?> ;
	            
				$('#all_docs').append('<div class="doc_attach_element"><img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle" style="margin-right:10px;"/><img src="../wp-content/themes/twentytwelve/images/remove.png" width=20 align="absmiddle" style="margin-right:10px; cursor:pointer;" class="delete_doc" id="del*' + id + '"/><a href="<?php echo get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/"; ?>' + value + '" target="_blank">' + value + '</a><div class="clearfix"></div></div>');
	            
	<?php
			} while($pod->fetch());
		}
	?>

		$('#all_docs').append('<div style="clear:both;" id="clearboth"></div>');

		$('.delete_doc').unbind('click').live('click', function() {
			var id_doc = $(this).attr('id').replace('del*', '');
			var $complete_path = $(this).parent().find("a").attr('href');
			$filename = $(this).parent().find("a").html();
			var check = confirm('Voulez-vous vraiment supprimer ce document ?');
			if (check) {
				$(this).parent().remove();
					$.ajax({
							url: '',
							type: 'get',
							data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=9',
							success: function(msg) {
							},
							error: function() {
							}
						});
				}
		});
});
</script>
	
<?php 
	}
} ?>
