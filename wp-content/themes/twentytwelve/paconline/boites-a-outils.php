<?php 
	if (isset($_POST['ajax']) && $_POST['ajax'] == "upload_doc") {
		require_once 'attachments/upload-and-save-doc.php';
	}
	if (isset($_GET['ajax'])) {
		require_once 'attachments/delete-doc.php';
	}
?>

<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/boite-a-outils.js"; ?>"></script>

<h3 class="ttl">Documents permanents</h3>
<h4>Ajouter un document</h4>
<div>
	<form action="" method="post" enctype="multipart/form-data" id="doc_uploader_form">
	<input type="hidden" id="ajax" name="ajax" value="upload_doc"/>
	<input type="hidden" id="type" name="type" value="*"/>
	<input type="file" id="doc_to_upload" name="doc_to_upload"/>
	<input type="submit" value="Transférer"/>
	</form>
</div>

<?php $pod = Pods("attachments", array("where" => "type='*'", "orderby"=>"id DESC", "limit"=>"-1"));?>

<?php while($pod->fetch()): ?>
	<div style="float:left; margin:15px;">
		<a href="<?php echo $pod->display('complete_path'); ?>" target="_blank">
			<img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle"/>
			<?php echo basename($pod->display('complete_path')); ?>
		</a>
		<input type="button" value="Supprimer" class="delete_doc" id="del_<?php echo $pod->field('id'); ?>"/>
	</div>
<?php endwhile;?>