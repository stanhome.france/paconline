<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_POST['ajax']) && $_POST['ajax'] == "upload_doc") {
	require_once 'attachments/upload-and-save-doc.php';
}

if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'evenements-des-cycles/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 2) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}
$annee_en_cours = date('Y');

$options_cible = get_liste_cible();
$options_event = get_liste_event();

if (!isset($_GET['id_event']) || $_GET['id_event'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$current_event = Pods('events', array('where' => "id=" . $_GET['id_event']));
$data_current_event = $current_event->data();
$data_current_event = $data_current_event[0];
?>
<?php if (is_null($data_current_event)):?>
<h3 class="ttl">Event Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$pod_cible = 'Events';
$id_pod_cible = $_GET['id_event'];
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
##########################################################################

###cibles pour cet event###
$this_event_cible_array = explode(',', $data_current_event->cible_evenement);
$this_event_cible = array();
foreach ($this_event_cible_array as $item) {
	if(isset($options_cible[$item])) $this_event_cible[] = $options_cible[$item];
}
$this_event_cible = implode(' + ', $this_event_cible);
?>
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/edit-event-cyclique.js"; ?>"></script>
	
<form id="edit_event_form" action="" method="get">
	<input type="hidden" id="identifiant_event" value="<?php echo $id_pod_cible; ?>"/>
	<input type="hidden" id="event_detail_page" value="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' .$_GET['id_event'] ?>">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Modification Evénement : <span class="red"><?php echo stripSlashes($data_current_event->designation); ?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>				
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Désignation</label></td>
				<td><input type="text" id="event_designation" value="<?php echo stripSlashes($data_current_event->designation); ?>" class="long"> </td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<?php require_once 'cycle-ou-periode.php';?>
			<tr>
				<td align="right"><label>Type</label></td>
				<td>
					<select id="event_type_list">
						<option value="">Vide...</option>
						<?php if (count($options_event) > 0):?>
							<?php foreach ($options_event as $kle_event => $val_event):?>
						<option value="<?php echo $kle_event?>" <?php if ($data_current_event->type_evenement == $kle_event) echo "selected"?>><?php echo stripslashes($val_event)?></option>	
							<?php endforeach;?>
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Cycle(s)</label></td>
				<td colspan="2">
					<input type="text" id="event_cycles_correspondants" disabled="disabled" value="<?php echo get_cycles_between_two_weeks($data_current_event->semaine_debut, $data_current_event->annee, $data_current_event->semaine_fin, $data_current_event->annee_fin, $data_current_event->permalink)?>"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<div class="espc_drt">
					<label>Cible(s)</label>
						<select id="event_cible_list">
							<option value="">Vider...</option>
							<?php if (count($options_cible) > 0):?>
								<?php foreach ($options_cible as $kle_cible => $val_cible):?>
							<option value="<?php echo $kle_cible?>"><?php echo stripslashes($val_cible)?></option>	
								<?php endforeach;?>
							<?php endif;?>
						</select><div class="clearfix"></div>
						<input type="button" id="edit_cible_to_event" value="Ajouter >>" class="small"/>
					</div>
				</td>
				<td>
					<input type="hidden" width="" id="event_cible" value="<?php echo $data_current_event->cible_evenement?>">
					<input type="text" id="event_cible_label" class="champ_non_editable vlong" value='<?php echo stripslashes($this_event_cible)?>'  />
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><input type="text" id="event_date_debut_pre_lancement" value="<?php echo pod2js_date($data_current_event->date_debut_pre_lancement) ?>"> </td>
							<td align="center"><input type="text" id="event_date_fin_pre_lancement" value="<?php echo pod2js_date($data_current_event->date_fin_pre_lancement) ?>"></td>
							<td align="center" class="brd_right"><input type="text" id="event_qte_pre_lancement"></td> 
						</tr>
						<tr>
							<th><label>Lancement</label></th>
							<td align="center"><input type="text" id="event_date_debut_extranet" value="<?php echo pod2js_date($data_current_event->date_debut_extranet) ?>"> </td>
							<td class="brd_right" align="center"><input type="text" id="event_date_fin_extranet" value="<?php echo pod2js_date($data_current_event->date_fin_extranet) ?>"></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"><span id="eroor_lancement" class="red"></span></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>							
			<tr><td colspan="3"><label id="info"></label></td></tr>
		</table>
	</div>
</form>

<?php include_once 'comment-bloc.php';?>

<?php
	$my_entity_id = $id_pod_cible; 
	$type_entity = 'E';
	include_once("remarques/remarques-visu.php");
	include_once('attachments/attachment-doc-creation.php');
?>


<script>
jQuery(document).ready(function($) {

	//AIMED1504
<?php
	$my_entity_id = $_GET['id_event'];
	$pod = Pods("attachments", array("where" => "id_entity=".$my_entity_id." and type='".$type_entity."'"));
	if ($pod->fetch()) {
	    do {
	        ?>
	            var value =" <?php echo basename($pod->display('complete_path')); ?>" ;
				var id = <?php echo basename($pod->display('id')); ?> ;
	            
				$('#all_docs').append('<div class="doc_attach_element"><img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle" style="margin-right:10px;"/><img src="../wp-content/themes/twentytwelve/images/remove.png" width=20 align="absmiddle" style="margin-right:10px; cursor:pointer;" class="delete_doc" id="del*' + id + '"/><a href="<?php echo get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/"; ?>' + value + '" target="_blank">' + value + '</a><div class="clearfix"></div></div>');
	            
	<?php
			} while($pod->fetch());
		}
	?>

		$('#all_docs').append('<div style="clear:both;" id="clearboth"></div>');

		$('.delete_doc').unbind('click').live('click', function() {
			var id_doc = $(this).attr('id').replace('del*', '');
			var $complete_path = $(this).parent().find("a").attr('href');
			$filename = $(this).parent().find("a").html();
			var check = confirm('Voulez-vous vraiment supprimer ce document ?');
			if (check) {
				$(this).parent().remove();
					$.ajax({
							url: '',
							type: 'get',
							data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=2',
							success: function(msg) {
							},
							error: function() {
							}
						});
				}
		});
});
</script>


<?php endif;?>
<?php endif;?>
