<?php
/**
 * Récupérer la liste des cibles
 */
function get_liste_cible() {
	$liste = array();
	if (get_option('_liste_cibles')) {
		$liste = unserialize(get_option('_liste_cibles'));
	}
	return $liste;
}

/**
 * Récupérer la liste des types d'évènments
 */
function get_liste_event() {
	$liste = array();
	if (get_option('_liste_evenements')) {
		$liste = unserialize(get_option('_liste_evenements'));
	}
	return $liste;
}

/**
 * Récupération des cycles appartenant à un intervalle de deux semaines:
 * semaine début et semaine fin
 * Format de retour : exemple 1,2,3 ou 5,6,7,8
 * Cette information sera enregistrée avec deux manières: 
 * dans le pod events avec le format cité au dessus
 * dans le pod cycles_events avec explode(',') sur plusieurs enregistrements
 */
function get_cycles_between_two_weeks($semaine_debut, $annee, $semaine_fin, $annee_fin, $permalink = '') {
	$debut = 100*$annee+$semaine_debut;
	$fin = 100*$annee_fin+$semaine_fin;
	$where1 = "(100 * annee + semaine_debut) BETWEEN $debut AND $fin";
	$where2 = "(100*annee_fin+semaine_fin) BETWEEN $debut AND $fin";
	$where3 = "(100*annee+semaine_debut) >= $debut AND (100*annee_fin+semaine_fin) <= $fin";
	$where4 = "(100*annee+semaine_debut) <= $debut AND (100*annee_fin+semaine_fin) >= $fin";
	$where = $where1 . " OR " . $where2 . " OR " . $where3 . " OR " . $where4;
	//if (isset($permalink) && $permalink != '') $where = "($where) AND (permalink like '%$permalink%')";
	$cycles = Pods('cycles', array('where' => $where));
	$result = array();
	while ($cycles->fetch()) {
		$result[] = $cycles->display('identifiant');
	}
	sort($result);
	$result = implode(',', $result);
	return $result;
}