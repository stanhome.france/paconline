<?php
require_once dirname(dirname(__FILE__)) . '/include-functions.php';


/*
 * Ajout d'un nouveau cycle
 */
if ($_GET['ajax'] == 1) {
	
	if (   (!isset($_GET['designation'])    || $_GET['designation'] == ''   )
		|| (!isset($_GET['semaine_debut'])  || $_GET['semaine_debut'] == '' )
		|| (!isset($_GET['semaine_fin'])    || $_GET['semaine_fin'] == ''   )
		|| (!isset($_GET['date_debut'])     || $_GET['date_debut'] == ''    )
		|| (!isset($_GET['date_fin'])       || $_GET['date_fin'] == ''      )
		|| (!isset($_GET['type'])           || $_GET['type'] == ''          )
		|| (!isset($_GET['cible'])          || $_GET['cible'] == ''         )
		|| (!isset($_GET['extranet_debut']) || $_GET['extranet_debut'] == '')
		|| (!isset($_GET['extranet_fin'])   || $_GET['extranet_fin'] == ''  )
	) {
			
			echo -1;
			exit();
	}
	$events = Pods('events');
	$cycles_new = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	$cycles_periodes = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin'], $_GET['cycle_ou_periode']);
	//$higher_level = getHigherLevelDiffusion($cycles_periodes);
	$data = array(
		'permalink'                => $_GET['cycle_ou_periode'],
		'designation'              => $_GET['designation'],
		'semaine_debut'            => $_GET['semaine_debut'],
		'semaine_fin'              => $_GET['semaine_fin'],
		'date_debut'               => js2mysql_date($_GET['date_debut']),
		'date_fin'                 => js2mysql_date($_GET['date_fin']),
		'type_evenement'           => $_GET['type'],
		'cible_evenement'          => $_GET['cible'],
		'date_debut_pre_lancement' => js2mysql_date($_GET['pre_lancement_debut']),
		'date_fin_pre_lancement'   => js2mysql_date($_GET['pre_lancement_fin']),
		'qte_pre_lancement'        => $_GET['qte_pre_lancement'],
		'date_debut_extranet'      => js2mysql_date($_GET['extranet_debut']),
		'date_fin_extranet'        => js2mysql_date($_GET['extranet_fin']),
		'action_info'              => $_GET['action_info'],
		'lancement_chaud'          => $_GET['lancement_chaud'],
		'publie'                   => $_GET['publie'],
		'cycles_correspondants'    => $cycles_new,
		'annee'                    => $_GET['annee'],
		'annee_fin'                => $_GET['annee_fin'],
		'date_lancement_chaud_dd'  => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
		'date_lancement_chaud_dr'  => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : '',
		'diffusion_level'          => 10
	);
	$new_event_id = $events->add($data);
	
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $new_event_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	
	if ($new_event_id > 0) {
		$cycles_events = Pods('cycles_events');
		$cycles_correspondants = explode(',', $cycles_new);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_event' => $new_event_id);
			$cycles_events->add($data);
		}
		echo $new_event_id;
	}
}


/*
 * Récupération date debut pour event
 */
if ($_GET['ajax'] == 11) {
	if (isset($_GET['semaine']) && $_GET['semaine'] != '' && isset($_GET['annee']) && $_GET['annee'] != '') {
		$semaine = (int)$_GET['semaine'];
		$annee = (int)$_GET['annee'];
		if (!is_numeric($semaine) || !is_numeric($annee))
			$debut_semaine = -1;
		else 
			$debut_semaine = getDateDebut($annee, $semaine);
	}
	echo $debut_semaine;
}


/*
 * Récupération date fin pour event
 */
if ($_GET['ajax'] == 12) {
	if (isset($_GET['semaine']) && $_GET['semaine'] != '' && isset($_GET['annee']) && $_GET['annee'] != '') {
		$semaine = (int)$_GET['semaine'];
		$annee = (int)$_GET['annee'];
		if (!is_numeric($semaine) || !is_numeric($annee))
			$fin_semaine = -1;
		else 
			$fin_semaine = getDateFin($annee, $semaine);
	}
	echo $fin_semaine;
}

/*
 * Calcul des cycles correspondants aux semaines debut et fin
 */
if ($_GET['ajax'] == 13) {
	if (isset($_GET['debut']) && $_GET['debut'] != '' && isset($_GET['fin']) && $_GET['fin'] != '') {
		echo get_cycles_between_two_weeks($_GET['debut'], $_GET['annee'], $_GET['fin'], $_GET['annee_fin'], $_GET['permalink']);
	}
}

/*
 * Suppression d'un évent
 */
if ($_GET['ajax'] == 5) {
	if (!isset($_GET['id_event'])) {
		echo -1;
		exit();
	}
	$id_event = $_GET['id_event'];
	$event_pod = Pods('events', $id_event);
	$remarques_pod = Pods('remarques', array('where' => "id_entite=$id_event AND type_entite='E'"));
	if ($event_pod->delete()) {
		$event_cycle_pod = Pods('cycles_events', array('where' => "id_event=$id_event"));
		$total_cycles_events = $event_cycle_pod->total();
		if ($total_cycles_events > 0) {
			while ($event_cycle_pod->fetch()) {
				$temp_pod = pods('cycles_events', $event_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		while ($remarques_pod->fetch()) {
			$pod_temp = Pods('remarques', $remarques_pod->field('id'));
			$pod_temp->delete();
		}
		echo 1;
	}
	else 
		echo 0;
}


/*
 * Edition d'un event
 */
if ($_GET['ajax'] == 3) {
	
	if (   (!isset($_GET['designation'])    || $_GET['designation'] == ''   )
		|| (!isset($_GET['type'])           || $_GET['type'] == ''          )
		|| (!isset($_GET['cible'])          || $_GET['cible'] == ''         )
		|| (!isset($_GET['extranet_debut']) || $_GET['extranet_debut'] == '')
		|| (!isset($_GET['extranet_fin'])   || $_GET['extranet_fin'] == ''  )
	) {
			
			echo -1;
			exit();
	}
	$events = Pods('events');
	$cycles_edi = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	$data = array(
		'designation'              => $_GET['designation'],
		'type_evenement'           => $_GET['type'],
		'semaine_debut'            => $_GET['semaine_debut'],
		'semaine_fin'              => $_GET['semaine_fin'],
		'date_debut'               => js2mysql_date($_GET['date_debut']),
		'date_fin'                 => js2mysql_date($_GET['date_fin']),
		'cible_evenement'          => $_GET['cible'],
		'date_debut_pre_lancement' => js2mysql_date($_GET['pre_lancement_debut']),
		'date_fin_pre_lancement'   => js2mysql_date($_GET['pre_lancement_fin']),
		'qte_pre_lancement'        => $_GET['qte_pre_lancement'],
		'date_debut_extranet'      => js2mysql_date($_GET['extranet_debut']),
		'date_fin_extranet'        => js2mysql_date($_GET['extranet_fin']),
		'action_info'              => $_GET['action_info'],
		'lancement_chaud'          => $_GET['lancement_chaud'],
		'publie'                   => $_GET['publie'],
		'cycles_correspondants'    => $cycles_edi,
		'annee'                    => $_GET['annee'],
		'annee_fin'                => $_GET['annee_fin'],
		'date_lancement_chaud_dd'  => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
		'date_lancement_chaud_dr'  => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : ''
	);
	$edited_event_id = $events->save($data, null, $_GET['id_event']);
	
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $edited_event_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	
	if ($edited_event_id > 0) {
		####suppression des relations avec les cycles correspondants####
		$event_cycle_pod = Pods('cycles_events', array('where' => "id_event=$edited_event_id"));
		$total_cycles_events = $event_cycle_pod->total();
		if ($total_cycles_events > 0) {
			while ($event_cycle_pod->fetch()) {
				$temp_pod = pods('cycles_events', $event_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		####etablissement des nouvelles relations avec de nouveux cycles####
		$cycles_events = Pods('cycles_events');
		$cycles_correspondants = explode(',', $cycles_edi);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_event' => $edited_event_id);
			$cycles_events->add($data);
		}
		echo $edited_event_id;
	}
	
}
