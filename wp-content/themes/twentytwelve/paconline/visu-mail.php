<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>
<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_POST['ajax']) && $_POST['ajax'] != -1) {
	ob_end_clean();
	require_once 'mails/ajax.php';
	exit();
}

$current_mail = Pods('mails', $_GET['id_mail']);
$current_mail->fetch();

?>


<form id="save_existed_mail_model" action="" method="post">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Contenu du mail: <?php echo stripslashes($current_mail->display('objet'))?></h2>
				</td>
			</tr>
			<tr>
				<td width="100px;"><label>Objet du Mail</label></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><input id="mail_objet" style="width:81%;" type="text" value="<?php echo stripslashes($current_mail->display('objet'))?>"></td>
			</tr>
			<tr>
				<td><label>Contenu du Mail</label></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><textarea id="mail_contenu" cols="100"><?php echo stripslashes($current_mail->display('contenu'))?></textarea></td>
			</tr>
		</table>
	</div>
</form>

<script type="text/javascript">
( function( $ ) {
	tinyMCE.init({
		height: "500",
        mode : "exact",
        elements : "mail_contenu",
        language : "fr",
        entity_encoding : "raw",
        plugins : "paste,table",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
        theme_advanced_buttons3 : "tablecontrols"
	});

	//enregistrement d'un mail model
	$("#save_existed_mail_model").submit(function() {
		var error_msg = 'Les champs suivant (sont) obligatoires: \n';
		var error = false;
		if($("#mail_objet").val() == '') {
			error = true;
			error_msg += 'L\'objet \n';
		}
		if(tinyMCE.get('mail_contenu').getContent() == '') {
			error = true;
			error_msg += 'Le contenu \n';
		}
		if(error) alert(error_msg);
		if(error) return false;

		var data = 'ajax=2&id_mail=<?php echo $_GET['id_mail']?>';
		data += '&objet='+$("#mail_objet").val();
		data += '&contenu='+tinyMCE.get('mail_contenu').getContent();
		
		$.ajax({
			type: 'post',
			data: data,
			success: function(msg) {
				if (msg == 'mail_saved') {
						alert('Mail Enregistré');
						window.location = '<?php echo get_permalink(get_page_by_path('mails'))?>';
				} else {
					alert('mail non enregistré! une erreur est survenue, contacter votre admin');
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error 6: jqXHR = ' + jqXHR + '\ntextStatus = ' + textStatus + '\nerrorThrown = ' + errorThrown);
			}
		});
		return false;
	});
	
})( jQuery );

function delete_mail_model(id_model) {
	if(!confirm('Etes vous surs de bien vouloir supprimer ce modèle?')) return false;
	else {
		var data = 'ajax=3&id_modele='+id_model;
		$.ajax({
			type: 'post',
			data: data,
			success: function(msg) {
				if(msg == 'deleted') {
					alert('Mail modèle Bien supprimé');
					window.location = '<?php echo get_permalink(get_page_by_path('mails'))?>';
				}
			},
			error: function() {
				alert('Erreur Ajax!');
			}
		});
	}
}
</script>