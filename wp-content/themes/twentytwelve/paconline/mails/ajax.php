<?php

/*
 * Enregistrement d'un nouveau mail modèle
 */
if ($_POST['ajax'] == 1) {
	$mails_pod = Pods('mails');
	
	$agents = ($_POST['agents'] != "null") ? $_POST['agents'] : '';
	
	
	$data = array(
		'objet' => $_POST['objet'],
		'contenu' => $_POST['contenu'],
		'agents' => $agents,
	);
	
	if ($mails_pod->save($data) > 0) echo 'mail_saved';
	
	exit();
}

/*
 * Edition d'un mail modèle
 */
if ($_POST['ajax'] == 2) {
	$mails_pod = Pods('mails', $_POST['id_mail']);
	
	$data = array(
		'objet' => $_POST['objet'],
		'contenu' => $_POST['contenu'],
	);
	
	if ($mails_pod->save($data, null, $_POST['id_mail'])) echo 'mail_saved';
	
	exit();
}

/*
 * Suppression d'un mail modèle
 */
if ($_POST['ajax'] == 3) {
	$mails_pod = Pods('mails', $_POST['id_modele']);
	
	if ($mails_pod->delete()) echo 'deleted';
	else echo 'not_deleted';
	
	exit();
}