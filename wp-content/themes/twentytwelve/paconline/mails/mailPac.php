<?php
require_once get_template_directory().'/paconline/logs/log-helper.php';
class mailPac {
	
	private $mail_model_objet;
	private $mail_model_contenu;
	private $added_groups = array();
	private $attached_files = array();
	
	
	/**
	 * Constructeur
	 * @param integer $mail
	 */
	public function __construct($mail_objet, $mail_contenu) {
		$this->mail_model_objet = $mail_objet;
		$this->mail_model_contenu = $mail_contenu;
	}
	
	/**
	 * Ajout d'un groupe au prochain envoi
	 * @param integer $group
	 */
	public function addGroupToMailList($groupId) {
		$this->added_groups[] = $groupId;
	}
	
	/**
	 * Ajout f'un fichier attaché au prochain envoi
	 */
	public function addAttachedFile($attachedFile) {
		$this->attached_files[] = $attachedFile;
	}
	
	/**
	 * Envoyer le mail aux groupes ajoutés
	 */
	public function sendMailToAddedGroups($log_path_file, $attachedFiles, $from = array()) {
		if (count($this->added_groups) > 0) {
			foreach ($this->added_groups as $groupId) {
				$group = new Groups_Group($groupId);
				$users = $group->users;
				if (count($users) > 0) {
					foreach ($users as $user) {
						$user_email = $user->user_email;
						if(count($from) == 0) {
							$headers = 'From: PAC ONLINE <christelle.courtin@yrnet.com>' . "\r\n";
						} else {
							$headers = 'From: ' . $from[1] . ' <'. $from[0] . '>' . "\r\n";
						}
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						if (wp_mail($user_email, $this->mail_model_objet, $this->mail_model_contenu, $headers, $attachedFiles)) {
							$content = "Mail envoyé à $user_email";
							sh_write_to_log($content, $log_path_file);
						} else {
							$content = "Mail non envoyé à $user_email";
							sh_write_to_log($content, $log_path_file);
						}
						//error_log(json_encode($content ));
					}
				}
			}
		}
	}
	
	/**
	 * Setter: Edit mail model objet to send
	 */
	public function setMailModelObjet($new_mail_model_objet) {
		$this->mail_model_objet = $new_mail_model_objet;
	}
	
	/**
	 * Getter: Get mail model objet
	 */
	public function getMailModelObjet() {
		return $this->mail_model_objet;
	}
	
	/**
	 * Setter: Edit mail model contenu to send
	 */
	public function setMailModelContenu($new_mail_model_contenu) {
		$this->mail_model_contenu = $new_mail_model_contenu;
	}
	
	/**
	 * Getter: Get mail model contenu
	 */
	public function getMailModelContenu() {
		return $this->mail_model_contenu;
	}
	
	/**
	 * Getter: Get groups list
	 */
	public function getAddedGroups() {
		return $this->added_groups;
	}
	
	/**
	 * Getter: Get Attached Files
	 */
	public function getAttachedFiles() {
		return $this->attached_files;
	}
}	