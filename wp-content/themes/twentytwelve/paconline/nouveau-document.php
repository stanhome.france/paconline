<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'documents/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 2) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}
$annee_en_cours = date('Y');

$options_cible = get_liste_cible();

?>


<form id="save_new_document" action="" method="get">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table  cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Nouveau document</h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td  align="right"><label>Référence Document</label></td>
				<td><input type="text" id="document_reference" class="long"/></td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<?php require_once 'cycle-ou-periode.php';?>
			<tr>
				<td align="right"><label>Désignation</label></td>
				<td><input type="text" id="document_designation" class="long"/></td>
			</tr>
			<tr>
				<td align="right" ><label>Cycle(s)</label></td>
				<td colspan="2">
					<input type="text" id="document_cycles_correspondants" disabled="disabled">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right" >
				<div class="espc_drt">
				<label>Cible</label>
					<select id="document_cible_list">
						<option value="">Vide...</option>
						<?php if (count($options_cible) > 0):?>
							<?php foreach ($options_cible as $kle_cible => $val_cible):?>
								<option value="<?php echo $kle_cible?>"><?php echo  stripslashes($val_cible)?></option>
							<?php endforeach;?>
						<?php endif;?>
					</select>
					<div class="clearfix"></div>
					<input type="button" id="add_new_cible_to_document" value="Ajouter >>" class="small">
					</div>
				</td>
				<td>
					<input type="hidden" width="" id="document_cible">
					<input type="text" id="document_cible_label" class="champ_non_editable vlong" />
				</td>
			</tr>
			<tr>
				<td align="right"><label>Critères de distribution</label></td>
				<td colspan="2"><input type="text" id="document_critere_distribution" class="vlong"/></td>
			</tr>
			<tr>
				<td align="right"><label>Réf. Lot d'appartenance</label></td>
				<td colspan="2"><input type="text" id="document_reference_lot_appartenance" class="vlong"/></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Extranet</label></td>
				<td colspan="2"><input type="text" id="document_reference_extranet" class="vlong"/></td>
			</tr>
			<tr>
				<td align="right"><label>Qté doc. Dans un lot</label></td>
				<td><input type="text" id="document_qte_doc_par_lot" class="long"/></td>
			</tr>
			<tr>
				<td align="right"><label>Qté doc imprimés</label></td>
				<td><input type="text" id="document_qte_doc_imprimes" class="long"/></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><input type="text" id="document_date_debut_pre_lancement"></td>
							<td align="center"><input type="text" id="document_date_fin_pre_lancement"></td>
							<td align="center" class="brd_right"><input type="text" id="document_qt_pre_lancement"></td>
						</tr>
						<tr>
							<th>Lancement</th>
							<td align="center"><input type="text" id="document_date_debut_lancement"> </td>
							<td align="center"><input type="text" id="document_date_fin_lancement"> </td>
							<td  align="center" class="brd_right" style="border-right: 0px;"></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr><td></td><td><label id="info"></label></td></tr>
		</table>
	</div>
</form>

<?php 
$pod_cible = 'Docs';
?>
<?php include_once 'comment-bloc.php';

$type_entity = 'D';
include_once('attachments/attachment-doc-creation.php');
?>

<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript">
( function( $ ) {
	/*
	 * Les champs dates
	 */
	$("#document_date_debut_pre_lancement").datepicker();
	$("#document_date_fin_pre_lancement").datepicker();
	$("#document_date_debut_lancement").datepicker();
	$("#document_date_fin_lancement").datepicker();
	$("#document_date_lancement_chaud_dd").datepicker();
	$("#document_date_lancement_chaud_dr").datepicker();

	/*
	 * Les champs ineditables
	 */
	$(".champ_non_editable").attr('disabled', true);

	/*
	 * Calculer les cycles correspondants
	 */
	$("#document_semaine_debut").bind('change', function() {
		get_cycles_correspondants('document', $("#document_semaine_debut").val(), $("#document_annee").val(), $("#document_semaine_fin").val(), $("#document_annee_fin").val(), $("#document_attach_list").val());
	});
	$("#document_semaine_fin").bind('change', function() {
		get_cycles_correspondants('document', $("#document_semaine_debut").val(), $("#document_annee").val(), $("#document_semaine_fin").val(), $("#document_annee_fin").val(), $("#document_attach_list").val());
	});
	$("#document_attach_list").bind('change', function() {
		get_cycles_correspondants('document', $("#document_semaine_debut").val(), $("#document_annee").val(), $("#document_semaine_fin").val(), $("#document_annee_fin").val(), $("#document_attach_list").val());
	});


	/*
	 * Ajouter multiple cibles et types
	 */
	 $("#add_new_cible_to_document").bind('click', function() {
			if($("#document_cible_list").val() != '') {
				if($("#document_cible").val() == '') {
					$("#document_cible_label").val(String($("#document_cible_list option:selected").text()));
					$("#document_cible").val(String($("#document_cible_list").val()));
				} else {
					var temp = '';
					temp = $("#document_cible").val();
					if(temp.indexOf($("#document_cible_list").val()) == -1) {
						$("#document_cible").val($("#document_cible").val() + ',' + String($("#document_cible_list").val()));
						$("#document_cible_label").val($("#document_cible_label").val() + ' + ' + String($("#document_cible_list option:selected").text()));
					}
				}
			}
		});
		$("#document_cible_list").change(function() {
			if($("#document_cible_list").val() == '') {
				$("#document_cible_label").val('');
				$("#document_cible").val('');
			}
		});

	/*
	 * Afficher champs dates lancment à  chaud pour DD et DR quand 
	 * le boutton radio lancement à  chaud est coché
	 */
	$('input[type=radio][name=document_lancement_chaud]').change(function() {
		if($(this).val() != 0) {
			$("#lancment_chaud_dd").show();
			$("#lancment_chaud_dr").show();
		} else {
			$("#lancment_chaud_dd").hide();
			$("#lancment_chaud_dr").hide();
		}
	});

	/*
	 * Ajout d'un nouveau document et aller dans la liste des cycles
	 */
	$("#save_new_document").submit(function() {
		var empty = false;
		var empty_msg = "Vous devez aussi saisir les champs suivants: \n";
		if($("#document_semaine_debut").val() == '' || $("#document_annee").val() == '' ) {
			empty = true;
			empty_msg += "L'année et la semaine de début \n";
		}
		if($("#document_semaine_fin").val() == '' || $("#document_annee_fin").val() == '' ) {
			empty = true;
			empty_msg += "L'année et la semaine de fin \n";
		}
		if($("#document_reference").val() == '') {
			empty = true;
			empty_msg += "La référence du document \n";
		}
		if($("#document_designation").val() == '') {
			empty = true;
			empty_msg += "La désignation du document \n";
		}
		if($("#document_cible").val() == '') {
			empty = true;
			empty_msg += "Une ou plusieurs cibles \n";
		}
		if($("#document_date_debut_lancement").val() == '' || $("#document_date_fin_lancement").val() == '') {
			empty = true;
			empty_msg += "Les dates de début et de fin de lancement \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à  chaud DD \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à  chaud DR \n";
		}

		if(empty) alert(empty_msg);
		if(empty) return false;

		var data = '';
		data += 'cycle_ou_periode=' + $("#document_attach_list").val() + '&';
		data += 'annee=' + $("#document_annee").val() + '&';
		data += 'annee_fin=' + $("#document_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#document_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#document_semaine_fin").val() + '&';
		data += 'reference=' + encodeURIComponent($("#document_reference").val()) + '&';
		data += 'designation=' + encodeURIComponent($("#document_designation").val()) + '&';
		data += 'cible=' + $("#document_cible").val() + '&';
		data += 'critere_distribution=' + encodeURIComponent($("#document_critere_distribution").val()) + '&';
		data += 'reference_lot_appartenance=' + encodeURIComponent($("#document_reference_lot_appartenance").val()) + '&';
		data += 'reference_extranet=' + encodeURIComponent($("#document_reference_extranet").val()) + '&';
		data += 'qte_doc_par_lot=' + $("#document_qte_doc_par_lot").val() + '&';
		data += 'qte_doc_imprimes=' + $("#document_qte_doc_imprimes").val() + '&';
		data += 'date_debut_pre_lancement=' + $("#document_date_debut_pre_lancement").val() + '&';
		data += 'date_fin_pre_lancement=' + $("#document_date_fin_pre_lancement").val() + '&';
		data += 'qt_pre_lancement=' + $("#document_qt_pre_lancement").val() + '&';
		data += 'date_debut_lancement=' + $("#document_date_debut_lancement").val() + '&';
		data += 'date_fin_lancement=' + $("#document_date_fin_lancement").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=document_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=document_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=document_publie]:checked').val() + '&';
		data += 'is_mail=' + $('input[type=radio][name=document_is_mail]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#document_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#document_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=D&';
		}
		data += 'ajax=1';

		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg != '-1') {
					alert('Document bien ajouté');
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					window.setTimeout("redirect2DetailDocument("+msg+")", 2000);
				} else {
					$("#info").html('Erreur!!!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				}
			},
			error: function() {
			}
		});

		return false;
	});

	/*
	 * AJout d'un nouveau document et rester dans la meme page pour ajouter un autre
	 */
	$("#save_new_next_document").bind('click', function() {
		var empty = false;
		var empty_msg = "Vous devez aussi saisir les champs suivants: \n";
		if($("#document_semaine_debut").val() == '' || $("#document_annee").val() == '' ) {
			empty = true;
			empty_msg += "L'année et la semaine de début \n";
		}
		if($("#document_semaine_fin").val() == '' || $("#document_annee_fin").val() == '' ) {
			empty = true;
			empty_msg += "L'année et la semaine de fin \n";
		}
		if($("#document_reference").val() == '') {
			empty = true;
			empty_msg += "La référence du document \n";
		}
		if($("#document_designation").val() == '') {
			empty = true;
			empty_msg += "La désignation du document \n";
		}
		if($("#document_cible").val() == '') {
			empty = true;
			empty_msg += "Une ou plusieurs cibles \n";
		}
		if($("#document_date_debut_lancement").val() == '' || $("#document_date_fin_lancement").val() == '') {
			empty = true;
			empty_msg += "Les dates de début et de fin de lancement \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dd").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à  chaud DD \n";
		}
		if($('input[type=radio][name=document_lancement_chaud]:checked').val() == 1 && $("#document_date_lancement_chaud_dr").val() == ''){
			empty = true;
			empty_msg += "La date de lancement à  chaud DR \n";
		}

		if(empty) alert(empty_msg);
		if(empty) return false;

		var data = '';
		data += 'cycle_ou_periode=' + $("#document_attach_list").val() + '&';
		data += 'annee=' + $("#document_annee").val() + '&';
		data += 'annee_fin=' + $("#document_annee_fin").val() + '&';
		data += 'semaine_debut=' + $("#document_semaine_debut").val() + '&';
		data += 'semaine_fin=' + $("#document_semaine_fin").val() + '&';
		data += 'cycles=' + $("#document_cycles").val() + '&';
		data += 'reference=' + encodeURIComponent($("#document_reference").val()) + '&';
		data += 'designation=' + encodeURIComponent($("#document_designation").val()) + '&';
		data += 'cible=' + $("#document_cible").val() + '&';
		data += 'critere_distribution=' + encodeURIComponent($("#document_critere_distribution").val()) + '&';
		data += 'reference_lot_appartenance=' + encodeURIComponent($("#document_reference_lot_appartenance").val()) + '&';
		data += 'reference_extranet=' + encodeURIComponent($("#document_reference_extranet").val()) + '&';
		data += 'qte_doc_par_lot=' + $("#document_qte_doc_par_lot").val() + '&';
		data += 'qte_doc_imprimes=' + $("#document_qte_doc_imprimes").val() + '&';
		data += 'date_debut_pre_lancement=' + $("#document_date_debut_pre_lancement").val() + '&';
		data += 'date_fin_pre_lancement=' + $("#document_date_fin_pre_lancement").val() + '&';
		data += 'qt_pre_lancement=' + $("#document_qt_pre_lancement").val() + '&';
		data += 'date_debut_lancement=' + $("#document_date_debut_lancement").val() + '&';
		data += 'date_fin_lancement=' + $("#document_date_fin_lancement").val() + '&';
		data += 'action_info=' + $('input[type=radio][name=document_action_info]:checked').val() + '&';
		data += 'lancement_chaud=' + $('input[type=radio][name=document_lancement_chaud]:checked').val() + '&';
		data += 'publie=' + $('input[type=radio][name=document_publie]:checked').val() + '&';
		data += 'is_mail=' + $('input[type=radio][name=document_is_mail]:checked').val() + '&';
		data += 'date_lancement_chaud_dd=' + $("#document_date_lancement_chaud_dd").val() + '&';
		data += 'date_lancement_chaud_dr=' + $("#document_date_lancement_chaud_dr").val() + '&';
		if ($('#files_to_attach').val() != '') {
			data += 'files_to_attach=' + $('#files_to_attach').val().trim() + '&';
			data += 'type_entite=D&';
		}
		data += 'ajax=1';
		
		$.ajax({
			url: '',
			type: 'get',
			data: data,
			success: function(msg) {
				if(msg != '-1') {
					send_comment('dr', $("#pod_cible").val(), msg);
					send_comment('dd', $("#pod_cible").val(), msg);
					$("#document_reference").val('');
					$("#document_designation").val('');
					$("#document_critere_distribution").val('');
					$("#document_reference_lot_appartenance").val('');
					$("#document_reference_extranet").val('');
					$("#document_qte_doc_par_lot").val('');
					$("#document_qte_doc_imprimes").val('');
					$("#files_to_attach").val('');
					$(".doc_attach_element").remove();
					alert('Document bien ajouté');
					//Commentaires
					tinyMCE.get('mceEditor_dd').setContent('');
					tinyMCE.get('mceEditor_dr').setContent('');
				} else {
					$("#Erreur").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				}
			},
			error: function() {
			}
		});

		return false;
	});

	
	//AIMED1504

		$('.delete_doc').unbind('click').live('click', function() {
			var id_doc = $(this).attr('id').replace('del*', '');
			var $complete_path = $(this).parent().find("a").attr('href');
			$filename = $(this).parent().find("a").html();
			var check = confirm('Voulez-vous vraiment supprimer ce document ?');
			if (check) {
				$(this).parent().remove();
					$.ajax({
							url: '',
							type: 'get',
							data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=2',
							success: function(msg) {
							},
							error: function() {
							}
						});
				}
		});
})( jQuery );

function redirect2DetailDocument(id_new_doc)
{
	window.location = '<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc='?>' + id_new_doc;
}
</script>