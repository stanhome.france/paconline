<style>

.frame_print {
	border: 1px solid #D1D1D1;border-radius: 10px 10px 10px 10px;clear: both;  margin-bottom: 10px; padding: 20px;width: 670px;font:0.78em arial;
}
table td,table th{padding:5px;}
table.tab1 td,table.tab1 th{text-align:center;border:1px solid #010101;}
h3{font: bold 14px arial;margin-bottom: 10px;color: #00214A;}
.radio_date td {padding:5px;}

</style>
<?php
require_once 'include-functions.php';

$annee_en_cours = date('Y');
$options_numeros_cycles = get_liste_numeros_cycles();

$marques = get_liste_marque();
$cibles = get_liste_cible();
$types = get_liste_event();
$gammes = get_liste_gamme();

$pod_cible = 'Cycles';
$id_pod_cible = $_GET['id_cycle'];
		
$current_cycle = Pods('cycles', $_GET['id_cycle']);
$data_current_cycle = $current_cycle->data();
$data_current_cycle = $data_current_cycle[0];
?>

<?php 
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
##########################################################################
$is_cycle = false;
$where_temp = '';
if ($data_current_cycle->permalink == 'cycles') {
	$is_cycle = true;
	$where_temp = ' AND permalink=\'cycles\'';
} else {
	$where_temp = ' AND permalink=\'periodes\'';
}
?>

<h3>Fiche <?php echo $data_current_cycle->identifiant?></h3>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Publié: <?php echo ($data_current_cycle->publie == 1 ? 'Oui' : 'Non')?><br/>
Action Info: <?php echo ($data_current_cycle->action_info == 1 ? 'Oui' : 'Non')?><br/>
Archivé: <?php echo ($data_current_cycle->archive == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud: <?php echo ($data_current_cycle->lancement_chaud == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud DD: <?php echo pod2js_date($data_current_cycle->date_lancement_chaud_dd) ?><br/>
Lancement à chaud DR: <?php echo pod2js_date($data_current_cycle->date_lancement_chaud_dr) ?><br/><br/>
</td>
<td width="350">

Semaine de début: <?php echo 'S' . $data_current_cycle->semaine_debut . ' ' . $data_current_cycle->annee . ' (' . pod2js_date($data_current_cycle->date_debut) . ')'?><br/>
Semaine de fin: <?php echo 'S' . $data_current_cycle->semaine_fin . ' ' . $data_current_cycle->annee_fin . ' (' . pod2js_date($data_current_cycle->date_fin) . ')'?><br/><br/>

Diffusion aux DD: <?php echo pod2js_date($data_current_cycle->diffusion_dd)?><br/>
Diffusion aux DR: <?php echo pod2js_date($data_current_cycle->diffusion_dr)?><br/>
Date début commerciale: <?php echo pod2js_date($data_current_cycle->date_debut_commerciale)?><br/>
Date de fin commerciale: <?php echo pod2js_date($data_current_cycle->date_fin_commerciale)?><br/><br/>
</td>
</tr>
</table>
<?php
	$temp1 = formatCommentText($content_comment_dd);
	$temp2 = formatCommentText($content_comment_dr);
?>
<h3>Commentaires visibles par les DD</h3>
<div class="frame_print">
<?php echo $temp1?>
</div>
<h3>Commentaires visibles par les DR</h3>
<div class="frame_print">
<?php echo $temp2?>
</div>
<?php 
$proms_to_print_stanhome = array();
$proms_to_print_kiotis = array();
$events_to_print = array();
$docs_to_print = array();
?>
<h3>Entités attachées <?php echo ($is_cycle) ? ' au cycle' : ' à la période'?></h3>
<div class="frame_print">
<?php 
$pod_cycles_promos = Pods('cycles_promos', array('where' => 'id_cycle='.$data_current_cycle->id, 'limit' => '-1'));
if ($pod_cycles_promos->total() > 0):
$stanhome_key = array_search('Stanhome', $marques);
?>
	<h6>Promotions Stanhome</h6>
	<?php 
		while ($pod_cycles_promos->fetch()):
			$pod_promos = Pods('promotions', array('where' => 'id='.$pod_cycles_promos->field('id_promo') . $where_temp . ' AND marque=' . $stanhome_key, 'limit' => '-1'));
			while ($pod_promos->fetch()):
				$proms_to_print_stanhome[] = $pod_promos->field('id');
				$promo_identifiant = $pod_promos->display('reference_extranet').' : '.$pod_promos->display('description');
				echo '+ ' . stripslashes($promo_identifiant) . '<br />';
			endwhile;
		endwhile;
		if (count($proms_to_print_stanhome) == 0) echo 'Pas de promotions attachées';
	?>
<?php endif;?>
<?php 
$pod_cycles_promos = Pods('cycles_promos', array('where' => 'id_cycle='.$data_current_cycle->id, 'limit' => '-1'));
if ($pod_cycles_promos->total() > 0):
$kiotis_key = array_search('Kiotis', $marques);
?>
	<h6>Promotions Kiotis</h6>
	<?php 
		while ($pod_cycles_promos->fetch()):
			$pod_promos = Pods('promotions', array('where' => 'id='.$pod_cycles_promos->field('id_promo') . $where_temp . ' AND marque=' . $kiotis_key, 'limit' => '-1'));
			while ($pod_promos->fetch()):
				$proms_to_print_kiotis[] = $pod_promos->field('id');
				$promo_identifiant = $pod_promos->display('reference_extranet').' : '.$pod_promos->display('description');
				echo '+ ' . stripslashes($promo_identifiant) . '<br />';
			endwhile;
		endwhile;
		if (count($proms_to_print_kiotis) == 0) echo 'Pas de promotions attachées';
	?>
<?php endif;?>
<?php 
$pod_cycles_events = Pods('cycles_events', array('where' => 'id_cycle='.$data_current_cycle->id, 'limit' => '-1'));
if ($pod_cycles_events->total() > 0):
?>
	<h6>Evènements</h6>
	<?php 
		while ($pod_cycles_events->fetch()):
			$pod_events = Pods('events', array('where' => 'id='.$pod_cycles_events->field('id_event') . $where_temp, 'limit' => '-1'));
			while ($pod_events->fetch()):
				$events_to_print[] = $pod_events->field('id');
				$event_designation = $pod_events->display('designation');
				echo '+ ' . stripslashes($event_designation) . '<br />';
			endwhile;
		endwhile;
		if (count($events_to_print) == 0) echo 'Pas d\'events attachés';
	?>
<?php endif;?>

<?php 
$pod_cycles_docs = Pods('cycles_docs', array('where' => 'id_cycle='.$data_current_cycle->id, 'limit' => '-1'));
if ($pod_cycles_docs->total() > 0):
?>
	<h6>Documents</h6>
	<?php 
		while ($pod_cycles_docs->fetch()):
			$pod_docs = Pods('documents', array('where' => 'id='.$pod_cycles_docs->field('id_doc') . $where_temp, 'limit' => '-1'));
			while ($pod_docs->fetch()):
				$docs_to_print[] = $pod_docs->field('id');
				$doc_designation = $pod_docs->display('designation');
				echo '+ ' . stripslashes($doc_designation) . '<br />';
			endwhile;
		endwhile;
		if (count($docs_to_print) == 0) echo 'Pas de documents attachés';
	?>
<?php endif;?>
</div>
<?php if (count($proms_to_print_stanhome) > 0 || count($proms_to_print_kiotis) > 0 || count($events_to_print) > 0 || count($docs_to_print) > 0):?>
<h4>Les informations detaillées des entités attachées</h4>

<!-- Fiches promotions Stanhome attachéées -->
<?php if (count($proms_to_print_stanhome) > 0):?>
<h4>Promotions Stanhome</h4>
<?php foreach ($proms_to_print_stanhome as $item):?>
<?php 
$promotion = Pods('promotions', array('where' => "id=$item"));
while ($promotion->fetch()):
?>
<h3>Fiche <?php echo stripslashes($promotion->display('reference_extranet') .' : '.$pod_promos->display('description'))?> </h3>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Publié: <?php echo ($promotion->field('publie') == 1 ? 'Oui' : 'Non')?><br/>
Action Info: <?php echo ($promotion->field('action_info') == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud: <?php echo ($promotion->field('lancement_chaud') == 1 ? 'Oui' : 'Non')?><br/>
<?php if ($promotion->field('lancement_chaud') == 1):?>
Lancement à chaud DD: <?php echo formatDate2print($promotion->display('date_lancement_chaud_dd')) ?><br/>
Lancement à chaud DR: <?php echo formatDate2print($promotion->display('date_lancement_chaud_dr')) ?><br/><br/>
<?php endif;?>
</td>
<td width="350">
Semaine de début: <?php echo 'S' . $promotion->field('semaine_debut') . ' ' . $promotion->field('annee')?><br/>
Semaine de fin: <?php echo 'S' . $promotion->field('semaine_fin') . ' ' . $promotion->field('annee_fin')?><br/><br/>
</td>
</tr></table>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Cycle(s): <?php echo $promotion->display('cycles')?><br/>
Marque: <?php echo $marques[$promotion->display('marque')]?><br/>
Gamme: <?php echo $gammes[$promotion->display('gamme')]?><br/>
Référence facturation: <?php echo $promotion->display('')?><br/>
Référence Lot: <?php echo $promotion->display('reference_lot')?><br/>
Prévision: <?php echo $promotion->display('prevision')?><br/>
Qté. limitée: <?php echo $promotion->display('qte_limitee')?><br/><br/>
</td>
<td width="350">
Début Pre_Lancement: <?php echo formatDate2print($promotion->display('date_debut_pre_lancement'))?><br/>
Fin Pre_Lancement: <?php echo formatDate2print($promotion->display('date_fin_pre_lancement'))?><br/>
Qté. pré_Lancement: <?php echo $promotion->display('qte_pre_lancement')?><br/>
Début Lancement: <?php echo formatDate2print($promotion->display('date_debut_lancement'))?><br/>
Fin lancement: <?php echo formatDate2print($promotion->display('date_fin_lancement'))?><br/><br/>
</td></tr></table>
TVA: <?php echo $promotion->display('tva')?><br/>
Prix N. TTC: <?php echo $promotion->field('prix_normal_ttc')?> &euro;<br/>
Prix N. HT: <?php echo $promotion->field('prix_normal_ht')?> &euro;<br/>
Prix P. TTC: <?php echo $promotion->field('prix_promo_ttc')?> &euro;<br/>
Prix P. HT: <?php echo $promotion->field('prix_promo_ht')?> &euro;<br/><br/>

<?php
$data_current_promo = $promotion->data();
$data_current_promo = $data_current_promo[0];
$detail_promo = unserialize($data_current_promo->detail_promotion);
?>
<table cellpadding="0" cellspacing="0" class="tab1">
	<tr><th>Qté</th><th>Réf. Lot</th><th>Désignation</th><th>Réf. Article(s)</th><th>Prix N. HT</th><th>Prix N. TTC</th></tr>
	<?php foreach ($detail_promo as $item):?>
	<tr>
	<td width="50"><?php echo $item['quantite']?></td>
	<td width="50"><?php echo $item['ref_lot']?></td>
	<td class="designation" width="198"><?php echo $item['designation']?></td>
	<td class="refs" width="150"><?php echo str_replace(',', ' ou ', $item['reference'])?></td>
	<td width="78" ><?php echo $item['prix_n_ht']?> &euro;</td>
	<td width="78"><?php echo $item['prix_n_ttc']?> &euro;</td>
	</tr>
	<?php endforeach;?>
</table>

<?php 
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='Promos' AND id_pod_cible=" . $promotion->field('id') . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='Promos' AND id_pod_cible=" . $promotion->field('id') . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
	$temp1 = formatCommentText($content_comment_dd);
	$temp2 = formatCommentText($content_comment_dr);
?>
<h3>Commentaires visibles par les DD</h3>
<div class="frame_print">
<?php echo $temp1?>
</div>
<h3>Commentaires visibles par les DR</h3>
<div class="frame_print">
<?php echo $temp2?>
</div>
<?php endwhile;?>
<?php endforeach;?>
<?php endif;?>

<!-- Fiches promotions attachéées -->
<?php if (count($proms_to_print_kiotis) > 0):?>
<h4>Promotions Kiotis</h4>
<?php foreach ($proms_to_print_kiotis as $item):?>
<?php 
$promotion = Pods('promotions', array('where' => "id=$item"));
while ($promotion->fetch()):
?>
<h3>Fiche <?php echo stripslashes($promotion->display('reference_extranet').' : '.$pod_promos->display('description'))?></h3>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Publié: <?php echo ($promotion->field('publie') == 1 ? 'Oui' : 'Non')?><br/>
Action Info: <?php echo ($promotion->field('action_info') == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud: <?php echo ($promotion->field('lancement_chaud') == 1 ? 'Oui' : 'Non')?><br/>
<?php if ($promotion->field('lancement_chaud') == 1):?>
Lancement à chaud DD: <?php echo formatDate2print($promotion->display('date_lancement_chaud_dd')) ?><br/>
Lancement à chaud DR: <?php echo formatDate2print($promotion->display('date_lancement_chaud_dr')) ?><br/><br/>
<?php endif;?>
</td>
<td width="350">
Semaine de début: <?php echo 'S' . $promotion->field('semaine_debut') . ' ' . $promotion->field('annee')?><br/>
Semaine de fin: <?php echo 'S' . $promotion->field('semaine_fin') . ' ' . $promotion->field('annee_fin')?><br/><br/>
</td>
</tr></table>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Cycle(s): <?php echo $promotion->display('cycles')?><br/>
Marque: <?php echo $marques[$promotion->display('marque')]?><br/>
Gamme: <?php echo $gammes[$promotion->display('gamme')]?><br/>
Référence facturation: <?php echo $promotion->display('')?><br/>
Référence Lot: <?php echo $promotion->display('reference_lot')?><br/>
Prévision: <?php echo $promotion->display('prevision')?><br/>
Qté. limitée: <?php echo $promotion->display('qte_limitee')?><br/><br/>
</td>
<td width="350">
Début Pre_Lancement: <?php echo formatDate2print($promotion->display('date_debut_pre_lancement'))?><br/>
Fin Pre_Lancement: <?php echo formatDate2print($promotion->display('date_fin_pre_lancement'))?><br/>
Qté. pré_Lancement: <?php echo $promotion->display('qte_pre_lancement')?><br/>
Début Lancement: <?php echo formatDate2print($promotion->display('date_debut_lancement'))?><br/>
Fin lancement: <?php echo formatDate2print($promotion->display('date_fin_lancement'))?><br/><br/>
</td></tr></table>
TVA: <?php echo $promotion->display('tva')?><br/>
Prix N. TTC: <?php echo $promotion->field('prix_normal_ttc')?> &euro;<br/>
Prix N. HT: <?php echo $promotion->field('prix_normal_ht')?> &euro;<br/>
Prix P. TTC: <?php echo $promotion->field('prix_promo_ttc')?> &euro;<br/>
Prix P. HT: <?php echo $promotion->field('prix_promo_ht')?> &euro;<br/><br/>

<?php
$data_current_promo = $promotion->data();
$data_current_promo = $data_current_promo[0];
$detail_promo = unserialize($data_current_promo->detail_promotion);
?>
<table cellpadding="0" cellspacing="0" class="tab1">
	<tr><th>Qté</th><th>Réf. Lot</th><th>Désignation</th><th>Réf. Article(s)</th><th>Prix N. HT</th><th>Prix N. TTC</th></tr>
	<?php foreach ($detail_promo as $item):?>
	<tr>
	<td width="50"><?php echo $item['quantite']?></td>
	<td width="50"><?php echo $item['ref_lot']?></td>
	<td class="designation" width="198"><?php echo $item['designation']?></td>
	<td class="refs" width="150"><?php echo str_replace(',', ' ou ', $item['reference'])?></td>
	<td width="78" ><?php echo $item['prix_n_ht']?> &euro;</td>
	<td width="78"><?php echo $item['prix_n_ttc']?> &euro;</td>
	</tr>
	<?php endforeach;?>
</table>

<?php 
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='Promos' AND id_pod_cible=" . $promotion->field('id') . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='Promos' AND id_pod_cible=" . $promotion->field('id') . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
	$temp1 = formatCommentText($content_comment_dd);
	$temp2 = formatCommentText($content_comment_dr);
?>
<h3>Commentaires visibles par les DD</h3>
<div class="frame_print">
<?php echo $temp1?>
</div>
<h3>Commentaires visibles par les DR</h3>
<div class="frame_print">
<?php echo $temp2?>
</div>
<?php endwhile;?>
<?php endforeach;?>
<?php endif;?>

<!-- Fiches events  attachés -->
<?php if (count($events_to_print) > 0):?>
<h4>Events:</h4>
<?php foreach ($events_to_print as $item):?>
<?php 
$event = pods('events', array('where' => "id=$item"));
while ($event->fetch()):
?>
<h3>Fiche <?php echo stripslashes($event->display('designation'))?></h3>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Publié: <?php echo ($event->field('publie') == 1 ? 'Oui' : 'Non')?><br/>
Action Info: <?php echo ($event->field('action_info') == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud: <?php echo ($event->field('lancement_chaud') == 1 ? 'Oui' : 'Non')?><br/>
<?php if ($event->field('lancement_chaud') == 1):?>
Lancement à chaud DD: <?php echo formatDate2print($event->display('date_lancement_chaud_dd')) ?><br/>
Lancement à chaud DR: <?php echo formatDate2print($event->display('date_lancement_chaud_dr')) ?><br/><br/>
<?php else:?>
<br/>
<?php endif;?>
</td>
<td width="350">
Semaine de début: <?php echo 'S' . $event->display('semaine_debut') . ' ' . $event->display('annee') . ' (' . formatDate2print($event->display('date_debut')) . ')'?><br/>
Semaine de fin: <?php echo 'S' . $event->display('semaine_fin') . ' ' . $event->display('annee_fin') . ' (' . formatDate2print($event->display('date_debut')) . ')'?><br/><br/>
</td></tr></table>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Type: <?php echo $types[$event->display('type_evenement')]?><br/>
<?php
	###cibles pour cet event###
	$this_event_cible_array = explode(',', $event->display('cible_evenement')); 
	$this_event_cible = array();
	foreach ($this_event_cible_array as $item) {
		if(isset($cibles[$item])) $this_event_cible[] = $cibles[$item];
	}
$this_event_cible = implode(' + ', $this_event_cible);
?>
Cible(s): <?php echo $this_event_cible?><br/>
Cycle(s): <?php echo $event->display('cycles_correspondants')?><br/><br/>
</td>
<td width="350">
Début Pre_Lancement: <?php echo formatDate2print($event->display('date_debut_pre_lancement'))?><br/>
Fin Pre_Lancement: <?php echo formatDate2print($event->display('date_fin_pre_lancement'))?><br/>
Qté. pré_Lancement: <?php echo $event->display('qte_pre_lancement')?><br/>
Début Lancement: <?php echo formatDate2print($event->field('date_debut_extranet'))?><br/>
Fin lancement: <?php echo formatDate2print($event->field('date_fin_extranet'))?><br/><br/>
</td></tr></table>
<?php 
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='Events' AND id_pod_cible=" . $event->field('id') . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='Events' AND id_pod_cible=" . $event->field('id') . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
	$temp1 = formatCommentText($content_comment_dd);
	$temp2 = formatCommentText($content_comment_dr);
?>
<h3>Commentaires visibles par les DD</h3>
<div class="frame_print">
<?php echo $temp1?>
</div>
<h3>Commentaires visibles par les DR</h3>
<div class="frame_print">
<?php echo $temp2?>
</div>
<?php endwhile;?>
<?php endforeach;?>
<?php endif;?>

<!-- Fiches documents  attachés -->
<?php if (count($docs_to_print) > 0):?>
<h4>Documents:</h4>
<?php foreach ($docs_to_print as $item):?>
<?php 
$doc = pods('documents', array('where' => "id=$item"));
while ($doc->fetch()):
?>
<h3>Fiche <?php echo stripslashes($doc->display('designation'))?></h3>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Publié: <?php echo ($doc->field('publie') == 1 ? 'Oui' : 'Non')?><br/>
Action Info: <?php echo ($doc->field('action_info') == 1 ? 'Oui' : 'Non')?><br/>
Lancement à chaud: <?php echo ($doc->field('lancement_chaud') == 1 ? 'Oui' : 'Non')?><br/>
<?php if ($doc->field('lancement_chaud') == 1):?>
Lancement à chaud DD: <?php echo formatDate2print($doc->display('date_lancement_chaud_dd')) ?><br/>
Lancement à chaud DR: <?php echo formatDate2print($doc->display('date_lancement_chaud_dr')) ?><br/><br/>
<?php else:?>
<br/>
<?php endif;?>
</td>
<td width="350">
Semaine de début: <?php echo 'S' . $doc->display('semaine_debut') . ' ' . $doc->display('annee')?><br/>
Semaine de fin: <?php echo 'S' . $doc->display('semaine_fin') . ' ' . $doc->display('annee_fin')?><br/><br/>
</td></tr></table>
<table cellpading="0" cellspacing="0" border="0" class="radio_date"><tr>
<td width="350">
Référence Document: <?php echo $doc->display('reference_document')?><br/>
<?php 
	###cibles pour cet event###
	$this_doc_cible_array = explode(',', $doc->display('cible'));
	$this_doc_cible = array();
	foreach ($this_doc_cible_array as $item) {
		$this_doc_cible[] = $cibles[$item];
	}
	$this_doc_cible = implode(' + ', $this_doc_cible);
?>
Cible(s): <?php echo $this_doc_cible?><br/>
Cycle(s): <?php echo $doc->display('cycles')?><br/>
Critères de distribution: <?php echo $doc->display('criteres_distribution')?><br/>
Réf. Lot d'appartenance: <?php echo $doc->display('reference_lot_appartenance')?><br/>
Référence Extranet: <?php echo $doc->display('reference_extranet')?><br/>
Qté doc. Dans un lot: <?php echo $doc->display('qte_doc_par_lot')?><br/>
Qté doc imprimés: <?php echo $doc->display('qte_doc_imprimes')?><br/><br/>
</td>
<td width="350">
Début Pre_Lancement: <?php echo formatDate2print($doc->display('date_debut_pre_lancement'))?><br/>
Fin Pre_Lancement: <?php echo formatDate2print($doc->display('date_fin_pre_lancement'))?><br/>
Qté. pré_Lancement: <?php echo $doc->display('qte_pre_lancement')?><br/>
Début Lancement: <?php echo formatDate2print($doc->field('date_debut_lancement'))?><br/>
Fin lancement: <?php echo formatDate2print($doc->field('date_fin_lancement'))?><br/><br/>
</td></tr></table>
<?php 
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='Docs' AND id_pod_cible=" . $doc->field('id') . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='Docs' AND id_pod_cible=" . $doc->field('id') . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
	$temp1 = formatCommentText($content_comment_dd);
	$temp2 = formatCommentText($content_comment_dr);
?>
<h3>Commentaires visibles par les DD</h3>
<div class="frame_print">
<?php echo $temp1?>
</div>
<h3>Commentaires visibles par les DR</h3>
<div class="frame_print">
<?php echo $temp2?>
</div>
<?php endwhile;?>
<?php endforeach;?>
<?php endif;?>

<?php endif;?>
