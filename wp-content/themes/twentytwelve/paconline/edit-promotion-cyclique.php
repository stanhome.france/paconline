<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_POST['ajax']) && $_POST['ajax'] == "upload_doc") {
	require_once 'attachments/upload-and-save-doc.php';
}

if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'promotions/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 1) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}
$annee_en_cours = date('Y');

$options_tva = get_liste_tva();
$options_marque = get_liste_marque();
$options_gamme = get_liste_gamme();

$selection_references = get_selection_reference();
if (count($selection_references) > 0) $no_articles = true;

if (!isset($_GET['id_promo']) || $_GET['id_promo'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$current_promo = Pods('promotions',  array('where' => "id=" . $_GET['id_promo'])); 
$data_current_promo= $current_promo->data();
$data_current_promo = $data_current_promo[0];
?>
<?php if (is_null($data_current_promo)):?>
<h3 class="ttl">Promotion Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$pod_cible = 'Promos';
$id_pod_cible = $_GET['id_promo'];
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
##########################################################################

?>
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/tools.js"></script>

<form id="edit_promo_form" action="" method="get">
	<input type="hidden" id="identifiant_promo" value="<?php echo $id_pod_cible; ?>"/>
	<input type="hidden" id="promotion_is_valid" value="oui">
	<input type="hidden" id="promo_detail_page" value="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' .$_GET['id_promo'] ?>">
	<?php 
		$detail_promo = explode('][', $data_current_promo->detail_promotion);
		$user = wp_get_current_user();
		$user = $user->user_login;
		$_SESSION[$user]['edit'] = $detail_promo;
		
		
	?>
	<input type="hidden" id="nombre_lignes_promo" value="<?php echo count($detail_promo)?>">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">	
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Modification Promotion : <span class="red"><?php echo stripslashes($data_current_promo->reference_extranet .' : '.$data_current_promo->description)?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>				
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Référence</label></td>
				<td><input type="text" id="promotion_reference_extranet" value="<?php echo stripslashes($data_current_promo->reference_extranet)?>" class="long"/></td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<?php require_once 'cycle-ou-periode.php';?>
			<tr>
        		<td align="right"><label>Description</label></td>
        		<td><input type="text" id="promotion_description" value="<?php echo stripslashes($data_current_promo->description)?>" class="long"/></td>
        	</tr>
			
			<tr>
				<td align="right"><label>Cycle(s)</label></td>
				<td align="left"><input type="text" id="promotion_cycles_correspondants" disabled="disabled" value="
				<?php echo get_cycles_between_two_weeks($data_current_promo->semaine_debut, $data_current_promo->annee, $data_current_promo->semaine_fin, $data_current_promo->annee_fin, $data_current_promo->permalink)?>" class="long"/></td>
			</tr>
			<tr>
				<td align="right"><label>Marque</label></td>
				<td>
					<select id="promotion_marque_list">
						<option value="">Choisir...</option>
						<?php if (count($options_marque) > 0):?>
							<?php foreach ($options_marque as $kle_marque => $val_marque):?>
								<option value="<?php echo $kle_marque?>" <?php if ($data_current_promo->marque == $kle_marque) echo 'selected'?>><?php echo  stripslashes($val_marque)?></option>
							<?php endforeach;?>				
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Gamme</label></td>
				<td>
					<select id="promotion_gamme_list">
						<option value="">Choisir...</option>
						<?php if (count($options_gamme) > 0):?>
							<?php foreach ($options_gamme as $kle_gamme => $val_gamme):?>
								<option value="<?php echo $kle_gamme?>" <?php if ($data_current_promo->gamme == $kle_gamme) echo 'selected'?>><?php echo  stripslashes($val_gamme)?></option>
							<?php endforeach;?>				
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Référence Facturation</label></td>
				<td><input type="text" id="promotion_reference_facturation" value="<?php echo $data_current_promo->reference_facturation?>" class="long"/></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Lot</label></td>
				<td><input type="text" id="promotion_reference_lot" value="<?php echo $data_current_promo->reference_lot?>" class="long"/></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><input type="text" id="promotion_date_debut_pre_lancement" value="<?php echo pod2js_date($data_current_promo->date_debut_pre_lancement)?>"></td>
							<td align="center"><input type="text" id="promotion_date_fin_pre_lancement" value="<?php echo pod2js_date($data_current_promo->date_fin_pre_lancement)?>"></td>
							<td align="center" class="brd_right"><input type="text" id="promotion_qte_pre_lancement" value="<?php echo $data_current_promo->qte_pre_lancement?>"></td>
						</tr>
						<tr>
							<th>Lancement</th>
							<td align="center"><input type="text" id="promotion_date_debut_lancement" value="<?php echo pod2js_date($data_current_promo->date_debut_lancement)?>"></td>
							<td align="center"><input type="text" id="promotion_date_fin_lancement" value="<?php echo pod2js_date($data_current_promo->date_fin_lancement)?>"></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Prévision</label></td>
				<td><input type="text" id="promotion_prevision" value="<?php echo $data_current_promo->prevision?>"></td>
			</tr>
			<tr>
				<td align="right"><label>Quantité limitée</label></td>
				<td>
					Oui<input type="radio" name="promotion_qte_limitee" value="1" <?php if ($data_current_promo->qte_limitee == 1) echo 'checked'?>>
					Non<input type="radio" name="promotion_qte_limitee" value="0" <?php if ($data_current_promo->qte_limitee == 0) echo 'checked'?>>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Prix TTC</th>
							<th align="center">Prix HT</th>
							<th align="center" class="brd_right">TVA</th>
						</tr>
						<tr>
							<th><label>Prix Normal</label></th>
							<td align="center"><input type="text" id="prix_n_ttc" value="<?php echo $data_current_promo->prix_normal_ttc?>" disabled="disabled"></td>
							<td align="center"><input type="text" id="prix_n_ht" value="<?php echo $data_current_promo->prix_normal_ht?>" disabled="disabled"></td>
							<td align="center" class="brd_right">
								<select id="tva_promo">
									<option value="">Choisir...</option>
									<?php if (count($options_tva) > 0):?>
										<?php foreach ($options_tva as $kle_tva => $val_tva):?>
											<option value="<?php echo $kle_tva?>" <?php if ($data_current_promo->tva == $kle_tva) echo 'selected'?>><?php echo  stripslashes($val_tva)?></option>
										<?php endforeach;?>				
									<?php endif;?>
								</select>
							</td>
						</tr>
						<tr>
							<th><label>Prix Promotion</label></th>
							<td align="center"><input type="text" id="prix_p_ttc" value="<?php echo $data_current_promo->prix_promo_ttc?>"></td>
							<td align="center"><input type="text" id="prix_p_ht" value="<?php echo $data_current_promo->prix_promo_ht?>"></td>
							<td  align="center" class="brd_right" style="border-right: 0px;">
								<input type="image" id="calcul_promo" alt="Calculer" src="<?php bloginfo('template_directory'); echo '/images/calculator.jpg' ?>">
							</td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="button" id="add_new_ligne" value="Ajouter une ligne >>" class="small">
				</td>
			</tr>
			<tr id="tr_references_en_promo">
				<td colspan="3">
					<table id="references_en_promo" cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<th align="center">Quantité</th>
							<th align="center">Réf. Lot</th>
							<th align="center">Désignation</th>
							<th align="center">Réf. Article(s)</th>
							<th align="center">Prix N. HT</th>
							<th align="center">Prix N. TTC</th>
							<th align="center" class="brd_right"></th>
						</tr>
						<?php $data = $data_current_promo->detail_promotion;
						$data = substr ( $data , 1 , -1 ) ;
						$articles = explode('][', $data);
						$detail_promo = array();
						foreach ($articles as $rowarticle) {
						    $article = explode('|', $rowarticle);
						    $promo = array( 
						        'quantite' =>  $article[0],
						        'ref_lot' =>  $article[1],
						        'designation' =>  $article[2],
						        'reference' =>  $article[3],
						        'prix_n_ht' =>  $article[4],
						        'prix_n_ttc' =>  $article[5]
						        );
						    array_push($detail_promo, $promo);
						}
						
						
						if (count($detail_promo) > 0):?>
							<?php foreach ($detail_promo as $kle => $item):?>
						<tr id="<?php echo $kle?>">
							<td><input type="text" id="qte_<?php echo $kle?>" class="qte_obligatoire" style= "width: 65%;" onchange="changeqtes()" value="<?php echo $item['quantite']?>"></td>
							<td><textarea id="ref_lot_<?php echo $kle?>" ><?php echo $item['ref_lot']?></textarea></td>
							<td><textarea id="design_<?php echo $kle?>" ><?php echo $item['designation']?></textarea></td>
							<td>
							<?php 
								$temp = explode(',', $item['reference']);
							?>
								<?php if (count($temp) > 1):?>
								<input type="hidden" id="refs_<?php echo $kle?>" value="<?php echo str_replace('_', ',', $item['reference'])?>">
								<textarea id="refs_show_<?php echo $kle?>" ><?php echo str_replace(',', ' ou ', $item['reference'])?></textarea>
								<?php else:?>
								<input type="text" id="refs_<?php echo $kle?>" value="<?php echo $item['reference']?>" style= "width: 94%;"/>
								<?php endif;?>
							</td>
							<td><input type="text" id="prx_n_ht_<?php echo $kle?>" value="<?php echo $item['prix_n_ht']?>" disabled="disabled"></td>
							<td><input type="text" id="prx_n_ttc_<?php echo $kle?>" value="<?php echo $item['prix_n_ttc']?>" disabled="disabled"></td>
							<td class="brd_right"><input type="image" src="<?php bloginfo('template_directory'); echo '/images/remove.png' ?>" alt="Supprimer" onclick="delete_ligne(1, '<?php echo $kle?>');return false;"></td>
						</tr> 
							<?php endforeach;?>
						<?php endif;?>
						<tr class="last_tr_promo"><td colspan="7" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</form>


<script type="text/javascript" src="<?php echo includes_url('js/jqdialog/jqdialog.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo includes_url('js/jqdialog/jqdialog.css') ?>">

<?php include_once 'comment-bloc.php';?>


<!-- Ici, les documents attachés -->
<?php
	$type_entity = 'P';
	$my_entity_id = $id_pod_cible; 
	include_once("remarques/remarques-visu.php");
	include_once('attachments/attachment-doc-creation.php');
?>

<?php endif;?>
<?php endif;?>

<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/grid-localisation.js"; ?>"></script>
<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/edit-promotion-cyclique.js"; ?>"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	get_cycles_correspondants('promotion', $("#promotion_semaine_debut").val(), $("#promotion_annee").val(), $("#promotion_semaine_fin").val(), $("#promotion_annee_fin").val(), $("#promotion_attach_list").val());
	
	
	/*
	 * Ouvrir popup pour séléctionner les références pour une promo
	 */
	$('#add_new_ligne').click( function() {
		if($("#tva_promo").val() == '') alert('Choisissez Le taux du TVA');
		if($("#tva_promo").val() == '') return false;
		var content = '';
		content += '<table>';
		content += '<tr id="popup_actions"><td align="center"><input type="button" value="Valider" id="valid_choice" onclick="add_ligne(\'<?php bloginfo('template_directory'); echo '/images/remove.png' ?>\')" class="small"></td>';
		content += '<td align="center"><input type="button" value="Annuler" id="cancel_choice" onclick="cancel_add_ligne()" class="small"></td>';
		content += '</tr></table><br>';
		content += '<table id="jqgrid"></table><div id="pagergrid"></div>';
		$.jqDialog.content(content);
		//var url = '<?php echo get_permalink(get_page_by_path('recuperation-des-articles-pour-le-grid-popin'))?>';
		var url = '<?php echo get_site_url() . '/recuperation-des-articles-pour-le-grid-popin.php'?>';
		var i = 0;
		list = new Array;
		$("#jqgrid").jqGrid({ 
			url:url, 
			datatype: "json",
			height: 'auto', 
			colNames:['Référence','Désignation'], 
			colModel:[ {name:'reference',index:'reference', width:150, searchoptions:{sopt: ['cn', 'bw', 'eq']}}, {name:'designation',index:'designation', width:500, searchoptions:{sopt: ['cn', 'bw', 'eq']}}], 
				rowNum:10, 
				rowList:[5,10,20],  
			pager: '#pagergrid', 
			sortname: 'reference', 
			viewrecords: true, 
			sortorder: "asc", 
			multiselect: true,
			emptyrecords: "Aucun article trouvé",
			caption:"Liste des références",
			gridComplete: function() {
				var ids = $(this).jqGrid('getDataIDs');
				for (var i = 0; i < ids.length; i++){
				    if (in_array(list, ids[i])){
				    	$(this).setSelection(ids[i], false);
				    }
				}
			},
			onSelectRow: function(reference) {
				if(!in_array(list, reference)) {
					list[i] = reference;
					i++;
				} else {
					delete list[list.indexOf(reference)];
				}
			} 
		});
		$("#jqgrid").jqGrid('navGrid','#pagergrid',{edit:false,add:false,del:false});
		window.setTimeout("centerPopin()",700);
	} );

	// **************************************************************************
	// pour la suppression de fichiers rattachÃ©s
	// **************************************************************************
	
	//AIMED1504
	<?php
		$my_entity_id = $_GET['id_promo'];
		$pod = Pods("attachments", array("where" => "id_entity=".$my_entity_id." and type='".$type_entity."'"));
		if ($pod->fetch()) {
			do {
	?>
	            var value =" <?php echo basename($pod->display('complete_path')); ?>" ;
				var id = <?php echo basename($pod->display('id')); ?> ;
	            
				$('#all_docs').append('<div class="doc_attach_element"><img src="../wp-content/themes/twentytwelve/images/pictos/picto_document.JPG" width=20 align="absmiddle" style="margin-right:10px;"/><img src="../wp-content/themes/twentytwelve/images/remove.png" width=20 align="absmiddle" style="margin-right:10px; cursor:pointer;" class="delete_doc" id="del*' + id + '"/><a href="<?php echo get_option("siteurl")."/wp-content/uploads/".date('Y')."/".date('m')."/"; ?>' + value + '" target="_blank">' + value + '</a><div class="clearfix"></div></div>');
	            
	<?php
			} while($pod->fetch());
		}
	?>

		$('#all_docs').append('<div style="clear:both;" id="clearboth"></div>');

		$('.delete_doc').unbind('click').live('click', function() {
			var id_doc = $(this).attr('id').replace('del*', '');
			var $complete_path = $(this).parent().find("a").attr('href');
			$filename = $(this).parent().find("a").html();
			var check = confirm('Voulez-vous vraiment supprimer ce document ?');
			if (check) {
				$(this).parent().remove();
					$.ajax({
							url: '',
							type: 'get',
							data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=1',
							success: function(msg) {
							},
							error: function() {
							}
						});
				}
		});
	});

function centerPopin() {
	$.jqDialog.maintainPosition($('#jqDialog_box'));
}
function changeqtes()
{
	var composition = construct_lignes_promo();
	ajax2valid_lignes_promo(composition);
	}

</script>