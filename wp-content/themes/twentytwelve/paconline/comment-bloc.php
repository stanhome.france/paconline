<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>

<div class="comment">
	<?php if ((isset($_SESSION['readonly_tinymce']) && $_SESSION['readonly_tinymce']) || isset($_GET['is_prev'])):?>
		<!-- Mode Visualisation -->
		<?php 
			$to_display_part = '<h3 class="dd ttl">Commentaires visibles par les DD</h3>';
			$orig = stripslashes($content_comment_dd);
			$temp1 = str_replace('<pre>', '<div>', $orig);
			$temp1 = str_replace('<pre dir="ltr">', '<div>', $temp1);
			$temp1 = str_replace('<pre ="">', '<div>', $temp1);
			$temp1 = str_replace('</pre>', '</div>', $temp1);
			$to_display_part .= '<div class="frame">';
			$to_display_part .= '<div>'.$temp1.'</div>';
			$to_display_part .= '<div class="clearfix"></div>'; 
			$to_display_part .= '</div>';
			echo do_shortcode( '[groups_member group="StanHome_CellulePAC,StanHome_DD,StanHome_DR,StanHome_ComitePAC,StanHome_Presta"]'.$to_display_part.'[/groups_member]' );
				
			$to_display_part = '<h3 class="ttl">Commentaire non accessible aux DD</h3>';
			$orig = stripslashes($content_comment_dr);
			$temp2 = str_replace('<pre>', '<div>', $orig);
			$temp2 = str_replace('<pre dir="ltr">', '<div>', $temp2);
			$temp2 = str_replace('<pre ="">', '<div>', $temp2);
			$temp2 = str_replace('</pre>', '</div>', $temp2);
			$to_display_part .= '<div class="frame">';
			$to_display_part .= '<div>'.$temp2.'</div>';
			$to_display_part .= '<div class="clearfix"></div>'; 
			$to_display_part .= '</div>';
			echo do_shortcode( '[groups_non_member group="StanHome_DD"]'.$to_display_part.'[/groups_non_member]' );
			?>
	<?php else:?>

		<!-- Mode Edition -->
		<h3 class="dd ttl">Commentaires visibles par les DD</h3>
		<!-- Contenu du commentaire dd actuellement sauvegardé en base de données (pour comparaison à l'upload d'un fichier)-->
		<div id="dd_comment_saved" style="display: none;"><?php if(isset($content_comment_dd)) echo stripslashes($content_comment_dd)?></div>
		<div class="frame">
			<textarea id="mceEditor_dd" cols="80"><?php if(isset($content_comment_dd)) echo stripslashes($content_comment_dd)?></textarea>
			<div class="clearfix"></div> 
		</div>
		<h3 class="ttl">Commentaire non accessible aux DD</h3>
		<!-- Contenu du commentaire dr actuellement sauvegardé en base de données (pour comparaison à l'upload d'un fichier)-->
		<div id="dr_comment_saved" style="display: none;"><?php if(isset($content_comment_dr)) echo stripslashes($content_comment_dr)?></div>
		<div class="frame">
			<textarea id="mceEditor_dr" cols="80"><?php if(isset($content_comment_dr)) echo stripslashes($content_comment_dr)?></textarea>
			<div class="clearfix"></div> 
		</div>
		<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>
		<script type="text/javascript">
			( function( $ ) {
				tinyMCE.init({
					height: "1200",
			        mode : "exact",
			        elements : "mceEditor_dd",
			        language : "fr",
			        entity_encoding : "raw",
			        plugins : "paste,table",
			        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
			        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
			        theme_advanced_buttons3 : "tablecontrols"
			                 });
				tinyMCE.init({
					height: "1200",
					mode : "exact",
			        elements : "mceEditor_dr",
			        language : "fr",
			        entity_encoding : "raw",
			        plugins : "paste,table",
			        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
			        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
			        theme_advanced_buttons3 : "tablecontrols"
			     });	
			})( jQuery );
		</script>
	<?php endif;?>
</div>

<input type="hidden" id="url_to_send_comment" value="<?php echo get_permalink(get_page_by_path('commentaires-ajax'))?>">
<input type="hidden" id="pod_cible" value="<?php echo $pod_cible?>">
<input type="hidden" id="id_pod_cible" value="<?php if (isset($id_pod_cible)) echo $id_pod_cible?>">
 
<?php unset($_SESSION['raeadonly_tinymce'])?>