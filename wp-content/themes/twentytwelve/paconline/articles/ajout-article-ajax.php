<?php
require_once dirname(dirname(__FILE__)) . '/include-functions.php';

$error = '';

if (!isset($_GET['designation'])) {
	$error = -3;
}
if (!isset($_GET['reference'])) {
	$error = -3;
}
if (!isset($_GET['tva'])) {
	$error = -3;
}
if (!isset($_GET['ht'])) {
	$error = -3;
}
if (!isset($_GET['ttc'])) {
	$error = -3;
}

/*
 * Ajouter un nouvel Article
 */
if ($_GET['ajax'] == 1) {
	if ($error == -3) {
		echo $error;
		exit();
	}
	if (!is_numeric($_GET['ht'])) {
		echo 'ht';
		exit();
	}
	
	if (!is_numeric($_GET['ttc'])) {
		echo 'ttc';
		exit();
	}
	
	if (entiteExiste('articles', $_GET['reference'])) {
		echo 'article_existe';
		exit();
	}
	
	$article_pod = Pods('articles');
	
	$liste_tva = get_liste_tva();
	$tva_val = $liste_tva[$_GET['tva']];
	$ht = $_GET['ttc'] / (1 + $tva_val / 100);
	
	if ($ht == 0) $ht = (string)$ht;
	$data = array(
		'designation' => $_GET['designation'],
		'reference' => $_GET['reference'],
		'tva' => $_GET['tva'],
		'prix_ttc' => $_GET['ttc'],
		'prix_ht' => $ht
	);
	$new_article_id = $article_pod->add($data);
	echo $new_article_id;
}

/*
 * Modifier un article
 */
if ($_GET['ajax'] == 2) {
	if ($error == -3) {
		echo $error;
		exit();
	}
	if (!is_numeric($_GET['ht'])) {
		echo 'ht';
		exit();
	}
	
	if (!is_numeric($_GET['ttc'])) {
		echo 'ttc';
		exit();
	}
	
	
	if (isset($_GET['id_article']) && $_GET['id_article'] > 0) {
		
		if (entiteExiste('articles', $_GET['reference'], true, $_GET['id_article'])) {
			echo 'article_existe';
			exit();
		}
		$article_pod = Pods('articles');
		
		$liste_tva = get_liste_tva();
		$tva_val = $liste_tva[$_GET['tva']];
		$ht = $_GET['ttc'] / (1 + $tva_val / 100);
		if ($ht == 0) $ht = (string)$ht;
		$data = array(
			'designation' => $_GET['designation'],
			'reference' => $_GET['reference'],
			'tva' => $_GET['tva'],
			'prix_ht' => $ht,
			'prix_ttc' => $_GET['ttc']
		);
		$edited_article_id = $article_pod->save($data, null, $_GET['id_article']);
		echo $edited_article_id;
	}
}

/*
 * Calcul du prix TTC à partir du Prix HT
 */
if ($_GET['ajax'] == 3) {
	if (!isset($_GET['tva']) || $_GET['tva'] == '') {
		echo -3;
		exit();
	}
	if (!is_numeric($_GET['ttc'])) {
		echo 'ttc';
		exit();
	}
	$liste_tva = get_liste_tva();
	$tva_index = $_GET['tva'];
	$tva_val = $liste_tva[$tva_index];
	$ttc = $_GET['ttc'];
	$ht = $ttc / (1 + $tva_val / 100);
	echo round($ht, 2);
}

