<?php

/**
 * Récupérer la liste des TVA pour le forulaire d'ajout 
 * d'un nouvel article
 */
function get_liste_tva() {
	$liste = array();
	if (get_option('_liste_tva')) {
		$liste = unserialize(get_option('_liste_tva'));
	}
	return $liste;
}

/**
 * Récupérer la liste des marques pour le forulaire d'ajout 
 * d'un nouvel article
 */
function get_liste_marque() {
	$liste = array();
	if (get_option('_liste_marques')) {
		$liste = unserialize(get_option('_liste_marques'));
	}
	return $liste;
}

/**
 * Récupérer la liste des gammes pour le forulaire d'ajout 
 * d'un nouvel article
 */
function get_liste_gamme() {
	$liste = array();
	if (get_option('_liste_gammes')) {
		$liste = unserialize(get_option('_liste_gammes'));
	}
	return $liste;
}