<?php

$error = '';

if (!isset($_GET['id_article'])) {
	$error = -1;
}

if ($error == -1) {
	echo $error;
	exit();
}

if ($_GET['ajax'] == 1) {
	$id_article = $_GET['id_article'];
	$article_pod = Pods('articles', $id_article);
	if ($article_pod->delete())
		echo 1;
	else 
		echo 0;
}