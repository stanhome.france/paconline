<?php 
require_once 'include-functions.php';

$numeros_cycles = get_liste_numeros_cycles();
$cibles = get_liste_cible();
$types = get_liste_event();
$marques = get_liste_marque();
$gammes = get_liste_gamme();

$cycle_id = $_GET['cycle_id'];
$where_extension = $_GET['where_extension'];
?>
<?php
$promos = array();
$cycles_promos = Pods('promotions',
array('join' => 'LEFT JOIN wp_pods_cycles_promos ON wp_pods_cycles_promos.id_promo = t.id',
												  'cache_mode' => 'cache',
												  'where' => "wp_pods_cycles_promos.id_cycle=".$cycle_id.$where_extension, 'limit' => "-1"));
while ($result = $cycles_promos->fetch()) {
	if (!isset($promos[$result['marque']])) {
		$promos[$result['marque']] = array();
	}
	if (!isset($promos[$result['marque']][$result['gamme']])) {
		$promos[$result['marque']][$result['gamme']] = array();
	}
	array_push($promos[$result['marque']][$result['gamme']], $result);
}

if ($cycles_promos->total() > 0  && (!isset($_GET['filtre']) || $_GET['filtre'] == 'all' || $_GET['filtre'] == 'promos')) {
	?>
<li>
<div style="color: #81030f;">Promotions</div>
<ul>
<?php foreach($promos as $promo_marque_key => $promo_marque_val) { ?>
	<li><?php echo stripslashes($marques[$promo_marque_key]); ?>
	<ul>
	<?php foreach ($promo_marque_val as $promo_gamme_key => $promo_gamme_val) {	?>
		<li><?php echo stripslashes($gammes[$promo_gamme_key]);	?>
		<ul>
		<?php foreach ($promo_gamme_val as $the_promo) { ?>
			<li>
			<div style="padding: 5px; width: 750px;"><?php 
			display_binary_icons_by_array($the_promo, 'P');
			$promo_url = get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $the_promo['id'];
			?> <input type="image"
				src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>"
				onclick="previsualisation_entite('<?php echo $promo_url?>')"> <a
				<?php if ($the_promo['lancement_chaud'] == 1) { ?>
				style="color: red;" <?php } ?>
				href="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo=' . $the_promo['id']?>"><?php echo $the_promo['reference_extranet'].' : '.$the_promo['description']; ?>
				<?php
				$composition = ' : '; 
				echo $composition . '    ' . $the_promo['cycles'];
				?> </a></div>
			</li>
			<?php } ?>
		</ul>
		</li>
		<?php }?>
	</ul>
	</li>
	<?php } ?>
</ul>
</li>
	<?php }

	$events = array();
	$cycles_events = Pods('events',
	array('join' => 'LEFT JOIN wp_pods_cycles_events ON wp_pods_cycles_events.id_event = t.id',
												  'cache_mode' => 'cache',
												  'where' => "id_cycle=".$cycle_id.$where_extension, 'limit' => "-1"));
	while ($result = $cycles_events->fetch()) {
		if (!isset($events[$result['type_evenement']])) {
			$events[$result['type_evenement']] = array();
		}
		array_push($events[$result['type_evenement']], $result);
	}
	if ($cycles_events->total() > 0 && (!isset($_GET['filtre']) || $_GET['filtre'] == 'all' || $_GET['filtre'] == 'events')) {
		?>
<li>
<div style="color: #81030f;">Evènements</div>
<ul>
<?php foreach ($events as $event_by_type_key => $event_by_type_val) { ?>
	<li><?php echo stripslashes($types[$event_by_type_key]); ?>
	<ul>
	<?php foreach ($event_by_type_val as $the_event) { ?>
		<li>
		<div style="padding: 5px; width: 750px;"><?php display_binary_icons_by_array($the_event, 'E');
		$event_url = get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $the_event['id']; ?>
		<input type="image"
			src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>"
			onclick="previsualisation_entite('<?php echo $event_url?>')"> <a
			<?php if ($the_event['lancement_chaud'] == 1) { ?>
			style="color: red;" <?php } ?>
			href="<?php echo get_permalink(get_page_by_path('details-evenment-cyclique')) . '?id_event=' . $the_event['id']; ?>"><?php echo $the_event['designation']; ?>
		du1  <?php echo php2html_date($the_event['date_debut']) ?> au <?php echo php2html_date($the_event['date_fin']); ?>
		<?php
		$cycles_periodes = cycles_periodes_suffixe($the_event['semaine_debut'], $the_event['annee'], $the_event['semaine_fin'], $the_event['annee_fin'], $the_event['permalink']);
		echo ' : ' . $cycles_periodes;
		?> </a></div>
		</li>
		<?php } ?>
	</ul>
	</li>
	<?php } ?>
</ul>
</li>
	<?php }

	$cycles_docs = Pods('documents',
	array('join' => 'LEFT JOIN wp_pods_cycles_docs ON wp_pods_cycles_docs.id_doc = t.id',
											  'cache_mode' => 'cache',
											  'where' => "id_cycle=".$cycle_id.$where_extension, 'limit' => "-1"));
	if ($cycles_docs->total() > 0  && (!isset($_GET['filtre']) || $_GET['filtre'] == 'all' || $_GET['filtre'] == 'docs')) {
		?>
<li>
<div style="color: #81030f;">Documents</div>
<ul>
<?php while ($cycles_docs->fetch()) { ?>
	<li>
	<div style="padding: 5px; width: 750px;"><?php display_binary_icons($cycles_docs, 'D');
	if ($cycles_docs->display('is_mail') == 'Yes') { ?> <img
		src="<?php bloginfo('template_directory'); echo '/images/etat/mail.png' ?>">
		<?php }
		$doc_url = get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $cycles_docs->field('id')?>
	<input type="image"
		src="<?php bloginfo('template_directory'); echo '/images/preview-normal.png' ?>"
		onclick="previsualisation_entite('<?php echo $doc_url?>')"> <a
		<?php if ($cycles_docs->display('lancement_chaud') == 'Yes') { ?>
		style="color: red;" <?php } ?>
		href="<?php echo get_permalink(get_page_by_path('details-document-cyclique')) . '?id_doc=' . $cycles_docs->field('id'); ?>"><?php echo $cycles_docs->display('reference_document'); ?>
	: <?php echo $cycles_docs->display('designation'); ?> <?php 
	$cycles_periodes = cycles_periodes_suffixe($cycles_docs->field('semaine_debut'), $cycles_docs->field('annee'), $cycles_docs->field('semaine_fin'), $cycles_docs->field('annee_fin'), $cycles_docs->field('permalink'));
	echo ' : ' . $cycles_periodes;
	?> </a></div>
	</li>
	<?php } ?>
</ul>
</li>
	<?php } ?>
