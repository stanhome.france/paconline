<?php

if ($_FILES['doc_to_upload']['error'] == 0) {

	$file_to_add = $_POST['files_to_attach'];
	
	$path = realpath(dirname(__FILE__)).'/../../../uploads/'.date('Y');
	if (!is_dir($path)) {
		@mkdir($path.'/'. date('m'), 0777, true);
	}
	
	if (sizeof($_FILES) != 0) {
		// transformation du nom du fichier
		$search = array("'", " ", "ä", "à", "á", "â", "ã", "å", "ò", "ó", "ô", "õ", "ö", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "ù", "ú", "û", "ü"); 
		$replace = array("_", "_", "a", "a", "a", "a", "a", "a", "o", "o", "o", "o", "o", "e", "e", "e", "e", "i", "i", "i", "i", "u", "u", "u", "u");
		$filename = str_replace($search, $replace, $_FILES['doc_to_upload']['name']);
		
		$file_path = $path.'/'.date('m').'/'.$filename;
		if (file_exists($file_path)) {
			$file_path = $path.'/'.date('m').'/'.date("dmYHs").'_'.$filename;
			$file_to_add .= addSlashes(date("dmYHs").'_'.$filename)." ";
		} else {
			$file_to_add .= addSlashes($filename)." ";
		}
		
		move_uploaded_file($_FILES['doc_to_upload']['tmp_name'], $file_path);
	}
}
?>
<input type="text" value="<?php  echo $file_to_add; ?>" id="file_uploaded"/>