<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'evenements-des-cycles/ajax.php';
	exit();
}


$cibles = get_liste_cible();
$types = get_liste_event();


if (!isset($_GET['id_event']) || $_GET['id_event'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$pod_cible = 'Events';
$id_pod_cible = $_GET['id_event'];
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
	
##########################################################################
?>
<?php 
$current_event = Pods('events', array('where' => "id=" . $_GET['id_event']));
$data_current_event = $current_event->data();
$data_current_event = $data_current_event[0];
?>
<?php if (is_null($data_current_event)):?>
<h3 class="ttl">Evénment Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
###cibles pour cet event###
$this_event_cible_array = explode(',', $data_current_event->cible_evenement);
$this_event_cible = array();
foreach ($this_event_cible_array as $item) {
	if(isset($cibles[$item])) $this_event_cible[] = $cibles[$item];
}
$this_event_cible = implode(' + ', $this_event_cible);
?>


<form>
	<?php if (!isset($_GET['is_prev'])):?> <?php require_once 'barre-actions.php';?><?php endif;?>
	<div class="frame">		
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<h2 class="ttl">Détail Evénement : <span class="red"><?php echo stripSlashes($data_current_event->designation); ?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Désignation :</label></td>
				<td><?php echo stripSlashes($data_current_event->designation); ?></td>
				<td rowspan="2" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Type :</label></td>
				<td>
					<select disabled="disabled">
						<option value=""><?php echo $types[$data_current_event->type_evenement]?></option>	
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Cible(s) :</label></td>
				<td  colspan="2" ><input type="text" class="vlong" value='<?php echo stripslashes($this_event_cible)?>'  disabled="disabled"></td>
			</tr>
			<tr>
				<td align="right"><label>Cycle(s) :</label></td>
				<td  colspan="2"><input type="text" class="vlong" value='<?php echo get_cycles_between_two_weeks($data_current_event->semaine_debut, $data_current_event->annee, $data_current_event->semaine_fin, $data_current_event->annee_fin, $data_current_event->permalink)?>'  disabled="disabled"></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_event->date_debut_pre_lancement) ?></span></td>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_event->date_fin_pre_lancement) ?></span></td>
							<td align="center" class="brd_right"><span class="gras"><?php echo $data_current_event->qte_pre_lancement?></span></td> 
						</tr>
						<tr>
							<th><label>Lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_event->date_debut_extranet) ?></span></td>
							<td class="brd_right" align="center"><span class="gras"><?php echo pod2js_date($data_current_event->date_fin_extranet) ?></span></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"><span id="eroor_lancement" class="red"></span></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>	
</form>

<?php include_once 'comment-bloc.php';?>
<!-- Ici, la visualisation des documents attachés et les remarques -->
<?php
	$my_entity_id = $id_pod_cible; 
	$type_entity = 'E';
	include_once("attachments/attachment-doc-visu.php");
	include_once("remarques/remarques-visu.php");
	include_once("avenants/avenants-visu.php");
?>
		
<?php endif;?>
<?php endif;?>


<script type="text/javascript">
( function( $ ) {
	/*
	 * Suppression d'un event
	 */
	$("#delete_event").bind('click', function(){
		if(confirm("Etes vous certain de vouloir supprimer cet évent?")) {
			$.ajax({
				url: '',
				type: 'get',
				data: 'id_event=' + <?php echo $_GET['id_event']?> + '&ajax=5',
				success: function(msg) {
					switch(msg) {
						case '-1':
							$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '0':
							$("#info").html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '1':
							$("#info").html('Event supprimé!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							window.location = '<?php echo get_permalink(get_page_by_path('cycles')) ?>';
							break;
					}
				},
				error: function() {
				}
			});
		}
	});
})( jQuery );
</script>