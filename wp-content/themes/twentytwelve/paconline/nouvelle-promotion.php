<?php
require_once 'include-functions.php';
/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
// 	require_once 'find-articles.php';
	require_once 'promotions/ajax.php';
	exit();
}
//AIMED1504
if (isset($_GET['ajax']) && $_GET['ajax'] == 1) {
    ob_end_clean();
    require_once 'delete-doc.php';
    exit();
}
// if (isset($_GET['ajax']) && $_GET['ajax'] == 4) {
//     ob_end_clean();
//     require_once 'find-articles.php';
//     exit();
// }
$annee_en_cours = date('Y');
$no_articles = false;

$options_marque = get_liste_marque();
$options_gamme = get_liste_gamme();
$options_tva = get_liste_tva();

$selection_references = get_selection_reference();
if (count($selection_references) > 0) $no_articles = true;

?>


<form id="save_new_promotion" action="" method="get">
	<input type="hidden" id="promotion_is_valid" value="non">
	<input type="hidden" id="nombre_lignes_promo" value="0">
	<input type="hidden" id="link_to_detail_promo" value="<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo='?>">
	<?php require_once 'barre-actions.php';?>
	<div class="frame">
		<table  cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top">
					<h2 class="ttl">Nouvelle Promotion</h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Référence</label></td>
				<td><input type="text" id="promotion_reference_extranet" class="long"/></td>
				<td rowspan="3" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr>
        		<td align="right"><label>Description</label></td>
        		<td><input type="text" id="promotion_description" class="long"/></td>
        	</tr>
			<?php require_once 'cycle-ou-periode.php';?>
			<tr>
				<td align="right"><label>Cycle(s)</label></td>
				<td><input type="text" id="promotion_cycles_correspondants" disabled="disabled" class="long"/></td>
			</tr>
			<tr>
				<td align="right"><label>Marque</label></td>
				<td>
					<select id="promotion_marque_list">
						<option value="">Choisir...</option>
						<?php if (count($options_marque) > 0):?>
							<?php foreach ($options_marque as $kle_marque => $val_marque):?>
								<option value="<?php echo $kle_marque?>"><?php echo  stripslashes($val_marque)?></option>
							<?php endforeach;?>				
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Gamme</label></td>
				<td>
					<select id="promotion_gamme_list">
						<option value="">Choisir...</option>
						<?php if (count($options_gamme) > 0):?>
							<?php foreach ($options_gamme as $kle_gamme => $val_gamme):?>
								<option value="<?php echo $kle_gamme?>"><?php echo  stripslashes($val_gamme)?></option>
							<?php endforeach;?>				
						<?php endif;?>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Référence Facturation</label></td>
				<td><input type="text" id="promotion_reference_facturation" class="long"/></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Lot</label></td>
				<td><input type="text" id="promotion_reference_lot" class="long"/></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><input type="text" id="promotion_date_debut_pre_lancement"></td>
							<td align="center"><input type="text" id="promotion_date_fin_pre_lancement"></td>
							<td align="center" class="brd_right"><input type="text" id="promotion_qte_pre_lancement"></td>
						</tr>
						<tr>
							<th>Lancement</th>
							<td align="center"><input type="text" id="promotion_date_debut_lancement"></td>
							<td align="center"><input type="text" id="promotion_date_fin_lancement"></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Prévision</label></td>
				<td align="right"><input type="text" id="promotion_prevision"></td>
			</tr>
			<tr>
				<td align="right"><label>Quantité limitée</label></td>
				<td align="right">
					Oui<input type="radio" name="promotion_qte_limitee" value="1">
					Non<input type="radio" name="promotion_qte_limitee" value="0" checked="checked">
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Prix TTC</th>
							<th align="center">Prix HT</th>
							<th align="center" class="brd_right">TVA</th>
						</tr>
						<tr>
							<th><label>Prix Normal</label></th>
							<td align="center"><input type="text" id="prix_n_ttc" disabled="disabled"></td>
							<td align="center"><input type="text" id="prix_n_ht" disabled="disabled"></td>
							<td align="center" class="brd_right">
								<select id="tva_promo">
									<option value="">Choisir...</option>
									<?php if (count($options_tva) > 0):?>
										<?php foreach ($options_tva as $kle_tva => $val_tva):?>
											<option value="<?php echo $kle_tva?>"><?php echo  stripslashes($val_tva)?></option>
										<?php endforeach;?>				
									<?php endif;?>
								</select>
							</td>
						</tr>
						<tr>
							<th><label>Prix Promotion</label></th>
							<td align="center"><input type="text" id="prix_p_ttc"></td>
							<td align="center"><input type="text" id="prix_p_ht" disabled="disabled"></td>
							<td  align="center" class="brd_right" style="border-right: 0px;">
								<input type="image" id="calcul_promo" alt="Calculer" src="<?php bloginfo('template_directory'); echo '/images/calculator.jpg' ?>">
							</td>
						</tr>
						<tr><td colspan="3" class="error" align="center"><span id="eroor_lancement" class="red"></span></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="button" id="add_new_ligne" value="Ajouter une ligne >>" class="small">
				</td>
			</tr>
			<tr id="tr_references_en_promo" style="display: none;">
				<td colspan="3">
					<table id="references_en_promo" cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<th align="center">Quantité</th>
							<th align="center">Réf. Lot</th>
							<th align="center">Désignation</th>
							<th align="center">Réf. Article(s)</th>
							<th align="center">Prix N. HT</th>
							<th align="center">Prix N. TTC</th>
							<th align="center" class="brd_right"></th>
						</tr>
						<tr class="last_tr_promo"><td colspan="7" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
		</table>
		<script type="text/javascript" src="<?php echo includes_url('js/jqdialog/jqdialog.min.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo includes_url('js/jqdialog/jqdialog.css') ?>">
	</div>
</form>


<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/promotion.js"; ?>"></script>
<?php 
$pod_cible = 'Promos';
include_once 'comment-bloc.php';
$type_entity = 'P';
include_once('attachments/attachment-doc-creation.php');
?>

<script type="text/javascript" src="<?php echo get_option("siteurl")."/wp-content/themes/twentytwelve/js/grid-localisation.js"; ?>"></script>
<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript">
( function( $ ) {

	/*
	 * Ouvrir popup pour séléctionner les références pour une promo
	 */
	 $('#add_new_ligne').click( function() {
			if($("#tva_promo").val() == '') alert('Choisissez Le taux du TVA');
			if($("#tva_promo").val() == '') return false;
			var content = '';
			content += '<table>';
			content += '<tr id="popup_actions"><td align="center"><input type="button" value="Valider" id="valid_choice" onclick="add_ligne(\'<?php bloginfo('template_directory'); echo '/images/remove.png' ?>\')" class="small"></td>';
			content += '<td align="center"><input type="button" value="Annuler" id="cancel_choice" onclick="cancel_add_ligne()" class="small"></td>';
			content += '</tr></table><br>';
			content += '<table id="jqgrid"></table><div id="pagergrid"></div>';
			$.jqDialog.content(content);
			//var url = '<?php echo get_permalink(get_page_by_path('recuperation-des-articles-pour-le-grid-popin'))?>';
			var url = '<?php echo get_site_url() . '/recuperation-des-articles-pour-le-grid-popin.php'?>';
			var i = 0;
			list = new Array;
			$("#jqgrid").jqGrid({ 
				url:url, 
				datatype: "json",
				height: 'auto', 
				colNames:['Référence','Désignation'], 
				colModel:[ {name:'reference',index:'reference', width:150, searchoptions:{sopt: ['cn', 'bw', 'eq']}}, {name:'designation',index:'designation', width:500, searchoptions:{sopt: ['cn', 'bw', 'eq']}}], 
 				rowNum:10, 
 				rowList:[5,10,20],  
				pager: '#pagergrid', 
				sortname: 'reference', 
				viewrecords: true, 
				sortorder: "asc", 
				multiselect: true,
				emptyrecords: "Aucun article trouvé",
				caption:"Liste des références",
				gridComplete: function() {
					var ids = $(this).jqGrid('getDataIDs');
					for (var i = 0; i < ids.length; i++){
					    if (in_array(list, ids[i])){
					    	$(this).setSelection(ids[i], false);
					    }
					}
				},
				onSelectRow: function(reference) {
					if(!in_array(list, reference)) {
						list[i] = reference;
						i++;
					} else {
						delete list[list.indexOf(reference)];
					}
				} 
			});
			$("#jqgrid").jqGrid('navGrid','#pagergrid',{edit:false,add:false,del:false});
			window.setTimeout("centerPopin()",700);
		} );


			//AIMED1504


				$('.delete_doc').unbind('click').live('click', function() {
					var id_doc = $(this).attr('id').replace('del*', '');
					var $complete_path = $(this).parent().find("a").attr('href');
					$filename = $(this).parent().find("a").html();
					var check = confirm('Voulez-vous vraiment supprimer ce document ?');
					if (check) {
						$(this).parent().remove();
							$.ajax({
									url: '',
									type: 'get',
									data:   '&id_doc='+ id_doc +  '&doc_filename= ' + $filename + '&complete_path= ' + $complete_path + '&ajax=1',
									success: function(msg) {
									},
									error: function() {
									}
								});
						}
				});
})( jQuery );

function centerPopin() {
	$.jqDialog.maintainPosition($('#jqDialog_box'));
}
function redirect2DetailPromo(id_new_promo)
{
	window.location = '<?php echo get_permalink(get_page_by_path('details-promotion-cyclique')) . '?id_promo='?>'+id_new_promo;
}

</script>
