<?php
if (get_option('nb_pixels_per_day')) {
	$nb_pixels_per_day = get_option('nb_pixels_per_day');
} else {
	$nb_pixels_per_day = 8;
}
// nombre de semaine avant et après la semaine courant
if (get_option('rayon')) {
	$rayon = get_option('rayon');
} else {
	$rayon = 8;
}  

$mois_annee = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
$current_week = Date("W");
$current_year = Date("Y");

$nb_pixels_per_week = $nb_pixels_per_day * 7;
$begin_week = $current_week - $rayon;
$end_week = $current_week + $rayon;
$diametre = 2 * $rayon + 1;


$week_tab = array();
for($i = $begin_week; $i <= $end_week; $i++) {
	$week = $i;
	$year = $current_year;
	if ($i < 1) {
		$week = 52 + $i;
		$year--;
	}
	if ($i > 52) {
		$week = $i - 52;
		$year++;
	}
	array_push($week_tab, array($week, $year));
}

// recherche de l'id de l'année de la première semaine de la vue calendaire
$db_year_id = Pods('annees', array('where' => 'annee = "'.$week_tab[0][1].'"'));
if ($db_year_id->fetch()) {
	$year_id = $db_year_id->display("id");
} else {
	$year_id = 0;
}

// recherche de l'id de l'année de la dernière semaine de la vue calendaire
$db_last_year_id = Pods('annees', array('where' => 'annee = "'.$week_tab[count($week_tab) - 1][1].'"'));
if ($db_last_year_id->fetch()) {
	$last_year_id = $db_last_year_id->display("id");
} else {
	$last_year_id = 0;
}

// recherche de la date de début de la première semaine de la vue calendaire
$db_first_week = Pods('calendriers', array('where' => 'designation = "S'.$week_tab[0][0].'" and id_annee='.$year_id));
if ($db_first_week->fetch()) {
	$begin_first_week = $db_first_week->display("date_debut");
} else {
	$begin_first_week = "no date"; ?>
	<h3 class="ttl">Vue calendaire</h3>
	<p>Le calendrier pour l'année <?php echo $year_id; ?> n'a pas été généré. La vue calendaire ne peut être calculée.</p>
<?php
	exit(); 
}

// recherche de la date de fin de la dernière semaine de la vue calendaire
$db_last_week = Pods('calendriers', array('where' => 'designation = "S'.$week_tab[count($week_tab) - 1][0].'" and id_annee='.$last_year_id));
if ($db_last_week->fetch()) {
	$end_last_week = $db_last_week->display("date_fin");
} else {
	$end_last_week = "no date"; ?>
	<h3 class="ttl">Vue calendaire</h3>
	<p>Le calendrier pour l'année <?php echo $last_year_id; ?> n'a pas été généré. La vue calendaire ne peut être calculée.</p>
<?php
	exit(); 
}

if ($end_last_week != "no date") {
	$end_last_week_exploded = explode('-', $end_last_week); 
	$end_last_week_ts = mktime(0, 0, 0, $end_last_week_exploded[0], $end_last_week_exploded[1], $end_last_week_exploded[2]);
}

if ($begin_first_week != "no date") {
	$begin_first_week_exploded = explode('-', $begin_first_week);
	$begin_first_week_ts = mktime(0, 0, 0, $begin_first_week_exploded[0], $begin_first_week_exploded[1], $begin_first_week_exploded[2]);
}

$nb_days_on_month = Date("t", $begin_first_week_ts);

$nb_days_on_month = $nb_days_on_month - $begin_first_week_exploded[1] + 1;
$month = $begin_first_week_exploded[0];
$year = $begin_first_week_exploded[2];

// *************************************************************************************
// get_period_coords($date_debut, $date_fin, $begin_first_week_ts, $end_last_week_ts)
// retourne un tableau
// array($nb_days_before_period (nombre de jours avant la div), 
//   	 $nb_days_of_period (nombre de jours de largeur de la div))
// *************************************************************************************
function get_period_coords($date_debut, $date_fin, $begin_first_week_ts, $end_last_week_ts) {
    $begin_period_exploded = explode('-', $date_debut);
	$begin_period_ts = mktime(0, 0, 0, $begin_period_exploded[0], $begin_period_exploded[1], $begin_period_exploded[2]);
	
	$end_period_exploded = explode('-', $date_fin);
	$end_period_ts = mktime(0, 0, 0, $end_period_exploded[0], $end_period_exploded[1], $end_period_exploded[2]);
	
	if ($begin_period_ts > $begin_first_week_ts) {
		$nb_days_before_period = ($begin_period_ts - $begin_first_week_ts) / 86400;
	} else {
		$begin_period_ts = $begin_first_week_ts;
		$nb_days_before_period = 0;
	}
	
	if ($end_period_ts > $end_last_week_ts) {
		$end_period_ts = $end_last_week_ts;
	}
	
	$nb_days_of_period = (($end_period_ts - $begin_period_ts) / 86400) + 1;
	
	return array($nb_days_before_period, $nb_days_of_period);
}

require_once 'include-functions.php';

// recherche du groupe le plus fort d'un user dans la liste des groups auquels il appartient
// le groupe le plus fort se trouve dans $high_level_group
$high_level_group = get_high_level_group_from_current_user();

// N'afficher que les cycles non archivés et visibles par le groupe dont le user courant fait partie
$where_extension = "";
if ($high_level_group == "StanHome_CellulePAC" or $high_level_group == "StanHome_ComitePAC_Work") {
	$where_extension = "";
}
if ($high_level_group == "StanHome_Presta") {
	$where_extension = " and diffusion_level >= 10";
}
if ($high_level_group == "StanHome_DR") {
	$where_extension = " and (diffusion_level = 30 or diffusion_level = 40)";
}
if ($high_level_group == "StanHome_DD") {
	$where_extension = " and diffusion_level = 40";
}

?>

<script>
var timer;
function right_action() {
	if (757 - parseInt($('.content').css('left')) < <?php echo $diametre * $nb_pixels_per_week; ?>) {
		$('.content').css('left', parseInt($('.content').css('left')) - 2);
	}
}

function left_action() {
	if (parseInt($('.content').css('left')) < 0) {
		$('.content').css('left', parseInt($('.content').css('left')) + 2);
	}
}

jQuery(document).ready(function($) {
	$('.right').mousedown(function() {
		timer = setInterval("right_action()", 10);
	});
	$('.right').mouseup(function() {
		clearInterval(timer);
	});
	$('.left').mousedown(function() {
		timer = setInterval("left_action()", 10);
	});
	$('.left').mouseup(function() {
		clearInterval(timer);
	});
});
</script>

<h3 class="ttl">Vue calendaire</h3>

<div class="left_calendar">
	<input type="button" value="" class="left clndr_prev" />
</div>
<div class="right_calendar">
	<input type="button" value="" class="right clndr_next" />
</div>
<div class="calendar_content">
	<div class="content" style="width:<?php echo $diametre * $nb_pixels_per_week; ?>px;">
		<!-- Affichage des mois -->
		<div style="width:100%; height:30px;">
<?php
	for($i = 0; $i < 12; $i++) {
        $nb_days_on_month = Date("t", mktime(0, 0, 0, $month, 1, $year));
		$param = get_period_coords($month.'-01-'.$year,
								   $month.'/'.$nb_days_on_month.'/'.$year,
								   $begin_first_week_ts,
								   $end_last_week_ts);
		
?>
			<div  class="month" style="left:<?php echo $param[0] * $nb_pixels_per_day; ?>px; width:<?php echo $param[1] * $nb_pixels_per_day; ?>px;">
				<?php if ($param[1] * $nb_pixels_per_day > 100) { echo $mois_annee[$month - 1]." ".$year; } ?>
			</div>
<?php 
		$month++;
		if ($month == 13) {
			$month = 1;
			$year++;
		}
		if ($month < 10) {
			$month = '0'.$month;
		}
	}
?>
		</div>
	
		<!-- Affichage des semaines -->
		<div style="width:100%; height:30px;">
<?php
	$i = 0; 	
	foreach($week_tab as $week) { 
		if ($week[0] == $current_week) {
			$cell_current = "current_wk";
		}
		else{$cell_current="";}
		$i++;
?>			
			<div style=" width:<?php echo $nb_pixels_per_week; ?>px;" class="week <?php echo $cell_current?>" >
				<?php echo "S".$week[0]; ?>
			</div>
<?php
	} ?>
		</div>
	
<?php
	$begin_first_week_mysql_format = substr($begin_first_week, 6, 4).'-'.substr($begin_first_week, 0, 2).'-'.substr($begin_first_week, 3, 2);
	$end_last_week_mysql_format = substr($end_last_week, 6, 4).'-'.substr($end_last_week, 0, 2).'-'.substr($end_last_week, 3, 2);
	$cycles = Pods('cycles', array('where' => 'permalink="periodes"'.$where_extension.' and ((date_debut >= "'.$begin_first_week_mysql_format.'" or date_fin <= "'.$end_last_week_mysql_format.'") or (date_debut <= "'.$begin_first_week_mysql_format.'" and date_fin >= "'.$end_last_week_mysql_format.'"))', 'orderby' => 'semaine_fin asc'));
?>
		<!-- Périodes -->
		<div style="width:100%; height:30px;">
<?php
	$cycles_tab = array();
	$i = 0;
	while($cycles->fetch()) {
		$param = get_period_coords($cycles->display('date_debut'),
								   $cycles->display('date_fin'),
								   $begin_first_week_ts,
							       $end_last_week_ts);
		$id_explode = explode(" ", $cycles->display('identifiant'));
?>
			<a alt="<?php echo $cycles->display('identifiant'); ?>" 
			   title="<?php echo $cycles->display('identifiant'); ?>" 
			   href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'/?id_cycle='.$cycles->field('id'); ?>">
				<div style="left:<?php echo $param[0] * $nb_pixels_per_day; ?>px; width:<?php echo $param[1] * $nb_pixels_per_day; ?>px;" class="periode_clnd">
					<?php echo $id_explode[2]; ?>
				</div>
			</a>
<?php
		$i++; 
	}
?>
		</div>
		
			
<?php
	$cycles = Pods('cycles', array('where' => 'permalink like "cycles%"'.$where_extension.' and ((date_debut >= "'.$begin_first_week_mysql_format.'" or date_fin <= "'.$end_last_week_mysql_format.'") or (date_debut <= "'.$begin_first_week_mysql_format.'" and date_fin >= "'.$end_last_week_mysql_format.'"))', 'orderby' => 'semaine_fin asc'));
?>
		<!-- Cycles -->
		<div style="width:100%; height:30px;">
<?php
	$cycles_tab = array();
	$i = 0;
	while($cycles->fetch()) {
		$param = get_period_coords($cycles->display('date_debut'),
								   $cycles->display('date_fin'),
								   $begin_first_week_ts,
							       $end_last_week_ts);
		if ($i % 2 == 0) {
			$cell_class = "cycle_even";
		} else {
			$cell_class = "cycle_odd";
		}
		$id_explode = explode(" ", $cycles->display('identifiant'));
?>
			<a alt="<?php echo $cycles->display('identifiant'); ?>" 
			   title="<?php echo $cycles->display('identifiant'); ?>" 
			   href="<?php echo get_permalink(get_page_by_path('detail-cycle')).'/?id_cycle='.$cycles->field('id'); ?>">
				<div style="left:<?php echo $param[0] * $nb_pixels_per_day; ?>px; width:<?php echo $param[1] * $nb_pixels_per_day; ?>px;" class="cycle_clnd <?php echo $cell_class; ?>">
					<?php echo 'C'.$id_explode[2]; ?>
				</div>
			</a>
<?php
		$i++; 
	}
?>
		</div>
		
				<div style="width:<?php echo $diametre * $nb_pixels_per_week; ?>px;" class="holiday">Les vacances scolaires</div>
		
		<!-- Vacances scolaires -->
		<div style="width:100%; height:15px;">
<?php
	$holydays = Pods('calendriers', array('where' => 'vacances=1 and semaine=0 and (id_annee='.$year_id.' or id_annee='.$last_year_id.')'));
	while($holydays->fetch()) { 
		$param = get_period_coords($holydays->display('date_debut'), 
								   $holydays->display('date_fin'), 
								   $begin_first_week_ts, 
								   $end_last_week_ts);

		$date_debut_format_fr = substr($holydays->display('date_debut'), 3, 2)."/".substr($holydays->display('date_debut'), 0, 2)."/".substr($holydays->display('date_debut'), 6, 4);
		$date_fin_format_fr = substr($holydays->display('date_fin'), 3, 2)."/".substr($holydays->display('date_fin'), 0, 2)."/".substr($holydays->display('date_fin'), 6, 4);
?>
			<div alt="<?php echo $date_debut_format_fr.' au '.$date_fin_format_fr; ?>" 
				 title="<?php echo $date_debut_format_fr.' au '.$date_fin_format_fr; ?>"
				 style="left:<?php echo $param[0] * $nb_pixels_per_day; ?>px; width:<?php echo $param[1] * $nb_pixels_per_day; ?>px;" class="ferie">
				<?php echo $holydays->display("designation"); ?>
			</div>
<?php
	}
?>
		</div>
	</div>
</div>













