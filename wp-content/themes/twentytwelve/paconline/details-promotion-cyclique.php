<script type="text/javascript" src="<?php echo includes_url('js/jquery/jquery.ui.datepicker-fr.js') ?>"></script>
<script type="text/javascript" src="../wp-content/themes/twentytwelve/js/tools.js"></script>
<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'promotions/ajax.php';
	exit();
}


$marques = get_liste_marque();
$gammes = get_liste_gamme();
$tva = get_liste_tva();

if (!isset($_GET['id_promo']) || $_GET['id_promo'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$pod_cible = 'Promos';
$id_pod_cible = $_GET['id_promo'];
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
##########################################################################
?>
<?php 
$current_promo = Pods('promotions', array('where' => "id=" . $_GET['id_promo']));
$data_current_promo = $current_promo->data();
$data_current_promo = $data_current_promo[0];
?>
<?php if (is_null($data_current_promo)):?>
<h3 class="ttl">Promotion Introuvable</h3>
<?php exit();?>
<?php else:?>


<form>
	<?php if (!isset($_GET['is_prev'])):?> <?php require_once 'barre-actions.php';?><?php endif;?>
	<div class="frame">		
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<h2 class="ttl">Détail Promotion : <span class="red"><?php echo stripslashes($data_current_promo->reference_extranet .' : '.$data_current_promo->description)?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td align="right"><label>Référence :</label></td>
				<td><?php echo stripslashes($data_current_promo->reference_extranet)?></td>
				<td rowspan="2" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr>
        		<td align="right"><label>Description</label></td>
        		<td><?php echo stripslashes($data_current_promo->description)?></td>
        	</tr>
			
			<tr>
				<?php 
					$temp = $data_current_promo->cycles;
					$temp = explode(',', $temp);
					sort($temp);
					$temp = implode(',', $temp);
				?>
				<td align="right"><label>Cycle(s) :</label></td>
				<td><?php  echo $data_current_promo->cycles ?></td>
			</tr>
			<tr>
				<td align="right"><label>Marque :</label></td>
				<td><?php echo $marques[$data_current_promo->marque]?></td>
			</tr>
			<tr>
				<td align="right"><label>Gamme :</label></td>
				<td><?php echo $gammes[$data_current_promo->gamme]?></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Facturation :</label></td>
				<td><?php echo $data_current_promo->reference_facturation?></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Lot :</label></td>
				<td><?php echo $data_current_promo->reference_lot?></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_promo->date_debut_pre_lancement) ?></span></td>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_promo->date_fin_pre_lancement) ?></span></td>
							<td align="center" class="brd_right"><span><?php echo $data_current_promo->qte_pre_lancement?></span></td> 
						</tr>
						<tr>
							<th><label>Lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_promo->date_debut_lancement) ?></span></td>
							<td class="brd_right" align="center"><span class="gras"><?php echo pod2js_date($data_current_promo->date_fin_lancement) ?></span></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"><span id="eroor_lancement" class="red"></span></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Prévision :</label></td>
				<td><?php echo $data_current_promo->prevision?></td>
			</tr>
			<tr>
				<td align="right"><label>Quantité limitée :</label></td>
				<td><?php echo ($data_current_promo->qte_limitee == 1 ? 'Oui' : 'Non')?></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Prix TTC</th>
							<th align="center">Prix HT</th>
							<th align="center" class="brd_right">TVA</th>
						</tr>
						<tr>
							<th><label>Prix Normal</label></th>
							<td align="center"><span class="gras"><?php echo $data_current_promo->prix_normal_ttc?></span></td>
							<td align="center"><span class="gras"><?php echo $data_current_promo->prix_normal_ht ?></span></td>
							<td align="center" class="brd_right"><span><?php echo $tva[$data_current_promo->tva]?></span></td> 
						</tr>
						<tr>
							<th><label>Prix Promo</label></th>
							<td align="center"><span class="gras"><?php echo $data_current_promo->prix_promo_ttc?></span></td>
							<td align="center"><span class="gras"><?php echo $data_current_promo->prix_promo_ht ?></span></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"><span id="eroor_lancement" class="red"></span></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<th align="center">Quantité</th>
							<th align="center">Réf. Lot</th>
							<th align="center">Désignation</th>
							<th align="center">Réf. Article(s)</th>
							<th align="center">Prix N. HT</th>
							<th align="center" class="brd_right">Prix N. TTC</th>
						</tr>
						<?php 
						$data = $data_current_promo->detail_promotion;
						$data = substr ( $data , 1 , -1 ) ;
						$articles = explode('][', $data);
						$detail_promo = array();
						foreach ($articles as $rowarticle) {
						    $article = explode('|', $rowarticle);
						    $promo = array( 
						        'quantite' =>  $article[0],
						        'ref_lot' =>  $article[1],
						        'designation' =>  $article[2],
						        'reference' =>  $article[3],
						        'prix_n_ht' =>  $article[4],
						        'prix_n_ttc' =>  $article[5]
						        );
						    array_push($detail_promo, $promo);
						}
   						 if (count($detail_promo) > 0):
						 foreach ($detail_promo as $item):?>
						<tr>
							<td align="center"><?php echo isset($item['quantite']) ? $item['quantite'] : ''?></td>
							<td align="center"><?php echo isset($item['ref_lot']) ? $item['ref_lot'] : ''?></td>
							<td align="center"><?php echo isset($item['designation']) ? str_replace(' OU ', ' ou ', $item['designation']) : ''?></td>
							<td align="center"><?php echo isset($item['reference']) ? str_replace(',', ' ou ', $item['reference']) : ''?></td>
							<td align="center"><?php echo isset($item['prix_n_ht']) ? $item['prix_n_ht'] : ''?></td>
							<td class="brd_right" align="center"><?php echo isset($item['prix_n_ttc']) ? $item['prix_n_ttc'] : ''?></td>
						</tr>
							<?php endforeach;?>
						<?php endif;?>
						<tr><td colspan="6" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</form>

<?php include_once 'comment-bloc.php';?>

<!-- Ici, la visualisation des documents attachés et les remarques -->
<?php
	$my_entity_id = $id_pod_cible; 
	$type_entity = 'P';
	include_once("attachments/attachment-doc-visu.php");
	include_once("remarques/remarques-visu.php");
	include_once("avenants/avenants-visu.php");
?>

<?php endif;?>
<?php endif;?>

<script type="text/javascript">
( function( $ ) {
	get_cycles_correspondants('promotion', $("#promotion_semaine_debut").val(), $("#promotion_annee").val(), $("#promotion_semaine_fin").val(), $("#promotion_annee_fin").val(), $("#promotion_attach_list").val());
	
	
	/*
	 * Suppression d'une promo
	 */
	$("#delete_promotion").bind('click', function(){
		if(confirm("Etes vous certain de vouloir supprimer cette promo?")) {
			$.ajax({
				url: '',
				type: 'get',
				data: 'id_promo=' + <?php echo $_GET['id_promo']?> + '&ajax=7',
				success: function(msg) {
					switch(msg) {
						case '-1':
							$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '0':
							$("#info").html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							break;
						case '1':
							$("#info").html('Promotion supprimée!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
							window.location = '<?php echo get_permalink(get_page_by_path('cycles')) ?>';
							break;
					}
				},
				error: function() {
					alert('error ajax!');
				}
			});
		}
	});
})( jQuery );
</script>