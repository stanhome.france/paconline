<?php
/*
 * Ce fichier Permet de centraliser les inclusions 
 * des fichiers de fonctions relatives aux différentes
 * fonctionalités du PAC, cela permettra d'utiliser une fonction 
 * développée pour une fonctionalté  dans une autyre fonctionalité
 */
require_once 'articles/ajout-article.php';
require_once 'cycles/functions.php';
require_once 'evenements-des-cycles/functions.php';
require_once 'documents/functions.php';
require_once 'promotions/functions.php';
require_once 'mails/mailPac.php';

/**
 * Récupération de la date de début depuis le pod calendrier
 */
function getDateDebut($annee, $semaine) {
	$pod_annees = Pods('annees', array('where' => "annee=$annee"));
	if ($pod_annees->total() > 0) {
		$id_annee = $pod_annees->field('id');
		$pod_calendrier = Pods('calendriers', array('where' => "id_annee=$id_annee AND designation = 'S$semaine'"));
		if ($pod_calendrier->total() > 0) {
			$date_debut = $pod_calendrier->display('date_debut');
			return pod2html_date($date_debut);
		}
	}
}

/**
 * Récupération de la date de fin depuis le pod calendrier
 */
function getDateFin($annee, $semaine) {
	$pod_annees = Pods('annees', array('where' => "annee=$annee"));
	if ($pod_annees->total() > 0) {
		$id_annee = $pod_annees->field('id');
		$pod_calendrier = Pods('calendriers', array('where' => "id_annee=$id_annee AND designation = 'S$semaine'"));
		if ($pod_calendrier->total() > 0) {
			$date_fin = $pod_calendrier->display('date_fin');
			return pod2html_date($date_fin);
		}
	}
}

/**
 * Convertir prix de pod vers prix affichage
 */
function pod2html_prix($prix) {
	return str_replace('&nbsp;', '', str_replace(',', '.', str_replace('$', '', $prix)));
}


/*
 * Fonctions communes aux fichiers externalisés pour
 * semaines et date, radios et barre d'actions
 */

/**
 * Fonction pour récupération du prefix id des éléments html des forms
 */
function get_prefix_id($page_name) {
	$prefix_id = '';
	switch ($page_name) {
		case 'cycle':
			$prefix_id = 'cycle';
			break;
		case 'evenement':
			$prefix_id = 'event';
			break;
		case 'evenment':
			$prefix_id = 'event';
			break;
		case 'event':
			$prefix_id = 'event';
			break;
		case 'document':
			$prefix_id = 'document';
			break;
		case 'promotion':
			$prefix_id = 'promotion';
			break;
		case 'mail':
			$prefix_id = 'mail';
			break;
	}
	return $prefix_id;
}

/**
 * Fonction pour récupération de l'objet courant
 */
function get_current_object($page_name) {
	$current_obj = 'data_current_';
	switch ($page_name) {
		case 'cycle':
			$current_obj .= 'cycle';
			break;
		case 'evenement':
			$current_obj .= 'event';
			break;
		case 'evenment':
			$current_obj .= 'event';
			break;
		case 'event':
			$current_obj .= 'event';
			break;
		case 'document':
			$current_obj .= 'doc';
			break;
		case 'promotion':
			$current_obj .= 'promo';
			break;
	}
	return $current_obj;
}

/**
 * Fonction pour récupérer le nom de la table du pod associé
 */
function get_table_name($prefix_id) {
	$table_name = '';
	switch ($prefix_id) {
		case 'cycle':
			$table_name = 'cycles';
			break;
		case 'promotion':
			$table_name = 'promotions';
			break;
		case 'event':
			$table_name = 'events';
			break;
		case 'document':
			$table_name = 'documents';
			break;
	}
	return $table_name;
}

/**
 * Fonction pour récupération lien vers page de visualisation
 */
function get_prev_page_link($page_name) {
	$page_detail = 'detail';
	switch ($page_name) {
		case 'cycle':
			$page_detail .= '-cycle';
			break;
		case 'evenement':
			$page_detail .= 's-evenment-cyclique';
			break;
		case 'evenment':
			$page_detail .= 's-evenment-cyclique';
			break;
		case 'event':
			$page_detail .= 's-evenment-cyclique';
			break;
		case 'document':
			$page_detail .= 's-document-cyclique';
			break;
		case 'promotion':
			$page_detail .= 's-promotion-cyclique';
			break;
	}
	return $page_detail;
}

/**
 * Fonction pour récupération lien vers page d'édition
 */
function get_edit_page_link($page_name) {
	$page_edit = 'edit-';
	switch ($page_name) {
		case 'cycle':
			$page_edit .= 'cycle';
			break;
		case 'evenement':
			$page_edit .= 'event-cyclique';
			break;
		case 'evenment':
			$page_edit .= 'event-cyclique';
			break;
		case 'event':
			$page_edit .= 'event-cyclique';
			break;
		case 'document':
			$page_edit .= 'document-cyclique';
			break;
		case 'promotion':
			$page_edit .= 'promotion-cyclique';
			break;
	}
	return $page_edit;
}

/**
 * Fonction pour récupérer le nom du parametre id
 */
function get_id_parameter($page_name) {
	$get_id = 'id_';
	switch ($page_name) {
		case 'cycle':
			$get_id .= 'cycle';
			break;
		case 'evenement':
			$get_id .= 'event';
			break;
		case 'evenment':
			$get_id .= 'event';
			break;
		case 'event':
			$get_id .= 'event';
			break;
		case 'document':
			$get_id .= 'doc';
			break;
		case 'promotion':
			$get_id .= 'promo';
			break;
	}
	return $get_id;
}

/**
 * Fonction pour récupérer le type sur lequel une remarque ou un avenaant sera fait
 */
function get_target_remarque_avenant($page_name) {
	$target = '';
	switch ($page_name) {
		case 'cycle':
			$target .= 'C';
			break;
		case 'evenement':
			$target .= 'E';
			break;
		case 'evenment':
			$target .= 'E';
			break;
		case 'event':
			$target .= 'E';
			break;
		case 'document':
			$target .= 'D';
			break;
		case 'promotion':
			$target .= 'P';
			break;
	}
	return $target;
}

/**
 * Comparaison de deux dates
 */
function compareDateWithToday($date) {
	$today = date('d/m/Y');
	$date = explode('/', $date);
	$today = explode('/', $today);
	$date_str =  $date[2].$date[1].$date[0];
	$today_str =  $today[2].$today[1].$today[0];
	if ($today_str > $date_str) return 1;
	if ($today_str < $date_str) return 2;
	if ($today_str == $date_str) return 3;
}

/**
 * Voir si une entité existe déjà
 */
function entiteExiste($podName, $champ_value, $scop=false, $id=null) {
	$existe = false;
	switch ($podName) {
		case 'cycles':
			$where = "identifiant='$champ_value'";
			if ($scop) {
				if (isset($id)) $where .= " AND id<>$id";
			}
			$cycles = Pods('cycles', array('where' => $where));
			if ($cycles->total() > 0) $existe = true;
			break;
		case 'events':
			
			break;
		case 'promos':
			
			break;
		case 'documents':
			
			break;
		case 'articles':
			$where = "reference='$champ_value'";
		if ($scop) {
				if (isset($id)) $where .= " AND id<>$id";
			}
			$artices = Pods('articles', array('where' => $where));
			if ($artices->total() > 0) $existe = true;
			break;
	}
	return $existe;
}

/**
 * Vérifier s'il y a au moins une entité attachée à un cycle avec action informatique = oui
 * si c'est le cas il faut afficher le picto action informatqiue prés du cycle parent
 */
function is_entite_attachee_action_informatique($id_cycle) {
	############################Events############################
	$pods_cycles_events = Pods('cycles_events', array('where' => "id_cycle=$id_cycle", 'limit' => "-1", 'select' => "t.id_event"));
	if ($pods_cycles_events->total() > 0) {
		while ($pods_cycles_events->fetch()) {
			$pods_events = Pods('events', array('where' => 'id='.$pods_cycles_events->field('id_event').' AND action_info=1', 'select' => "t.action_info", 'limit' => "-1"));
			if ($pods_events->total() > 0) return true;
		}
	}
	
	############################documents############################
	$pods_cycles_docs = Pods('cycles_docs', array('where' => "id_cycle=$id_cycle", 'limit' => "-1", 'select' => "t.id_doc"));
	if ($pods_cycles_docs->total() > 0) {
		while ($pods_cycles_events->fetch()) {
			$pods_docs = Pods('documents', array('where' => 'id='.$pods_cycles_docs->field('id_doc').' AND action_info=1', 'select' => "t.action_info", 'limit' => "-1"));
			if ($pods_docs->total() > 0) return true;
		}
	}
	
	############################Promotions############################
	$pods_cycles_promos = Pods('cycles_promos', array('where' => "id_cycle=$id_cycle", 'limit' => "-1", 'select' => "t.id_promo"));
	if ($pods_cycles_promos->total() > 0) {
		while ($pods_cycles_promos->fetch()) {
			$pods_promos = Pods('documents', array('where' => 'id='.$pods_cycles_promos->field('id_promo').' AND action_info=1', 'select' => "t.action_info", 'limit' => "-1"));
			if ($pods_promos->total() > 0) return true;
		}
	}
	
	return false;
}

/*
 * Retourne le groupe de plus haut niveau d'un user
 */
function get_high_level_group_from_current_user() {
	$current_user = wp_get_current_user();
	$user = new Groups_User( $current_user->ID );
	$groups = $user->__get( 'groups' );
	$high_level_group = "Registered";
	foreach($groups as $group) {
		if ($group->name == "StanHome_CellulePAC") {
			$high_level_group = "StanHome_CellulePAC";
			break;
		}
		if ($group->name == "StanHome_ComitePAC") {
			$high_level_group = "StanHome_ComitePAC";
			break;
		}
		if ($group->name == "StanHome_Presta") {
			$high_level_group = "StanHome_Presta";
			break;
		}
		if ($group->name == "StanHome_DR") {
			$high_level_group = "StanHome_DR";
			break;
		}
		if ($group->name == "StanHome_DD") {
			$high_level_group = "StanHome_DD";
			break;
		}
	}
	return $high_level_group;
}

/*
 * Formatage dates page detail cycle print
 */
function formatDate2print($date) {
	$date = explode('/', $date);
	return $date[1] . '/' . $date[0] . '/' . $date[2];
}

/*
 * Permet l'affichage de l'icone sur le diffusion
 */
function display_diffusion_icons($entity) {
	if ($entity->field('diffusion_level') >= 10 and $entity->field('diffusion_level') <= 30) {
		echo '<img src="';
		bloginfo('template_directory');
		echo '/images/etat/diffusion-icon.png" title="En cours de diffusion" alt="En cours de diffusion">';
	}
	if ($entity->field('diffusion_level') == 40) {
		echo '<img src="';
		bloginfo('template_directory');
		echo '/images/etat/diffusion-icon_on.png" title="Diffusé" alt="Diffusé">';
	}
	if ($entity->field('diffusion_level') == 0) {
		echo '<img src="';
		bloginfo('template_directory');
		echo '/images/etat/diffusion-icon_off.png" title="Pas diffusé" alt="Pas diffusé">';
	}
}

/*
 * Permet l'affichage des icones fini? et action info
 */
function display_binary_icons($entity, $entite_type='C') {
	if ($entity->display('action_info') == 'Yes') {
		echo '<img class="pictos_etats" src="';
		bloginfo('template_directory');
		echo '/images/etat/computer-icon.png" title="Action informatique" alt="Action informatique">';
	}
	
	$high_level_group = get_high_level_group_from_current_user();
	$avenants = Pods('avenants', array('where' => 'id_entite='.$entity->field('id').' AND type_entite=\''.$entite_type.'\'', 'limit' => '-1'));
	if ($avenants->total() > 0) {
		$nombre_avenants = $avenants->total();
		echo "<em class='nbr_avenants'>$nombre_avenants</em>";
		/*echo '<img src="';
		bloginfo('template_directory');
		echo '/images/urgent.png" title="Avenant sur cette entité" alt="Avenant sur cette entité">';*/
	}	
}


function display_binary_icons_by_array($entity, $entite_type='C') {
	if ($entity['action_info'] == 1) {
		echo '<img class="pictos_etats" src="';
		bloginfo('template_directory');
		echo '/images/etat/computer-icon.png" title="Action informatique" alt="Action informatique">';
	}

	$high_level_group = get_high_level_group_from_current_user();
	$avenants = Pods('avenants', array('where' => 'id_entite='.$entity['id'].' AND type_entite=\''.$entite_type.'\'', 'limit' => '-1'));
	if ($avenants->total() > 0) {
		$nombre_avenants = $avenants->total();
		echo "<em class='nbr_avenants'>$nombre_avenants</em>";
		/*echo '<img src="';
		 bloginfo('template_directory');
		echo '/images/urgent.png" title="Avenant sur cette entité" alt="Avenant sur cette entité">';*/
	}
}



/**
 * Récupérer l'émail et le nom d'un utilisateur à partir du Login
 */
function get_email_and_name_by_login($login) {
	$result = array();
	$user_infos = get_userdatabylogin($login);
	$result = array($user_infos->user_email, $user_infos->first_name. ' ' . $user_infos->last_name);
	return $result;
}

/**
 * Envoi d'émail à CellulePac aprés ajout d'un(e) remarque/avenant
 */
function send_mail_remarque($objet_mail, $content_mail, $from_infos) {
	$mail_pac = new mailPac($objet_mail, $content_mail);
	$group = new Groups_Group(1);
	$group = $group->read_by_name('StanHome_CellulePAC');
	$group_id = $group->group_id;
	$mail_pac->addGroupToMailList($group_id);
	$log_path_file = get_template_directory().'/log/remarques-avenants.log';
	$mail_pac->sendMailToAddedGroups($log_path_file, array(), $from_infos);
}

/**
 * Envoi d'émail aux groupes choisis après ajout d'unavenant
 */
function send_mail_avenant($objet_mail, $content_mail, $destination_mail, $from_infos) {
	foreach ($destination_mail as $destination) {
		$mail_pac = new mailPac($objet_mail, $content_mail);
		$group = new Groups_Group(1);
		$group = $group->read_by_name($destination);
		$group_id = $group->group_id;
		$mail_pac->addGroupToMailList($group_id);
		$log_path_file = get_template_directory().'/log/remarques-avenants.log';
		$mail_pac->sendMailToAddedGroups($log_path_file, array(), $from_infos);
	}
}

/**
 * Ajouter les cycles ou périodes après les désignation de chaque entité sous jacente
 */
function cycles_periodes_suffixe($semaine_debut, $annee, $semaine_fin, $annee_fin, $permalink) {
	$cycles_periodes = get_cycles_between_two_weeks($semaine_debut, $annee, $semaine_fin, $annee_fin, $permalink);
	$cycles_periodes = explode(',', $cycles_periodes);
	$cycles_periodes_temp = array();
	foreach ($cycles_periodes as $item) {
		$item = explode(' ', $item);
		$cycles_periodes_temp[] = $item[1] . ' ' . $item[2] . '/' .$item[0];
	}
	$result = implode(' - ', $cycles_periodes_temp);
	return $result;
}

/**
 * Diffusé et vu par DD ou DR
 */
function diffuse_dd_dr($diffusion_level) {
	if ($diffusion_level == 30) {
		echo '<img src="';
		echo bloginfo('template_directory');
		echo '/images/etat/lancement-chaud-dr.png' . '" alt="Diffusé et actuellement vu par DR" />';
	}
	if ($diffusion_level == 40) {
		echo '<img src="';
		echo bloginfo('template_directory');
		echo '/images/etat/lancement-chaud-dd.png' . '" alt="Diffusé et actuellement vu par DD" />';
	}
}

/**
 * Fonction pour transormer le droit de visualisation en texte
 */
function droitVisuToString($entite) {
	$visu_level = $entite->field('diffusion_level');
	$visu_string = '';
	switch ($visu_level) {
		case 0:
			$visu_string = 'la cellule PAC';
			break;
		case 10:
			$visu_string = 'la cellule PAC et le comité PAC';
			break;
		case 20:
			$visu_string = 'la cellule PAC, le comité PAC et les prestataires';
			break;
		case 30:
			$visu_string = 'la cellule PAC, le comité PAC, les prestataires et les DR';
			break;
		case 40:
			$visu_string = 'la cellule PAC, le comité PAC, les prestataires, les DR et les DD';
			break;
	}
	return $visu_string;
}

/**
 * Formatter l'objet du mail: remarque et avenant
 */
function appendToSubject($entite_type, $entite_id) {
	$result = '';
	switch($entite_type) {
		case 'C':
			$cycle = Pods("cycles", array('where' => 'id = '.$entite_id));
			$cycle->fetch();
			$designation = $cycle->display('identifiant');
			if ($cycle->display('permalink') == 'cycles') $result = "le cycle ". stripslashes($designation);
			else $result = "la période ". stripslashes($designation);
			break;
		case 'P':
			$promotion = Pods('promotions', array('where' => 'id = '.$entite_id));
			$promotion->fetch();
			$designation = $promotion->display('reference_extranet')." du(es) ".$promotion->display('cycles');
			$result = "la promotion ". stripslashes($designation);
			break;
		case 'E':
			$event = Pods("events", array('where' => 'id = '.$entite_id));
			$event->fetch();
			$designation = $event->display('designation');
			$result = "l'évènement ". stripslashes($designation);
			break;
		case 'D':
			$document = Pods("documents", array('where' => 'id = '.$entite_id));
			$document->fetch();
			$designation = $document->display('designation');
			$result = "le document ". stripslashes($designation);
			break;
		default:
			$designation = "";
			$result = "une entité inconnue ". stripslashes($designation);
	}
	return $result;
}

/**
 * Formatter l'objet du mail: diffusion
 */
function prependToSubjectDiffusion($entite_id) {
	$result = '';
	$cycle = Pods("cycles", array('where' => 'id = '.$entite_id));
	$cycle->fetch();
	$designation = $cycle->display('identifiant'); 
	$designation = explode(' ', $designation);
	$designation = $designation[1]." ".$designation[2]." ".$designation[0];
	if ($cycle->display('permalink') == 'cycles') { 
		$result = "Le " . $designation.'  est diffusé, vous pouvez le consulter ';
	} else {
		$result = "La " . $designation.'  est diffusée, vous pouvez la consulter ';
	}
	return $result;
}

/**
 * Fonction qui permet de formatter le texte des commentaires
 */
function formatCommentText($comment) {
	$orig = stripslashes($comment);
	$temp = str_ireplace('<pre>', '<div>', $orig);
	$temp = str_ireplace('<pre dir="ltr">', '<div>', $temp);
	$temp = str_ireplace('<pre dir="">', '<div>', $temp);
	$temp = str_ireplace('<pre style', '<div style', $temp);
	$temp = str_ireplace('</pre>', '</div>', $temp);
	$temp = str_ireplace('Verdana', 'Arial', $temp);
	$temp = str_ireplace('Helv', 'Arial', $temp);
	$temp = str_ireplace('Arial Black', 'Arial', $temp);
	$temp = str_ireplace('Calibri', 'Arial', $temp);
	$temp = str_ireplace('<colgroup>', '', $temp);
	$temp = str_ireplace('</colgroup>', '', $temp);
	$temp = str_ireplace('<caption>', '', $temp);
	$temp = str_ireplace('</caption>', '', $temp);
	return $temp;
}

/**
 * Récupérer le niveau le plus elevé pour une entité nouvellement crée
 */
function getHigherLevelDiffusion($cycles_periodes) {
	$cycles_periodes = explode(',', $cycles_periodes);
	$higher_level_diffusion = 0;
	foreach ($cycles_periodes as $item) {
		$cycle_periode = Pods('cycles', array('where' => "identifiant='$item'"));
		$cycle_periode->fetch();
		if ($cycle_periode->field('diffusion_level') > $higher_level_diffusion)
			$higher_level_diffusion = $cycle_periode->field('diffusion_level');
	}
	return $higher_level_diffusion;
}

/**
 * Si Diffusion Level ne permet pas aux DD/DR de consulter une entité, il faut empecher de consulter l'entité concernée
 */
function removeMailAvenantDdDr($type_entite, $id_entite) {
	$diffusion_level = '';
	$pod_entite = '';
	$result = array(true, true);;
	switch($type_entite) {
		case 'C':
			$pod_entite = Pods('cycles', array('where' => "id=$id_entite"));
			break;
		case 'P':
			$pod_entite = Pods('promotions', array('where' => "id=$id_entite"));
			break;
		case 'D':
			$pod_entite = Pods('events', array('where' => "id=$id_entite"));
			break;
		case 'E':
			$pod_entite = Pods('documents', array('where' => "id=$id_entite"));
			break;
	}
	$pod_entite->fetch();
	$diffusion_level = $pod_entite->field('diffusion_level');
	if($diffusion_level == 30) $result[0] = false;
	if($diffusion_level == 40) $result[1] = false;
	return $result;
}