<?php
require_once 'include-functions.php';

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'documents/ajax.php';
	exit();
}

$cibles = get_liste_cible();

if (!isset($_GET['id_doc']) || $_GET['id_doc'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
$pod_cible = 'Docs';
$id_pod_cible = $_GET['id_doc'];
############################récupération commentaires#####################
$commentaires_dd = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dd'"));
$content_comment_dd = '';
$content_comment_dr = '';
if ($commentaires_dd->total() > 0)
	$content_comment_dd = $commentaires_dd->display('content');
	
$commentaires_dr = Pods('commentaires', array('where' => "pod_cible='" . $pod_cible . "' AND id_pod_cible=" . $id_pod_cible . " AND visibility='" . "dr'"));
$content_comment_dr = '';
if ($commentaires_dr->total() > 0)
	$content_comment_dr = $commentaires_dr->display('content');
##########################################################################
?>
<?php 
$current_doc = Pods('documents', array('where' => "id=" . $_GET['id_doc']));
$data_current_doc = $current_doc->data();
$data_current_doc = $data_current_doc[0];
?>
<?php if (is_null($data_current_doc)):?>
<h3 class="ttl">Document Introuvable</h3>
<?php exit();?>
<?php else:?>
<?php 
###cibles pour cet event###
$this_doc_cible_array = explode(',', $data_current_doc->cible);
$this_doc_cible = array();
foreach ($this_doc_cible_array as $item) {
	$this_doc_cible[] = $cibles[$item];
}
$this_doc_cible = implode(' + ', $this_doc_cible);
?>


<form>
	<?php if (!isset($_GET['is_prev'])):?> <?php require_once 'barre-actions.php';?><?php endif;?>
	<div class="frame">		
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" valign="top" >
					<h2 class="ttl">Détail Document : <span class="red"><?php echo $data_current_doc->reference_document?></span></h2>
				</td>
				<?php require_once 'zone-radios.php';?>
			</tr>
			<tr><td colspan="3"></td></tr>
			<tr>
				<td  align="right"><label>Référence Document :</label></td>
				<td><?php echo $data_current_doc->reference_document?></td>
				<td rowspan="2" class="grey_area">
				<?php require_once 'semaines-dates.php';?>
				</td>
			</tr>
			<tr>
				<td align="right"><label>Désignation :</label></td>
				<td><?php echo stripslashes($data_current_doc->designation)?></td>
			</tr>
			<tr>
				<td align="right"><label>Cible(s) :</label></td>
				<td colspan="2"><input type="text" class="vlong" value='<?php echo stripslashes($this_doc_cible)?>'  disabled="disabled"></td>
			</tr>
			<tr>
				<td align="right"><label>Cycle(s) :</label></td>
				<td  colspan="2"><input type="text" class="champ_non_editable vlong" value='<?php echo get_cycles_between_two_weeks($data_current_doc->semaine_debut, $data_current_doc->annee, $data_current_doc->semaine_fin, $data_current_doc->annee_fin, $data_current_doc->permalink)?>'  disabled="disabled"></td>
			</tr>
			<tr>
				<td align="right"><label>Critères de distribution :</label></td>
				<td><?php echo stripslashes($data_current_doc->criteres_distribution)?></td>
			</tr>
			<tr>
				<td align="right"><label>Réf. Lot d'appartenance :</label></td>
				<td><?php echo $data_current_doc->reference_lot_appartenance?></td>
			</tr>
			<tr>
				<td align="right"><label>Référence Extranet :</label></td>
				<td><?php echo stripslashes($data_current_doc->reference_extranet)?></td>
			</tr>
			<tr>
				<td align="right"><label>Qté doc. Dans un lot :</label></td>
				<td><?php echo $data_current_doc->qte_doc_par_lot?></td>
			</tr>
			<tr>
				<td align="right"><label>Qté doc imprimés</label></td>
				<td><?php echo $data_current_doc->qte_doc_imprimes?></td>
			</tr>
			<tr>
				<td colspan="3">
					<table cellpadding="0" cellspacing="0" class="tab_commerc" width="95%">
						<tr>
							<td class="empty"></td>
							<th align="center">Date début</th>
							<th align="center">Date fin</th>
							<th align="center" class="brd_right">Quantité</th>
						</tr>
						<tr>
							<th><label>Pre-lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_doc->date_debut_pre_lancement) ?></span> </td>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_doc->date_fin_pre_lancement) ?></span></td>
							<td align="center" class="brd_right"><span><?php echo $data_current_doc->qt_pre_lancement?></span></td> 
						</tr>
						<tr>
							<th><label>Lancement</label></th>
							<td align="center"><span class="gras"><?php echo pod2js_date($data_current_doc->date_debut_lancement) ?></span></td>
							<td class="brd_right" align="center"><span class="gras"><?php echo pod2js_date($data_current_doc->date_fin_lancement) ?></span></td>
							<td  align="center" class="brd_right" style="border-right: 0px;"><span id="eroor_lancement" class="red"></span></td>
						</tr>
						<tr><td colspan="3" class="error" align="center"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</form>

<?php include_once 'comment-bloc.php';?>

<!-- Ici, la visualisation des documents attachés et les remarques -->
<?php
	$my_entity_id = $id_pod_cible; 
	$type_entity = 'D';
	include_once("attachments/attachment-doc-visu.php");
	include_once("remarques/remarques-visu.php");
	include_once("avenants/avenants-visu.php");
	?>
		
<?php endif;?>
<?php endif;?>

<script type="text/javascript">
( function( $ ) {
	/*
	 * Suppression d'un document
	 */
	$("#delete_document").bind('click', function() {
		if(confirm("Etes vous certain de vouloir supprimer ce document?")) {
			$.ajax({
				url: '',
				type: 'get',
				data: 'id_doc= ' + <?php echo $_GET['id_doc']?> + '&ajax=5',
				success: function(msg) {
					switch(msg) {
					case '-1':
						$("#info").html('Erreur!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						break;
					case '0':
						$("#info").html('Problème d\'accés à la base de données!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						break;
					case '1':
						$("#info").html('Document supprimé!').fadeOut('slow').fadeIn('slow').fadeOut('slow');
						window.location = '<?php echo get_permalink(get_page_by_path('cycles')) ?>';
						break;
				}
				},
				error: function() {
					alert('Error Ajax!');
				}
			});
		}
		return false;
	});
	
})( jQuery );
</script>