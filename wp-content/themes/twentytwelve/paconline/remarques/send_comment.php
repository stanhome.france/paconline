<?php
/*
 * Envoyer une remarque
*/
if(isset($_POST['action']) && $_POST['action'] == 'send_comment') {  
	// ajouter la remarque dans la base
	$pod = Pods('remarques');
	
	$auteur = wp_get_current_user();
	$auteur = $auteur->user_login;
	$data = array('id_entite' => $_POST["id_entite"],
				  'type_entite' => $_POST["type_entite"],
				  'creation_date' => Date("Y-m-d"),
				  'content' => $_POST["comment"],
				  'auteur' => $auteur);
	$new_item_id = $pod->add($data);
	
	
	// Envoyer un mail à la cellule PAC
	$subject = $_POST['mail_objet'] . ': ' . appendToSubject($_POST["type_entite"], $_POST["id_entite"]);
	$subject = 'REMARQUE ' . $subject;
	$content = '"REMARQUE"<br/>' . stripslashes($_POST['mail_contenu']);
	$from_infos = get_email_and_name_by_login($auteur);
	send_mail_remarque(stripslashes($subject), $content, $from_infos);
}
?>