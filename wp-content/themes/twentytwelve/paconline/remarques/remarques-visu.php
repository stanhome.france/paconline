<?php 
	require_once dirname(dirname(__FILE__)) . '/include-functions.php';
	$remarques = Pods('remarques', array('where' => 'id_entite = '.$my_entity_id.' and type_entite="'.$type_entity.'"'));
	if ($remarques->fetch()) {
		$to_display_part = '<p class="ttl">Les remarques</p>';
		$i = 1;
		$to_display_part .= '<div class="frame">';
		$to_display_part .= '<table cellpadding="0" cellspacing="0" class="tab_commerc" width="100%">';
		$to_display_part .= '<tr>';
		$to_display_part .= '<th align="center">Numéro</th>';
		$to_display_part .= '<th align="center">Date</th>';
		$to_display_part .= '<th align="center">Auteur</th>';
		$to_display_part .= '<th align="center" class="brd_right">Remarque</th>';
		$to_display_part .= '</tr>';
		do {
			$creation_date= $remarques->display('creation_date');
			$to_display_part .= '<tr>';
			$to_display_part .= '<td align="center">'.$i.'</td>';
			$to_display_part .= '<td align="center">'.$creation_date.'</td>';
			$to_display_part .= '<td align="center">'.$remarques->display('auteur').'</td>';
			$to_display_part .= '<td align="left" class="brd_right">'.stripSlashes($remarques->display('content')).'</td>';
			$to_display_part .= '</tr>';
			$i++;
		} while ($remarques->fetch());
		$to_display_part .= '<tr>';
		$to_display_part .= '<td colspan="4" class="error"></td>';
		$to_display_part .= '</tr>';
		$to_display_part .= '</table>';
		$to_display_part .= '</div>';
		echo do_shortcode( '[groups_member group="StanHome_ComitePAC"]'.$to_display_part.'[/groups_member]' );
		echo do_shortcode( '[groups_member group="StanHome_CellulePAC"]'.$to_display_part.'[/groups_member]' );
	}
?>