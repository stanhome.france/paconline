<?php
require_once dirname(dirname(__FILE__)) . '/include-functions.php';

/*
 * Ajout d'un document
 */
if ($_GET['ajax'] == 1) {
	$documents = Pods('documents');
	$cycles_new = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	$cycles_periodes = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin'], $_GET['cycle_ou_periode']);
	$data = array(
		'permalink'                  => $_GET['cycle_ou_periode'],
		'annee'                      => $_GET['annee'],
		'annee_fin'                  => $_GET['annee_fin'],
		'semaine_debut'              => $_GET['semaine_debut'],
		'semaine_fin'                => $_GET['semaine_fin'],
		'cycles'                     => $cycles_new,
		'reference_document'         => $_GET['reference'],
		'designation'                => $_GET['designation'],
		'cible'                      => $_GET['cible'],
		'criteres_distribution'       => $_GET['critere_distribution'],
		'reference_lot_appartenance' => $_GET['reference_lot_appartenance'],
		'reference_extranet'         => $_GET['reference_extranet'],
		'qte_doc_par_lot'            => $_GET['qte_doc_par_lot'],
		'qte_doc_imprimes'           => $_GET['qte_doc_imprimes'],
		'date_debut_pre_lancement'   => js2mysql_date($_GET['date_debut_pre_lancement']),
		'date_fin_pre_lancement'     => js2mysql_date($_GET['date_fin_pre_lancement']),
		'qt_pre_lancement'           => $_GET['qt_pre_lancement'],
		'date_debut_lancement'       => js2mysql_date($_GET['date_debut_lancement']),
		'date_fin_lancement'         => js2mysql_date($_GET['date_fin_lancement']),
		'publie'                     => $_GET['publie'],
		'action_info'                => $_GET['action_info'],
		'lancement_chaud'            => $_GET['lancement_chaud'],
		'is_mail'                    => $_GET['is_mail'],
		'date_lancement_chaud_dd'    => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
		'date_lancement_chaud_dr'    => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : '',
		'diffusion_level'            => 10
	);
	$new_doc_id = $documents->add($data);
	if (isset($_GET['files_to_attach'])) {
		$entity_id = $new_doc_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}

	if ($new_doc_id > 0) {
		$cycles_docs = Pods('cycles_docs');
		$cycles_correspondants = explode(',', $cycles_new);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_doc' => $new_doc_id);
			$cycles_docs->add($data);
		}
		echo $new_doc_id;
	}
}

/*
 * Calcul des cycles correspondants aux semaines debut et fin
 */
if ($_GET['ajax'] == 13) {
	if (isset($_GET['debut']) && $_GET['debut'] != '' && isset($_GET['fin']) && $_GET['fin'] != '') {
		echo get_cycles_between_two_weeks($_GET['debut'], $_GET['annee'], $_GET['fin'], $_GET['annee_fin'], $_GET['permalink']);
	}
}

/*
 * Edition d'un document
 */
if ($_GET['ajax'] == 3) {
	$documents = Pods('documents');
	$cycles_edi = get_cycles_between_two_weeks($_GET['semaine_debut'], $_GET['annee'], $_GET['semaine_fin'], $_GET['annee_fin']);
	$data = array(
		
		'annee'                      => $_GET['annee'],
		'annee_fin'                  => $_GET['annee_fin'],
		'semaine_debut'              => $_GET['semaine_debut'],
		'semaine_fin'                => $_GET['semaine_fin'],
		'cycles'                     => $cycles_edi,
		'designation'                => $_GET['designation'],
		'reference_document'         => $_GET['reference'],
		'cible'                      => $_GET['cible'],
		'criteres_distribution'       => $_GET['critere_distribution'],
		'reference_lot_appartenance' => $_GET['reference_lot_appartenance'],
		'reference_extranet'         => $_GET['reference_extranet'],
		'qte_doc_par_lot'            => $_GET['qte_doc_par_lot'],
		'qte_doc_imprimes'           => $_GET['qte_doc_imprimes'],
		'date_debut_pre_lancement'   => js2mysql_date($_GET['date_debut_pre_lancement']),
		'date_fin_pre_lancement'     => js2mysql_date($_GET['date_fin_pre_lancement']),
		'qt_pre_lancement'           => $_GET['qt_pre_lancement'],
		'date_debut_lancement'       => js2mysql_date($_GET['date_debut_lancement']),
		'date_fin_lancement'         => js2mysql_date($_GET['date_fin_lancement']),
		'action_info'                => $_GET['action_info'],
		'lancement_chaud'            => $_GET['lancement_chaud'],
		'publie'                     => $_GET['publie'],
		'is_mail'                    => $_GET['is_mail'],
		'date_lancement_chaud_dd'    => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dd']) : '',
		'date_lancement_chaud_dr'    => ($_GET['lancement_chaud'] == '1') ? js2mysql_date($_GET['date_lancement_chaud_dr']) : ''
	);
	$edited_doc_id = $documents->save($data, null, $_GET['id_doc']);

	if (isset($_GET['files_to_attach'])) {
		$entity_id = $edited_doc_id;
		require_once get_template_directory().'/paconline/attachments/save-only-doc.php';
	}
	
	if ($edited_doc_id > 0) {
		####suppression des relations avec les cycles correspondants####
		$doc_cycle_pod = Pods('cycles_docs', array('where' => "id_doc=$edited_doc_id"));
		$total_cycles_docs = $doc_cycle_pod->total();
		if ($total_cycles_docs > 0) {
			while ($doc_cycle_pod->fetch()) {
				$temp_pod = Pods('cycles_docs', $doc_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		####etablissement des nouvelles relations avec de nouveux cycles####
		$cycles_docs = Pods('cycles_docs');
		$cycles_correspondants = explode(',', $cycles_edi);
		foreach ($cycles_correspondants as $item) {
			$cycles = Pods('cycles', array('where' => "identifiant = '$item'"));
			$data = array('id_cycle' => $cycles->field('id'), 'id_doc' => $edited_doc_id);
			$cycles_docs->add($data);
		}
		echo $edited_doc_id;
	}
}


/*
 * Suppression d'un évent
 */
if ($_GET['ajax'] == 5) {
	if (!isset($_GET['id_doc'])) {
		echo -1;
		exit();
	}
	$id_doc = $_GET['id_doc'];
	$doc_pod = Pods('documents', $id_doc);
	$remarques_pod = Pods('remarques', array('where' => "id_entite=$id_doc AND type_entite='D'"));
	if ($doc_pod->delete()) {
		$doc_cycle_pod = Pods('cycles_docs', array('where' => "id_doc=$id_doc"));
		$total_cycles_docs = $doc_cycle_pod->total();
		if ($total_cycles_docs > 0) {
			while ($doc_cycle_pod->fetch()) {
				$temp_pod = Pods('cycles_docs', $doc_cycle_pod->field('id'));
				$temp_pod->delete();
			}
		}
		while ($remarques_pod->fetch()) {
			$pod_temp = Pods('remarques', $remarques_pod->field('id'));
			$pod_temp->delete();
		}
		echo 1;
	}
	else 
		echo 0;
}