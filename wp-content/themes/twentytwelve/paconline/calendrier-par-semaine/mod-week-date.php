<?php
	$pod = Pods('calendriers');
	
	// transformation des dates pour mysql
	// format en entrée : dd/mm/yyyy
	// format en sortie : yyyy-mm-dd
	$d = explode('/', $_GET["date_debut"]);
	$dd = $d[2].'-'.$d[1].'-'.$d[0];
	$d = explode('/', $_GET["date_fin"]);
	$df = $d[2].'-'.$d[1].'-'.$d[0];
	
	$data = array('date_debut' => $dd,
				  'date_fin' => $df);
	$modified_item_id = $pod->save($data, null, $_GET["id"]);
?>