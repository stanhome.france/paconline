<?php
$y = substr($_GET["year"], 1);

$noms_jours = array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
$noms_mois = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
				
// -------------------------------------------------------------------------------------
// mettre à jour les années
// -------------------------------------------------------------------------------------
$params = array('where'=>"annee = '".$y."'");
$stuff = Pods('annees', $params);
if ($stuff->fetch()) {
	$year_id = $stuff->field('id');
} else {
	$annee_pod = Pods('annees');
	$data = array('annee' => $y);
	$year_id = $annee_pod->add($data);
}
    echo "<script>alert(\"la variable est nulle\")</script>";
// -------------------------------------------------------------------------------------
// Mettre à jour les semaines
// -------------------------------------------------------------------------------------
// on cherche le premier lundi de l'année courante
$premier_lundi = strtotime("monday", mktime(0, 0, 0, 1, 1, $y));
// on cherche le numéro de semaine dont le lundi appartient
$numero_semaine = Date("W", $premier_lundi);
// si ce numero de semaine est à 2 alors le lundi de la semaine 1 se trouve 7 jours avant
if ($numero_semaine == 2) {
	$premier_lundi -= 604800; // moins 1 semaine
	$numero_semaine = Date("W", $premier_lundi);
}

$premier_lundi_txt = $noms_jours[Date("w", $premier_lundi)]." ".Date("d", $premier_lundi)." ".$noms_mois[Date("n", $premier_lundi) - 1]." ".Date("Y", $premier_lundi);

$calendrier_pod = Pods('calendriers');

$debut_semaine_courante = $premier_lundi;
$fin_semmaine_courante = $premier_lundi + 604790;
for($i = 1; $i <=53; $i++) {
	$params = array('where'=>"designation = 'S".$i."' and id_annee = ".$year_id);
	$stuff = Pods('calendriers', $params);
	if ($stuff->fetch()) {
		$designation = $stuff->display('designation');
		$id = $stuff->field('id');
	} else {
		$designation = "";
		$id = 0;
	}
	
	$data = array(
			'id_annee' => $year_id,
			'designation' => 'S'.$i,
			'date_debut' => Date("Y-m-d", $debut_semaine_courante),
			'date_fin' => Date("Y-m-d", $fin_semmaine_courante),
			'semaine' => "1",
			'vacances' => "0",
			'texte' => "Texte non défini"
	);
	
	if ($designation == 'S'.$i) {
		$new_calendrier_id = $calendrier_pod->save($data, null, $id);
	} else {
		$new_calendrier_id = $calendrier_pod->add($data);
	}
	
 	$debut_semaine_courante = $fin_semmaine_courante + 10;
	$fin_semmaine_courante = $debut_semaine_courante + 604790;
}



// -------------------------------------------------------------------------------------
// Mettre à jour les jours fériés
// -------------------------------------------------------------------------------------
function getHolidays($year = null)
{
	if ($year === null)
	{
		$year = intval(date('Y'));
	}

	$easterDate  = easter_date($year);
	$easterDay   = date('j', $easterDate);
	$easterMonth = date('n', $easterDate);
	$easterYear   = date('Y', $easterDate);

	$holidays = array(
			// Dates fixes
			array("Nouvel an", mktime(0, 0, 0, 1,  1,  $year)),  // 1er janvier
			array("Fête du travail", mktime(0, 0, 0, 5,  1,  $year)),  // F�te du travail
			array("Victoire des alliés", mktime(0, 0, 0, 5,  8,  $year)),  // Victoire des alli�s
			array("Fête nationale", mktime(0, 0, 0, 7,  14, $year)),  // F�te nationale
			array("Assomption", mktime(0, 0, 0, 8,  15, $year)),  // Assomption
			array("Toussaint", mktime(0, 0, 0, 11, 1,  $year)),  // Toussaint
			array("Armistice", mktime(0, 0, 0, 11, 11, $year)),  // Armistice
			array("Noël", mktime(0, 0, 0, 12, 25, $year)),  // Noel

			// Dates variables
			array("Pâques", mktime(0, 0, 0, $easterMonth, $easterDay + 2,  $easterYear)),
			array("Ascension", mktime(0, 0, 0, $easterMonth, $easterDay + 40, $easterYear)),
			array("Pentecôte", mktime(0, 0, 0, $easterMonth, $easterDay + 51, $easterYear))
	);

	sort($holidays);

	return $holidays;
}


$feries_pod = Pods('feries');

// on verifie que les les jours fériés ne sont pas déjà mémorisés dans la base
$params = array('where'=>"id_annee = ".$year_id);
$stuff = Pods('feries', $params);

// Si ce n'est pas le cas
if (!$stuff->fetch()) {
	// On calcule les jours fériés de l'année y
	$jours_feries = getHolidays($y);
	
	foreach($jours_feries as $jour) {
		$data = array(
				'id_annee' => $year_id,
				'designation' => $jour[0],
				'date_jour_ferie' => Date("Y-m-d", $jour[1])
		);
		$new_ferie_id = $feries_pod->add($data);
	}
}

?>
