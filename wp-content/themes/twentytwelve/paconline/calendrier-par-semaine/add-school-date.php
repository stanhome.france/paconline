<?php
	$pod = Pods('calendriers');
	
	// transformation des dates pour mysql
	// format en entrée : dd/mm/yyyy
	// format en sortie : yyyy-mm-dd
	$d = explode('/', $_GET["date_deb"]);
	$dd = $d[2].'-'.$d[1].'-'.$d[0];
	$d = explode('/', $_GET["date_fin"]);
	$df = $d[2].'-'.$d[1].'-'.$d[0];
	
	$data = array('id_annee' => $_GET["y"],
				  'vacances' => '1',
				  'semaine' => '0',
				  'designation' => $_GET["designation"],
				  'date_debut' => $dd,
				  'date_fin' => $df);
	$new_item_id = $pod->add($data);
?>