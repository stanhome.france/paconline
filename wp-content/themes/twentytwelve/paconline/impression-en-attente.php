<script type="text/javascript">
    jQuery(document).ready(function ($) {
        
        $("#filtre_radio_all").jqxRadioButton({ width: 58, height: 25 });
        $("#filtre_radio_cycles").jqxRadioButton({ width: 70, height: 25 });
        $("#filtre_radio_periodes").jqxRadioButton({ width: 100, height: 25 });

    	var filtre = '<?php echo isset($_GET['filtre']) ? $_GET['filtre'] : 'all' ?>';
    	switch(filtre) {
    		case 'all':
    			$("#filtre_radio_all").jqxRadioButton('check');
    			break;
    		case 'cycles':
    			$("#filtre_radio_cycles").jqxRadioButton('check');
    			break;
    		case 'periodes':
    			$("#filtre_radio_periodes").jqxRadioButton('check');
    			break;
    	}

        
        
        $('#filtre_radio_all').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('impression-en-attente')).'/?filtre=all'?>';
        }); 
        $('#filtre_radio_cycles').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('impression-en-attente')) . '/?filtre=cycles'?>';
        }); 
        $('#filtre_radio_periodes').bind('checked', function (event) {
        	window.location = '<?php echo get_permalink(get_page_by_path('impression-en-attente')) . '/?filtre=periodes'?>';
        });


        $("#start_agent_impression").bind('click', function() {
			var items = $("input[type=checkbox]:checked");
			var items_to_print_now = '';
			$.each(items, function() {
				items_to_print_now += $(this).attr("id") + ',';
			});
			if(items_to_print_now == '') alert('Choisissez au moins un(e) cycles/période');
			if(items_to_print_now == '') return false;
            $.ajax({
				url: '',
				type: 'get',
				data: 'ajax=1&items='+items_to_print_now,
				success: function(msg) {
					//alert(msg);
				},
				error: function() {
				}
			});
        });

	});
</script>
<?php 
require_once 'include-functions.php';

if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'agent-impression/ajax.php';
	exit();
}

$where_to_print = ' AND a_imprimer=1';
$where_temp = '';
if (isset($_GET['filtre']) && $_GET['filtre'] == 'periodes') $where_temp .= ' AND permalink=\'periodes\'';
elseif (isset($_GET['filtre']) && $_GET['filtre'] == 'cycles') $where_temp .= ' AND permalink=\'cycles\'';
//N'afficher que les cycles non archivés
$cycles = Pods('cycles', array('where'=>'archive=0' . $where_to_print . $where_temp, 'orderby' => 't.identifiant', 'limit' => "-1"));
$total = $cycles->total();

?>

<div class="frame btn_grp">
<div class="btn_group">
	<div class="btn_group1">
		<p id="filtre_radio_all">Tous</p>
		<p id="filtre_radio_cycles">Cycles</p>
		<p id="filtre_radio_periodes">Périodes</p>
		
		<input type="button" value="Imprimer manuellement" style="width: 155px;" id="start_agent_impression">
	</div>
</div>
</div>
<div class="frame">
<?php if ($total > 0):?>
	<h3 class="ttl">Cycles et Périodes en attente d'impression</h3>
	<div id="tree_cycles">
	<ul style="width:100%;">
		<?php
			$intercale_cycle_color = 0; 
			while ($cycles->fetch()): 
				if ($intercale_cycle_color % 2 == 0) {
					$li_cycle_bg = "#e8e8e8";
				} else {
					$li_cycle_bg = "tranparent";
				}
				$intercale_cycle_color++; ?>
		<li>
			<div style="background-color:<?php echo $li_cycle_bg; ?>; padding:5px; width:100%;">
			<input type="checkbox" id="<?php echo $cycles->field('id')?>">
			<?php $cycle_periode_url = get_permalink(get_page_by_path('detail-cycle')).'?id_cycle='.$cycles->field('id')?>
				<a <?php if ($cycles->display('lancement_chaud') == 'Yes'):?> style="color: red;" <?php endif;?> href="<?php echo $cycle_periode_url?>"><?php echo $cycles->display('identifiant')?> : 
				se déroule du <?php echo pod2html_date($cycles->display('date_debut')) ?> au <?php  echo pod2html_date($cycles->display('date_fin')) ?> : diffusion aux DR le <?php  echo pod2html_date($cycles->display('diffusion_dr')) ?>
				et aux DD le <?php  echo pod2html_date($cycles->display('diffusion_dd')) ?></a>
			</div>
			</li>
		<?php endwhile;?>
	</ul>
	</div>
<?php else:?>
	<h4>
		<?php 
			if (isset($_GET['entite']) && $_GET['entite'] == 'periode') echo 'Pas de périodes enregistrées';
			else echo 'Pas de cycles enregistrés';	
		?>
	</h4>
<?php endif;?>
</div>
