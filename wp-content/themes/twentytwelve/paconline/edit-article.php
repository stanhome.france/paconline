<?php 
require_once 'articles/ajout-article.php'; 

/*
 * Ce code est résérvé seulement pour les requetes AJAX
 * dans ce cas seulement ce code PHP est éxécuté
 */
if (isset($_GET['ajax']) && $_GET['ajax'] != -1) {
	ob_end_clean();
	require_once 'articles/ajout-article-ajax.php';
	exit();
}

$options_tva = get_liste_tva();
$options_marque = get_liste_marque();
$options_gamme = get_liste_gamme();


if (!isset($_GET['id_article']) || $_GET['id_article'] == ''):
?>
<h3 class="ttl">Page Introuvable</h3>
<?php 
else:
?>
<?php 
$current_article = pods_data('articles', $_GET['id_article']);
$data_current_article = $current_article->row;
?>
<?php 
if (is_null($data_current_article)):
?>
<h3 class="ttl">Article Introuvable</h3>
<?php 
else:
?>
<div id="edit_new_article" class="nouvel_article">			
		<form id="edit_new_article_form" action="" method="get">
			<?php require_once 'barre-actions.php';?>
			<div class="frame">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td colspan="2" valign="top">
						<h2 class="ttl">Modification Article : <span class="red"><?php echo $data_current_article['designation']?></span></h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>				
				<tr>
					<td align="right">
						<label>Référence</label>
						<input type="text" id="article_reference" value="<?php echo $data_current_article['reference']?>">
						<div class="clearfix"></div>
						<span id="error_reference" class="red"></span>
					</td>
					<td align="right" width="50%">
						<label>Désignation</label>
						<input type="text" id="article_designation" value="<?php echo $data_current_article['designation']?>">
						<div class="clearfix">
						</div><span id="error_designation" class="red"></span>
					</td>
				</tr>
				<tr>
					<td align="right"><label>Prix TTC</label>
						<input type="text" id="article_ttc" value="<?php echo $data_current_article['prix_ttc']?>">
						<div class="clearfix"></div><span id="error_ttc"></span></td>
					<td align="right"><label>TVA</label>
						<select id="article_tva">
							<option value="">Choisir...</option>
							<?php if (count($options_tva) > 0):?>
								<?php foreach ($options_tva as $kle => $value):?>
									<option value="<?php echo $kle?>" <?php if ($data_current_article['tva'] == $kle) echo 'selected'?>><?php echo "TVA $value %" ?></option>
								<?php endforeach;?>						
							<?php endif;?>
						</select><div class="clearfix"></div>
						<span id="error_tva" class="red"></span></td>
				</tr>
				<tr>
					<td align="right">
						<input type="button" value="Calculer Prix HT" id="calcult_ht" class="small">
					</td>
					<td align="right"><label>Prix HT</label>
						<input type="text" id="article_ht" value="<?php echo $data_current_article['prix_ht']?>" disabled="disabled">
						<div class="clearfix"></div><span id="error_ht"></span></td>
				</tr>
				
				<tr><td colspan="2"><span id="info"></span></td></tr>
			</table>
		</div>
	</form>	
</div>
<?php
endif;
endif;
?>

<script type="text/javascript">
( function( $ ) {

	/*
	 *Ajout Nouvel Article
	 */
	$("#edit_new_article_form").submit(function() {
		var errors_empty = false;
		if($("#article_designation").val() == '') {
			$("#error_designation").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_reference").val() == '') {
			$("#error_reference").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_tva").val() == '') {
			$("#error_tva").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_ht").val() == '') {
			$("#error_ht").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if($("#article_ttc").val() == '') {
			$("#error_ttc").html('Ce champ est obligatoire').fadeOut('slow').fadeIn('slow').fadeOut('slow');
			errors_empty = true;
		}
		if(errors_empty) return false;
		
		var data = '';
		data += 'designation=' + encodeURIComponent($("#article_designation").val()) + '&';
		data += 'reference=' + $("#article_reference").val() + '&';
		data += 'tva=' + $("#article_tva").val() + '&';
		data += 'ht=' + $("#article_ht").val() + '&';
		data += 'ttc=' + $("#article_ttc").val() + '&';
		data += 'id_article=<?php echo $_GET['id_article']?>&';
		$.ajax({
			url: '',
			type: 'get',
			data: data + 'ajax=2',
			success: function(msg) {
				switch(msg) {
					case '-3':
						alert('Erreur');
						break;
					case 'ht':
						alert('Prix HT doit etre numérique');
						break;
					case 'ttc':
						alert('Prix TTC doit etre numérique');
						break;
					case 'article_existe':
						alert('Article avec la meme référence existe dèjà');
						break;
				}
				if(msg >= 0) {
					$("#info").html('Article Bien Enregistré').fadeOut('slow').fadeIn('slow').fadeOut('slow');
				}
			},
			error: function() {
			}
		});
		return false;
	});

	/*
	 * Calcul automatique du prixx ttc à partir du prix ht et tva
	 */
	$("#calcult_ht").bind('click', function() {
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') alert('Prix TTC et TVA doivent etre saisis');
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') return false;
		var data = 'ttc='+$("#article_ttc").val()+'&tva='+$("#article_tva").val();
		$.ajax({
			url: '',
			type: 'get',
			data: data + '&ajax=3',
			success: function(msg) {
				switch(msg) {
					case 'ttc':
						alert('Le prix TTC doit etre numerique');
						break;
					case '-3':
						alert('Saisissez TVA');
						break;
				}
				if(msg != '-3' && msg != 'ttc') $("#article_ht").val(msg);
			},
			error: function() {
			}
		});
	});
	$("#article_ttc").bind('blur', function() {
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') alert('Prix TTC et TVA doivent etre saisis');
		if($("#article_tva").val() == '' || $("#article_ttc").val() == '') return false;
		var data = 'ttc='+$("#article_ttc").val()+'&tva='+$("#article_tva").val();
		$.ajax({
			url: '',
			type: 'get',
			data: data + '&ajax=3',
			success: function(msg) {
				switch(msg) {
					case 'ttc':
						alert('Le prix TTC doit etre numerique');
						break;
					case '-3':
						alert('Saisissez TVA');
						break;
				}
				if(msg != '-3' && msg != 'ttc') $("#article_ht").val(msg);
			},
			error: function() {
			}
		});
	});

})( jQuery );
</script>