<div class="comment">
	<h3 class="dd ttl">Commentaires visibles par les DD</h3>
	<div class="clearfix"></div>
	<div id="mceEditor_dd" style="border: 1px solid #E6E6E6;"><?php echo stripslashes($content_comment_dd)?></div>
	
	<h3 class="ttl">Commentaire non accessible aux DD</h3>
	<div class="clearfix"></div>
	<div id="mceEditor_dr" style="border: 1px solid #E6E6E6;"><?php echo stripslashes($content_comment_dr)?></div>
</div>
<script type="text/javascript" src="<?php echo includes_url('js/tinymce/tiny_mce.js') ?>"></script>
<script type="text/javascript">
( function( $ ) {
// 	tinyMCE.init({
//         mode : "exact",
//         elements : "mceEditor_dd",
//         language : "wp-langs-en"
//      });
// 	tinyMCE.init({
//         mode : "exact",
//         elements : "mceEditor_dr",
//         language : "wp-langs-en"
//      });	

	tinyMCE.init({
		height: "500",
        mode : "exact",
        elements : "mceEditor_dd",
        language : "fr",
        entity_encoding : "raw",
        plugins : "paste,table",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
        theme_advanced_buttons3 : "tablecontrols"
	});
	tinyMCE.init({
		height: "500",
        mode : "exact",
        elements : "mceEditor_dr",
        language : "fr",
        entity_encoding : "raw",
        plugins : "paste,table",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,forecolor,backcolor,|,cut,copy,paste,pasteword,|,undo,redo,|,link,unlink,|,justifyleft,justifycenter,justifyright,justifyfull,",
        theme_advanced_buttons2 : "numlist,bullist,|,indent,outdent,|,formatselect,fontselect,fontsizeselect,|,image,hr,|,removeformat",
        theme_advanced_buttons3 : "tablecontrols"
	});
})( jQuery );

</script>