<?php
require_once 'cycle.php';

/*
 * Ajouter une nouvelle cycle à la base de données
 */
$liste_cycles = array();

if (isset($_GET['new_cycle_value']) && $_GET['ajax'] == 13) {
	$new_cycle_value = $_GET['new_cycle_value'];

	if (!get_option('_liste_cycles')) {
		$liste_cycles[] = $new_cycle_value;
		if (add_option('_liste_cycles', serialize($liste_cycles)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_cycles = unserialize(get_option('_liste_cycles'));
		$liste_cycles[] = $new_cycle_value;
		$indice = array_search($new_cycle_value, $liste_cycles);
		if (update_option('_liste_cycles', serialize($liste_cycles)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une cycle de la base de données
 */
if (isset($_GET['liste_cycle_delete']) && $_GET['ajax'] == 14) {
	$liste_cycle_delete = explode(',', $_GET['liste_cycle_delete']);
	$option_array_cycle = get_array_cycle();
	if (count($liste_cycle_delete) > 0) {
		foreach ($liste_cycle_delete as $item) {
			foreach ($option_array_cycle as $kle => $value) {
				if ($item == $kle) unset($option_array_cycle[(int)$kle]);
			}
		}
		if (update_option('_liste_cycles', serialize($option_array_cycle)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}