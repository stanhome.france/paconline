<?php
require_once 'archivage.php';

/*
 * Modifier le délai d'archivage
*/
if(isset($_GET['archivage_delay']) && $_GET['ajax'] == 18) {
	if (is_numeric($_GET['archivage_delay'])) {
		$new_archivage_delay = intval($_GET['archivage_delay']);

			if($new_archivage_delay > 0){
				if (update_option('_archivage_delay', $new_archivage_delay)){
					echo 1;
				}
				else{
					echo -1;
				}
			}
			else{
				echo -2;
			}
	} else {
		echo -3;
	}
}


