<?php
require_once dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . '/wp-admin/custom-header.php';

/*
 * Upload Image Header depuis Front Office
 */
if (isset($_POST['ajax']) && $_POST['ajax'] == 17) {
	$size = getimagesize($_FILES['image_header']['tmp_name']);
	if ($_FILES['image_header']['error'] == 0) {
		if (!is_dir(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/' . date('m') . '/' . date('Y')))
			@mkdir(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/uploads/' . date('Y') . '/' . date('m'), 0777, true);
		
		$theme_mods_twentytwelve = get_option('theme_mods_twentytwelve');
		$header_image_data = $theme_mods_twentytwelve['header_image_data'];
		$attachment_id = $header_image_data->attachment_id;
		
		$size = getimagesize($_FILES['image_header']['tmp_name']);
		$size = $size[3];
		$size = explode(" ", $size);
		$width = $size[0];
		$height = $size[1];
		$width = str_replace('"', '', str_replace("width=", "", $width));
		$height = str_replace('"', '', str_replace("height=", "", $height));
		$width = (int)$width;
		$height = (int)$height;
		
		$upload_dir = wp_upload_dir();
		$upload_url = $upload_dir['baseurl'];
		
		if (move_uploaded_file($_FILES['image_header']['tmp_name'], 
			dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/uploads/' . date('Y') . '/' . date('m') . '/' . $_FILES['image_header']['name']))
		{
			$upload_url .= '/' . date('Y') . '/' . date('m'). '/' . $_FILES['image_header']['name'];
			if (!get_option('_image_page_accueil')) {
				if (!add_option('_image_page_accueil', $upload_url)) 
					$error_upload_header_image = true;
				else 
					header("Location: " . home_url());
			} else {
				if (!update_option('_image_page_accueil', $upload_url))
					$error_upload_header_image = true;
				else
					header("Location: " . home_url());
			}
		} else {
			$error_upload_header_image = true;
		}
		
	} else {
		$error_upload_header_image = true;
	}
	exit();
}

/*
 * Texte header
 */

if ($_GET['ajax'] == 17) {
	$texte_header = isset($_GET['texte_header']) ? $_GET['texte_header'] : '';
	if (update_option('blogdescription', $texte_header))
		echo 1;
	else
		echo 0;
}
