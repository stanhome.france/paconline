<?php
/**
 * Récupérer la liste des cycles enregistrées dans la base de données
 */
function get_options_cycle() {
	$option_value_cycle = get_option('_liste_cycles');
	return $option_value_cycle;
}

/**
 * Récupérer la liste des cycles et la mettre dans un array
 */
function get_array_cycle() {
	$option_value_cycle = get_option('_liste_cycles');
	$option_array_cycle = (count($option_value_cycle) > 0) ? unserialize($option_value_cycle) : array();
	return $option_array_cycle;
}