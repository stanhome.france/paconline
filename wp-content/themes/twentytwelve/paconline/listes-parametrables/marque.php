<?php
/**
 * Récupérer la liste des marques enregistrées dans la base de données
 */
function get_options_marque() {
	$option_value_marque = get_option('_liste_marques');
	return $option_value_marque;
}

/**
 * Récupérer la liste des marques et la mettre dans un array
 */
function get_array_marque() {
	$option_value_marque = get_option('_liste_marques');
	$option_array_marque = (count($option_value_marque) > 0) ? unserialize($option_value_marque) : array();
	return $option_array_marque;
}