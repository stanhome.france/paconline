<?php
require_once 'categorie.php';

/*
 * Ajouter une nouvelle categorie à la base de données
 */
$liste_categories = array();

if (isset($_GET['new_categorie_value']) && $_GET['ajax'] == 11) {
	$new_categorie_value = $_GET['new_categorie_value'];

	if (!get_option('_liste_categories')) {
		$liste_categories[] = $new_categorie_value;
		if (add_option('_liste_categories', serialize($liste_categories)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_categories = unserialize(get_option('_liste_categories'));
		$liste_categories[] = $new_categorie_value;
		$indice = array_search($new_categorie_value, $liste_categories);
		if (update_option('_liste_categories', serialize($liste_categories)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une categorie de la base de données
 */
if (isset($_GET['liste_categorie_delete']) && $_GET['ajax'] == 12) {
	$liste_categorie_delete = explode(',', $_GET['liste_categorie_delete']);
	$option_array_categorie = get_array_categorie();
	if (count($liste_categorie_delete) > 0) {
		foreach ($liste_categorie_delete as $item) {
			foreach ($option_array_categorie as $kle => $value) {
				if ($item == $kle) unset($option_array_categorie[(int)$kle]);
			}
		}
		if (update_option('_liste_categories', serialize($option_array_categorie)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}