<?php
require_once 'marque.php';

/*
 * Ajouter une nouvelle marque à la base de données
 */
$liste_marques = array();

if (isset($_GET['new_marque_value']) && $_GET['ajax'] == 7) {
	$new_marque_value = $_GET['new_marque_value'];

	if (!get_option('_liste_marques')) {
		$liste_marques[] = $new_marque_value;
		if (add_option('_liste_marques', serialize($liste_marques)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_marques = unserialize(get_option('_liste_marques'));
		$liste_marques[] = $new_marque_value;
		$indice = array_search($new_marque_value, $liste_marques);
		if (update_option('_liste_marques', serialize($liste_marques)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une marque de la base de données
 */
if (isset($_GET['liste_marque_delete']) && $_GET['ajax'] == 8) {
	$liste_marque_delete = explode(',', $_GET['liste_marque_delete']);
	$option_array_marque = get_array_marque();
	if (count($liste_marque_delete) > 0) {
		foreach ($liste_marque_delete as $item) {
			foreach ($option_array_marque as $kle => $value) {
				if ($item == $kle) unset($option_array_marque[(int)$kle]);
			}
		}
		if (update_option('_liste_marques', serialize($option_array_marque)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}