<?php
/**
 * Récupérer la liste des TVA enregistrées dans la base de données
 */
function get_options_tva() {
	$option_value_tva = get_option('_liste_tva');
	return $option_value_tva;
}

/**
 * Récupérer la liste des TVA et la mettre dans un array
 */
function get_array_tva() {
	$option_value_tva = get_option('_liste_tva');
	$option_array_tva = (count($option_value_tva) > 0) ? unserialize($option_value_tva) : array();
	return $option_array_tva;
}


