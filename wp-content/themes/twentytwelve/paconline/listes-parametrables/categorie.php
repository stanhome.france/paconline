<?php
/**
 * Récupérer la liste des categories enregistrées dans la base de données
 */
function get_options_categorie() {
	$option_value_categorie = get_option('_liste_categories');
	return $option_value_categorie;
}

/**
 * Récupérer la liste des categories et la mettre dans un array
 */
function get_array_categorie() {
	$option_value_categorie = get_option('_liste_categories');
	$option_array_categorie = (count($option_value_categorie) > 0) ? unserialize($option_value_categorie) : array();
	return $option_array_categorie;
}