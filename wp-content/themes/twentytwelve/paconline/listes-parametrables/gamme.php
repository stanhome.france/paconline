<?php
/**
 * Récupérer la liste des gammes enregistrées dans la base de données
 */
function get_options_gamme() {
	$option_value_gamme = get_option('_liste_gammes');
	return $option_value_gamme;
}

/**
 * Récupérer la liste des gammes et la mettre dans un array
 */
function get_array_gamme() {
	$option_value_gamme = get_option('_liste_gammes');
	$option_array_gamme = (count($option_value_gamme) > 0) ? unserialize($option_value_gamme) : array();
	return $option_array_gamme;
}