<?php
require_once 'tva.php';

global $wpdb;
$options_table = $wpdb->prefix . 'options';

/*
 * Ajouter un nouveau TVA au système
 */
if(isset($_GET['new_tva_value']) && $_GET['ajax'] == 1) {
	if (is_numeric($_GET['new_tva_value'])) {
		$new_tva_value = $_GET['new_tva_value'];
		
		if (!get_option('_liste_tva')) {
			$liste_tva[] = $new_tva_value;
			if (add_option('_liste_tva', serialize($liste_tva)))
				echo 0;
			else
				echo -1;
		} else {
			$liste_tva = unserialize(get_option('_liste_tva'));
			$liste_tva[] = $new_tva_value;
			$indice = array_search($new_tva_value, $liste_tva);
			if (update_option('_liste_tva', serialize($liste_tva)))
				echo "$indice";
			else
				echo -1;
		}
	} else {
		echo -2;
	}
}

/*
 * Supprimer des TVA
 */
if (isset($_GET['liste_tva_delete']) && $_GET['ajax'] == 2) {
	$liste_tva_delete = explode(',', $_GET['liste_tva_delete']);
	$option_array_tva = get_array_tva();
	if (count($liste_tva_delete) > 0) {
		foreach ($liste_tva_delete as $item) {
			foreach ($option_array_tva as $kle => $value) {
				if ($item == $kle) unset($option_array_tva[(int)$kle]);
			}
		}
		if (update_option('_liste_tva', serialize($option_array_tva)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}







