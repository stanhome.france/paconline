<?php
/**
 * Récupérer la liste des cibles enregistrées dans la base de données
 */
function get_options_cible() {
	$option_value_cible = get_option('_liste_cibles');
	return $option_value_cible;
}

/**
 * Récupérer la liste des cibles et la mettre dans un array
 */
function get_array_cible() {
	$option_value_cible = get_option('_liste_cibles');
	$option_array_cible = (count($option_value_cible) > 0) ? unserialize($option_value_cible) : array();
	return $option_array_cible;
}