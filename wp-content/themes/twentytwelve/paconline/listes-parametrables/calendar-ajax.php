<?php
/*
 * Modifier le nombre de pixels par jour et le rayon d'affichage de la vue calendaire
*/
if(isset($_GET['nb_pixels_per_day']) && isset($_GET['rayon']) && $_GET['ajax'] == 'calendar') {
	if (is_numeric($_GET['nb_pixels_per_day']) && is_numeric($_GET['rayon'])) {
		$new_nb_pixels_per_day = intval($_GET['nb_pixels_per_day']);
		$new_rayon = intval($_GET['rayon']);

		if($new_nb_pixels_per_day > 0 && $new_rayon > 0) {
			if (get_option('nb_pixels_per_day')) {
				update_option('nb_pixels_per_day', $new_nb_pixels_per_day);
			} else {
				add_option('nb_pixels_per_day', $new_nb_pixels_per_day);
			}
			if (get_option('rayon')) {
				update_option('rayon', $new_rayon);
			} else {
				add_option('rayon', $new_rayon);
			}
			echo 1;
		} else {
			echo -2;
		}
	} else {
		echo -3;
	}
}

?>