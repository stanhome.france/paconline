<?php
function get_secret_key() {
	return get_option('_secret_key');
}

function get_extranet_url() {
	return get_option('_extranet_url');
}
?>