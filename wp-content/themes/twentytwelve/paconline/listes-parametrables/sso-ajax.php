<?php
require_once 'sso.php';

/*
 * Modifier les paramètres de l'authentification SSO
*/
if(isset($_GET['secret_key']) && isset($_GET['extranet_url']) && $_GET['ajax'] == 'sso') {
	$new_secret_key = $_GET['secret_key'];
	$new_extranet_url = $_GET['extranet_url'];

	if (!get_option('_secret_key')) {
		if (add_option('_secret_key', $new_secret_key)) {
			echo 1;
		} else {
			echo -1;
		}
	} else {
		if (update_option('_secret_key', $new_secret_key)) {
			echo 1;
		} else {
			echo -1;
		}
	}
	
	if (!get_option('_extranet_url')) {
		if (add_option('_extranet_url', $new_extranet_url)) {
			echo 1;
		} else {
			echo -1;
		}
	} else {
		if (update_option('_extranet_url', $new_extranet_url)) {
			echo 1;
		} else {
			echo -1;
		}
	}
}
