<?php
/**
 * Récupérer la liste des periodes enregistrées dans la base de données
 */
function get_options_periode() {
	$option_value_periode = get_option('_liste_periodes');
	return $option_value_periode;
}

/**
 * Récupérer la liste des periodes et la mettre dans un array
 */
function get_array_periode() {
	$option_value_periode = get_option('_liste_periodes');
	$option_array_periode = (count($option_value_periode) > 0) ? unserialize($option_value_periode) : array();
	return $option_array_periode;
}