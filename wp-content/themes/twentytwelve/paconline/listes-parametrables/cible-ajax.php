<?php
require_once 'cible.php';

/*
 * Ajouter une nouvelle cible à la base de données
 */
$liste_cibles = array();

if (isset($_GET['new_cible_value']) && $_GET['ajax'] == 3) {
	$new_cible_value = $_GET['new_cible_value'];

	if (!get_option('_liste_cibles')) {
		$liste_cibles[] = $new_cible_value;
		if (add_option('_liste_cibles', serialize($liste_cibles)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_cibles = unserialize(get_option('_liste_cibles'));
		$liste_cibles[] = $new_cible_value;
		$indice = array_search($new_cible_value, $liste_cibles);
		if (update_option('_liste_cibles', serialize($liste_cibles)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une cible de la base de données
 */
if (isset($_GET['liste_cible_delete']) && $_GET['ajax'] == 4) {
	$liste_cible_delete = explode(',', $_GET['liste_cible_delete']);
	$option_array_cible = get_array_cible();
	if (count($liste_cible_delete) > 0) {
		foreach ($liste_cible_delete as $item) {
			foreach ($option_array_cible as $kle => $value) {
				if ($item == $kle) unset($option_array_cible[(int)$kle]);
			}
		}
		if (update_option('_liste_cibles', serialize($option_array_cible)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}