<?php
/**
 * Récupérer la liste des evenements enregistrées dans la base de données
 */
function get_options_evenement() {
	$option_value_evenement = get_option('_liste_evenements');
	return $option_value_evenement;
}

/**
 * Récupérer la liste des evenements et la mettre dans un array
 */
function get_array_evenement() {
	$option_value_evenement = get_option('_liste_evenements');
	$option_array_evenement = (count($option_value_evenement) > 0) ? unserialize($option_value_evenement) : array();
	return $option_array_evenement;
}