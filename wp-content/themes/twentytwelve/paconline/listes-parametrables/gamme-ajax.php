<?php
require_once 'gamme.php';

/*
 * Ajouter une nouvelle gamme à la base de données
 */
$liste_gammes = array();

if (isset($_GET['new_gamme_value']) && $_GET['ajax'] == 5) {
	$new_gamme_value = $_GET['new_gamme_value'];

	if (!get_option('_liste_gammes')) {
		$liste_gammes[] = $new_gamme_value;
		if (add_option('_liste_gammes', serialize($liste_gammes)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_gammes = unserialize(get_option('_liste_gammes'));
		$liste_gammes[] = $new_gamme_value;
		$indice = array_search($new_gamme_value, $liste_gammes);
		if (update_option('_liste_gammes', serialize($liste_gammes)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une gamme de la base de données
 */
if (isset($_GET['liste_gamme_delete']) && $_GET['ajax'] == 6) {
	$liste_gamme_delete = explode(',', $_GET['liste_gamme_delete']);
	$option_array_gamme = get_array_gamme();
	if (count($liste_gamme_delete) > 0) {
		foreach ($liste_gamme_delete as $item) {
			foreach ($option_array_gamme as $kle => $value) {
				if ($item == $kle) unset($option_array_gamme[(int)$kle]);
			}
		}
		if (update_option('_liste_gammes', serialize($option_array_gamme)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}