<?php
require_once 'periode.php';

/*
 * Ajouter une nouvelle periode à la base de données
 */
$liste_periodes = array();

if (isset($_GET['new_periode_value']) && $_GET['ajax'] == 15) {
	$new_periode_value = $_GET['new_periode_value'];

	if (!get_option('_liste_periodes')) {
		$liste_periodes[] = $new_periode_value;
		if (add_option('_liste_periodes', serialize($liste_periodes)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_periodes = unserialize(get_option('_liste_periodes'));
		$liste_periodes[] = $new_periode_value;
		$indice = array_search($new_periode_value, $liste_periodes);
		if (update_option('_liste_periodes', serialize($liste_periodes)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une periode de la base de données
 */
if (isset($_GET['liste_periode_delete']) && $_GET['ajax'] == 16) {
	$liste_periode_delete = explode(',', $_GET['liste_periode_delete']);
	$option_array_periode = get_array_periode();
	if (count($liste_periode_delete) > 0) {
		foreach ($liste_periode_delete as $item) {
			foreach ($option_array_periode as $kle => $value) {
				if ($item == $kle) unset($option_array_periode[(int)$kle]);
			}
		}
		if (update_option('_liste_periodes', serialize($option_array_periode)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}