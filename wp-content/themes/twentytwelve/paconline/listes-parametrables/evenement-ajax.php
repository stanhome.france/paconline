<?php
require_once 'evenement.php';

/*
 * Ajouter une nouvelle evenement à la base de données
 */
$liste_evenements = array();

if (isset($_GET['new_evenement_value']) && $_GET['ajax'] == 9) {
	$new_evenement_value = $_GET['new_evenement_value'];

	if (!get_option('_liste_evenements')) {
		$liste_evenements[] = $new_evenement_value;
		if (add_option('_liste_evenements', serialize($liste_evenements)))
			echo 0;
		else
			echo -1;
	} else {
		$liste_evenements = unserialize(get_option('_liste_evenements'));
		$liste_evenements[] = $new_evenement_value;
		$indice = array_search($new_evenement_value, $liste_evenements);
		if (update_option('_liste_evenements', serialize($liste_evenements)))
			echo "$indice";
		else
			echo -1;
	}
}

/*
 * Supprimer une evenement de la base de données
 */
if (isset($_GET['liste_evenement_delete']) && $_GET['ajax'] == 10) {
	$liste_evenement_delete = explode(',', $_GET['liste_evenement_delete']);
	$option_array_evenement = get_array_evenement();
	if (count($liste_evenement_delete) > 0) {
		foreach ($liste_evenement_delete as $item) {
			foreach ($option_array_evenement as $kle => $value) {
				if ($item == $kle) unset($option_array_evenement[(int)$kle]);
			}
		}
		if (update_option('_liste_evenements', serialize($option_array_evenement)))
			echo 1;
		else
			echo 0;
	} else {
		echo -1;
	}
}