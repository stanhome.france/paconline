<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
if (isset($_COOKIE['wordpress_logged_in_'.COOKIEHASH])) {
	$cookie_exp = explode('|', $_COOKIE['wordpress_logged_in_'.COOKIEHASH]);
	$username = $cookie_exp[0];
	$user = get_user_by('login', $username);
	wp_set_current_user( $user->ID, $username );
}
if (!isset($cron)) {
	if (!isset($_COOKIE['wordpress_logged_in_'.COOKIEHASH])) {
		wp_redirect(site_url("/wp-login.php?redirect_to=/"), 302);
	}
}


get_header();
?>


<script type="text/javascript">
    $(document).ready(function () {
        $("#online_news").jqxPanel({ width: "98%", height: 25 });
    });
</script>

<div class="clearfix"></div>
<div id="content" role="main">
	<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width=300 valign="top">
				<?php get_sidebar();?>
			</td>
			<td valign="top">
				<div id="online_news">
					<div style="font-weight:bold;">Information : <?php echo get_option('blogdescription'); ?></div>
				</div>
				<img alt="" src="<?php echo get_option('_image_page_accueil')?>" width="98%" />
			</td>
		</tr>
	</table>
	<div class="clearfix"></div>
</div><!-- #content -->

	<!-- <div style="clear:both;"> -->
	<?php //get_footer(); ?>
	<!-- </div> -->
</div>
</body>
</html>




