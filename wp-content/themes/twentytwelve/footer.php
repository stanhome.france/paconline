<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<div id="footer_pac">
<div id="copyright">Tous droits résérvés <span style="color:#2F2B98;font-weight:bold;">Stanhome France</span> <img style="vertical-align: middle;" alt="" src="<?php bloginfo('template_directory'); echo '/images/copyright.gif' ?>"> 2013 </div>
</div>