<?php
/*
 Plugin Name: Search tools
Plugin URI: http://www.netik.fr
Description: Outils pour effectuer les recherches sur Stanhome
Author: Frédéric Vissault
Version: 0.0.2
Author URI: http://www.netik.fr
*/


if (!defined("PATH_SEPARATOR")) {
	if (strpos($_ENV[ "OS" ], "Win") !== false)
		define("PATH_SEPARATOR", ";");
	else
		define("PATH_SEPARATOR", ":");
}

ini_set('include_path',ini_get('include_path') . PATH_SEPARATOR . dirname(__FILE__) . '/lucene/code');


// inclusion du moteur de recherche lucene
require_once('Zend/Search/Lucene.php');

require_once('TextExtractors/ZendSearchLuceneTextExtractor.php');
require_once('TextExtractors/CatdocDocExtractor.php');
require_once('TextExtractors/CatdocPptExtractor.php');
require_once('TextExtractors/CatdocXlsExtractor.php');
require_once('TextExtractors/PdfDocumentExtractor.php');
require_once('TextExtractors/ZendSearchLuceneDocxExtractor.php');
require_once('TextExtractors/ZendSearchLuceneHtmlExtractor.php');
require_once('TextExtractors/ZendSearchLucenePptxExtractor.php');
require_once('TextExtractors/ZendSearchLuceneXlsxExtractor.php');
require_once('TextExtractors/TxtDocumentExtractor.php');


class sh_search {

	private $index_path = '';
	private $index = null;

	function __construct($index_path) {
		if ($index_path == null) {
			throw new Exception('classe sh_search : construct : index_path ne peut pas �tre null');
		}
		$this->index_path = $index_path;
	}
	 
	public function get_index_path() {
		return $this->index_path;
	}

	public function set_index_path($index_path) {
		if ($index_path == null) {
			throw new Exception('classe sh_search : set_index_path : index_path ne peut pas �tre null');
		}
		$this->index_path = $index_path;
	}
	 
	/* **************************************************************************
	 create_index($index_path)

	Permet de cr�er un index
	************************************************************************** */
	public function create_index($index_path = '') {
		//echo "titi";
		if ($index_path) {
			$this->index_path = $index_path;
			$this->index = Zend_Search_Lucene::create($this->index_path);
		} else {
			if($this->index_path != '') {
				$this->index = Zend_Search_Lucene::create($this->index_path);
			}  else {
				throw new Exception('classe sh_search : create_index : index_path n\'est pas d�fini');
			}
		}
	}
	 
	/* **************************************************************************
	 open_index($index_path = '')

	Permet d'ouvrir un index dans le cas ou celui-ci est d�fini dans la classe
	Si ce n'est pas le cas, on cr�� l'index
	L'index ouvert ou cr�� est retourn�
	************************************************************************** */
	public function open_index($index_path = '') {
		if ($index_path == '') {
			// on ne renseigne pas le chemin de l'index
			if ($this->index_path == '') {
				throw new Exception('classe sh_search : open_index : impossible d\'ouvrir un index');
				return null;
			} else {
				// le chemin de l'index de la recherche est renseign�
				// --> on ouvre l'index
				$this->index = Zend_Search_Lucene::open($this->index_path);
			}
		} else {
			// on renseigne le chemin de l'index
			if ($this->index_path == '') {
				// le chemin de l'index de la recherche n'est pas renseign�
				// --> on cr�� l'index
				$this->index_path = $index_path;
				$this->index = Zend_Search_Lucene::create($this->index_path);
			} else {
				// le chemin de l'index de la recherche est renseign�
				// --> on ouvre l'index
				$this->index = Zend_Search_Lucene::open($this->index_path);
			}
		}
		// on retourne l'index ouvert
		return $this->index;
	}
	 
	/* **************************************************************************
	 add_to_index($docTitle, $docUrl)

	Ajoute un document � un index
	Les types de documents qui peuvent �tre ajout�s � l'index sont :
	doc, docx, xls, xlsx, pdf, ppt, pptx
	************************************************************************** */
	public function add_to_index($docTitle, $docUrl, $docType, $docId='', $dateDebut='', $dateFin='', $docContent='') {
		// d�terminer le type de document qu'on veut indexer
		// on va trouver cette information dans $docUrl et
		// chaque classe peut nous renseigner sur l'extension du document qu'elle
		// supporte.
		
		//Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_TextNum_CaseInsensitive());
		Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive ());
  
		$ext = substr($docUrl, -4);
		//$utf8 = true;
		
		//Si on ne fournit pas de contenu direct, extraction automatique depuis le doc
		/*if($docContent == ''){
			switch($ext) {
				case ".doc" :
					$docContent = CatdocDocExtractor::extract($docUrl);
					break;
				case ".xls" :
					$docContent = CatdocXlsExtractor::extract($docUrl);
					break;
				case ".pdf" :
					$docContent = PdfDocumentExtractor::extract($docUrl);
					$utf8 = false;
					break;
				case ".ppt" :
					$docContent = CatdocPptExtractor::extract($docUrl);
					break;
				case "docx" :
					$docContent = ZendSearchLuceneDocxExtractor::extract($docUrl);
					break;
				case "xlsx" :
					$docContent = ZendSearchLuceneXlsxExtractor::extract($docUrl);
					break;
				case "pptx" :
					$docContent = ZendSearchLucenePptxExtractor::extract($docUrl);
					break;
				case ".txt" :
					$docContent = TxtDocumentExtractor::extract($docUrl);
					break;
				default :
					$docContent = "";
					break;
			}
		}*/

		// r�cup�ration de l'index
		$index = $this->index;

		// Cr�ation du document � indexer
		$doc = new Zend_Search_Lucene_Document();

		// Ajout des champs � indexer (url, title, content)
		
		if($docId != ''){
			$doc->addField(Zend_Search_Lucene_Field::keyword('dataId', $docId));
		}
		if($dateDebut != ''){
			$doc->addField(Zend_Search_Lucene_Field::keyword('dateDebut', $dateDebut));
		}
		if($dateFin != ''){
			$doc->addField(Zend_Search_Lucene_Field::keyword('dateFin', $dateFin));
		}

		$doc->addField(Zend_Search_Lucene_Field::keyword('type' . $docType, $docType));
		$doc->addField(Zend_Search_Lucene_Field::keyword('url' . $docType, $docUrl));
		$doc->addField(Zend_Search_Lucene_Field::Text('title' . $docType, $docTitle, 'utf-8'));
		$doc->addField(Zend_Search_Lucene_Field::unStored('contenu' . $docType, $docContent, 'utf-8'));

		// Ajout du document � l'index
		$index->addDocument($doc);

		// Confirmation de l'ajout � l'index
		$index->commit();
	}
	 
	/* **************************************************************************
	 search_in_index($query)

	retourne null quand il n'est pas possible de faire la recherche,
	sinon les r�sultats de la recherche sur le contenu de $query
	$query doit imp�rativement �tre une chaine de caract�res
	************************************************************************** */
	public function search_in_index($query) {
		if ($query == null or $query == "" or !is_string($query)) {
			return null;
		}
		if ($this->index) {
			//$index = Zend_Search_Lucene::open($this->index);
			$index = $this->index;
			$hits = $index->find($query);
			return $hits;
		} else {
			return null;
		}
	}
	 
	/* **************************************************************************
	 hits_count($hits)

	retourne le nombre d'entr�e d'un r�sultat de recherche
	************************************************************************** */
	public function hits_count($hits) {
		if($hits) {
			return count($hits);
		} else {
			return 0;
		}
	}

	/* **************************************************************************
	 hit_score($hit)

	retourne la probabilit� d'une $query dans le r�sultat d'une recherche
	************************************************************************** */
	public function hit_score($hit) {
		if ($hit) {
			return $hit->score;
		} else {
			return 0;
		}
	}
	
	
	/**
	 * Supprime un document spécifique dans l'index
	 * @param int $dataId ID de l'entité en base
	 * @param string $type Type de l'entité (ex: attachment)
	 */
	public function delete_from_index($dataId, $type){
		$query = new Zend_Search_Lucene_Search_Query_Boolean();
		$idTerm = new Zend_Search_Lucene_Index_Term($dataId, "dataId");
		$typeTerm = new Zend_Search_Lucene_Index_Term($type, "type");
		
		$idQuery = new Zend_Search_Lucene_Search_Query_Term($idTerm);
		$typeQuery = new Zend_Search_Lucene_Search_Query_Term($typeTerm);
		
		$query->addSubquery($idQuery, true);
		$query->addSubquery($typeQuery, true);
		
		$hits = $this->index->find($query);
		if(count($hits) > 0){
			$this->index->delete($hits[0]->id);
			$this->index->commit();
			$this->index->optimize();
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Supprime tous les éléments correspondants à un type spécifique (ex: Cycles)
	 * @param Url à rechercher $urlTerm
	 */
	public function purge_by_type($type){
		$indexTerm = new Zend_Search_Lucene_Index_Term($type, "type");
		$typeQuery = new Zend_Search_Lucene_Search_Query_Term($indexTerm);
		$hits = $this->index->find($typeQuery);
		
		foreach ($hits as $hit){
			$this->index->delete($hit->id);
		}
		
		$this->index->commit();
	}
}

/* mise en place des diff�rents �v�nements pour effectuer des recherches
 avec l'aide de do_action */
add_option("sh_search_create_index", array("sh_search", "create_index"));
add_option("sh_search_add_to_index", array("sh_search", "add_to_index"));
add_option("sh_search_search_in_index", array("sh_search", "search_in_index"));
add_option("sh_search_hits_count", array("sh_search", "hits_count"));
add_option("sh_search_hit_score", array("sh_search", "hit_score"));
?>
