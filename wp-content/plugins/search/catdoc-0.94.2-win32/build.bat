@REM Assumes MinGW gcc installed
@cd src
gcc -DCATDOC_VERSION=\"0.94.2\" -O2 charsets.c substmap.c fileutil.c confutil.c numutils.c ole.c catdoc.c writer.c analyze.c rtfread.c reader.c ../compat/glob.c -I../compat -o ../catdoc
gcc -DCATDOC_VERSION=\"0.94.2\" -O2 charsets.c substmap.c fileutil.c confutil.c numutils.c ole.c xls2csv.c sheet.c xlsparse.c ../compat/glob.c -I../compat -o ../xls2csv
gcc -DCATDOC_VERSION=\"0.94.2\" -O2 charsets.c substmap.c fileutil.c confutil.c numutils.c ole.c catppt.c pptparse.c ../compat/glob.c -I../compat -o ../catppt
@cd ..
