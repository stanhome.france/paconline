<?php
require( dirname(__FILE__) . '/wp-load.php' );
require( dirname(__FILE__) . '/wp-includes/class-phpass.php' );

define("ADD_USER_VERBOSE", true);
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	<body>
<?php
if (!isset($filename)) {
	$filename = "dd-dr-users.csv";
}
$row = 1;
if (($handle = fopen($filename, "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
		// on passe la première ligne
		if ($row > 1) {
			if (ADD_USER_VERBOSE) {
				$num = count($data);
				for ($c = 0; $c < $num; $c++) {
					echo $data[$c] . " ";
				}
			}
			$user = get_user_by('login', $data[3]);
			if (!$user) {
				$log_exp = explode("@", $data[3]);
				$wp_hasher = new PasswordHash(8, TRUE);
				$password = $wp_hasher->HashPassword('pass'.$log_exp[1]);
				if ($data[2] != "?") {
					$user_email = $data[2];
				} else {
					$user_email = $data[3];
				}
				$added_user_id = wp_create_user(trim($data[3]), $password, $user_email);
				
				wp_update_user(array('ID' => $added_user_id, 
									 'user_nicename' => trim($data[3]), 
									 'display_name' => trim($data[1])." ".trim($data[0]),
									 'first_name' => trim($data[1]),
									 'last_name' => trim($data[0])));
				
				switch(trim($data[4])) {
					case "DD":
						$group = Groups_Group::read_by_name("StanHome_DD");
						Groups_User_Group::create(array('group_id' => $group->group_id, 'user_id' => $added_user_id));
						if (ADD_USER_VERBOSE) echo " => Profil ajouté au groupe des DD<br>";
						break;
					case "DR":
						$group = Groups_Group::read_by_name("StanHome_DR");
						Groups_User_Group::create(array('group_id' => $group->group_id, 'user_id' => $added_user_id));
						if (ADD_USER_VERBOSE) echo " => Profil ajouté au groupe des DR<br>";
						break;
					default:
						$group = Groups_Group::read_by_name("Registered");
						Groups_User_Group::create(array('group_id' => $group->group_id, 'user_id' => $added_user_id));
						if (ADD_USER_VERBOSE) echo " => Profil ajouté en registered uniquement<br>";
				}
				// pour l'instant si ce n'est pas un DD ou un DR on ne fait rien.
			} else {
				$added_user_id = $user->ID;
				wp_update_user(array('ID' => $added_user_id,
									 'user_nicename' => trim($data[3]),
									 'display_name' => trim($data[1])." ".trim($data[0]),
									 'first_name' => trim($data[1]),
									 'last_name' => trim($data[0])));
				if (ADD_USER_VERBOSE) echo " => Profil modifié<br>";
			}
			
		}
		$row++;
	}
	fclose($handle);
}
?>
	</body>
</html>