<?php
require( dirname(__FILE__) . '/wp-load.php' );
require_once get_template_directory().'/paconline/include-functions.php';
$page = $_GET['page']; // get the requested page 
$limit = $_GET['rows']; // get how many rows we want to have into the grid 
$sidx = $_GET['sidx']; // get index row - i.e. user click to sort 
$sord = $_GET['sord']; // get the direction
if(!$sidx) $sidx =1;

$where = '';
if (isset($_GET['_search']) && $_GET['_search']) {
	$searchField = $_GET['searchField'];
	$searchOper = $_GET['searchOper'];
	$searchString = $_GET['searchString'];
	if ($searchField == 'reference') {
		switch ($searchOper) {
			case 'cn':
				$where = " reference LIKE '%$searchString%' ";
				break;
			case 'bw':
				$where = "reference LIKE '$searchString%'";
				break;
			case 'eq':
				$where = "reference = '$searchString'";
				break;
		}
	} elseif ($searchField =='designation') {
		switch ($searchOper) {
			case 'cn':
				$where = " designation LIKE '%$searchString%' ";
				break;
			case 'bw':
				$where = "designation LIKE '$searchString%'";
				break;
			case 'eq':
				$where = "designation = '$searchString'";
				break;
		}
	}
}

$articles_temp = Pods('articles', array('limit' => "-1", 'where' => $where));
$totalRows = $articles_temp->total();
if( $totalRows >0 ) { $total_pages = ceil($totalRows/$limit); } 
else { $total_pages = 0; }

if ($page > $total_pages) $page=$total_pages;

$start = $limit*$page - $limit; // do not put $limit*($page - 1)


/*$articles_aff = Pods('articles', array('orderby' => "t.reference ASC", 'where' => $where, 'limit' => "-1"));
$total_aff = $articles_aff->total();*/

//echo "$limit : $page";
$articles = Pods('articles', array('orderby' => "t.reference ASC", 'limit' => $limit, 'page' => $page, 'where' => $where));
$total = $articles->total();


$i = 0;


if ($total > 0) {
	while ($articles->fetch()) {
		$row[$i]['id']=$articles->display('reference');
		$row[$i]['cell']=array($articles->display('reference'),$articles->display('designation'));
		$i++;
	}
}
$response = array (page=>$page ,total=>$total_pages , records=>$totalRows , rows => $row); 
echo  json_encode($response);