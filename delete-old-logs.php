<?php
require( dirname(__FILE__) . '/wp-load.php' );

function delete_old_logs($log_path) {
	$format = 'Y-m-d G:i:s'; 
	$today = date($format); 
	$last_week = date($format, strtotime ('-7 day' . $date));
	$last_week = explode(' ', $last_week); 
	$last_week = $last_week[0];
	$last_week = explode('-', $last_week);
	$last_week = $last_week[2] . '/' . $last_week[1] . '/' . $last_week[0];
	
	$content = utf8_encode(file_get_contents($log_path));
	$log = strstr($content, $last_week);
	file_put_contents($log_path, '');
	file_put_contents($log_path, utf8_decode($log));
}

$log_impression = get_template_directory().'/log/impression/system.log';
delete_old_logs($log_impression);

$log_diffusion = get_template_directory().'/log/diffusion.log';
delete_old_logs($log_diffusion);

$log_indexation = get_template_directory().'/log/indexation.log';
delete_old_logs($log_indexation);

$log_archivage = get_template_directory().'/log/archivage.log';
delete_old_logs($log_archivage);

$log_remarques_avenants = get_template_directory().'/log/remarques-avenants.log';
delete_old_logs($log_remarques_avenants);

$log_stanhome = get_template_directory().'/log/stanhome-log.log';
delete_old_logs($log_stanhome);

