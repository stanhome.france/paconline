<?php
/*
 * Permet d'effectuer la diffusion des entités
 */
require( dirname(__FILE__) . '/wp-load.php' );

// chargement des outils
require_once get_template_directory().'/paconline/include-functions.php';

// chargement du helper de log
require_once get_template_directory().'/paconline/logs/log-helper.php';
define("DIFFUSION_LOG_PATH", get_template_directory().'/log/diffusion.log');


define("DIFFUSION_VERBOSE_LOGS", true);

// function sh_do_diffusion() {
//     // début de la tache de diffusion
//     sh_write_to_log("La tâche planifiée \"Agent de diffusion \" commence", DIFFUSION_LOG_PATH);
    
//     // ************************************************************************
//     // parcourir tous les cycles et périodes mis en diffusion et non archivés et dont le niveau de diffusion n'est pas au maximum
//     $entites = Pods('cycles', array('where' => 'diffusion=1 and archive=0 and diffusion_level != 40', 'limit' => "-1"));
//     while ($entites->fetch()) {
//         $modified = diffusion_one_entity($entites, 'cycles');
        
//         if ($modified) {
//             // parcourir l'ensemble des entités qui la compose pour faire le même traitement
//             // pour les évènements
//             $attached_events = Pods('cycles_events', array("where" => "id_cycle = ".$entites->field('id'), 'limit' => "-1"));
//             while ($attached_events->fetch()) {
//                 $attached_entity = Pods('events', array("where" => "id = ".$attached_events->field('id_event')));
//                 if ($attached_entity->fetch()) {
//                     diffusion_one_entity($attached_entity, 'events');
//                 }
//             }
//             // pour les promotions
//             $attached_promos = Pods('cycles_promos', array("where" => "id_cycle = ".$entites->field('id'), 'limit' => "-1"));
//             while ($attached_promos->fetch()) {
//                 $attached_entity = Pods('promotions', array("where" => "id = ".$attached_promos->field('id_promo')));
//                 if ($attached_entity->fetch()) {
//                     diffusion_one_entity($attached_entity, 'promotions');
//                 }
//             }
//             // pour les documents
//             $attached_docs = Pods('cycles_docs', array("where" => "id_cycle = ".$entites->field('id')));
//             while ($attached_docs->fetch()) {
//                 $attached_entity = Pods('documents', array("where" => "id = ".$attached_docs->field('id_doc')));
//                 if ($attached_entity->fetch()) {
//                     diffusion_one_entity($attached_entity, 'documents');
//                 }
//             }
//         }
//      }
// //     // ************************************************************************
    
// //     // ************************************************************************
// //     // parcourir tous les cycles et périodes pas mis en diffusion et non archivés et dont le niveau de diffusion n'est pas au maximum
// //     $entites = Pods('cycles', array('where' => 'diffusion=0 and archive=0 and diffusion_level != 40', 'limit' => "-1"));
// //     while ($entites->fetch()) {
// //         $modified = diffusion_one_entity($entites, 'cycles');
        
// //         if ($modified) {
// //             // parcourir l'ensemble des entités qui la compose pour faire le même traitement
// //             // pour les évènements
// //             $attached_events = Pods('cycles_events', array("where" => "id_cycle = ".$entites->field('id'), 'limit' => "-1"));
// //             while ($attached_events->fetch()) {
// //                 $attached_entity = Pods('events', array("where" => "id = ".$attached_events->field('id_event')));
// //                 if ($attached_entity->fetch()) {
// //                     diffusion_one_entity($attached_entity, 'events');
// //                 }
// //             }
// //             // pour les promotions
// //             $attached_promos = Pods('cycles_promos', array("where" => "id_cycle = ".$entites->field('id'), 'limit' => "-1"));
// //             while ($attached_promos->fetch()) {
// //                 $attached_entity = Pods('promotions', array("where" => "id = ".$attached_promos->field('id_promo')));
// //                 if ($attached_entity->fetch()) {
// //                     diffusion_one_entity($attached_entity, 'promotions');
// //                 }
// //             }
// //             // pour les documents
// //             $attached_docs = Pods('cycles_docs', array("where" => "id_cycle = ".$entites->field('id')));
// //             while ($attached_docs->fetch()) {
// //                 $attached_entity = Pods('documents', array("where" => "id = ".$attached_docs->field('id_doc')));
// //                 if ($attached_entity->fetch()) {
// //                     diffusion_one_entity($attached_entity, 'documents');
// //                 }
// //             }
// //         }
// //     }
//     // ************************************************************************
//     // fin de la tache de diffusion
//     sh_write_to_log("La tâche planifiée \"Agent de diffusion\" se termine", DIFFUSION_LOG_PATH);
// }

// /*
//  * Diffusion d'une seule entité
//  */
// function diffusion_one_entity($entite, $entite_type) {
//     // traitement préliminaire suivant le type d'entité
//     $id = $entite->field('id');
//     if ($entite_type == "cycles") {
//         $entite_designation = $entite->display('identifiant');
//     }
//     if ($entite_type == "documents") {
//         $entite_designation = $entite->display('reference_document');
//     }
//     if ($entite_type == "events") {
//         $entite_designation = $entite->display('designation');
//     }
//     if ($entite_type == "promotions") {
//         $entite_designation = $entite->display('reference_extranet');
//     }
    
//     $modified = false;
    
//     // date du jour
//     $current_date = date('Y/m/d');
//     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Date courante : ".$current_date, DIFFUSION_LOG_PATH);
//     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Lancement à chaud : ".$entite->display('lancement_chaud'), DIFFUSION_LOG_PATH);
//     if ($entite->display('lancement_chaud') == 'Yes') {
//         $date_lancement_chaud_dd = date_conversion($entite->display('date_lancement_chaud_dd'));
//         $date_lancement_chaud_dr = date_conversion($entite->display('date_lancement_chaud_dr'));
//         if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Date lancement à chaud DD : ".$date_lancement_chaud_dd." DR : ".$date_lancement_chaud_dr, DIFFUSION_LOG_PATH);
//         if ($entite->display('diffusion_level') <= 10) {
//             if ($entite_type == "cycles") {
//                 $modified = saveif_dates_ok($current_date, "date_lancement_chaud_dd", $entite, $entite_type, 30, "Lancement à chaud=Oui : diffusion_level=30 : ".$entite_designation);
//                 $modified = saveif_dates_ok($current_date, "date_lancement_chaud_dr", $entite, $entite_type, 40, "Lancement à chaud=Oui : diffusion_level=40 : ".$entite_designation);
//             } else {
//                 saveif_dates_ok($current_date, "date_lancement_chaud_dd", $entite, $entite_type, 30, "Lancement à chaud=Oui : diffusion_level=30 : ".$entite_designation);
//                 saveif_dates_ok($current_date, "date_lancement_chaud_dr", $entite, $entite_type, 40, "Lancement à chaud=Oui : diffusion_level=40 : ".$entite_designation);
//             }
//         }
//         if ($entite->display('diffusion_level') == 30) {
//             if ($entite_type == "cycles") {
//                 $modified = saveif_dates_ok($current_date, "date_lancement_chaud_dr", $entite, $entite_type, 40, "Lancement à chaud=Oui : diffusion_level=40 : ".$entite_designation);
//             } else {
//                 saveif_dates_ok($current_date, "date_lancement_chaud_dr", $entite, $entite_type, 40, "Lancement à chaud=Oui : diffusion_level=40 : ".$entite_designation);
//             }
//         }
//     } else {
//         if ($entite_type == "cycles") {
//             // les dates de lancement à chaud ne sont pas renseignées, on regarde les dates de diffusion DD et DR
//             $diffusion_dd = date_conversion($entite->display('diffusion_dd'));
//             $diffusion_dr = date_conversion($entite->display('diffusion_dr'));
//             if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Date diffusion DD : ".$diffusion_dd." DR : ".$diffusion_dr, DIFFUSION_LOG_PATH);
//             if ($entite->display('diffusion_level') <= 10) {
//                 $modified = saveif_dates_ok($current_date, "diffusion_dd", $entite, $entite_type, 30, "Lancement à chaud=Non : diffusion_dd=renseignée : diffusion_level=30 : ".$entite_designation);
//                 $modified = saveif_dates_ok($current_date, "diffusion_dr", $entite, $entite_type, 40, "Lancement à chaud=Non : diffusion_dr=renseignée : diffusion_level=40 : ".$entite_designation);
//             }
//             // la date de diffusion DR est renseignée
//             if ($entite->display('diffusion_level') == 30) {
//                 $modified = saveif_dates_ok($current_date, "diffusion_dr", $entite, $entite_type, 40, "Lancement à chaud=Non : diffusion_dr=renseignée : diffusion_level=40 : ".$entite_designation);
//             }
//         }
//         if ($entite_type == "events") {
//             sh_write_to_log("Events diffusion_level : ".$entite->display('diffusion_level'), DIFFUSION_LOG_PATH);
//             if ($entite->display('diffusion_level') <= 10) {
//                 saveif_dates_ok($current_date, "date_debut", $entite, $entite_type, 30, "Lancement à chaud=Non : date de début=renseignée : diffusion_level=30 : ".$entite_designation);
//                 saveif_dates_ok($current_date, "date_debut", $entite, $entite_type, 40, "Lancement à chaud=Non : date de début=renseignée : diffusion_level=40 : ".$entite_designation);
//             }
//             if ($entite->display('diffusion_level') == 30) {
//                 saveif_dates_ok($current_date, "date_debut", $entite, $entite_type, 40, "Lancement à chaud=Non : date de début=renseignée : diffusion_level=40 : ".$entite_designation);
//             }
//         }
//         if ($entite_type == "documents") {
//             $date_debut = date_conversion2(getDateDebut($entite->field('annee'), $entite->display('semaine_debut')));
//             if ($entite->display('diffusion_level') <= 10) {
//                 if ($current_date >= $date_debut) {
//                     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Lancement à chaud=Non : date de début=renseignée : diffusion_level=30 : ".$entite_designation, DIFFUSION_LOG_PATH);
//                     // passer le diffusion level à 30
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 30);
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 40);
//                 }
//             }
//             if ($entite->display('diffusion_level') == 30) {
//                 if ($current_date >= $date_debut) {
//                     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Lancement à chaud=Non : date de début=renseignée : diffusion_level=40 : ".$entite_designation, DIFFUSION_LOG_PATH);
//                     // passer le diffusion level à 40
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 40);
//                 }
//             }
//         }
//         if ($entite_type == "promotions") {
//             $date_debut = date_conversion2(getDateDebut($entite->field('annee'), $entite->display('semaine_debut')));
//             if ($entite->display('diffusion_level') <= 10) {
//                 if ($current_date >= $date_debut) {
//                     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Lancement à chaud=Non : date de début=renseignée : diffusion_level=30 : ".$entite_designation, DIFFUSION_LOG_PATH);
//                     // passer le diffusion level à 30
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 30);
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 40);
//                 }
//             }
//             if ($entite->display('diffusion_level') == 30) {
//                 if ($current_date >= $date_debut) {
//                     if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("Lancement à chaud=Non : date de début=renseignée : diffusion_level=40 : ".$entite_designation, DIFFUSION_LOG_PATH);
//                     // passer le diffusion level à 40
//                     set_entity_diffusion_level($entite->field('id'), $entite_type, 40);
//                 }
//             }
//         }
//     }
//     return $modified;
// }

// /*
//  * positionne le diffusion_level d'une entité
//  */
// function saveif_dates_ok($current_date, $date_field_name, $entity, $entity_type, $entity_level, $log_message) {
//     $modified = false;
//     $date_debut = date_conversion($entity->display($date_field_name));
//     if ($current_date >= $date_debut) {
//         if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log($log_message, DIFFUSION_LOG_PATH);
//         // passer le diffusion level à $entity_level
//         set_entity_diffusion_level($entity->field('id'), $entity_type, $entity_level);
//         $modified = true;
//     }
//     return $modified;
// }


// /*
//  * Positionne le diffusion level de l'entite
//  */
// function set_entity_diffusion_level($entite_id, $entite_type, $level) {
//     $e = Pods($entite_type);
//     $e->save("diffusion_level", $level, $entite_id);
//     if ($level == 30) $txt = "DD";
//     if ($level == 40) {
//         $txt = "DR";
//         // sortir l'entité de la liste de diffusion
//         $e->save("diffusion", "No", $entite_id);
//         if (DIFFUSION_VERBOSE_LOGS) sh_write_to_log("l'entité ".$entite_id." sort de la liste de diffusion", DIFFUSION_LOG_PATH);
//     }
//     sh_write_to_log("L'entité ".$entite_id." est maintenant visible par les ".$txt, DIFFUSION_LOG_PATH);
// }

// /*
//  * Conversion des date dans le format aaaa/mm/jj
//  */
// function date_conversion($d) {
//     return substr($d, 6, 4)."/".substr($d, 0, 2)."/".substr($d, 3, 2);
// }

// function date_conversion2($d) {
//     return substr($d, 6, 4)."/".substr($d, 3, 2)."/".substr($d, 0, 2);
// }

//sh_do_diffusion();

//add_action('sh_diffusion_agent', 'sh_do_diffusion');

?>