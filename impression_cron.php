<?php
ini_set("max_execution_time", 600); 
require( dirname(__FILE__) . '/wp-load.php' );
ini_set("memory_limit","256M");

define("IS_CONTEXT", true);

require_once get_template_directory().'/paconline/logs/log-helper.php';
require_once get_template_directory().'/paconline/include-functions.php';

/**
 * Fonction "Cron" qui génére les cycles/périodes 
 * et leurs entités attachées 'documents, évents et promotions)
 * en PDF
 */
function sh_automatic_impression() {
	$content = PHP_EOL . PHP_EOL."###########################################################################" . PHP_EOL . PHP_EOL;
	sh_write_to_log($content, $log_path_file);
	$log_path_file = get_template_directory().'/log/impression/system.log';
	$pdfs_to_join_to_mail = array();
	$content = "Impression automatique (par cron) commence";
	sh_write_to_log($content, $log_path_file);
	###recuperation de tous les cycles et périodes à imprimer en pdf###
	$pod_cycles = Pods('cycles', array('where' => 'a_imprimer=1'));
	if ($pod_cycles->total() > 0) {
		while ($pod_cycles->fetch()) {
			$content = "Impression du cycle (de la période): " . $pod_cycles->field('identifiant');
			sh_write_to_log($content, $log_path_file);
			$print_url = get_option("siteurl")."/detail-cycle-print/?cron=1&id_cycle=".$pod_cycles->field('id');
			$content = "Url associée: " . $print_url;
			sh_write_to_log($content, $log_path_file);
			$content_pdf = file_get_contents($print_url);
			$content_pdf = str_replace('€', '&euro;', $content_pdf);
			echo "$content_pdf<br/>";
			$doc = new DOMDocument();
			$doc->loadHTML(utf8_decode($content_pdf));
			$content_pdf = $doc->saveHTML();
			require_once get_template_directory() . '/paconline/html2pdf_v4.03/html2pdf.class.php';
			$pdfName = WP_CONTENT_DIR . "/uploads/pdfs/" . $pod_cycles->field('identifiant') . ".pdf";
			$pdfs_to_join_to_mail[] = $pdfName;
			$html2pdf=new HTML2PDF("P","A4","fr");
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content_pdf);
			$html2pdf->Output($pdfName, "F");
			reset_flag_impression($pod_cycles->field('id'));
			$content = "Flag d'impression est remis à 0";
			sh_write_to_log($content, $log_path_file);
		}
	} else {
		$content = 'Pas de cycles/périodes à imprimer en mode automatique (via cron)';
		sh_write_to_log($content, $log_path_file);
	}
	$content = "Fin de l'impression automatique";
	sh_write_to_log($content, $log_path_file);

	
	if (count($pdfs_to_join_to_mail) > 0) {
		$content = "Création du mail à envoyer à la cellule PAC, contenant les fichiers joints " . implode(', ', $pdfs_to_join_to_mail);
		sh_write_to_log($content, $log_path_file);
		$mail_model = Pods('mails', array('where' => 'agents=\'impression\''));
		if ($mail_model->total() > 0) {
			while ($mail_model->fetch()) {
				$mail_pac = new mailPac($mail_model->field('objet'), $mail_model->field('contenu'));
				$group = new Groups_Group(1);
				$group = $group->read_by_name('StanHome_CellulePAC');
				$group_id = $group->group_id;
				$mail_pac->addGroupToMailList($group_id);
				$mail_pac->sendMailToAddedGroups($log_path_file, $pdfs_to_join_to_mail);
				$content = "Fin Envoi d'email à la cellule PAC";
				sh_write_to_log($content, $log_path_file);
			}
		}
		
	}
	$content = PHP_EOL . PHP_EOL ."###########################################################################" . PHP_EOL . PHP_EOL;
	sh_write_to_log($content, $log_path_file);
}


sh_automatic_impression();

?>