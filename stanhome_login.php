<?php
require( dirname(__FILE__) . '/wp-load.php' );
require( dirname(__FILE__) . '/wp-includes/class-phpass.php' );


if (isset($mode)) {
	if ($mode == "verbose") {
		define("SSO_VERBOSE", true);
	} else {
		define("SSO_VERBOSE", false);
	}
} else {
	define("SSO_VERBOSE", false);
}

if (get_option('_extranet_url')) {
	define("EXTRANET_REDIRECT_URL", get_option('_extranet_url'));
} else {
	define("EXTRANET_REDIRECT_URL", "http://extranet.stanhome.fr");
	if (SSO_VERBOSE) {
		echo "L'url de redirection n'est pas enregistrée dans Paconline";
	} else {
		// l'url de l'extranet n'est pas configurée, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}

if (get_option('_secret_key')) {
	define("SECRET_KEY", get_option('_secret_key'));
} else {
	if (SSO_VERBOSE) {
		echo "La clé secrète n'est pas enregistrée dans Paconline";		
	} else {
		// la clé secrète n'est pas configurée, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}

if (!isset($_GET["username"])) {
	if (SSO_VERBOSE) {
		echo "Le paramètre 'username' n'est pas défini dans l'url";		
	} else {
		// le username n'existe pas dans l'url, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}
$username = $_GET["username"];

if (!isset($_GET["time"])) {
	if (SSO_VERBOSE) {
		echo "Le paramètre 'time' n'est pas défini dans l'url";		
	} else {
		// le parametre time n'existe pas dans l'url, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}
$time = $_GET["time"];

if (!isset($_GET["token"])) {
	if (SSO_VERBOSE) {
		echo "Le paramètre 'token' n'est pas défini dans l'url";		
	} else {
		// le token n'existe pas dans l'url, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}
$token = $_GET["token"];

$token_not_hashed = $username.$time.SECRET_KEY;
$token_hashed = hash("sha256", $token_not_hashed);
$current_time = time();

/*
 * Vérification que le timeout n'est pas dépassé
 */

if ($current_time > $time) {
	if (SSO_VERBOSE) {
		echo "Le timeout est dépassé. La date courante est supérieure au timeout.";		
	} else {
		// le timeout est dépassé, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}

/*
 * Vérification que le token fourni dans l'url est valide
 */

if ($token_hashed != $token) {
	if (SSO_VERBOSE) {
		echo "Le token de l'url n'est pas identique à celui calculé par ce script";		
	} else {
		// le token fourni n'est pas bon, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
}

/*
 * Recherche du user dans la base
 */
$user = get_user_by('login', $username);

if (!$user) {
	if (SSO_VERBOSE) {
		echo "L'utilisateur n'est pas trouvé dans la base de paconline";		
	} else {
		// le user n'est pas trouvé dans la base de Paconline, on redirige vers l'extranet
		wp_redirect(EXTRANET_REDIRECT_URL, 301);
	}
	exit;
} else {
		wp_set_current_user( $user->ID, $username );
		wp_set_auth_cookie( $user->ID);
		do_action('wp_login', $username);
		
		if (is_user_logged_in()) {
			header("Location:http://paconline.stanhome.fr");
			//wp_redirect("http://paconline.stanhome.fr");
		} else {
			// le user n'est pas trouvé dans la base de Paconline, on redirige vers l'extranet
			wp_redirect(EXTRANET_REDIRECT_URL, 301);
		}
}

?>